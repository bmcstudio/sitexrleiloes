<?
	include("config/connect.php");
	include("language/portugues.php");
	include("functions.php");
	include("session.php");

	$uid = $_SESSION["userid"];

	if($_POST["submit"]!="" && $_POST["mobileno"]!="")
	{
		$qr1 = "select * from registration r left join countries c on r.country=c.countryId where id='".$uid."'";
		$rs1 = mysql_query($qr1);
		$total1 = mysql_num_rows($rs1);
		$row1 = mysql_fetch_object($rs1);

		$qryupd = "update registration set mobile_no='".$_POST["mobileno"]."',full_mobileno='+".$row1->countrycode.$_POST["mobileno"]."' where id='".$uid."'";
		mysql_query($qryupd) or die(mysql_error());
		header("location: mydetails.html?msg=1");
	}

	$qr = "select * from registration r left join countries c on r.country=c.countryId where id='".$uid."'";
	$rs = mysql_query($qr);
	$total = mysql_num_rows($rs);
	$row = mysql_fetch_object($rs);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript" src="function.js"></script>
<script language="javascript">
	function Check()
	{
		if(document.f1.mobileno.value=="")
		{
			alert("<?=$lng_plsentermobileno;?>");
			document.f1.mobileno.focus();
			return false;
		}
	}
</script>
</head>


<body class="admin_page">
    <? include("header.php"); ?>
    <div id="main_div">
		<div id="middle_div">
            <div id="cleaner"></div>
			<? include("leftside.php"); ?>
			<div class="inner-container content-center">
				<div class="titlebar">
					<div class="leftbar"></div>
					<div class="middlebar"><div class="page_title_font"><?=$lng_myauctionsavenue;?> - <?=$lng_mydetails;?></div></div>
					<div class="rightbar"></div>
				</div>
				<div class="bodypart">	
				<form name="f1" method="post" action="" onsubmit="return Check();">
					<? if($_GET["msg"]==1){ ?>
						<div style="margin-left: 80px; margin-top: 20px; text-align: left;" class="greenfont"><b><?=$lng_mobileupdate;?></b></div>
					<? } ?>
					<div><h4 class="grayboldfont"><?=$lng_yourdata;?></h4></span>
					<table border="0" cellpadding="5" cellspacing="5" align="left">
						<tr>
							<td class="normal_text"><strong><?=$lng_customerid;?></strong> </td>
							<td class="normal_text"><?=$row->id;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_username;?> :</strong> </td>
							<td class="normal_text"><?=ucfirst($row->username);?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_gender;?> :</strong> </td>
							<td class="normal_text"><?=$row->sex;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_firstname;?> :</strong> </td>
							<td class="normal_text"><?=$row->firstname;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_lastname;?> :</strong> </td>
							<td class="normal_text"><?=$row->lastname;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_birthdate;?> :</strong> </td>
							<td class="normal_text"><?=$row->birth_date;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_emailaddress;?> :</strong> </td>
							<td class="normal_text"><?=$row->email;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_country;?> :</strong> </td>
							<td class="normal_text"><?=$row->printable_name;?></td>
						</tr>
						<tr>
							<td class="normal_text"><strong><?=$lng_detailmobileno;?></strong> </td>
							<td class="normal_text"><input type="text" name="mobileno" value="<?=$row->mobile_no;?>" class="textboxclas" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><input type="image" name="image" value="image" src="<?=$lng_imagepath;?>send.jpg" onmouseout="this.src='<?=$lng_imagepath;?>send.jpg'" onmouseover="this.src='<?=$lng_imagepath;?>send.jpg'" /></td>
							<input type="hidden" value="submit" name="submit" />
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" class="normal_text"><?=$lng_mydetailnote;?></td>
						</tr>
					</table>
				</div>
				<div style="height: 20px; clear:both">&nbsp;</div>
				</form>				
				</div>
			</div>
            <div id="cleaner"></div>
		</div>
    </div>
    <? include("footer.php"); ?>
</body>
</html>
