<?
	include("config/connect.php");
	include("language/portugues.php");
	include("session.php");
	include("functions.php");

	$uid = $_SESSION["userid"];

	if($_POST["submit"]!="") {
		$pass = $_POST["newpass"];
		$qryupd = "update registration set password='".sha1($pass)."' where id='$uid'";
		mysql_query($qryupd) or die(mysql_error());
		$msg=1;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript" src="function.js"></script>
<script language="javascript">
	function check()
	{
		if(document.newpassword.newpass.value=="")
		{
			alert("<?=$lng_plsenternewpass;?>");
			document.newpassword.newpass.focus();
			return false;
		}

		if(document.newpassword.newpass.value.length<6)
		{
			alert("<?=$lng_passtooshort;?>");
			document.newpassword.newpass.focus();
			document.newpassword.newpass.select();
			return false;
		}

		if(document.newpassword.cnfnewpass.value=="")
		{
			alert("<?=$lng_plsenterconfpass;?>");
			document.newpassword.cnfnewpass.focus();
			return false;
		}

		if(document.newpassword.newpass.value!=document.newpassword.cnfnewpass.value)
		{
			alert("<?=$lng_passmismatch;?>");
			document.newpassword.cnfnewpass.focus();
			document.newpassword.cnfnewpass.select();
			return false;
		}
	}
</script>
</head>
<body class="admin_page">
    <?
    	include("header.php");
    ?>
    <div id="main_div">
		<div id="middle_div">
            <div id="cleaner"></div>
			<? include("leftside.php"); ?>
			<div class="inner-container content-center">
				<div class="titlebar">
					<div class="leftbar"></div>
					<div class="middlebar"><div class="page_title_font"><?=$lng_myauctionsavenue;?> - <?=$lng_changepassword;?></div></div>
					<div class="rightbar"></div>
				</div>
				<div class="bodypart">
				<div>
					<?
						if($msg==1)
						{
					?>
						<div style="height: 30px; margin-left: 10px;" align="left" class="greenfont"><?=$lng_passwordchanged;?></div>
					<?
						}
					?>
					<h4 class="grayboldfont"><?=$lng_yourpassword;?></h4>
						<form name="newpassword" action="" method="post" onsubmit="return check();">
							<div style="margin-top: 10px;">
								<div class="normal_text" style="height: 25px;"><?=$lng_enternewpassword;?></div>
								<div style="height: 30px;"><input type="password" name="newpass" size="30" maxlength="50" class="textboxclas" /></div>
								<div class="normal_text" style="height: 25px;"><?=$lng_retypenewpassword;?></div>
								<div style="height: 30px;"><input type="password" name="cnfnewpass" size="30" maxlength="50" class="textboxclas" /></div>
								<p><input type="image" name="submitimage" src="<?=$lng_imagepath;?>send.jpg" value="Submitimage" onmouseover="this.src='<?=$lng_imagepath;?>send.jpg'" onmouseout="this.src='<?=$lng_imagepath;?>send.jpg'" /></p>
								<input type="hidden" value="submit" name="submit" />
							</div>
						</form>
				</div>
				</div>
			</div>
            <div id="cleaner"></div>
		</div>
    </div>
    <?
        include("footer.php");
    ?>
</body>
</html>
