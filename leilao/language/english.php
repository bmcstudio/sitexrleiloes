<?
// //Common variables used in website
	
// 	$lng_imagepath		= "images/";
// 	$lng_imageext		= "gif";
// 	$lng_characset		= "iso-8859-1";
// 	$lng_prefix			= "";

// 	$lng_tabhome 		= "Home";
// 	$lng_tabmyavenue 	= "My SnaBid";
// 	$lng_tabregister	= "Register";
// 	$lng_tabhelp		= "Help";
// 	$lng_tabbuybids		= "Buy Bids";
	
// 	$lng_category		= "Categories";
// 	$lng_allliveauc		= "All Live Auctions";
// 	$lng_futureaucs		= "Future Auctions";
// 	$lng_futureauc		= "Future Auction";
// 	$lng_endedauc		= "Ended Auctions";

// 	$lng_news			= "News";
// 	$lng_morenews		= "More News";

// 	$lng_startdate		= "Start Date";
// 	$lng_starttime		= "Start Time";
// 	$lng_enddate		= "End Date";
// 	$lng_winner			= "Winner";
// 	$lng_ended			= "Ended";
// 	$lng_username		= "Username";
// 	$lng_password		= "Password";
// 	$lng_paysafe		= "Pay safely on SnaBid";
// 	$lng_termscondi		= "Terms & Conditions";
// 	$lng_aboutus		= "About us";
// 	$lng_contact		= "Contact";
// 	$lng_privacy		= "Privacy Policy";
// 	$lng_jobs			= "Jobs";
// 	$lng_welcome		= "Welcome";
// 	$lng_bids			= "Bids";
// 	$lng_availablebids	= "Available Bids";
// 	$lng_price			= "Price";
// 	$lng_bidder			= "Bidder";
// 	$lng_auctionid		= "Auction ID";
// 	$lng_endprice		= "End Price";
// 	$lng_linkmore		= "more";
// 	$lng_itemvalue		= "Item value: ";
	
// 	//Left side of user account
	
// 	$lng_myauctionsavenue = "My SnaBid";
// 	$lng_accview		= "View";
// 	$lng_accmain		= "MAIN";//*****
// 	$lng_acchome		= "My SnaBid Home";
// 	$lng_accauctions	= "AUCTIONS";
// 	$lng_myauctions		= "My Auctions";
// 	$lng_mybidbuddy		= "My BidBuddy";
// 	$lng_watchauctions	= "Watched Auctions";
// 	$lng_wonauctions	= "Won Auctions";
// 	$lng_accaccount		= "ACCOUNT";
// 	$lng_accbidaccount	= "Bid Account";
// 	$lng_accvouchers	= "Vouchers";
// 	$lng_accreferral	= "Referral";
// 	$lng_accdetails		= "DETAILS";
// 	$lng_mydetails		= "My Details";
// 	$lng_changepassword	= "Change Password";
// 	$lng_closeaccount	= "Close Account";
// 	$lng_accnewsletter	= "Newsletter";
// 	$lng_accsmssetting	= "SMS Setting";
// 	$lng_myaddresses	= "My Addresses";

// 	$lng_image			= "Image";
// 	$lng_description	= "Description";
// 	$lng_pricebidder	= "Price/Bidder";
// 	$lng_countdown		= "Countdown";
// 	$lng_placebid		= "Place Bid";
	
// 	$lng_inclvatexcl	= "incl. VAT, excl. delivery";
// 	$lng_inclvatexcl2	= "incl. VAT.";
// 	$lng_deliverycharge = "Delivery Charge";
	
// 	$lng_nextpage		= "Next Page";
// 	$lng_previouspage	= "Previous Page";

// 	//end left side of user account
	

// //end common variables


// //Language variables for index page
// 	$lng_lastwinner		= "Last winner";
// 	$lng_finalprice		= "Final price";
// 	$lng_memlogin		= "Member Login";
// 	$lng_forgotpass		= "Forgot password?";
// 	$lng_signup			= "New user sign up";
// 	$lng_sendmessage1	= "Send the message BID ";
// 	$lng_sendmessage2	= " to ".$SMSsendnumber.". SMS price is 100 dinars + vat"; //*****
// 	$lng_aucavenueid	= "SnaBid ID";
// 	$lng_actualprice	= "Actual price";
// 	$lng_readmore		= "Read<br>More";
	
// //end index page variables

// //Language variables for login page
	
// 	$lng_login 			= "Login";
// 	$lng_bidnow 		= "Bid now - These auctions are about to end";
// 	$lng_aucavenuelogin = "SnaBid Login";
// 	$lng_newtoaucavenue = "New to SnaBid";
// 	$lng_registernow	= "Register Now!";
// 	$lng_exciteauctions	= "SnaBid - home of the most exciting auctions on the internet";
// 	$lng_newestproduct	= "The newest products and amazing prices";
// 	$lng_loginvoucher	= $Currency."20 voucher, redeemable against the first auction you win";
// 	$lng_freeregister	= "Free Registration";
// 	$lng_alreadyregister= "Have you already registered?";
// 	$lng_logindata		= "Please enter your login data here";
// 	$lng_forgotdata		= "<a href='forgotpassword.html' class='blue_link'>Forgot</a> username or password?";
// 	$lng_invaliddata	= "Invalid username and password.";
// 	$lng_accountsuspend = "Your account is suspended by SnaBid.com.";
// 	$lng_accountdelete	= "Your account is deleted by SnaBid.com.";
// 	$lng_enterdata		= "Please enter username and password.";
// 	$lng_enterpassword	= "Please enter password.";
	

// //end login page variable

// //Language variables for registration page
	
// 	$lng_frregistration = "Free Registration";
// 	$lng_vouchermessage = $Currency."20 voucher, redeemable against the first auction you win";
// 	$lng_amazingproducts= "The chance to win amazing products at amazing prices";
// 	$lng_registrationdata= "To register with SnaBid, please complete the following";
// 	$lng_personalinfo	= "Personal Information";
// 	$lng_firstname		= "First Name";
// 	$lng_lastname		= "Last Name";
// 	$lng_birthdate		= "Date Of Birth";
// 	$lng_gender			= "Gender";
// 	$lng_male			= "Male";
// 	$lng_female			= "Female";
// 	$lng_addressline1   = "Address Line 1";
// 	$lng_addressline2	= "Address Line 2";
// 	$lng_towncity		= "Town/City";
// 	$lng_country		= "Country";
// 	$lng_postcode		= "Postcode";
// 	$lng_phoneno		= "Phone No";
// 	$lng_logininfo		= "Login Information";
// 	$lng_characterlong	= "(Be at least be 6 characters long)";
// 	$lng_mobilenumber	= "Mobile No.";
// 	$lng_retypepassword = "Retype Password";
// 	$lng_passsecurity	= "Password security";
// 	$lng_emailaddress	= "Email Address";
// 	$lng_confirmemail	= "Confirm Email Address";
// 	$lng_picturecharct  = "Type the characters you see in the picture below.";
// 	$lng_casesensitive  = "Letters are not case-sensitive";
// 	$lng_acceptterms	= "I have read, understood and accepted the SnaBid's Terms & Conditions.";
// 	$lng_acceptprivacy	= "I have read, understood and accepted the SnaBid's Privacy Policy.";
// 	$lng_acceptnewsletter= "Yes, I want to receive the SnaBid's Newsletter.";
// 	$lng_emailexists	= "This Email Address Already Exists!!!";
// 	$lng_usernameexists	= "This Username Already Exists!!!";
// 	$lng_confirmregister= "Please confirm your registration!";
// 	$lng_sentemailto	= "We've sent an email to ";
// 	$lng_registernote	= "We've sent you a verification email containing your login details and a confirmation link. To confirm your registration with SnaBid, please click on the link in the email and activate your account. Please note that this link is only valid for 48 hours.";
// 	$lng_importantword	= "Important:";
// 	$lng_dontreceivenote= "If you didn't receive this email, try checking your spam filter. If that doesn't work and you still haven't received the email, or the stated address is incorrect, <a href='helpdesk/contact.html?a=add' class='blue_link'>click here</a>.";
// 	$lng_correctcode	= "Please Enter Correct Security Code!!!";
	
// //end registration page variable

// // Language variables for myaccount/myauctions
	
// 	$lng_aucbiddingon	= "Auctions you are bidding on";
// 	$lng_notbiddingany	= "You are not bidding on any auctions";
// 	$lng_choosebidpack	= "Please choose which BidPack you'd like to buy";
// 	$lng_currentselect	= "You have currently selected: ";

// // end myaccount page variagle

// //Language variables for mybidbutler page
	
// 	$lng_activebidbuddy	 = "My Active BidBuddy";
// 	$lng_noactivebidbuddy="No active BidBuddy to display";
// 	$lng_bidbutlerbid	 = "Placed Bids";
// 	$lng_deletebutlerbid = "Delete";
// 	$lng_butlerstartprice= "Start Price";
	
// //end variables for mybidbutler page

// //Language variables for watchedauctions page
	
// 	$lng_notwatchingany	= "No watched auctions to display";
// 	$lng_deleteselected	= "Delete selected auction from the list";
// 	$lng_deletewatchauc	= "Delete";

// //end variables for watchedauctions page

// //Language variables for wonauctions page
	
// 	$lng_winprice		= "Win price";
// 	$lng_acceptdenied	= "Accept/Denied";
// 	$lng_payment		= "Payment";
// 	$lng_wonaccepted	= "Accepted";
// 	$lng_wondenied		= "Denied";
// 	$lng_clickhere		= "Click Here";
// 	$lng_lastdateaccept	= "Last Date to Accept ";
// 	$lng_acceptperiodover="Auction accept period is over";
// 	$lng_paymentperiodover="Auction payment <br />period is over";
// 	$lng_nowonauctions	= "No won auctions to display.";
		
// //end variables for wonauctions page

// //Language variables for acceptordenied page
		
// 	$lng_wonauctionaccept	= "Accept Won Auction";
// 	$lng_pleaseselect		= "Please select";
	
// //end variables for acceptordenied page

// //Language variables for buybids page
	
// 	$lng_buybidpack		= "Buy a BidPack";
// 	$lng_youchoosen1	= "You have choosen";
// 	$lng_youchoosen2	= "After payment the bids will be booked to your account";
// 	$lng_paymentmethod	= "Please choose your payment method:";
// 	$lng_bidsfor		= "bids for";
// 	$lng_bidpackchar	= "Bidpack: ";
// 	$lng_bidreffbonus	= "Referral Bonus";

// //end variables for buybids page

// //Language variables for vouchers page
	
// 	$lng_voucherdate	= "Date";
// 	$lng_voucherlabel	= "Voucher label";
// 	$lng_voucheramount	= "Amount/Bids";
// 	$lng_vouchercombinable = "Combinable";
// 	$lng_voucherauction	= "Auction";
// 	$lng_voucherstatus	= "Status";
// 	$lng_vouchervalidto	= "Valid to";
// 	$lng_voucherused	= "Used";
// 	$lng_voucherexpired	= "Expired";
// 	$lng_voucherrunning	= "Running";
// 	$lng_novoucher		= "You don't have any vouchers yet";
// 	$lng_vouvalidfor	= " (valid for ";
// 	$lng_vouvaliddays	= " days)";
	

// //end variables for vouchers page

// //Language variables for affiliate page
	
// 	$lng_invitationsent	= "Invitation sent!";
// 	$lng_referralurl	= "Your Referral URL :";
// 	$lng_referralmessage = "Enter the email address of the person you wish to<br> invite ";
// 	$lng_separatecoma	= "(separeated by coma) :";


// //end variables for affiliate page

// //Language variables for smssetting page

// 	$lng_receivesms		= "Receive a SMS alert when new auctions start";
// 	$lng_yesreceivesms	= "Yes, alert me when new auctions start.";
// 	$lng_smsturnon		= "Your SMS alert has been turned on!";
// 	$lng_smsturnoff		= "Your SMS alert has been turned off!";
// 	$lng_plsentermobile	= "Please enter your mobile no!";

// //end variables for smssetting page

// //Language variables for mydetails page

// 	$lng_customerid		= "Customer ID :";
// 	$lng_detailmobileno	= "Mobile number :";
// 	$lng_mydetailnote	= "To avoid abuses we do not allow you to edit your details once registered. You can edit only your mobile number.";
// 	$lng_mobileupdate	= "Your mobile no updated successfuly!";
// 	$lng_yourdata		= "Your Data : ";

// //end variables for mydetails page

// //Language variables for changepassword page
	
// 	$lng_yourpassword	= "Change your password";
// 	$lng_enternewpassword = "Please enter your new password";
// 	$lng_retypenewpassword= "Please retype your new password";
// 	$lng_passwordchanged= "Your password has been changed!";

// //end variables for changepassword page

// //Language variables for closeaccount page
	
// 	$lng_closeavenueacc	= "Close your SnaBid account";
// 	$lng_closeimportantnote	= "Important: if you close your account, you won't be able to re-register with SnaBid using this email address for two months. Please also note that you will lose any bids you currently have on your account.";
// 	$lng_yescloseaccount	= "Yes, I want to close my SnaBid account";

// //end variables for closeaccount page

// //Language variables for newsletter page

// 	$lng_wanttoknow		= "Want us to let you know about new acutions and special offers?";
// 	$lng_newslettersub	= "Subscribe to newsletter :";
// 	$lng_youremailadd	= "Your Email :";
// 	$lng_dontwanttoknow	= "Don't want to hear about our great deals any more?";
// 	$lng_newsletterunsub= "Unsubscribe to newsletter :";
// 	$lng_thankyounewslet= "Thank you for subscribing to our newsletter.";
// 	$lng_funandgoodluck	= "Have fun and good luck!";
// 	$lng_youraucaveteam	= "Your SnaBid - Team";
// 	$lng_unsubscribenews= "Your have unsubscribed from our newsletter ...";
// 	$lng_plsentersubemail = "Please enter emailaddress to subscribe!";
// 	$lng_plsenterunsubemail = "Please enter emailaddress to Unsubscribe!";
// 	$lng_plscheckemailadd = "Please check your email address. It has been entered incorrectly.";

// //end variables for newsletter page

// //Language variables for help page

// 	$lng_helptopics		= "Help Topics";
// 	$lng_quicklinks		= "Quick Links";
// 	$lng_lostpassword	= "Lost password";
// 	$lng_subunsubnewslet= "Subscribe/Unsubscribe to newsletter";
// 	$lng_lostuserdata	= "Lost user data";
// 	$lng_changeaddress	= "Change address";
// 	$lng_confirmwon		= "Confirm a won auction";
// 	$lng_reportabuse	= "Report abuse";
// 	$lng_emailaucavenue	= "Email SnaBid";
// 	$lng_contactbyemail	= "Send us an email<br>through Customer Support Center:";
	

// //end variables for help page

// //Language variables for productdetails page

// 	$lng_smallauctions	= "Auctions";
// 	$lng_addtowatchlist	= "Add auction to Watchlist";
// 	$lng_addedtowatchlist= "Added to Watchlist";
// 	$lng_fixedpriceauc= "Fixed price ";
// 	$lng_fixedpriceauction="Fixed price auction";
// 	$lng_offauction		= "100% off";
// 	$lng_centauction	= "Cent Auction";
// 	$lng_nightauction	= "Night Auction";
// 	$lng_nailbiterauction="NailBiter Auction";
// 	$lng_clicktoenlarge	= "Click on an image to enlarge it";
// 	$lng_bidhistory		= "Bid History";
// 	$lng_mybids			= "My Bids";
// 	$lng_bidhistorybid	= "BID";
// 	$lng_bidhistorybidder= "BIDDER";
// 	$lng_bidhistorytype	= "TYPE";
// 	$lng_biddingsinglebid = "single bid";
// 	$lng_biddingsmsbid	= "SMS Bid";
// 	$lng_biddingbutlerbid="BidBuddy";
// 	$lng_biddingtime	= "BIDDING TIME";
// 	$lng_witheachbid	= "With each bid, the price will increase by ";
// 	$lng_endlatest1		= "This auction will end latest on ";
// 	$lng_endlatest2		= " at ";
// 	$lng_savings		= "Savings";
// 	$lng_worthupto		= "Worth up to:";
// 	$lng_placedbids		= "Place Bids";
// 	$lng_fixedprice		= "Fixed Price:";
// 	$lng_typicalworthup	= "The &quot;worth up to&quot; price reflects the manufacturer's recommended retail price or typical high street retail price.";
// 	$lng_auctionended1	= "This auction ended on ";
// 	$lng_auctionended2	=  " at ";
// 	$lng_congratulations= "Congratulations,";
// 	$lng_bookbidbuddy	= "Book BidBuddy";
// 	$lng_bidbuddyoverview="BidBuddy Overview";
// 	$lng_bidfrom		= "BID FROM";
// 	$lng_bidto			= "BID TO";
// 	$lng_butlerbids		= "BIDS";
// 	$lng_bidbuddyadded	= "BidBuddy Added";
// 	$lng_noactivebidbuddy="No Active BidBuddy";
// 	$lng_bidbuddy		= "BidBuddy";
// 	$lng_youcantplace	= "This auction is NailBiter Auction. You can't place BidBuddy";
// 	$lng_nowliveonauc	= "Now live on SnaBid";
// 	$lng_aboutaucave	= "About SnaBid";
// 	$lng_newtoaucave	= "New to SnaBid?";
// 	$lng_getfirstauction= "Register now and get 20 euro off your first auction! ";
// 	$lng_productdetails	= "Product Details";
// 	$lng_noliveauction	= "No live auctions to display";

// //end variables for productdetails page

// //Language variables for allliveauction/futureauctions/endedauctions page

// 	$lng_liveauctions	= "Live Auctions";
// 	$lng_allauctions	= "All Auctions";
// 	$lng_insteadof		= "(instead of ";
// 	$lng_noliveauctioncat="No live auctions to display for this category";
	
// 	$lng_nofutureauction= "No future auctions to display";
// 	$lng_nofutureauctioncat="No future auctions to display for this category";

// 	$lng_endtime		= "End Time";
// 	$lng_noendedauction	= "No ended auctions to display";
// 	$lng_noendedauctioncat="No ended auctions to display for this category";
// //end variables for allliveauction page

// //Language variables for aboutus page

// 	$lng_aboutaucave	= "About SnaBid";

// //end variables for allliveauction page

// //Language variables for allnews page
	
// 	$lng_aucavenews		= "SnaBid News";
// 	$lng_newsdate		= "News Date:";
// 	$lng_nonewstodisp	= "No news to display";
	
// //end variables for allnews page

// //Language variables for bidhistory page
	
// 	$lng_purchasebids	= "Purchased / Free Bids";
// 	$lng_bidsdate		= "Date";
// 	$lng_historybids	= "Placed Bids";
// 	$lng_biddingstart	= "Start";
// 	$lng_biddingstatus	= "Status";
// 	$lng_bidliveauction	= "Live Auction";
// 	$lng_bidfutureauction= "Future Auction";
// 	$lng_donthaveanybid	= "You do not have any bids in your account yet";
// 	$lng_biddingsmsbids	= "SMS Bids";
// 	$lng_backbooking	= "Back Booking";

// //end variables for bidhistory page


// //Language variables for buybidsunsuccess page

// 	$lng_buybidspayment	= "Bids Payment";
// 	$lng_sorrygoback	= "Sorry, Your Payment process was not succesfull<br /><a href='buybids.html' class='alink'>Click Here</a> to go back.";

// //end variables for buybidsunsuccess page

// //Language variables for emailconfirmsuccess page

// 	$lng_emailconfirmsucc= "Email Confirmation";
// 	$lng_regsuccess		= "Registration Successfull";
// 	$lng_regcongratulat	= "Congratulations!!";
// 	$lng_yourregsuccess	= "Your email verification is confirmed successfully, Please <a href='myaccount.html' class='alink'>Click here</a> to continue...";

// //end variables for emailconfirmsuccess page


// //Language variables for forgotpassword page

// 	$lng_titleforgot	= "Forgot password?";
// 	$lng_didnotfindemail= "Sorry, We didn't find your email address in our system!";
// 	$lng_accountverify	= "This account is not verified yet. Please verify first.";
// 	$lng_emailsentto	= "An email was sent to :";
// 	$lng_forgottendata	= "You have forgotten your login data?";
// 	$lng_noproblementer	= "No problem, just enter your email and we will send the information to your email account.";
// 	$lng_enteryouremail	= "Your Email : ";

// //end variables for forgotpassword

// //Language variables for jobs page
	
// 	$lng_aucavejobs	= "SnaBid Jobs";

// //end variables for forgotpassword

// //Language variables for paymentsuccess/paymentunsuccess page

// 	$lng_wonauctionpay	= "Won Auction Payment";
// 	$lng_paymentreceive	= "Won Auction Payment Received Succesfull<br/>AUCTION ID: ";
// 	$lng_paymentnotreceive="Sorry!<br />Some problem occured in payment process.";
// 	$lng_paymentunsuccess="Sorry, Your Payment process was not succesfull<br /><a href='wonauctions.html' class='alink'>Click Here</a> to go back.";

// //end variables for paymentsuccess


// //Language variables for privacy page
	
// 	$lng_aucaveprivacy	= "SnaBid Privacy Policy";

// //end variables for privacy

// //Language variables for smsbid static page
	
// 	$lng_smsbidaucave	= "SnaBid SMS Bid";

// //end variables for smsbid static page

// //Language variables for terms static page
	
// 	$lng_aucaveterms	= "SnaBid Terms &amp; Conditions";

// //end variables for terms static page

// //Language variables for thankyou page
	
// 	$lng_youpurchased1	= "Congratulations! You have purchased ";
// 	$lng_youpurchased2	= ". Your transaction has completed successfully.";
// 	$lng_youcanbid		= "You can Bid";
// 	$lng_thankyoumessage= "<a href='index.html' class='alink'>Click here</a> to find an incredible bargains.";
// 	$lng_aucaveteam		= "SnaBid Team.";

// //end variables for thankyou page

// //Language variables for addresses page
	
// 	$lng_addresses		= "Addresses";
// 	$lng_hereeditadd	= "Here you can add or edit your addresses: ";
// 	$lng_enternewadd	= "Enter new address";
// 	$lng_addresschanged	= "Address Changed successfully!";
// 	$lng_deliveryaddress= "Delivery Address";
// 	$lng_onlyname		= "Name :";
// 	$lng_mandatoryfield	= "Mandatory fields";
// 	$lng_billingaddress	= "Billing Address";
	
// //end variables for addresses page

// //Language variables for wonauctionaccept page
	
// 	$lng_wonauctionaccept="Please confirm won auction";
// 	$lng_alreadyacceptdenied="You have already accept/denied this auction!";
// 	$lng_onlyaccept		= "Accept";
// 	$lng_onlydenied		= "Denied";

// //end variables for wonauctionaccept page

// //Language variables for choosevoucher page

// 	$lng_aucavevouchers	= "SnaBid Vouchers";
// 	$lng_makepayment	= "Make Payment";
// 	$lng_plsselvoucher	= "Please select voucher";
// 	$lng_selectone		= "select one";
// 	$lng_dontwantvou	= "If you don't want to use your voucher, please tick the checkbox.";
// 	$lng_auctiondetails	= "Auction Details";
// 	$lng_shippingcharge	= "Shipping Charge";
// 	$lng_vouchermaximum	= "Voucher Amount(Maximum";
// 	$lng_totalpayment	= "Total Payment";
// 	$lng_vouchernote1	= "Note: ";
// 	$lng_vouchernote2	= "This is Free Bids Voucher. After payment free bids will be credited in your account.";
	
	
// //Language variables for choosevoucher page


// //Language variables for helpdesk support centre

// 	//language variables for contact page
	
// 		$lng_helponlyname	= "Name";
// 		$lng_aucavesupport	= "SnaBid Support Centre";
// 		$lng_submitticket	= "Submit a Ticket";
// 		$lng_viewticket		= "View Ticket";
// 		$lng_pleaseuseform	= "Please use the form below to submit a ticket. Required fields are marked with <font class='red-text-12-b'>*</font>";
// 		$lng_helpemail		= "E-mail";
// 		$lng_helpcategory	= "Category";
// 		$lng_helppriority	= "Priority";
// 		$lng_helpsubject	= "Subject";
// 		$lng_helpmessage	= "Message";
// 		$lng_helpattachment	= "Attachments";
// 		$lng_helpacceptedfiles= "Accepted Files";
// 		$lng_helpmaxfilesize= "Max. file size";
// 		$lng_helpenternumber="Please enter the number displayed above";
// 		$lng_helpsubmittingsure="Before submitting please make sure of the following";
// 		$lng_helpallnecessary	= "All necessary information has been filled out";
// 		$lng_helpallcorrect	= "All information is correct and error-free";
// 		$lng_helplow		= "Low";
// 		$lng_helpmedium		= "Medium";
// 		$lng_helphigh		= "High";

// 	//end variables for contact page
	
// 	//language variables for viewtickets page
		
// 		$lng_helpexcisting	= "View Existing Tickets";
// 		$lng_helptrackingid	= "Tracking ID";
// 		$lng_helpforgottid	= "Forgot tracking ID?";
// 		$lng_helpplsenteremail="Please enter your e-mail address and your ticket IDs will be sent to you";
		
// 	//end variables for ticket page

// 	//language variables for ticket page
	
// 		$lng_helpticketstatus="Ticket status";
// 		$lng_helpcreateon	= "Created on";
// 		$lng_helplastupdate	= "Last update";
// 		$lng_helplastreplier= "Last replier";
// 		$lng_helpreplies	= "Replies";
// 		$lng_helpprinterfriend="Printer friendly version";
// 		$lng_helpdate		= "Date";
// 		$lng_helpaddreply	= "Add reply";

// 	//end variables for viewtickets page

// 	//language variables for reply_ticket page
		
// 		$lng_helpticketreply	= "Ticket Reply";
// 		$lng_helpentermessage="Please enter your message";
// 		$lng_helpreplysubmit = "Your reply was submitted";
// 		$lng_helpyourreplysubmit="Your reply to this ticket has been successfully submitted!";
// 		$lng_helpnotrackingid	= "No tracking ID";
// 		$lng_helpstatusnotvalid	= "Status not valid";
// 		$lng_helpclosed			= "Closed";
// 		$lng_helpopened			= "Opened";
// 		$lng_helptrackingnotfound	= "Tracking ID not found";
// 		$lng_helponlyticket		= "Ticket";
// 		$lng_helpyourticketbeen	= "Your ticket has been";
// 		$lng_helpviewyourticket	= "View your ticket";
// 		$lng_helpforgotonlyid	= "Forgot Tracking ID";
// 		$lng_helpwaitingstaff	= "'Waiting reply from staff";
// 		$lng_helpwaitingcust	= "Waiting reply from customer";

// 	//end variables for viewtickets page
	
// 	//language variables for forgotID page
		
// 		$lng_helptrackingidsent	= "Tracking ID sent";
// 		$lng_helpemaildeatilsent= "An e-mail with details about your tickets has been sent                                   to your address";
// 		$lng_helpalsocheckemail	= "<b>Be sure to also check for the email inside your                                   SPAM/Junk mailbox!</b>";
// 		$lng_helperror			= "Error";
// 		$lng_helpnoemailfound	="No tickets with your e-mail address were found";
// 		$lng_helpgoback			= "Go Back";
		
		
		
// 	//end variables for forgotID page

// //end variables for  helpdesk support centre

// //For all pages JAVASCRIPT ALERTS variable
		
// 		//acceptordenied.php
// 		$lng_plsselaccordenied = "Please Select Accepted or Denied!";	
		
// 		//addresses.php

// 		$lng_plsentername 	    = "Please Enter Name!";
// 		$lng_plsenteradd 	    = "Please Enter Address!";
// 		$lng_plsentercity		= "Please Enter City!";
// 		$lng_plsenterpostcode	= "Please Enter Postcode!";
// 		$lng_plsenterphoneno	= "Please Enter Phonenumber!";

// 		//affiliate.php
		
// 		$lng_plsenteremailadd	= "Please Enter Email Address!";
		
// 		//buybids.php
		
// 		$lng_plschoosemethod	= "Please choose how you'd like to buy!";	
		
// 		//forgotpassword.php
		
// 		$lng_entervalidemail	= "Please enter valid email address!";

// 		//choosevoucher.php
		
// 		$lng_plschoosevoucher		= "Please selecte voucher!";	

// 		//editpassword.php
		
// 		$lng_plsenternewpass	= "Please Enter New Password!";
// 		$lng_passtooshort		= "Password is too short!";
// 		$lng_plsenterconfpass	= "Please Enter Confirm New Password!";
// 		$lng_passmismatch		= "Password Mismatch!";
	
// 		//watchauction.php
		
// 		$lng_areyousuretodel	= "Are you sure to delete this watchauction?";
	
// 			//unsubscribeuser.php
	
// 		$lng_plstickcheckbox	= "Please Tick the check box to confirm closure of your account";
		
// 		//registration.php

// 		$lng_regenterfirstname	= "Please Enter Firstname!";	
// 		$lng_regenterlastname	= "Please Enter Lastname!";
// 		$lng_regselbirthdate	= "Please Select Birth Date!";
// 		$lng_regenteraddress	= "Please Enter Address!";
// 		$lng_regentercity		= "Please Enter City!";
// 		$lng_regenterpostcode	= "Please Enter Postcode!";
// 		$lng_regenterphoneno	= "Please Enter Phonenumber!";
// 		$lng_regusernametooshort= "Username is too short!";
// 		$lng_regenterpassword	= "Please Enter Password!";
// 		$lng_regpasstooshort	= "Password is too short!";
// 		$lng_regconfpassword	= "Please Enter Confirm Password!";
// 		$lng_regpassmismatch	= "Password Mismatch!";
// 		$lng_regenteremailadd	= "Please Enter Email Address!";
// 		$lng_regenterconfemail	= "Please Enter Confirm Email Address!";
// 		$lng_regemailmismatch	= "Email Mismatch!";
// 		$lng_regreadterms		= "Please accept SnaBid Terms & Conditions!";
// 		$lng_regreadprivacy		= "Please accept SnaBid Privacy Policy!";
// 		$lng_regenterseccode	= "Please Enter Security Code!";
		
	
// 		//productdetails.php
		
// 		$lng_plsbidstartprice	= "Please enter BidBuddy start price!";
// 		$lng_plsbidendprice		= "Please enter BidBuddy end price!";
// 		$lng_plsenterbid		= "Please enter BidBuddy bids!";
// 		$lng_plsmorethanone		= "You palce BidBuddy for more than one bid!";
// 		$lng_bidstartnotgreater	= "BidBuddy start price not greater than end price!";
// 		$lng_bidendpricemust	= "BidBuddy end price is must greater than start price!";
// 		$lng_bidfromvalueismust	= "BID FROM Value needs to be greater than the Current Auction Price!";
// 		$lng_youbidisrunning	= "Your BidBuddy is running you can't delete it!";
		
// 		//mydetails.php
		
// 		$lng_plsentermobileno	= "Please Enter mobile number!";
		
// 		//commonused
		
// 		$lng_pleaserechargebid	= "Please recharge your bid account!";		
		
// // End all pages javascript alerts

// // For All pages EMAIL variables start
	
// 	//wonauction information email
		
// 		$lng_mailwonaucinfo		= "Won Auction Information";
// 		$lng_mailhi				= "Hi ";
// 		$lng_mailcongratulation	= "Congratulations!";
// 		$lng_mailyouhavewon		= "You have won the following item at SnaBid.com.";
// 		$lng_mailauctionid		= "Auction ID";
// 		$lng_mailname			= "Name";
// 		$lng_mailprice			= "Price";
// 		$lng_mailclosingdate	= "Closing date";
// 		$lng_mailclosingnote	= "Won auctions must be accepted/denied within 7 days from the closing date";
// 		$lng_mailaccdenclick1	= "To accept/deny your won auction click here ";
// 		$lng_mailaccdenclick2	= "click here";
// 		$lng_mailsubjectauctionwon	= "Auction Closed - Item Won!";
	
// 	//end

// 	//affiliate email
	
// 		$lng_mailheythere		= "Hi there,";
// 		$lng_mailyoureceived1	= "You have received this email because ";
// 		$lng_mailyoureceived2	= " has invited you to join ";
// 		$lng_mailthesiteconst	= "The site consists of a comprehensive online bidding (including many exclusive products) where users (such as yourself!) bid online.";
// 		$lng_mailclicktoregister= "Click on the given below link and register with SnaBid";
// 		$lng_mailsubjectaffiliate = "Invitation for Register with SnaBid.com";
		
// 	// This is emailcontent of affiliate.php start
		
// 		$lng_emailcontent_affiliate='';
// 		$lng_emailcontent_affiliate.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>".$lng_mailheythere."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'></p>"."<br>".	
		
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailyoureceived1."%1\$s".$lng_mailyoureceived2." @.<a href='".$SITE_URL."registration.php?ref=%2\$s'>".$SITE_URL."registration.php?ref=%2\$s</a></td>
// 		</tr>";

// 		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailthesiteconst."</td>
// 		</tr>";

// 		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailclicktoregister."</td>
// 		</tr>";

// 		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>URL: <a href='".$SITE_URL."registration.php?ref=%2\$s'>".$SITE_URL."registration.php?ref=%2\$s</a></td></tr>
// 		</table>";
		
// 	// This is emailcontent of affiliate.php end 
	
		
// 	//end
	
// 	//registration confirmation/forgotpassword  email
		
// 		$lng_mailaccountinfo	= "Account Information";
// 		$lng_mailreghi			= "Hi ";
// 		$lng_mailreglogininfo	= "Here is the login information that you requested";
// 		$lng_mailaucaveusername	= "SnaBid username";
// 		$lng_mailaucaveuserid	= "SnaBid user ID";
// 		$lng_mailtoresetyourpass1= "To reset your password ".
// 		$lng_mailtoenableyouracc= "To enable your account ";
// 		$lng_mailtoresetyourpass2= "click here";
// 		$lng_mailhappywinning	= "Happy winning at SnaBid.com!";
// 		$lng_mailsubjectaccinfo	= "Account Information @ SnaBid.com";
	
// 	//end
	
// 	// This is emailcontent for forgotpassword.php start
		
// 		$lng_emailcontent_forgotpassword='';
// 		$lng_emailcontent_forgotpassword.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailaccountinfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailreghi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailreglogininfo.":</td>
// 		</tr>";
		
// 		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaucaveusername.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaucaveuserid.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailtoresetyourpass1.": <a href='".$SITE_URL."password.php?ud=%4\$s&pd=%5\$s&key=%6\$s'>".$lng_mailtoresetyourpass2."</a></td>
// 		</tr>";

// 		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td><br>".$lng_mailhappywinning."<br></td>
// 		</tr>
// 		</table>";
		
// 	// This is emailcontent for forgotpassword.php end
	
// 	// This is emailcontent for registration.php start
		
// 		$lng_emailcontent_registraion='';
// 		$lng_emailcontent_registraion.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailaccountinfo."</p>"."<br>".	
	
// 	"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailreghi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailreglogininfo.":</td>
// 		</tr>";
		
// 		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaucaveusername.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaucaveuserid.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailtoenableyouracc." <a href='".$SITE_URL."password.html?auc_key=%4\$s'>".$lng_mailtoresetyourpass2."</a></td>
// 		</tr>";

// 		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailhappywinning."</td>
// 		</tr>
// 		</table>";
		
// 	// This is emailcontent for registration.php end
	
// 	//acceptordenied wonauction information email
		
// 		$lng_mailauctioninfo	= "Won Auction Information";
// 		$lng_mailaccepthi		= "Hi ";
// 		$lng_mailwonandaccept	= "You have won and accepted the following item.";
// 		$lng_mailwonanddenied	= "You have won and denied the following item.";
// 		$lng_mailwonacceptid	= "Auction ID";
// 		$lng_mailwonname		= "Name";
// 		$lng_mailwonprice		= "Price";
// 		$lng_mailaccedenieddate	= "Acceptance/Denial date";
// 		$lng_mailauctionpaid	= "The auction price must be paid within 7 days from the acceptance date.";
// 		$lng_mailtomakepay1		= "To make the payment ";
// 		$lng_mailtomakepay2		= "click here";
// 		$lng_mailsubjectacceptordenied = "Auction Accept/Denied - Item Won!";
	
// 	//This email content for acceptordenied.php's Accept start
		
// 		$lng_emailcontent_acceordeaccept='';
		
// 		$lng_emailcontent_acceordeaccept.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccepthi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>";
// 		$lng_emailcontent_acceordeaccept .= $lng_mailwonandaccept;
// 		"</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonacceptid.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonname.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonprice.": %4\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccedenieddate.": %5\$s</td>
// 		</tr>";
// 		"</table>";
		
// 	//This email content for acceptordenied.php's Accept end
	
// 	//This email content for acceptordenied.php's Denied start
		
// 		$lng_emailcontent_acceordedenied='';
		
// 		$lng_emailcontent_acceordedenied.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccepthi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>";
// 		$lng_emailcontent_acceordedenied .= $lng_mailwonanddenied;
// 		"</td>
// 		</tr>";

// 		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonacceptid.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonname.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonprice.": %4\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccedenieddate.": %5\$s</td>
// 		</tr>";
// 		"</table>";
		
// 	//This email content for acceptordenied.php's Denied end
	
// 	//This email content for acceptordenied.php's Accept and mailflag = 0 start
		
// 		$lng_emailcontent_acceordeflag='';
		
// 		$lng_emailcontent_acceordeflag.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccepthi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>";
// 		$lng_emailcontent_acceordeflag .= $lng_mailwonandaccept;
// 		"</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonacceptid.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonname.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonprice.": %4\$s</td>
// 		</tr>";

// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccedenieddate.": %5\$s</td>
// 		</tr>";
// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailauctionpaid."</td>
// 		</tr>";
		
// 		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailtomakepay1."<a href='".$SITE_URL."login.php?wid=%6\$s'>".$lng_mailtomakepay2."</a></td>
// 		</tr>";
// 		"</table>";
		
// 	//This email content for acceptordenied.php's Accept and mailflag = 0 end	
	
// 	//This email content for wonauctionaccept.php's Accept start
		
// 		$lng_emailcontent_wonaccept='';
		
// 		$lng_emailcontent_wonaccept.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td> ".$lng_mailaccepthi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>";
// 		$lng_emailcontent_wonaccept .= $lng_mailwonandaccept;
// 		"</td>
// 		</tr>";

// 		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonacceptid.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonname.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonprice.": %4\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccedenieddate.": %5\$s</td>
// 		</tr>
// 		</table>";
					
// 	//This email content for wonauctionaccept.php's Accept end
	
// 	//This email content for wonauctionaccept.php's Denied start
		
// 		$lng_emailcontent_wondenied='';
		
// 		$lng_emailcontent_wondenied.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td> ".$lng_mailaccepthi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>";
// 		$lng_emailcontent_wondenied .= $lng_mailwonanddenied;
// 		"</td>
// 		</tr>";

// 		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonacceptid.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonname.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonprice.": %4\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccedenieddate.": %5\$s</td>
// 		</tr>
// 		</table>";
					
// 	//This email content for wonauctionaccept.php's Denied end
	
// 	//This email content for wonauctionaccept.php's Accept and mailflag = 0 start
		
// 		$lng_emailcontent_wonacceptflag='';
		
// 		$lng_emailcontent_wonacceptflag.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
// 		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td> ".$lng_mailaccepthi."%1\$s, </td>
// 		</tr>";

// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>";
// 		$lng_emailcontent_wonacceptflag .= $lng_mailwonandaccept;
// 		"</td>
// 		</tr>";

// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonacceptid.": %2\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonname.": %3\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwonprice.": %4\$s</td>
// 		</tr>";

// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailaccedenieddate.": %5\$s</td>
// 		</tr>";
// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailauctionpaid."</td>
// 		</tr>";
		
// 		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailtomakepay1."<a href='".$SITE_URL."login.php?wid=%5\$s'>".$lng_mailtomakepay2."</a></td>
// 		</tr>
// 		</table>";
			
		
// 	//This email content for wonauctionaccept.php's Accept and mailflag = 0 end
		
// 	//end

// 	//subscribe for newsletter email
		
// 		$lng_mailnewsletterhi	= "Hi,";
// 		$lng_mailthanksinterest	= "Thanks for your interest in our newsletter.";
// 		$lng_mailyouarenowsub	= "You are now successfully subscribe for a SnaBid newsletter.";
// 		$lng_mailwhydonttake1	= "Why don't you take a look ";
// 		$lng_mailwhydonttake2	= " to see what incredible bargains we have on SnaBid.";
// 		$lng_mailhavefun		= "Have fun and good luck!";
// 		$lng_mailformhere		= "From everyone at SnaBid";
// 		$lng_mailnewslettersubject = "Thanks for subscribing to the SnaBid newsletter";
		
// 	//This is emailcontent for newsletter.php start
		
// 		$lng_emailcontent_newsletter ='';
// 		$lng_emailcontent_newsletter.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'></p>"."<br>".	
	
// 	"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

// 		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailnewsletterhi."</td>
// 		</tr>";

// 		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailthanksinterest."</td>
// 		</tr>";
		
// 		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailyouarenowsub."</td>
// 		</tr>";

// 		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailwhydonttake1."<a href='".$SITE_URL."index.php'>".$SITE_URL."index.php</a>".$lng_mailwhydonttake2."</td>
// 		</tr>";

// 		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailhavefun."</td>
// 		</tr>";

// 		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
// 		<td>".$lng_mailformhere."</td>
// 		</tr>
// 		</table>";
		
// 	//This is emailcontent for newsletter.php end
// 	//end
	
// // end email variables

// //ERROR Messages for smsbidplace page
	
// 	$lng_wrongparameter	= "ERROR: Wrong parameter passed.";
// 	$lng_userdoesnotex	= "ERROR : User does not exists";
// 	$lng_auctionnotrunn	= "ERROR : This auction is not running auction";
// 	$lng_auctioncurrpause="ERROR : This auction is currently pause";
// 	$lng_aucdoesnotex	= "ERROR : This auction does not exists";
// 	$lng_okmessage1		= "OK : Your Bid Placed Successfully. Price after bid placed is ";
// 	$lng_okmessage2		= " EUR and Due time is ";
// 	$lng_invalidmessage	= "ERROR : Invalid message data";

// //smsbulknotify page variables
	
// 	$lng_bulkmessage1	= "Auction ";
// 	$lng_bulkmessage2	= " is just started";

// //end smsbulknotify page variables

// //end messages

// // New variables define for SnaBid start
	
// 	$lng_openauction = "Open Auction";
// 	$lng_moreliveauction = "More Live Auction";
// 	$lng_howitwork   = "How it works?";
// 	$lng_regforfree	 = "<a href='registration.html' class='register'>Register for Free</a>&nbsp;or&nbsp;<a href='login.html' class='register'>Sign in</a>";
// 	$lng_regforfree2 = "<a href='../registration.html' class='register'>Register for Free</a>&nbsp;or&nbsp;<a href='../login.html' class='register'>Sign in</a>";
// 	$lng_logout = "Logout";
// 	$lng_paymethod = "Money bookers";

// // New variables define for SnaBid end

// 	$lng_pause		= 	"Pause";
// 	$lng_nobidspalced = "No Bids Placed";
	// $lng_subtotal   = "Subtotal";
?>