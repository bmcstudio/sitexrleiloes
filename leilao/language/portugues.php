<?
//Common variables used in website

	$lng_imagepath		= "images/";
	$lng_banenrpath		= "banners/";
	$lng_imageext		= "gif";
	// $lng_characset		= "iso-8859-1";
	$lng_characset		= "utf-8";
	$lng_prefix			= "";

	$lng_tabhome 		= "Home";
	$lng_tabmyavenue 	= "Minha Conta";
	$lng_tabregister	= "Registre-se";
	$lng_tabhelp		= "Ajuda";
	$lng_tabbuybids		= "Compre Bids";

	$lng_category		= "Categorias";
	$lng_allliveauc		= "Todos Leilões Ativos";
	$lng_futureaucs		= "Próximos Leilões";
	$lng_futureauc		= "Próximo Leilão";
	$lng_endedauc		= "Leilões Finalizados";

	$lng_news			= "Novidades";
	$lng_morenews		= "Mais Novidades";

	$lng_startdate		= "Data de Início";
	$lng_starttime		= "Hora de Início";
	$lng_enddate		= "Data Final";
	$lng_winner			= "Ganhador";
	$lng_ended			= "Finalizado";
	$lng_username		= "Nome de Usuário";
	$lng_password		= "Senha";
	$lng_paysafe		= "Pague com Seguraça no XRLeilões";
	$lng_termscondi		= "Termos de Uso";
	$lng_promocoes		= "Promções";
	$lng_depoimentos	= "Depoimentos";
	$lng_aboutus		= "Sobre nós";
	$lng_services		= "Serviços";
	$lng_contact		= "Fale conosco";
	$lng_privacy		= "Política de Privacidade";
	$lng_jobs			= "Trabalhe Conosco";
	$lng_welcome		= "Bem-Vindo";
	$lng_bids			= "Bids";
	$lng_availablebids	= "Bids Disponíveis";
	$lng_price			= "Preço";
	$lng_bidder			= "Usuário";
	$lng_auctionid		= "ID do Leilão";
	$lng_endprice		= "Preço Final";
	$lng_linkmore		= "mais";
	$lng_itemvalue		= "Valor do Item: ";

	//Left side of user account

	$lng_myauctionsavenue = "Meu Leilão";
	$lng_accview		= "Ver";
	$lng_accmain		= "PRINCIPAL";//*****
	$lng_acchome		= "Home";
	$lng_accauctions	= "LEILÕES";
	$lng_myauctions		= "Meus Leilões";
	$lng_mybidbuddy		= "Meu Auto Lance";
	$lng_watchauctions	= "Lista de Preferidos";
	$lng_wonauctions	= "Leilões Ganhos";
	$lng_accaccount		= "CONTA";
	$lng_accbidaccount	= "Carteira de Bid";
	$lng_accvouchers	= "Cupons";
	$lng_accreferral	= "Indique Amigos";
	$lng_accdetails		= "DETALHES";
	$lng_mydetails		= "Meus Detalhes";
	$lng_changepassword	= "Alterar Senha";
	$lng_closeaccount	= "Finalizar Conta";
	$lng_accnewsletter	= "Newsletter";
	$lng_accsmssetting	= "Configurações SMS";
	$lng_myaddresses	= "Meu Endereço";

	$lng_image			= "Imagem";
	$lng_description	= "Descrição";
	$lng_pricebidder	= "Preço por Bid";
	$lng_countdown		= "Cronometro";
	$lng_placebid		= "Bid Local";

	$lng_inclvatexcl	= "Taxa de Entrega não inclusa";
	$lng_inclvatexcl2	= "Mais Imposto.";
	$lng_deliverycharge = "Taxa de Entrega";

	$lng_nextpage		= "Próxima Página";
	$lng_previouspage	= "Página Anterior";

	//end left side of user account


//end common variables


//Language variables for index page
	$lng_lastwinner		= "Último Ganhador";
	$lng_finalprice		= "Preço Final";
	$lng_memlogin		= "Member Login";
	$lng_forgotpass		= "Perdeu a Senha?";
	$lng_signup			= "Novo Usuário, Cadastre-se";
	$lng_sendmessage1	= "Enviar a Mensagem BID ";
	$lng_sendmessage2	= " para ".$SMSsendnumber.". SMS o preço é 100 dinars + imposto"; //*****
	$lng_aucavenueid	= "ID Leilão";
	$lng_actualprice	= "Preço Atual";
	$lng_readmore		= "Leia<br>Mais";

//end index page variables

//Language variables for login page

	$lng_login 			= "Login";
	$lng_bidnow 		= "Participe JÁ - Estes leilões estão prestes a terminar";
	$lng_aucavenuelogin = "Login Leião";
	$lng_newtoaucavenue = "Novo no Leilão";
	$lng_registernow	= "Registre Já!";
	$lng_exciteauctions	= "XRLeilões.com.br - O seu portal de negócios online!";
	$lng_newestproduct	= "Produtos novos e preços Surpreendentes";
	$lng_loginvoucher	= " cupom válido para o primeiro leilão que você ganhar";
	$lng_freeregister	= "Cadastro Grátis";
	$lng_alreadyregister= "Já é registrado?";
	$lng_logindata		= "Por favor, digite seus dados de login aqui";
	$lng_forgotdata		= "<a href='forgotpassword.html' class='blue_link'>Perdeu</a> usuario ou senha?";
	$lng_invaliddata	= "Nome de Usuário ou Senha Inválidos.";
	$lng_accountsuspend = "Sua conta está suspensa no Leilão.";
	$lng_accountdelete	= "Sua conta foi deletada do Leilão.";
	$lng_enterdata		= "Por Favor, informe seu nome de usuario e senha";
	$lng_enterpassword	= "Por Favor, informe sua senha.";


//end login page variable

//Language variables for registration page

	$lng_frregistration = "Cadastro Grátis";
	$lng_vouchermessage = " cupom válido para o primeiro leilão que você ganhar";
	$lng_vouchermessage2 = " Bids grátis por se cadastrar";
	$lng_amazingproducts= "A chance de ganhar incróveis produtos a preços surpreendentes";
	$lng_registrationdata= "Para se cadastrar no XRLeilões, por favor preencha o seguinte";
	$lng_personalinfo	= "Informações Pessoais";
	$lng_firstname		= "Primeiro Nome";
	$lng_lastname		= "Último Nome";
	$lng_birthdate		= "Data de Nascimento";
	$lng_gender			= "Sexo";
	$lng_male			= "Masculino";
	$lng_female			= "Feminino";
	$lng_addressline1   = "Endereço";
	$lng_addressline2	= "Complemento";
	$lng_towncity		= "Cidade-Estado";
	$lng_country		= "País";
	$lng_cpf			= "CPF";
	$lng_rg				= "RG";
	$lng_postcode		= "CEP";
	$lng_phoneno		= "Telefone";
	$lng_logininfo		= "Informações de Login";
	$lng_characterlong	= "(no mínimo 6 caracteres)";
	$lng_cpflong		= "(informe somente números)";
	$lng_mobilenumber	= "Celular";
	$lng_retypepassword = "Confirme sua Senha";
	$lng_passsecurity	= "Senha";
	$lng_emailaddress	= "Email";
	$lng_confirmemail	= "Confirme seu endereço de email";
	$lng_picturecharct  = "Digite os caracteres que aparecem na figura abaixo.";
	$lng_casesensitive  = "Indiferente a letras maiúsculas e minúsculas";
	$lng_acceptterms	= "Eu li, entendi e aceito os <a href='http://www.xrleiloes.com.br/leilao/terms_and_conditions.html' target='_blank'>termos, condições e regulamentos</a> de serviço do XRLeilões.";
	$lng_acceptprivacy	= "Eu li, entendi e aceito a <a href='http://www.xrleiloes.com.br/leilao/privacy_policy.html' target='_blank'>política de privacidade</a> do XRLeilões.";
	$lng_acceptnewsletter= "Sim, eu gostaria de receber a Newsletter .";
	$lng_emailexists	= "Este endereço de email já existe!!!";
	$lng_usernameexists	= "Este nome de usuário já existe!!!";
	$lng_cpfexists		= "Este CPF já está cadastrado, informe outro!!!";
	$lng_confirmregister= "Obrigado por escolher a XRLeilões!";
	$lng_sentemailto	= "Nós enviamos um email para ";
	$lng_registernote	= "Enviamos suas informaçoes para o Moderador do site. Seus dados serão avaliados e dentro de alguns dias, você receberá um e-mail com a confimação do seu cadastro.";
	$lng_importantword	= "Importante:";
	$lng_dontreceivenote= "Se você não receber este e-mail, entre em contato através do email <strong>alexandre@xrleiloes.com.br</strong>.";
	$lng_correctcode	= "Por favor Entre Código de Segurança Correto!!!";

//end registration page variable

// Language variables for myaccount/myauctions

	$lng_aucbiddingon	= "Leilões em Andamento";
	$lng_notbiddingany	= "Voce não deu lance em nenhum leilão";
	$lng_choosebidpack	= "Por favor, escolha BidPack que você gostaria de comprar";
	$lng_currentselect	= "Você Selecionou: ";

// end myaccount page variagle

//Language variables for mybidbutler page

	$lng_activebidbuddy	 = "Meus Auto Lances Ativos";
	$lng_noactivebidbuddy="Nenhum Auto Lances ativo para mostrar";
	$lng_bidbutlerbid	 = "Bids Feitos";
	$lng_deletebutlerbid = "Deletar";
	$lng_butlerstartprice= "Preço Inicial";

//end variables for mybidbutler page

//Language variables for watchedauctions page

	$lng_notwatchingany	= "Nenhum leilão assistido para mostrar";
	$lng_deleteselected	= "Excluir leilões selecionados";
	$lng_deletewatchauc	= "Deletar";

//end variables for watchedauctions page

//Language variables for wonauctions page

	$lng_winprice		= "Preço Ganho";
	$lng_acceptdenied	= "Aceitar/Negar";
	$lng_payment		= "Pagamento";
	$lng_wonaccepted	= "Aceito";
	$lng_wondenied		= "Negado";
	$lng_clickhere		= "Clique Aqui";
	$lng_lastdateaccept	= "Última Data para Aceitar ";
	$lng_acceptperiodover="Períodos de Aceitação do Leilão Acabou";
	$lng_paymentperiodover="Leilões Pagos <br />periodo finalizado";
	$lng_nowonauctions	= "Nenhum Leilão ganho para mostrar";

//end variables for wonauctions page

//Language variables for acceptordenied page

	$lng_wonauctionaccept	= "Leilões Ganhos Aceitos";
	$lng_pleaseselect		= "Por Favor, selecione";

//end variables for acceptordenied page

//Language variables for buybids page

	$lng_buybidpack		= "Compar um BidPack";
	$lng_youchoosen1	= "Você Selcionou";
	$lng_youchoosen2	= "Após confirmar o pagamento os Bids estarão disponíveis em até 2 dias úteis";
	$lng_paymentmethod	= "Por Favor selecione uma forma de pagamento:";
	$lng_bidsfor		= "bids por";
	$lng_bidpackchar	= "Bidpack: ";
	$lng_bidreffbonus	= "Remessa de Bonus";

//end variables for buybids page

//Language variables for vouchers page

	$lng_voucherdate	= "Data";
	$lng_voucherlabel	= "Voucher";
	$lng_voucheramount	= "Preço/Bids";
	$lng_vouchercombinable = "Combináveis";
	$lng_voucherauction	= "Leilões";
	$lng_voucherstatus	= "Status";
	$lng_vouchervalidto	= "Válido para";
	$lng_voucherused	= "Usado";
	$lng_voucherexpired	= "Expirado";
	$lng_voucherrunning	= "Ativo";
	$lng_novoucher		= "Você não tem nenhum Voucher AINDA";
	$lng_vouvalidfor	= " (valido por ";
	$lng_vouvaliddays	= " dias)";


//end variables for vouchers page

//Language variables for affiliate page

	$lng_invitationsent	= "Convite Enviado!";
	$lng_referralurl	= "Sua referencia URL :";
	$lng_referralmessage = "Digite o endereço de e-mail da pessoa que você deseja<br> enviar ";
	$lng_separatecoma	= "(separados por ;) :";


//end variables for affiliate page

//Language variables for smssetting page

	$lng_receivesms		= "Receba uma mensagem SMS quando um novo leilão começar";
	$lng_yesreceivesms	= "Sim, me informar quando um leilão começar.";
	$lng_smsturnon		= "Seu alerta por SMS foi ativado!";
	$lng_smsturnoff		= "Seu alerta por SMS foi desativado!";
	$lng_plsentermobile	= "Por favor, indique o seu celular!";

//end variables for smssetting page

//Language variables for mydetails page

	$lng_customerid		= "Cliente ID :";
	$lng_detailmobileno	= "Numero do Celular :";
	$lng_mydetailnote	= "Para evitar abusos, não permitimos que você edite seus detalhes, uma vez registada. Você só pode editar o seu número móvel.";
	$lng_mobileupdate	= "Numero de telefone altaerado com sucesso!";
	$lng_yourdata		= "Seus Dados : ";

//end variables for mydetails page

//Language variables for changepassword page

	$lng_yourpassword	= "Altere sua Senha";
	$lng_enternewpassword = "Por favor, digite sua nova senha";
	$lng_retypenewpassword= "Por favor, redigite sua nova senha";
	$lng_passwordchanged= "Sua senha foi alterada!";

//end variables for changepassword page

//Language variables for closeaccount page

	$lng_closeavenueacc	= "Finalizar sua conta";
	$lng_closeimportantnote	= "Importante: Se voc fechar sua conta, você não será capaz de voltar a registar com o Leilão usando esse endereço de e-mail por dois meses. Observe também que você perderá todos os lances que você tem atualmente em sua conta.";
	$lng_yescloseaccount	= "Sim, eu quero fechar minha conta";

//end variables for closeaccount page

//Language variables for newsletter page

	$lng_wanttoknow		= "Gostaria de receber mais detalhes sobre nossas ofertas em seu email?";
	$lng_newslettersub	= "Inscrever em newsletter :";
	$lng_youremailadd	= "Seu Email :";
	$lng_dontwanttoknow	= "Não gostaria de receber mais detalhes sobre nossas ofertas em seu email?";
	$lng_newsletterunsub= "Descadastrar em newsletter :";
	$lng_thankyounewslet= "Obrigado por se cadastrar em nossa newsletter.";
	$lng_funandgoodluck	= "Divirta-se e boa sorte!";
	$lng_youraucaveteam	= "Seu XRLeilões - Equipe";
	$lng_unsubscribenews= "Seu descadastramento em nossa newsletter ...";
	$lng_plsentersubemail = "Por favor, infome endereço de email para se Cadastrar!";
	$lng_plsenterunsubemail = "Por favor, infome endereço de email para se Descadastrar!";
	$lng_plscheckemailadd = "Verifique seu e-mail. Foi digitado incorretamente.";

//end variables for newsletter page

//Language variables for help page

	$lng_helptopics		= "Topicos de Ajuda";
	$lng_quicklinks		= "Links Rápidos";
	$lng_lostpassword	= "Senha Perdida";
	$lng_subunsubnewslet= "Cadastrar/Descadastrar em nossa Newsletter";
	$lng_lostuserdata	= "Perdeu os dados do usuario";
	$lng_changeaddress	= "Alterar Endereço";
	$lng_confirmwon		= "Confirme um leilão ganho";
	$lng_reportabuse	= "Informar Abuso";
	$lng_emailaucavenue	= "Email XRLeilões";
	$lng_contactbyemail	= "Envie-nos um email<br>através do centro de apoio ao cliente:";


//end variables for help page

//Language variables for productdetails page

	$lng_smallauctions	= "Leilões";
	$lng_addtowatchlist	= "Adicionar é Lista de Preferidos ";
	$lng_addedtowatchlist= "Adicionado é Lista de Preferidos";
	$lng_fixedpriceauc= "Preço Fixo ";
	$lng_fixedpriceauction="Leilão de Preço Fixo";
	$lng_offauction		= "100% off";
	$lng_centauction	= "Leilão de 1c";
	$lng_nightauction	= "Leilão Noturdo";
	$lng_nailbiterauction="Leilão NailBiter";
	$lng_clicktoenlarge	= "Clique na imagem para ampliá-la";
	$lng_bidhistory		= "Histórico de Lances";
	$lng_mybids			= "Meus Lances";
	$lng_bidhistorybid	= "BID";
	$lng_bidhistorybidder= "USUÁRIO";
	$lng_bidhistorytype	= "TIPO";
	$lng_biddingsinglebid = "lance simples";
	$lng_biddingsmsbid	= "SMS Lance";
	$lng_biddingbutlerbid="Auto Lance";
	$lng_biddingtime	= "TEMPO";
	$lng_witheachbid	= "Cada Lance aumentará o valor do item em ";
	$lng_endlatest1		= "Este leilão terminara em ";
	$lng_endlatest2		= " às ";
	$lng_startlatest1	= "Este leilão começará em ";
	$lng_savings		= "Economia";
	$lng_worthupto		= "Valor de Mercado:";
	$lng_placedbids		= "Lances Feitos";
	$lng_fixedprice		= "Preço Fixo:";
	$lng_typicalworthup	= "O 'Valor de Mercado' reflete o valor recomendado.";
	$lng_auctionended1	= "Este leilão foi encerrado em ";
	$lng_auctionended2	=  " às ";
	$lng_congratulations= "Parabéns,";
	$lng_bookbidbuddy	= "Auto Lances";
	$lng_bidbuddyoverview="Meus Auto Lances";
	$lng_bidfrom		= "BID DE";
	$lng_bidto			= "BID PARA";
	$lng_butlerbids		= "BIDS";
	$lng_bidbuddyadded	= "Auto lance Adicionado";
	$lng_noactivebidbuddy="Nenhum Auto Lances Ativo";
	$lng_bidbuddy		= "Auto lance";
	$lng_youcantplace	= "Este leilão é Lance Simples.";
	$lng_nowliveonauc	= "Agora no XRLeilão";
	$lng_aboutaucave	= "Sobre o XRLeilão";
	$lng_contactaucave	= "Fale com o XRLeilão";
	$lng_newtoaucave	= "Novo no Shock Bee?";
	$lng_getfirstauction= "Registre-se agora! ";
	$lng_productdetails	= "Detalhes do Cavalo";
	$lng_noliveauction	= "Nenhum Leilão para mostrar";

//end variables for productdetails page

//Language variables for allliveauction/futureauctions/endedauctions page

	$lng_liveauctions	= "Leilões Ativos";
	$lng_allauctions	= "Todos os Leilões";
	$lng_insteadof		= "(inves de ";
	$lng_noliveauctioncat="Nenhum Leilão ativo nesta categoria";

	$lng_nofutureauction= "Nenhum Leilão futuro para exibir";
	$lng_nofutureauctioncat="Nenhum Leilão futuro para exibir nesta categoria";

	$lng_endtime		= "Finalizado";
	$lng_noendedauction	= "Nenhum leilão finalizado para exibir";
	$lng_noendedauctioncat="Nenhum leilão finalizado para exibir nesta categoria";
//end variables for allliveauction page

//Language variables for aboutus page

	$lng_aboutaucave	= "Sobre o XRLeilões";

//end variables for allliveauction page

//Language variables for allnews page

	$lng_aucavenews		= "Novidades XRLeilões";
	$lng_newsdate		= "Data:";
	$lng_nonewstodisp	= "Nenhuma novidade para exibir";

//end variables for allnews page

//Language variables for bidhistory page

	$lng_purchasebids	= "Comprado / Gratis";
	$lng_bidsdate		= "Data";
	$lng_historybids	= "Bids Dados";
	$lng_biddingstart	= "Inicio";
	$lng_biddingstatus	= "Status";
	$lng_bidliveauction	= "Leilões ativos";
	$lng_bidfutureauction= "Proximos Leilões";
	$lng_donthaveanybid	= "Você ainda não tem nenhum bid em sua conta";
	$lng_biddingsmsbids	= "Bids SMS";
	$lng_backbooking	= "Voltar";

//end variables for bidhistory page


//Language variables for buybidsunsuccess page

	$lng_buybidspayment	= "Pagamento de Bids";
	$lng_sorrygoback	= "Desculpe seu pagamento não foi processado<br /><a href='buybids.html' class='alink'>Clique aqui</a> para voltar.";

//end variables for buybidsunsuccess page

//Language variables for emailconfirmsuccess page

	$lng_emailconfirmsucc= "Confirmação de email";
	$lng_regsuccess		= "Rigistrado com sucesso";
	$lng_regcongratulat	= "Parabéns!!";
	$lng_yourregsuccess	= "Seu e-mail de verificação foi confirmado com sucesso, por favor <a href='myaccount.html' class='alink'>Clique aqui</a> para continuar...";

//end variables for emailconfirmsuccess page


//Language variables for forgotpassword page

	$lng_titleforgot	= "Perdeu a Senha?";
	$lng_didnotfindemail= "Desculpe, Não encontramos o seu endereço de e-mail em nosso sistema!";
	$lng_accountverify	= "Essa conta não foi verificada ainda. Por favor, verifique primeiro.";
	$lng_emailsentto	= "Um e-mail foi enviado para :";
	$lng_forgottendata	= "Você esqueceu seus dados de login?";
	$lng_noproblementer	= "Não tem problema, basta inserir o seu email e enviaremos as informações para a sua conta de e-mail.";
	$lng_enteryouremail	= "Seu Email : ";

//end variables for forgotpassword

//Language variables for jobs page

	$lng_aucavejobs	= "Trabalhe na XRLeilões";

//end variables for forgotpassword

//Language variables for paymentsuccess/paymentunsuccess page

	$lng_wonauctionpay	= "Pagamento de Leilões Ganhos";
	$lng_paymentreceive	= "Pagamento de leilão ganho recebido com sucesso<br/>ID DO LEILÃO: ";
	$lng_paymentnotreceive="Desculpe!<br />Alguns problemas ocorreram no processo de pagamento.";
	$lng_paymentunsuccess="Desculpe, seu processo de pagamento não foi concluido<br /><a href='wonauctions.html' class='alink'>Clique Aqui</a> para voltar.";

//end variables for paymentsuccess


//Language variables for privacy page

	$lng_aucaveprivacy	= "Politica de Privacidade do Leilão";

//end variables for privacy

//Language variables for smsbid static page

	$lng_smsbidaucave	= "Leilão SMS Bid";

//end variables for smsbid static page

//Language variables for terms static page

	$lng_aucaveterms	= "Leilão Termos &amp; Condições";
	$lng_aucpromo	= "Promoções";

//end variables for terms static page

//Language variables for thankyou page

	$lng_youpurchased1	= "Parabéns! Você Comprou ";
	$lng_youpurchased2	= ". Sua transação foi concluída com sucesso.";
	$lng_youcanbid		= "Você pode dar Bid";
	$lng_thankyoumessage= "<a href='index.html' class='alink'>Clique aqui</a> para encontrar preços incriveis";
	$lng_aucaveteam		= "Equipe XRLeilões.";

//end variables for thankyou page

//Language variables for addresses page

	$lng_addresses		= "Endereços";
	$lng_hereeditadd	= "Aqui você pode adicionar ou editar seus endereços: ";
	$lng_enternewadd	= "Informe seu novo endereço";
	$lng_addresschanged	= "Endereço alterado com sucesso!";
	$lng_deliveryaddress= "Endereço de Entrega";
	$lng_onlyname		= "Nome :";
	$lng_mandatoryfield	= "Campos Obrigatórios";
	$lng_billingaddress	= "Endereço de Cobrança";

//end variables for addresses page

//Language variables for wonauctionaccept page

	$lng_wonauctionaccept="Por Favor, confirme seu leilão ganho";
	$lng_alreadyacceptdenied="Você já aceitou/negou este Bid!";
	$lng_onlyaccept		= "Aceitar";
	$lng_onlydenied		= "Negar";

//end variables for wonauctionaccept page

//Language variables for choosevoucher page

	$lng_aucavevouchers	= "Vouchers Leilão";
	$lng_makepayment	= "Efetuar pagamento";
	$lng_plsselvoucher	= "Por favor selecione um voucher";
	$lng_selectone		= "selecione um";
	$lng_dontwantvou	= "Se você não quiser usar seu cupom, assinale a opção.";
	$lng_auctiondetails	= "Detalhes do Leilão";
	$lng_shippingcharge	= "Custo de Entrega";
	$lng_vouchermaximum	= "Valor do Bonus(Máximo";
	$lng_totalpayment	= "Pagamento Total";
	$lng_vouchernote1	= "Note: ";
	$lng_vouchernote2	= "Estes são Vale-Bids. Após pagamento eles serão creditados em sua conta.";


//Language variables for choosevoucher page


//Language variables for helpdesk support centre

	//language variables for contact page

		$lng_helponlyname	= "Nome";
		$lng_aucavesupport	= "Centro de Suporte XRLeilões";
		$lng_submitticket	= "Enviar um Ticket";
		$lng_viewticket		= "Ver Tickets";
		$lng_pleaseuseform	= "Utilize o formulário abaixo para enviar um ticket. Os campos obrigatórios estão marcados com <font class='red-text-12-b'>*</font>";
		$lng_helpemail		= "E-mail";
		$lng_helpcategory	= "Categoria";
		$lng_helppriority	= "Prioridade";
		$lng_helpsubject	= "Assunto";
		$lng_helpmessage	= "Mensagem";
		$lng_helpattachment	= "Anexos";
		$lng_helpacceptedfiles= "Arquivos Aceitos";
		$lng_helpmaxfilesize= "Tamanho Máx.";
		$lng_helpenternumber="Por favor, informe o número exibido acima";
		$lng_helpsubmittingsure="Antes de enviar certifique-se da seguinte";
		$lng_helpallnecessary	= "Toda a informação necessária foi preenchida";
		$lng_helpallcorrect	= "Toda a informação está correta e livre de erros";
		$lng_helplow		= "Baixa";
		$lng_helpmedium		= "Média";
		$lng_helphigh		= "Alta";

	//end variables for contact page

	//language variables for viewtickets page

		$lng_helpexcisting	= "Ver Tickets Abertos";
		$lng_helptrackingid	= "ID de rastreamento";
		$lng_helpforgottid	= "Perdeu ID de rastreamento?";
		$lng_helpplsenteremail="Por favor digite seu endereço de email e as IDs dos seus tickets será enviados para você";

	//end variables for ticket page

	//language variables for ticket page

		$lng_helpticketstatus="Status do Ticket";
		$lng_helpcreateon	= "Criado em";
		$lng_helplastupdate	= "Ultima atualização";
		$lng_helplastreplier= "Ultima resposta";
		$lng_helpreplies	= "Respostas";
		$lng_helpprinterfriend="Versão para Impressão";
		$lng_helpdate		= "Data";
		$lng_helpaddreply	= "Add. resposta";

	//end variables for viewtickets page

	//language variables for reply_ticket page

		$lng_helpticketreply	= "Ticket Respondidos";
		$lng_helpentermessage="Por Favor, digite uma mensagem";
		$lng_helpreplysubmit = "Sua resposta foi enviada";
		$lng_helpyourreplysubmit="Sua resposta para este ticket foi enviado com sucesso!";
		$lng_helpnotrackingid	= "Sem ID de rastreamento";
		$lng_helpstatusnotvalid	= "Status inválido";
		$lng_helpclosed			= "Fechado";
		$lng_helpopened			= "Aberto";
		$lng_helptrackingnotfound	= "ID de rastreamento não econtrado";
		$lng_helponlyticket		= "Ticket";
		$lng_helpyourticketbeen	= "Seus ticket foram";
		$lng_helpviewyourticket	= "Ver seus ticket";
		$lng_helpforgotonlyid	= "Perdeu ID de rastreamento";
		$lng_helpwaitingstaff	= "'Aguarando responsta da equipe";
		$lng_helpwaitingcust	= "Aguardando resposta do cliente";

	//end variables for viewtickets page

	//language variables for forgotID page

		$lng_helptrackingidsent	= "ID de rastreamento enviado";
		$lng_helpemaildeatilsent= "Um e-mail com detalhes sobre o seu ticket foi enviado para o endereço";
		$lng_helpalsocheckemail	= "<b>Não deixe de verificar o e-mail dentro de sua caixa de lixo eletrônico!</b>";
		$lng_helperror			= "Erro";
		$lng_helpnoemailfound	="Nenhum ticket com o seu endereço de e-mail foram encontrados";
		$lng_helpgoback			= "Voltar";



	//end variables for forgotID page

//end variables for  helpdesk support centre

//For all pages JAVASCRIPT ALERTS variable

		//acceptordenied.php
		$lng_plsselaccordenied = "Por favor selecione aceito ou negado!";

		//addresses.php

		$lng_plsentername 	    = "Por Favor, informe o nome!";
		$lng_plsenteradd 	    = "Por Favor, informe o endereço!";
		$lng_plsentercity		= "Por Favor, informe a cidade!";
		$lng_plsenterpostcode	= "Por Favor, informe o CEP!";
		$lng_plsenterphoneno	= "Por Favor, informe o Telefone!";

		//affiliate.php

		$lng_plsenteremailadd	= "Por Favor, Informe o Endereço de email!";

		//buybids.php

		$lng_plschoosemethod	= "Por favor, escolha como você gostaria de comprar!";

		//forgotpassword.php

		$lng_entervalidemail	= "Por Favor, informe um endereço de email válido";

		//choosevoucher.php

		$lng_plschoosevoucher	= "Por Favor, selecion um Voucher!";

		//editpassword.php

		$lng_plsenternewpass	= "Por Favor, Informe Sua Nova Senha!";
		$lng_passtooshort		= "A senha é muito curta!";
		$lng_plsenterconfpass	= "Por Favor, Informe a Confirmação de Senha!";
		$lng_passmismatch		= "Incompatibilidade de senha!";

		//watchauction.php

		$lng_areyousuretodel	= "Tem certeza de eliminar este leilão participado";

		//unsubscribeuser.php

		$lng_plstickcheckbox	= "Por Favor, Marque a caixa de verificação para confirmar o fechamento da sua conta";

		//registration.php

		$lng_regenterfirstname	= "Por Favor, Informe Primeiro Nome!";
		$lng_regenterlastname	= "Por Favor, Informe Lastname!";
		$lng_regselbirthdate	= "Por Favor, Selecione Data de Nascimento!";
		$lng_regenteraddress	= "Por Favor, Informe o Endereço!";
		$lng_regentercity		= "Por Favor, Informe a Cidade!";
		$lng_regenterpostcode	= "Por Favor, Informe CEP!";
		$lng_regenterphoneno	= "Por Favor, Informe telefone!";
		$lng_regentercpf		= "Prof Favor, Informe seu CPF!";
		$lng_regenterrg			= "Prof Favor, Informe seu RG!";
		$lng_regusernametooshort= "Nome de usuário é muito curto!";
		$lng_regenterpassword	= "Por Favor, Informe Senha!";
		$lng_regpasstooshort	= "Senha Muito Curta!";
		$lng_regconfpassword	= "Por Favor, Informe Confirmação de Senha!";
		$lng_regpassmismatch	= "Senhas Diferentes!";
		$lng_regenteremailadd	= "Por Favor, Informe Endereço de Email";
		$lng_regenterconfemail	= "Por Favor, Informe Confirmação de Email!";
		$lng_regemailmismatch	= "Emails Não Conferem!";
		$lng_regreadterms		= "Por Favor, aceite os temos e condições!";
		$lng_regreadprivacy		= "Por Favor, aceite a politica de privacidade!";
		$lng_regenterseccode	= "Por Favor, Informe o Codigo de Segurança!";


		//productdetails.php

		$lng_plsbidstartprice	= "Por Favor, informe o preço inicial doAuto Lances !";
		$lng_plsbidendprice		= "Por Favor, informe o preço final doAuto Lances!";
		$lng_plsenterbid		= "Por Favor, informe Auto Lances bids!";
		$lng_plsmorethanone		= "Você informou Auto Lance para mais de um Lance!";
		$lng_bidstartnotgreater	= "Preço inicial Não superior ao preço final!";
		$lng_bidendpricemust	= "O preço é preciso ser maior que o preço final!";
		$lng_bidfromvalueismust	= "BID FROM Valor precisa ser maior do que o preço atual!";
		$lng_youbidisrunning	= "Seu Auto Lances está em execução não se pode excluí-lo!";

		//mydetails.php

		$lng_plsentermobileno	= "Por Favor, Informe o numero de celular!";

		//commonused

		$lng_pleaserechargebid	= "Por Favor, compre bids!";

// End all pages javascript alerts

// For All pages EMAIL variables start

	//wonauction information email

		$lng_mailwonaucinfo		= "informação do Leilão ganho";
		$lng_mailhi				= "Olá ";
		$lng_mailcongratulation	= "Parabéns!";
		$lng_mailyouhavewon		= "Você acaba de vencer o seguinte Lote no Leilão.";
		$lng_mailauctionid		= "Leilão ID";
		$lng_mailname			= "Nome";
		$lng_mailprice			= "Preço";
		$lng_mailclosingdate	= "Data de Fechamento";
		$lng_mailclosingnote	= "Os Leilões ganhos devem ser aceitos / recusados no prazo de 7 dias a contar da data de encerramento";
		$lng_mailaccdenclick1	= "Para aceitar / negar o seu leilão ganho,  ";
		$lng_mailaccdenclick2	= "<strong>CLIQUE AQUI</strong>";
		$lng_mailsubjectauctionwon	= "Parabéns você acaba de vencer um lote na XRLeilões!";
		$lng_youchoosen3	= "Frete a combinar.";

	//end

	//affiliate email

		$lng_mailheythere		= "Olá,";
		$lng_mailyoureceived1	= "Você recebeu este e-mail porque ";
		$lng_mailyoureceived2	= " convidou você para participar ";
		$lng_mailthesiteconst	= "O site consiste em um leilão on-line onde os usuários (tais como a você!) efetuam lances.";
		$lng_mailclicktoregister= "Clique no link a seguir e registar-se no XRLeilões";
		$lng_mailsubjectaffiliate = "Conhecha o site XRLeilões.com.br";

	// This is emailcontent of affiliate.php start

		$lng_emailcontent_affiliate="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";
		$lng_emailcontent_affiliate.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>".$lng_mailheythere."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'></p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailyoureceived1."%1\$s".$lng_mailyoureceived2." do site <a href='".$SITE_URL."registration.php?ref=%2\$s'>Leilão</a></td>
		</tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailthesiteconst."</td>
		</tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailclicktoregister."</td>
		</tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>URL: <a href='".$SITE_URL."registration.php?ref=%2\$s'>Leilão</a></td></tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td><hr>www.XRLeiloes.com.br - <strong>Seu portal de Negócios Online!</strong></td>
		</tr>";

		$lng_emailcontent_affiliate.="</table>";


	// This is emailcontent of affiliate.php end


	//end

	//registration confirmation/forgotpassword  email

		$lng_mailaccountinfo	= "Informações da conta";
		$lng_mailreghi			= "Olá ";
		$lng_mailreglogininfo	= "Aqui estão algumas informações sobre a sua conta que você solicitou";
		$lng_mailaucaveusername	= "login";
		$lng_mailaucaveuserid	= "ID de usuário";
		$lng_mailtoresetyourpass1= "Para resetar sua senha ";
		$lng_mailtoenableyouracc = "Seus dados foram enviados para aprovação do moderador. Aguarde um e-mail de confirmação para validar o seu cadastro.";
		$lng_mailtoresetyourpass2= "<strong>CLIQUE AQUI</strong>";
		$lng_mailhappywinning	= "XRLeilões, seu portal de negócios online!!!";
		$lng_mailsubjectaccinfo	= "Informações sobre cadastro na XRLeilões.com.br";

	//end

	// This is emailcontent for forgotpassword.php start

		$lng_emailcontent_forgotpassword="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";
		$lng_emailcontent_forgotpassword.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailaccountinfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreghi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreglogininfo.":</td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveusername.": %2\$s</td>
		</tr>";

		// $lng_emailcontent_forgotpassword.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		// <td>".$lng_mailaucaveuserid.": %3\$s</td>
		// </tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtoresetyourpass1.": <a href='".$SITE_URL."password.php?ud=%4\$s&pd=%5\$s&key=%6\$s'>".$lng_mailtoresetyourpass2."</a></td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td><br>".$lng_mailhappywinning."<br></td>
		</tr>
		</table>";

	// This is emailcontent for forgotpassword.php end

	// This is emailcontent for registration.php start

		$lng_emailcontent_registraion="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";
		$lng_emailcontent_registraion.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailaccountinfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_registraion.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreghi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreglogininfo.":</td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveusername.": %2\$s</td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveuserid.": %3\$s</td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtoenableyouracc."</td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailhappywinning."</td>
		</tr>
		</table>";

	// This is emailcontent for registration.php end

	// EMAIL DE CADASTRO ENVIADO PARA O ADMIN

		$lng_emailcad="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";
		$lng_emailcad.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 18px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>Solicitação de Cadastramento</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Dados do usuario: </td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Data de Registro: %14\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>ID o usuário: %1\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Usuario: %2\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Nome Completo: %3\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Sexo: %5\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Nascimento: %6\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Endereço: %18\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Cidade: %20\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>CEP: %28\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Estado: %22\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>País: %15\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone 1: %24\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone 2: %25\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Celular: %17\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>email: %7\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>CPF: %31\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>RG: %32\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Profissão: %35\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Empresa: %34\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>CNPJ: %33\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Renda: %36\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Autonomo: %37\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Endereço da Empresa: %19\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Bairro da Empresa: %38\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Cidade da Empresa: %21\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>CEP da Empresa: %29\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Estado da Empresa: %23\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Pais da Empresa: %16\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone 1 da Empresa: %25\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone 2 da Empresa: %26\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Banco: %39\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Agência: %40\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Conta Corrente: %41\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone do Banco: %42\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Nome de Referência Pessoal 1: %43\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone da Referência Pessoal 1: %45\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Nome de Referência Pessoal 2: %44\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone da Referência Pessoal 2: %46\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Nome de Referência Comercial 1: %47\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone da Referência Comercial 1: %49\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Nome de Referência Comercial 2: %48\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Telefone da Referência Comercial 2: %50\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Já comprou cavalo antes? %52\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Se sim, onde? %51\$s</td>
		</tr>";

		$lng_emailcad.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>Após conferencia e confirmação dos dados é necessária a aprovação ou negação do cadastro.<br><br>Por favor, escolha a opção desejada: <a href='http://www.xrleiloes.com.br/leilao/confirmarCadastro.php?ok=sim&userID=%1\$s' target='_blank'>ACEITAR</a> ou <a href='http://www.xrleiloes.com.br/leilao/confirmarCadastro.php?ok=nao&userID=%1\$s' target='_blank'>NEGAR</a>.</td>
		</tr>";

		$lng_emailcad.="</table>";

	// FM EMAIL DE CADASTRO ADMIN

	//acceptordenied wonauction information email

		$lng_mailauctioninfo	= "Informações do Leilão Ganho";
		$lng_mailaccepthi		= "Ola ";
		$lng_mailwonandaccept	= "Você ganhou e aceitou o seguinte item.";
		$lng_mailwonanddenied	= "Você ganhou e negou o seguinte item.";
		$lng_mailwonacceptid	= "Leilão ID";
		$lng_mailwonname		= "Nome";
		$lng_mailwonprice		= "Preço";
		$lng_mailaccedenieddate	= "Data de Aceitação / negação";
		$lng_mailauctionpaid	= "O preço de leilão deve ser pago no prazo de 7 dias a contar da data de aceitação.";
		$lng_mailtomakepay1		= "Para fazer o pagamento ";
		$lng_mailtomakepay2		= "<strong>CLIQUE AQUI</strong>";
		$lng_mailsubjectacceptordenied = "Leilão Aceito / Negado - Item ganho!";

	//This email content for acceptordenied.php's Accept start

		$lng_emailcontent_acceordeaccept="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";

		$lng_emailcontent_acceordeaccept.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_acceordeaccept .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		"</table>";

	//This email content for acceptordenied.php's Accept end

	//This email content for acceptordenied.php's Denied start

		$lng_emailcontent_acceordedenied="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";

		$lng_emailcontent_acceordedenied.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_acceordedenied .= $lng_mailwonanddenied;
		"</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		"</table>";

	//This email content for acceptordenied.php's Denied end

	//This email content for acceptordenied.php's Accept and mailflag = 0 start

		$lng_emailcontent_acceordeflag="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";

		$lng_emailcontent_acceordeflag.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_acceordeflag .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailauctionpaid."</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtomakepay1."<a href='".$SITE_URL."login.php?wid=%6\$s'>".$lng_mailtomakepay2."</a></td>
		</tr>";
		"</table>";

	//This email content for acceptordenied.php's Accept and mailflag = 0 end

	//This email content for wonauctionaccept.php's Accept start

		$lng_emailcontent_wonaccept="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";

		$lng_emailcontent_wonaccept.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td> ".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_wonaccept .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>
		</table>";

	//This email content for wonauctionaccept.php's Accept end

	//This email content for wonauctionaccept.php's Denied start

		$lng_emailcontent_wondenied="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";

		$lng_emailcontent_wondenied.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td> ".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_wondenied .= $lng_mailwonanddenied;
		"</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>
		</table>";

	//This email content for wonauctionaccept.php's Denied end

	//This email content for wonauctionaccept.php's Accept and mailflag = 0 start

		$lng_emailcontent_wonacceptflag="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";

		$lng_emailcontent_wonacceptflag.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".

		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td> ".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_wonacceptflag .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailauctionpaid."</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtomakepay1."<a href='".$SITE_URL."login.php?wid=%5\$s'>".$lng_mailtomakepay2."</a></td>
		</tr>
		</table>";


	//This email content for wonauctionaccept.php's Accept and mailflag = 0 end

	//end

	//subscribe for newsletter email

		$lng_mailnewsletterhi	= "Ola,";
		$lng_mailthanksinterest	= "Obrigado pelo seu interesse em nossa newsletter.";
		$lng_mailyouarenowsub	= "Está cadastrado para receber a newsletter do Leilão.";
		$lng_mailwhydonttake1	= "Por que você não dá uma olhada ";
		$lng_mailwhydonttake2	= " para ver o que ofertas incríveis que temos no Leilão.";
		$lng_mailhavefun		= "Divirta-se e boa Sorte!";
		$lng_mailformhere		= "De todos no Leilão";
		$lng_mailnewslettersubject = "Obrigado por se cadastar em nossa newsletter";

	//This is emailcontent for newsletter.php start

		$lng_emailcontent_newsletter ="<img src='".$SITE_URL."images/logo.png' height='100'><hr><br><br>";
		$lng_emailcontent_newsletter.= "<font style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'></p>"."<br>".

	"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailnewsletterhi."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailthanksinterest."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailyouarenowsub."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwhydonttake1."<a href='".$SITE_URL."index.php'>".$SITE_URL."index.php</a>".$lng_mailwhydonttake2."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailhavefun."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 16px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailformhere."</td>
		</tr>
		</table>";

// end email variables

//ERROR Messages for smsbidplace page

	$lng_wrongparameter	= "ERRO : Parametro errado informado";
	$lng_userdoesnotex	= "ERRO : Usuário não existe";
	$lng_auctionnotrunn	= "ERRO : Este leilão ainda não começou";
	$lng_auctioncurrpause="ERRO : Este leilão esta em pausa";
	$lng_aucdoesnotex	= "ERRO : Este leilão não existe";
	$lng_okmessage1		= "OK : Seu Lance foi Dado com Sucesso. O preço após o lance é ";
	$lng_okmessage2		= " R$ e Tempo restante é ";
	$lng_invalidmessage	= "ERRO : Dados de mensagens inválidas";

//smsbulknotify page variables

	$lng_bulkmessage1	= "Leilão ";
	$lng_bulkmessage2	= " está apenas começando";

//end smsbulknotify page variables

//end messages

// New variables define for SnaBid start

	$lng_openauction = "Leilões Ativos";
	$lng_moreliveauction = "Mais Leilões Ativos";
	$lng_howitwork   = "Como Funciona?";
	$lng_regforfree	 = "<a href='registration.html' class='register'>Registre-se</a>&nbsp;ou&nbsp;<a href='login.html' class='register'>Efetue Login</a>";
	$lng_regforfree2 = "<a href='../registration.html' class='register'>Registre-se</a>&nbsp;ou&nbsp;<a href='../login.html' class='register'>Efetue Login</a>";
	$lng_logout = "Deslogar";
	$lng_paymethod = "PayPal";
	$lng_paymethod2 = "PagSeguro";

// New variables define for SnaBid end

	$lng_pause		= 	"Pausado";
	$lng_nobidspalced = "Nenhum Lance Feito";
	$lng_subtotal   = "Subtotal";
?>