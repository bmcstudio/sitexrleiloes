<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
include("config/connect.php");
include("../functions.php");
$staticvar = "contact";
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');

/* Print header */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<body>
<div id="main_div">
<?
	include("header.php");
?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$hesklang['view_ticket'];?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2_static">
			<div class="staticbody">
			<div style="height: 20px;">&nbsp;</div>
			<?
				include("leftstatic.php");
			?>	
				<div class="staticright">
					<div style="text-align: right;"><a href="contact.html?a=add" class="alink"><?=$hesklang['backtomain'];?></a></div>
				<?
					if($_POST['send']!="")
					{
						$message=hesk_input_reply($_POST['message'],$hesklang['enter_message']);
						$message=hesk_makeURL($message);
						$message=nl2br($message);
						$orig_name=hesk_input_reply($_POST['orig_name'],"$hesklang[int_error]: No orig_name");
						$replyto=hesk_isNumber($_POST['orig_id'],"$hesklang[int_error]: No or invalid orig_id");
						$trackingID=hesk_input_reply($_POST['orig_track'],"$hesklang[int_error]: No orig_track");
						$trackingURL=$hesk_settings['hesk_url'].'/ticket.html?track='.$trackingID.'&Refresh='.rand(10000,99999);
				
					}
		?>
		<?
		if($_POST['send']!="" && $_POST['message']!="")
		{
		
			
			/* Attachments */
			if ($hesk_settings['attachments']['use']) {
				require_once('inc/attachments.inc.php');
				$attachments = array();
				for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
					$att = hesk_uploadFile($i);
					if (!empty($att)) {
						$attachments[$i] = $att;
					}
				}
			}
			$myattachments='';
			/* Connect to database */
			require_once('inc/database.inc.php');
			hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
			
			if ($hesk_settings['attachments']['use'] && !empty($attachments)) {
				foreach ($attachments as $myatt) {
					$sql = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$trackingID', '$myatt[saved_name]', '$myatt[real_name]', '$myatt[size]')";
					$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
					$myattachments .= hesk_dbInsertID() . '#' . $myatt['real_name'] .',';
				}
			}
			
			/* Make sure the ticket is open */
			$sql = "UPDATE `hesk_tickets` SET `status`='1',`lastreplier`='0',`lastchange`=NOW() WHERE `id`=$replyto LIMIT 1";
			$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
			
			/* Add reply */
			$sql = "
			INSERT INTO `hesk_replies` (
			`replyto`,`name`,`message`,`dt`,`attachments`
			)
			VALUES (
			'$replyto','$orig_name','$message',NOW(),'$myattachments'
			)
			";
			$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
			
			$sql = "SELECT `subject`,`category` FROM `hesk_tickets` WHERE `id`=$replyto LIMIT 1";
			$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
			$ticket = hesk_dbFetchAssoc($result);
			$category=$ticket['category'];
			
			/* Need to notify any admins? */
			$admins=array();
			$sql = "SELECT `email`,`isadmin`,`categories` FROM `hesk_users` WHERE `notify`='1'";
			$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
			while ($myuser=hesk_dbFetchAssoc($result))
			{
				/* Is this an administrator? */
				if ($myuser['isadmin']) {$admins[]=$myuser['email']; continue;}
				/* Not admin, is he allowed this category? */
				$cat=substr($myuser['categories'], 0, -1);
				$myuser['categories']=explode(",",$cat);
				if (in_array($category,$myuser['categories']))
				{
					$admins[]=$myuser['email']; continue;
				}
			}
			if (count($admins)>0)
			{
			$trackingURL_admin=$hesk_settings['hesk_url'].'/admin_ticket.php?track='.$trackingID;
			
			/* Get e-mail message for customer */
			$fp=fopen('emails/new_reply_by_customer.txt','r');
			$message=fread($fp,filesize('emails/new_reply_by_customer.txt'));
			fclose($fp);
			
			$message=str_replace('%%NAME%%',$orig_name,$message);
			$message=str_replace('%%SUBJECT%%',$ticket['subject'],$message);
			$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
			$message=str_replace('%%TRACK_URL%%',$trackingURL_admin,$message);
			$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
			$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
			
			/* Send e-mail to staff */
			$email=implode(',',$admins);
			$headers="From: $hesk_settings[noreply_mail]\n";
			$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
			@mail($email,$hesklang['new_reply_ticket'],$message,$headers);
			} // End if
	
?>

						<p>&nbsp;</p>
						
						<h3 class="darkblue-text-17-b" align="center"><?php echo $hesklang['reply_submitted']; ?></h3>
						
						<p>&nbsp;</p>
						
						<p align="center" class="normal_text"><?php echo $hesklang['reply_submitted_success']; ?>!</p>
						<p align="center" style="margin-top: 10px;"><a href="<?php echo $trackingURL; ?>" class="alink"><?php echo $hesklang['view_your_ticket']; ?></a></p>
						
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
				
				</div>
		 </div>
		 </div>
		 <div id="cleaner"></div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
<?	
	session_unset($_SESSION['c_name']);     
    session_unset($_SESSION['c_email']);
    session_unset($_SESSION['c_category']);
    session_unset($_SESSION['c_priority']);
    session_unset($_SESSION['c_subject']);
    session_unset($_SESSION['c_message']);
	include("footer.php"); 
	exit;
}
	
?>
<?
$trackingID=strtoupper(hesk_input($_GET['track']));
if (empty($trackingID)) {print_form();}

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

/* Get ticket info */
$sql = "SELECT * FROM `hesk_tickets` WHERE `trackid`='$trackingID' LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbNumRows($result) != 1) {hesk_error($hesklang['ticket_not_found']);}
$ticket = hesk_dbFetchAssoc($result);

/* Get category name and ID */
$sql = "SELECT *,".$hesklang['lng_prefix']."name as name FROM `hesk_categories` WHERE `id`=$ticket[category] LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
/* If this category has been deleted use the default category with ID 1 */
    if (hesk_dbNumRows($result) != 1)
    {
        $sql = "SELECT *,".$hesklang['lng_prefix']."name as name FROM `hesk_categories` WHERE `id`=1 LIMIT 1";
        $result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
    }
$category = hesk_dbFetchAssoc($result);

/* Get replies */
$sql = "SELECT * FROM `hesk_replies` WHERE `replyto`='$ticket[id]' ORDER BY `id` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$replies = hesk_dbNumRows($result);
?>

<h3 align="center" class="darkblue-text-17-b" style="width:710px; margin-top: 20px;margin-bottom: 10px;"><?php echo $ticket['subject']; ?></h3>



<div width="710" style="border:solid:1px;">
<table border="0" width="720" cellspacing="1" cellpadding="5" class="white">
<tr>
<td class="grey">

<table border="0" cellspacing="1" cellpadding="5">
<?php
echo '
<tr>
<td class="normal_text">'.$hesklang['trackID'].'&nbsp;:&nbsp; </td>
<td class="normal_text">'.$trackingID.'</td>
</tr>
<tr>
<td class="normal_text">'.$hesklang['ticket_status'].'&nbsp;:&nbsp; </td>
<td class="normal_text">';
$random=rand(10000,99999);


    switch ($ticket['status']) {
    case 0:
        echo '<font class="open">'.$hesklang['open'].'</font> &nbsp;<strong>[</strong>&nbsp;<a class="alink" href="change_status.html?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>&nbsp;<strong>]</strong>';
        break;
    case 1:
        echo '<font class="replied">'.$hesklang['wait_staff_reply'].'</font> &nbsp;<strong>[</strong>&nbsp;<a class="alink" href="change_status.html?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>&nbsp;<strong>]</strong>';
        break;
    case 2:
        echo '<font class="waitingreply">'.$hesklang['wait_cust_reply'].'</font> <strong>[</strong>&nbsp;<a class="alink" href="change_status.html?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>&nbsp;<strong>]</strong>';
        break;
    default:
        echo '<font class="resolved">'.$hesklang['closed'].'</font> <strong>[</strong>&nbsp;<a class="alink"
        href="change_status.html?track='.$trackingID.'&s=1&Refresh='.$random.'">'.$hesklang['open_action'].'</a>&nbsp;<strong>]</strong>';
    }

echo '</td>
</tr>
<tr>
<td class="normal_text">'.$hesklang['created_on'].'&nbsp;:&nbsp; </td>
<td class="normal_text">'.$ticket['dt'].'</td>
</tr>
<tr class="white">
<td class="normal_text">'.$hesklang['last_update'].'&nbsp;:&nbsp; </td>
<td class="normal_text">'.$ticket['lastchange'].'</td>
</tr>
<tr>
<td class="normal_text">'.$hesklang['last_replier'].'&nbsp;:&nbsp; </td>
<td class="normal_text">';
        if ($ticket['lastreplier']) { echo $hesklang['staff']; }
        else { echo $hesklang['customer']; }
echo '</td>

</tr>
<tr class="white">
<td class="normal_text">'.$hesklang['category'].'&nbsp;:&nbsp; </td>
<td class="normal_text">'.$category['name'].'</td>
</tr>
<tr>
<td class="normal_text">'.$hesklang['replies'].'&nbsp;:&nbsp; </td>
<td class="normal_text">'.$replies.'</td>
</tr>
<tr class="white">
<td class="normal_text">'.$hesklang['priority'].'&nbsp;:&nbsp; </td>
<td class="normal_text">';
        if ($ticket['priority']==1) {echo '<font class="red-text-12-b">'.$hesklang['high'].'</font>';}
        elseif ($ticket['priority']==2) {echo '<font class="medium">'.$hesklang['medium'].'</font>';}
        else {echo $hesklang['low'];}
echo '</td>
</tr>';		
?>
<tr>
<td>&nbsp;</td>
<td><a href="print.html?track=<?php echo $trackingID; ?>" class="alink" target="_blank"><?php echo $hesklang['printer_friendly']; ?></a></td>
</tr>
</table>

</td>
</tr>
<tr>
<td class="white">
    <table border="0" cellspacing="5" cellpadding="5" style="margin-left: -5px;">
    <tr>
    <td class="normal_text"><?php echo $hesklang['date']; ?>&nbsp;:&nbsp;</td>
    <td class="normal_text"><?php echo $ticket['dt']; ?></td>
    </tr>
    <tr>
    <td class="normal_text"><?php echo $hesklang['name']; ?>&nbsp;:&nbsp;</td>
    <td class="normal_text"><?php echo $ticket['name']; ?></td>
    </tr>
    <tr>
    <td class="normal_text"><?php echo $hesklang['email']; ?>&nbsp;:&nbsp;</td>
    <td class="normal_text"><a class="alink" href="mailto:<?php echo $ticket['email']; ?>"><?php echo $ticket['email']; ?></a></td>
    </tr>
    <?php /*?><tr>
    <td class="white"><?php echo $hesklang['ip']; ?>:</td>
    <td class="white"><?php echo $ticket['ip']; ?></td>
    </tr><?php */?>
    </table>

<?php
/* custom fields */
if ($hesk_settings['use_custom']) {

    $myclass='class="normal_text"';
    echo '<table border="0" cellspacing="1">';

    foreach ($hesk_settings['custom_fields'] as $k=>$v) {
        if ($v['use']) {
            if ($myclass) {$myclass='';}
            else {$myclass='class="normal_text"';}
echo <<<EOC
    <tr>
    <td $myclass>$v[name]:</td>
    <td $myclass>$ticket[$k]</td>
    </tr>

EOC;
        }
    }

    echo '</table>';
}

?>

<p style="margin-top: 10px;"><b class="noauction_message"><?php echo $hesklang['message']; ?>&nbsp;:&nbsp;</b><p><br />
<p class="normal_text"><?php echo $ticket['message']; ?><br>&nbsp;</p>

<?php
if ($hesk_settings['attachments']['use'] && !empty($ticket['attachments'])) {
    echo '<p><b>'.$hesklang['attachments'].':</b><br>';
    $att=explode(',',substr($ticket['attachments'], 0, -1));
    foreach ($att as $myatt) {
        list($att_id, $att_name) = explode('#', $myatt);
        echo '<img src="img/clip.gif" width="20" height="20"><a class="alink"  href="download_attachment.html?att_id='.$att_id.'&amp;track='.$trackingID.'">'.$att_name.'</a><br>';
    }
    echo '</p>';
}
?>

</td>
</tr>

<?php
$i=1;
while ($reply = hesk_dbFetchAssoc($result))
{
if ($i) {$color='class="normal_text"'; $i=0;}
else {$color='class="normal_text"'; $i=1;}
echo <<<EOC
    <tr>
    <td $color>
        <table border="0" cellspacing="1">
        <tr>
        <td $color>$hesklang[date]&nbsp;:&nbsp;</td>
        <td $color>$reply[dt]</td>
        </tr>
        <tr>
        <td $color>$hesklang[name]&nbsp;:&nbsp;</td>
        <td $color>$reply[name]</td>
        </tr>
        </table>
    <p><br><b class="noauction_message">$hesklang[message]&nbsp;:&nbsp;</b></p><br>
    <p class="normal_text">$reply[message]</p><br>

EOC;

if ($hesk_settings['attachments']['use'] && !empty($reply['attachments'])) {
    echo '<p><b class="noauction_message">'.$hesklang['attachments'].':</b><br>';
    $att=explode(',',substr($reply['attachments'], 0, -1));
    foreach ($att as $myatt) {
        list($att_id, $att_name) = explode('#', $myatt);
        echo '<img src="img/clip.gif" width="20" height="20"><a class="alink" href="download_attachment.html?att_id='.$att_id.'&amp;track='.$trackingID.'">'.$att_name.'</a><br>';
    }
    echo '</p>';
}

echo '</td></tr>';
}
?>
</table>
</div>

</div>

<div style="padding-left:10px; clear:both">
<div class="ticketdevider"></div>
<h3 width="720" class="darkblue-text-17-b" align="center"><?php echo $hesklang['add_reply']; ?></h3><br />

<form method="POST" action="" enctype="multipart/form-data">
<p width="720" align="center" class="normal_text"><?php echo $hesklang['message']; ?>: <font class="red-text-12-b">*</font><br>
<textarea name="message" rows="12" cols="60"></textarea></p>

<?php
/* attachments */
if ($hesk_settings['attachments']['use']) {

?>

<p width="720" align="center" style="margin-top: 10px;" class="normal_text">

<?php
echo $hesklang['attachments'].':<br>';
    for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
        echo '<input type="file" name="attachment['.$i.']" size="50" class="textboxclas"><br>';
    }
?>
<br />
<span class="normal_text"><?php echo $hesklang['accepted_types']; ?>:</span> <?php echo '*'.implode(', *', $hesk_settings['attachments']['allowed_types']); ?><br>
<?php echo $hesklang['max_file_size']; ?>: <?php echo $hesk_settings['attachments']['max_size']; ?> Kb
(<?php echo sprintf("%01.2f",($hesk_settings['attachments']['max_size']/1024)); ?> Mb)

</p>

<?php
}
?>

<p width="720" align="center" style="margin-top: 20px;"><input type="hidden" name="orig_id" value="<?php echo $ticket['id']; ?>">
<input type="hidden" name="orig_name" value="<?php echo $ticket['name']; ?>" />
<input type="hidden" name="orig_track" value="<?php echo $trackingID; ?>">
<input type="image" src="<?=$hesklang['imagepath'];?>Submit reply.png" onmouseout="this.src='<?=$hesklang['imagepath'];?>Submit reply.png'" onmouseover="this.src='<?=$hesklang['imagepath'];?>Submit reply-hover.png'" class="button"></p>

<input type="hidden" name="send" value="send" />
</form>
</div>
				</div>
				
				</div>
				</div>
			<div id="cleaner"></div>
		 
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
<?
	include("footer.php");
?>		
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');

/*** START FUNCTIONS ***/

function print_form() {
global $hesk_settings, $hesklang;
?>
<?php /*?><p class="smaller"><a href="<?php echo $hesk_settings['site_url']; ?>"
class="smaller"><?php echo $hesk_settings['site_title']; ?></a> &gt;
<a href="index.php?a=start" class="smaller"><?php echo $hesk_settings['hesk_title']; ?></a>
&gt; <?php echo $hesklang['view_ticket']; ?><br>&nbsp;</p><?php */?>
<p>&nbsp;</p>
<h3 class="darkblue-text-17-b" align="center"><?php echo $hesklang['view_ticket']; ?></h3>
<p>&nbsp;</p>

<form action="ticket.html" method="GET">
<div align="center">
<center>
<table class="white" cellspacing="1" cellpadding="8">
<tr>
<td class="white">
<p align="center" class="normal_text"><?php echo $hesklang['ticket_trackID']; ?>: <input type="text" name="track" maxlength="10"
size="12" class="textboxclas"></p>
<p align="center" style="margin-top: 15px;"><input type="hidden" name="Refresh" value="<?php echo rand(10000,99999); ?>">
<input type="image" src="<?=$hesklang['imagepath'];?>View ticket.gif" onmouseout="this.src='<?=$hesklang['imagepath'];?>View ticket.gif'" onmouseover="this.src='<?=$hesklang['imagepath'];?>View ticket-m_o.gif'" value="<?php echo $hesklang['view_ticket']; ?>" class="button"></p>
</td>
</tr>
</table>
</center>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
				</div>
				</div>
				<div style="height: 20px; clear:both">&nbsp;</div>
				</div>
				</div>
	    </div>
		<div class="cleaner"></div>	
	</div>
</div>
</div>
<!-- footer Start -->
<?
	include("footer.php");
?>
<!-- footer End -->

<div class="cleaner"></div>	
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
//require_once('footer.php');
exit();
} // End print_form()

?>
