<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
define('INSTALL',1);
define('HESK_NEW_VERSION','0.94.1');

/* Get all the required files and functions */
require('../hesk_settings.inc.php');
require('../inc/common.inc.php');
hesk_session_start();

/* Check for license agreement */
if (empty($_SESSION['license_agree'])) {
    $agree=hesk_input($_GET['agree']);
    if ($agree == 'YES') {
        $_SESSION['license_agree']=1;
        $_SESSION['step']=1;
    } else {
        $_SESSION['step']=0;
    }
}

if (!isset($_SESSION['step'])) {
    $_SESSION['step']=0;
}

/* Test database connection */
if (isset($_POST['dbtest'])) {
    require('../inc/database.inc.php');
    $db_success = 1;
    $hesk_settings['database_host']=hesk_Input($_POST['host'],"Please enter your MySQL server host name");
    $hesk_settings['database_name']=hesk_Input($_POST['name'],"Please enter your MySQL database name");
    $hesk_settings['database_user']=hesk_Input($_POST['user'],"Please enter your MySQL user name");
    $hesk_settings['database_pass']=hesk_Input($_POST['pass'],"Please enter your MySQL user password");

    /* Connect to database */
    $hesk_db_link = @mysql_connect($hesk_settings['database_host'],$hesk_settings['database_user'], $hesk_settings['database_pass']) or $db_success=0;

    /* Select database works ok? */
    if ($db_success == 1 && !mysql_select_db($hesk_settings['database_name'], $hesk_db_link)) {
        $db_success=2;
    }

    if ($db_success == 2) {
        hesk_iDatabase(2);
        exit();
    } elseif ($db_success == 1) {

        /* Check if these MySQL tables already exist, stop if they do */
        $tables_exist=0;
        $sql='SHOW TABLES FROM `'.$hesk_settings['database_name'].'`';
        $result = hesk_dbQuery($sql);

        while ($row=mysql_fetch_array($result, MYSQL_NUM)) {
            if ($row[0] ==  'hesk_categories' || $row[0] ==  'hesk_replies' || $row[0] ==  'hesk_tickets' || $row[0] ==  'hesk_users') {
                $tables_exist += 1;
            } elseif ($row[0] ==  'hesk_attachments') {
                $tables_exist = 99;
                break;
            } elseif ($row[0] ==  'hesk_std_replies') {
                $hesk_settings['update_from']='094';
            }
        }

        mysql_free_result($result);

        if ($tables_exist == 99) {
            $_SESSION['step']=0;
            $_SESSION['license_agree']=0;
            hesk_iFinish(1);
        } elseif ($tables_exist != 4) {
            $_SESSION['step']=0;
            $_SESSION['license_agree']=0;
            hesk_iFinish(2);
        }

        /* All ok, save settings update database tables */
        hesk_iSaveSettings();
        hesk_iTables();

        /* Close database conenction and move to the next step */
        mysql_close($hesk_db_link);
        $_SESSION['step']=3;
    } else {
        hesk_iDatabase(1);
        exit();
    }

}


switch ($_SESSION['step']) {
case 1:
   hesk_iCheckSetup();
   break;
case 2:
   hesk_iDatabase();
   break;
case 3:
   hesk_iFinish();
   break;
default:
   hesk_iStart();
}

function hesk_iFinish($problem=0) {
    global $hesk_settings;
    hesk_iHeader();
?>

<table border="0">
<tr>
<td><hr width="750"></td>
</tr>
<tr>
<td>UPDATE STEPS:<br>
<font color="#008000">1. License agreement</font> -&gt; <font color="#008000">2. Check setup</font> -&gt; <font color="#008000">3. Database settings</font> -&gt; <b>4. Update database tables</b></td>
</tr>
<tr>
<td><hr width="750"></td>
</tr>
</table>

<h3>Update database tables</h3>

<div align="center"><center>
<table border="0" width="750" cellspacing="1" cellpadding="5" class="white">
<tr>
<td>

<table>
<tr>
<td>-&gt; Testing database connection...</td>
<td><font color="#008000"><b>SUCCESS</b></td>
</tr>
<tr>
<td>-&gt; Update database tables...</td>

<?php
if ($problem==1) {
?>

    <td><font color="#FF0000"><b>ERROR: hesk_attachments table exists</b></td>
    </tr>
    </table>

    <p style="color:#FF0000;">Database table <i>hesk_attachments</i> exists on
    the server, your Hesk had already been updated to version <?php echo HESK_NEW_VERSION; ?>!</p>
    <p align="center"><a href="index.php">Click here to continue</a></p>

<?php
} elseif ($problem==2) {
?>

    <td><font color="#FF0000"><b>ERROR: Old tables not found</b></td>
    </tr>
    </table>

    <p style="color:#FF0000;">Old Hesk database tables not found, unable to update. If you are trying
    to install a new copy of Hesk please run the installation program again and select
    <b>New install</b> from the installation page!</p></p>
    <p align="center"><a href="index.php">Click here to continue</a></p>

<?php
} else {
?>

    <td><font color="#008000"><b>SUCCESS</b></font></td>
    </tr>
    </table>

    <p>Congratulations, you have successfully completed Hesk <?php echo HESK_NEW_VERSION; ?> database update!</p>

    <p style="color:#FF0000"><b>Next steps:</b></p>

    <ol>
    <li><font color="#FF0000"><b>IMPORTANT:</b></font> Before doing anything else please <b>delete</b> the <b>install</b> folder from your server!
    You can leave this browser window open.<br>&nbsp;</li>
    <li>Setup your help desk from the <a href="../admin.php?goto=admin_settings.php">Administration panel</a></li>
    </ol>

    <p>&nbsp;</p>

    <p align="center">For further instructions please see the readme.htm file!</p>

<?php
} // End else
?>

<p>&nbsp;</p>

</td>
</tr>
</table>
</center></div>

<hr width="750">
<?php
    hesk_iFooter();
    exit();
} // End hesk_iFinish()


function hesk_iTables() {
global $hesk_settings;

/* This function updates MySQL tables to current version */
$sql="CREATE TABLE `hesk_attachments` (
  `att_id` mediumint(8) unsigned NOT NULL auto_increment,
  `ticket_id` varchar(10) NOT NULL default '',
  `saved_name` varchar(255) NOT NULL default '',
  `real_name` varchar(255) NOT NULL default '',
  `size` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`att_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=MyISAM";
$result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");

if ($hesk_settings['update_from'] == '094') {

    if ($hesk_settings['attachments']['use']) {

        /* Update attachments for tickets */
        $sql = "SELECT * FROM `hesk_tickets` WHERE `attachments` != ''";
        $result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");
        while ($ticket = hesk_dbFetchAssoc($result))
        {
            $att=explode('#####',substr($ticket['attachments'], 0, -5));
            $myattachments = '';
            foreach ($att as $myatt) {

                $name = substr(strstr($myatt, $ticket['trackid']),16);
                $saved_name = strstr($myatt, $ticket['trackid']);
                $size = filesize($hesk_settings['server_path'].'/attachments/'.$saved_name);

                $sql2 = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$ticket[trackid]', '$saved_name', '$name', '$size')";
                $result2 = hesk_dbQuery($sql2) or hesk_error("$hesklang[cant_sql]: $sql2</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
                $myattachments .= hesk_dbInsertID() . '#' . $name .',';
            }

            $sql2 = "UPDATE `hesk_tickets` SET `attachments` = '$myattachments' WHERE `id` = $ticket[id] LIMIT 1";
            $result2 = hesk_dbQuery($sql2) or hesk_error("$hesklang[cant_sql]: $sql2</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        }

        /* Update attachments for replies */
        $sql = "SELECT * FROM `hesk_replies` WHERE `attachments` != ''";
        $result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");
        while ($ticket = hesk_dbFetchAssoc($result))
        {
            $sql2 = "SELECT `trackid` FROM `hesk_tickets` WHERE `id` = '$ticket[replyto]' LIMIT 1";
            $result2 = hesk_dbQuery($sql2) or hesk_error("Couldn't execute SQL: $sql2. Please make sure you delete any old installations of Hesk before installing this version!");
            $trackingID = hesk_dbResult($result2,0,0);

            $att=explode('#####',substr($ticket['attachments'], 0, -5));
            $myattachments = '';
            foreach ($att as $myatt) {

                $name = substr(strstr($myatt, $trackingID),16);
                $saved_name = strstr($myatt, $trackingID);
                $size = filesize($hesk_settings['server_path'].'/attachments/'.$saved_name);

                $sql2 = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$trackingID', '$saved_name', '$name', '$size')";
                $result2 = hesk_dbQuery($sql2) or hesk_error("$hesklang[cant_sql]: $sql2</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
                $myattachments .= hesk_dbInsertID() . '#' . $name .',';
            }

            $sql2 = "UPDATE `hesk_replies` SET `attachments` = '$myattachments' WHERE `id` = $ticket[id] LIMIT 1";
            $result2 = hesk_dbQuery($sql2) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        }

    }

    return true;
}

$sql="CREATE TABLE `hesk_std_replies` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `title` varchar(70) NOT NULL default '',
  `message` text NOT NULL,
  `reply_order` smallint(5) unsigned NOT NULL default '0',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM";
$result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");

$sql="ALTER TABLE `hesk_categories`
CHANGE `name` `name` varchar(60) NOT NULL default '',
ADD `cat_order` smallint(5) unsigned NOT NULL default '0'";
$result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");

$sql="ALTER TABLE `hesk_replies`
CHANGE `name` `name` varchar(50) NOT NULL default '',
ADD `attachments` TEXT";
$result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");

$sql="ALTER TABLE `hesk_tickets`
CHANGE `name` `name` varchar(50) NOT NULL default '',
CHANGE `category` `category` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '1',
CHANGE `priority` `priority` enum('1','2','3') NOT NULL default '3',
CHANGE `subject` `subject` varchar(70) NOT NULL default '',
ADD `lastchange` datetime NOT NULL default '0000-00-00 00:00:00' AFTER `dt`,
CHANGE `status` `status` enum('0','1','2','3') default '1',
ADD `lastreplier` enum('0','1') NOT NULL default '0',
ADD `archive` enum('0','1') NOT NULL default '0',
ADD `attachments` text,
ADD `custom1` VARCHAR( 255 ) NOT NULL default '',
ADD `custom2` VARCHAR( 255 ) NOT NULL default '',
ADD `custom3` VARCHAR( 255 ) NOT NULL default '',
ADD `custom4` VARCHAR( 255 ) NOT NULL default '',
ADD `custom5` VARCHAR( 255 ) NOT NULL default '',
ADD INDEX `archive` ( `archive` )";
$result = hesk_dbQuery($sql) or hesk_error("Couldn't execute SQL: $sql. Please make sure you delete any old installations of Hesk before installing this version!");

/* Change status of closed tickets to the new "Resolved" status */
$sql="UPDATE `hesk_tickets` SET `status`='3' WHERE `status`='0'";
$result = hesk_dbQuery($sql);

/* Populate lastchange */
$sql="UPDATE `hesk_tickets` SET `lastchange`=`dt`";
$result = hesk_dbQuery($sql);

/* Update categories with order values */
$sql = "SELECT `id` FROM `hesk_categories`";
$result = hesk_dbQuery($sql);
$i = 10;
    while ($mycat=hesk_dbFetchAssoc($result))
    {
        $sql = "UPDATE `hesk_categories` SET `cat_order`=$i WHERE `id`=$mycat[id] LIMIT 1";
        hesk_dbQuery($sql);
        $i += 10;
    }

} // End hesk_iTables()


function hesk_iSaveSettings() {
    global $hesk_settings;

    if (empty($hesk_settings['print_font_size'])) {$hesk_settings['print_font_size']=12;}

    if ($hesk_settings['update_from'] == '094') {
        $settings_file_content=file_get_contents('../hesk_settings.inc.php') or hesk_error($hesklang['err_openset'] . '(r)');
        $settings_file_content=str_replace('0.94',HESK_NEW_VERSION,$settings_file_content);
    } else {

$settings_file_content='<?php
/* Settings file for Hesk '.HESK_NEW_VERSION.' */
/*** Please read the README.HTM file for more information on these settings ***/

$hesk_settings[\'site_title\']=\''.$hesk_settings['site_title'].'\';
$hesk_settings[\'site_url\']=\''.$hesk_settings['site_url'].'\';

/* Contacts */
$hesk_settings[\'support_mail\']=\''.$hesk_settings['support_mail'].'\';
$hesk_settings[\'webmaster_mail\']=\''.$hesk_settings['webmaster_mail'].'\';
$hesk_settings[\'noreply_mail\']=\''.$hesk_settings['noreply_mail'].'\';

/* Help desk settings */
$hesk_settings[\'hesk_url\']=\''.$hesk_settings['hesk_url'].'\';
$hesk_settings[\'hesk_title\']=\''.$hesk_settings['hesk_title'].'\';
$hesk_settings[\'server_path\']=\''.$hesk_settings['server_path'].'\';
$hesk_settings[\'language\']=\'english\';
$hesk_settings[\'max_listings\']='.$hesk_settings['max_listings'].';
$hesk_settings[\'print_font_size\']='.$hesk_settings['print_font_size'].';
$hesk_settings[\'debug_mode\']=0;
$hesk_settings[\'secimg_use\']=1;
$hesk_settings[\'secimg_sum\']=\'H29PEW9SQR\';

/* File attachments */
$hesk_settings[\'attachments\']=array (
    \'use\'           =>  0,
    \'max_number\'    =>  2,
    \'max_size\'      =>  1024, // kb
    \'allowed_types\' =>  array(\'.gif\',\'.jpg\',\'.jpeg\',\'.zip\',\'.rar\',\'.csv\',\'.doc\',\'.txt\',\'.pdf\')
);

/* Custom fields */
$hesk_settings[\'use_custom\']=0;
$hesk_settings[\'custom_place\']=0;
$hesk_settings[\'custom_fields\']=array (
    \'custom1\'  => array(\'use\'=>0,\'req\'=>0,\'name\'=>\'Custom field 1\',\'maxlen\'=>255),
    \'custom2\'  => array(\'use\'=>0,\'req\'=>0,\'name\'=>\'Custom field 2\',\'maxlen\'=>255),
    \'custom3\'  => array(\'use\'=>0,\'req\'=>0,\'name\'=>\'Custom field 3\',\'maxlen\'=>255),
    \'custom4\'  => array(\'use\'=>0,\'req\'=>0,\'name\'=>\'Custom field 4\',\'maxlen\'=>255),
    \'custom5\'  => array(\'use\'=>0,\'req\'=>0,\'name\'=>\'Custom field 5\',\'maxlen\'=>255)
);

/* Database settings */
$hesk_settings[\'database_host\']=\'' . $hesk_settings['database_host'] . '\';
$hesk_settings[\'database_name\']=\'' . $hesk_settings['database_name'] . '\';
$hesk_settings[\'database_user\']=\'' . $hesk_settings['database_user'] . '\';
$hesk_settings[\'database_pass\']=\'' . $hesk_settings['database_pass'] . '\';
#############################
#     DO NOT EDIT BELOW     #
#############################
$hesk_settings[\'hesk_version\']=\''.HESK_NEW_VERSION.'\';
if ($hesk_settings[\'debug_mode\']) {
    error_reporting(E_ALL ^ E_NOTICE);
} else {
    ini_set(\'display_errors\', 0);
    ini_set(\'log_errors\', 1);
}
if (!defined(\'IN_SCRIPT\')) {die(\'Invalid attempt!\');}
if (is_dir(\'install\') && !defined(\'INSTALL\')) {die(\'Please delete the <b>install</b> folder from your server for security reasons then refresh this page!\');}
?>';

    } // End else update_from

$fp=fopen('../hesk_settings.inc.php','w') or hesk_error($hesklang['err_openset'] . '(w)');
fputs($fp,$settings_file_content);
fclose($fp);

return true;
} // End hesk_iSaveSettings()


function hesk_iDatabase($problem=0) {
    global $hesk_settings;
    hesk_iHeader();
?>

<table border="0">
<tr>
<td><hr width="750"></td>
</tr>
<tr>
<td>UPDATE STEPS:<br>
<font color="#008000">1. License agreement</font> -&gt; <font color="#008000">2. Check setup</font> -&gt; <b>3. Database settings</b> -&gt; 4. Update database tables</td>
</tr>
<tr>
<td><hr width="750"></td>
</tr>
</table>

<h3>Database settings</h3>

<div align="center"><center>
<table border="0" width="750" cellspacing="1" cellpadding="5" class="white">
<tr>
<td>
<p><b>Hesk will not work unless the information below is correct and database connection
test is successful. For correct database information contact your hosting company,
I cannot help you find this information!</b></p>

<p><b>Make sure your database settings below are the same as with your old Hesk database!</b></p>

<?php
if ($problem==1) {
    echo '<p style="color:#FF0000;"><b>Database connection failed!</b><br>Double-check all the information below. If not sure contact your hosting company for the correct information!</p>';
} elseif ($problem==2) {
    echo '<p style="color:#FF0000;"><b>Database connection failed!</b><br>Double-check <b>database name</b> and make sure the user has access to the database. If not sure contact your hosting company for the correct information!</p>';
}
?>

<form action="update.php" method="POST">
<table>
<tr>
<td>Database Host:</td>
<td><input type="text" name="host" value="<?php echo $hesk_settings['database_host']; ?>" size="40"></td>
</tr>
<tr>
<td>Database Name:</td>
<td><input type="text" name="name" value="<?php echo $hesk_settings['database_name']; ?>" size="40"></td>
</tr>
<tr>
<td>Database User (login):</td>
<td><input type="text" name="user" value="<?php echo $hesk_settings['database_user']; ?>" size="40"></td>
</tr>
<tr>
<td>User Password:</td>
<td><input type="text" name="pass" value="<?php echo $hesk_settings['database_pass']; ?>" size="40"></td>
</tr>
</table>

<p align="center"><input type="hidden" name="dbtest" value="1"><input type="submit" value="Continue to Step 4"></p>
</form>

</td>
</tr>
</table>
</center></div>

<hr width="750">
<?php
    hesk_iFooter();
} // End hesk_iDatabase()




function hesk_iCheckSetup() {
    global $hesk_settings;
    hesk_iHeader();
    $_SESSION['all_passed']=1;
    $correct_this=array();
?>

<table border="0">
<tr>
<td><hr width="750"></td>
</tr>
<tr>
<td>UPDATE STEPS:<br>
<font color="#008000">1. License agreement</font> -&gt; <b>2. Check setup</b> -&gt; 3. Database settings -&gt; 4. Update database tables</td>
</tr>
<tr>
<td><hr width="750"></td>
</tr>
</table>

<h3>Check setup</h3>

<p>Checking wether your server meets all requirements and that files are setup correctly</p>

<div align="center"><center>
<table border="0" width="750" cellspacing="1" cellpadding="3" class="white">
<tr>
<td class="admin_white"><b>Required</b></td>
<td class="admin_white"><b>Your setting</b></td>
<td class="admin_white"><b>Status</b></td>
</tr>

<tr>
<td class="admin_gray"><b>PHP version</b><br>Should be at least PHP 4 >= 4.3.2</td>
<td class="admin_gray" valign="center" nowrap><b><?php echo PHP_VERSION; ?></b></td>
<td class="admin_gray" valign="center">
<?php
if (function_exists('version_compare') && version_compare(PHP_VERSION,'4.3.2','>=')) {
    echo '<font color="#008000"><b>Passed</b></font>';
} else {
    $_SESSION['all_passed']=0;
    echo '<font color="#FF0000"><b>Failed</b></font>';
    $correct_this[]='You are using an old and non-secure version of PHP, ask your host to update your PHP version!';
}
?>
</td>
</tr>

<tr>
<td class="admin_white"><b>hesk_settings.inc.php file</b><br>Must be uploaded and writable by the script</td>
<td class="admin_white" valign="center" nowrap>
<?php
$mypassed=1;
if (file_exists('../hesk_settings.inc.php')) {
    echo '<b><font color="#008000">Exists</font>, ';
    if (is__writable('../hesk_settings.inc.php')) {
        echo '<font color="#008000">Writable</font></b>';
    } else {
        echo '<font color="#FF0000">Not writable</font></b>';
        $mypassed=2;
    }
} else {
    $mypassed=0;
    echo '<b><font color="#FF0000">Not uploaded</font>, <font color="#FF0000">Not writable</font></b>';
}
?>
</td>
<td class="admin_white" valign="center">
<?php
if ($mypassed==1) {
    echo '<font color="#008000"><b>Passed</b></font>';
} elseif ($mypassed==2) {
    $_SESSION['all_passed']=0;
    echo '<font color="#FF0000"><b>Failed</b></font>';
    $correct_this[]='Make sure the <b>hesk_settings.inc.php</b> file is writable: on Linux chmod it to 666 or rw-rw-rw-, on Windows (IIS) make sure IUSR account has modify/read/write permissions';
} else {
    $_SESSION['all_passed']=0;
    echo '<font color="#FF0000"><b>Failed</b></font>';
    $correct_this[]='Upload the <b>hesk_settings.inc.php</b> file to the server and make sure it\'s writable!';
}
?>
</td>
</tr>

<tr>
<td class="admin_gray"><b>attachments directory</b><br>Must exist and be writable by the script</td>
<td class="admin_gray" valign="center" nowrap>
<?php
$mypassed=1;

if (!file_exists('../attachments'))
{
    @mkdir('../attachments', 0777);
}

if (is_dir('../attachments')) {
    echo '<b><font color="#008000">Exists</font>, ';
    if (is__writable('../attachments/')) {
        echo '<font color="#008000">Writable</font></b>';
    } else {
        echo '<font color="#FF0000">Not writable</font></b>';
        $mypassed=2;
    }
} else {
    $mypassed=0;
    echo '<b><font color="#FF0000">Not uploaded</font>, <font color="#FF0000">Not writable</font></b>';
}
?>
</td>
<td class="admin_gray" valign="center">
<?php
if ($mypassed==1) {
    echo '<font color="#008000"><b>Passed</b></font>';
} elseif ($mypassed==2) {
    $_SESSION['all_passed']=0;
    echo '<font color="#FF0000"><b>Failed</b></font>';
    $correct_this[]='Make sure the <b>attachments</b> directory is writable: on Linux chmod it to 777 or rwxrwxrwx, on Windows (IIS) make sure IUSR account has modify/read/write permissions';
} else {
    $_SESSION['all_passed']=0;
    echo '<font color="#FF0000"><b>Failed</b></font>';
    $correct_this[]='Within hesk folder create a new one called <b>attachments</b> and make sure it is writable: on Linux chmod it to 777 or rwxrwxrwx, on Windows (IIS) make sure IUSR account has modify/read/write permissions';
}
?>
</td>

<tr>
<td class="admin_white"><b>File uploads</b><br>To use file attachments <i>file_uploads</i> must be enabled in PHP</td>
<td class="admin_white" valign="center" nowrap>
<?php
$mypassed=1;
$can_use_attachments=1;
if (ini_get('file_uploads')) {
    echo '<b><font color="#008000">Enabled</font>';
} else {
    $mypassed=0;
    $can_use_attachments=0;
    echo '<b><font color="#FFA500">Disabled</font></b>';
}
?>
</td>
<td class="admin_white" valign="center">
<?php
if ($mypassed==1) {
    echo '<font color="#008000"><b>Passed</b></font>';
} else {
    echo '<font color="#FFA500"><b>Unavailable*</b></font>';
}
?>
</td>
</tr>

<tr>
<td class="admin_white"><b>ZLib Support</b><br>PHP must be compiled with ZLib support</td>
<td class="admin_white" valign="center" nowrap>
<?php
$mypassed=1;
if (function_exists('gzdeflate')) {
    echo '<b><font color="#008000">Enabled</font>';
} else {
    $mypassed=0;
    $_SESSION['all_passed']=0;
    echo '<font color="#FF0000"><b>Disabled</b></font>';
    $correct_this[]='PHP needs to be compiled with ZLib support enabled (--with-zlib[=DIR]) in order for Hesk to work correctly. Contact your host and ask them to enable ZLib support for PHP.';
}
?>
</td>
<td class="admin_white" valign="center">
<?php
if ($mypassed==1) {
    echo '<font color="#008000"><b>Passed</b></font>';
} else {
    echo '<font color="#FF0000"><b>Failed</b></font>';
}
?>
</td>
</tr>

<tr>
<td class="admin_gray"><b>Old install.php deleted</b><br>The old install.php file should be deleted</td>
<?php
if (file_exists('../install.php')) {
    $_SESSION['all_passed']=0;
    echo '
<td class="admin_gray" valign="center" nowrap><font color="#FF0000"><b>Not deleted</b></font></td>
<td class="admin_gray" valign="center"><font color="#FF0000"><b>Failed</b></font>
    ';
    $correct_this[]='Delete the old <b>install.php</b> file from your main Hesk folder!';

} else {
    echo '
<td class="admin_gray" valign="center" nowrap><font color="#008000"><b>Deleted</b></font></td>
<td class="admin_gray" valign="center"><font color="#008000"><b>Passed</b></font>
';
}
?>
</td>
</tr>

</table>
</center></div>

<?php
if ($can_use_attachments==0) {
    echo '<p><font color="#FFA500"><b>*</b></font> Hesk will still work if all other tests are successful, but File attachments won\'t work.</p>';
}
?>

<p>&nbsp;</p>

<?php
if (!empty($correct_this)) {
?>
<div align="center"><center>
<table border="0" width="750" cellspacing="1" cellpadding="3">
<tr>
<td>
<p><font color="#FF0000"><b>You will not be able to continue update until the required tests are passed. Things you need to correct before continuing update:</b></font></p>
<ol>
<?php
foreach($correct_this as $mythis) {
    echo "<li><p>$mythis</p></li>";
}
?>
</ol>
<form method="POST" action="update.php">
<p align="center">&nbsp;<br><input type="submit" value="Test again"></p>
</form>
</td>
</tr>
</table>
</center></div>
<?php
} else {
$_SESSION['step']=2;
?>
<form method="POST" action="update.php">
<div align="center"><center>
<table border="0">
<tr>
<td>
<p align="center"><font color="#008000"><b>All required tests passed, you may now continue to database setup</b></font></p>
<p align="center"><input type="submit" value="Continue to Step 3"></p>
</td>
</tr>
</table>
</center></div>
</form>
<?php
}
?>

<hr width="750">
<?php
    hesk_iFooter();
} // End hesk_iCheckSetup()



function hesk_iStart() {
    global $hesk_settings;
    hesk_iHeader();
?>

<table border="0">
<tr>
<td><hr width="750"></td>
</tr>
<tr>
<td>UPDATE STEPS:<br>
<b>1. License agreement</b> -&gt; 2. Check setup -&gt; 3. Database settings -&gt; 4. Update database tables</td>
</tr>
<tr>
<td><hr width="750"></td>
</tr>
</table>

<h3>License agreement</h3>

<p><b>Summary:</b></p>

<ul>
<li>The script is provided "as is", without any warranty. Use at your own risk.</li>
<li>You are not allowed to redistribute this script or any software based on this script over the Internet or in any other medium without express written permission</li>
<li>You mustn't edit or remove &quot;Powered by&quot; links without purchasing a <a href="http://www.phpjunkyard.com/copyright-removal.php" target="_blank">Copyright removal license</a></li>
</ul>

<p><b>The entire License agreement:</b></p>

<p align="center"><textarea rows="15" cols="70">
LICENSE AGREEMENT

The &quot;script&quot; is all files included with the PHPJunkyard distribution archive as well as all files produced as a result of the installation scripts. Klemen Stirn (&quot;Author&quot;,&quot;PHPJunkyard&quot;) is the author and sole copyrgihts owner of the script. The &quot;Licensee&quot; (&quot;you&quot;) is the person downloading or using the Licensed version of script. &quot;User&quot; is any person using or viewing the script with their HTML browser.

&quot;Powered by&quot; link is herein defined as an anchor link pointing to PHPJunkyard website and/or script webpage, usually located at the bottom of the script and visible to users of the script without looking into source code.

&quot;Copyright headers&quot; is a written copyright notice located in script source code and normally not visible to users.

This License may be modified by the Author at any time. The new version of the License becomes valid when published on PHPJunkyard website. You are encouraged to regularly check back for License updates.

THIS SCRIPT IS PROVIDED &quot;AS IS&quot; AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLEMEN STIRN BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SCRIPT, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

&gt;&gt; REMOVING POWERED BY LINKS
You are not allowed to remove or in any way edit the &quot;Powered by&quot; links in this script without purchasing a Copyright removal license (&quot;CRL&quot;). You can purchase a CRL at
http://www.phpjunkyard.com/copyright-removal.php

If you remove copyrights without purchasing this license and paying the licensee fee, you are in a direct violation of European Union and International copyright laws. Your Licence to use the scripts is immediately terminated and you must delete all copies of the entire program from your web server. Klemen Stirn may, at any time, terminate this License agreement if Klemen Stirn determines, that this License agreement has been breached.

Under no circumstance is the removal of copyright headers from the script source code permitted.

This License Agreement is governed by the laws of Slovenia, European Union. Both the Licensee and Klemen Stirn submit to the jurisdiction of the courts of Slovenia, European Union. Both the Licensee and Klemen Stirn agree to commence any litigation that may arise hereunder in the courts located in Slovenia.

If any provision hereof shall be held illegal, invalid or unenforceable, in whole or in part, such provision shall be modified to the minimum extent necessary to make it legal, valid and enforceable, and the legality, validity and enforceability of all other provisions of this Agreement shall not be affected thereby. No delay or failure by either party to exercise or enforce at any time any right or provision hereof shall be considered a waiver thereof or of such party's right thereafter to exercise or enforce each and every right and provision of this Agreement.
</textarea></p>

<hr width="750">

<script language="javascript" type="text/javascript"><!--
function hesk_checkAgree() {
    if (document.license.agree[0].checked) {
        return true;
    } else {
        alert('You must agree to the License agreement in order to use Hesk!');
        return false;
    }
}
//-->
</script>

<form method="GET" action="update.php" name="license" onSubmit="return hesk_checkAgree()">
<div align="center"><center>
<table border="0">
<tr>
<td>
<p><b>Do you agree to the License agreement and all the terms incorporated therein?</b> <font color="#FF0000"><i>(required)</i></font><br>
<label><input type="radio" name="agree" value="YES"> YES</label><br>
<label><input type="radio" name="agree" value="NO"> NO</label>
<p align="center"><input type="submit" value="Continue to Step 2"></p>
</td>
</tr>
</table>
</center></div>
</form>

<hr width="750">
<?php
    hesk_iFooter();
} // End hesk_iStart()


function hesk_iHeader() {
    global $hesk_settings;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Update Hesk to version <?php echo HESK_NEW_VERSION; ?></title>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<link href="../hesk_style.css" type="text/css" rel="stylesheet">
<script language="Javascript" type="text/javascript" src="hesk_javascript.js"><!--
// -->
</script>
</head>
<body marginwidth="0" leftmargin="0">


<div align="center">
<center>
<table border="0" cellspacing="0" cellpadding="5" width="770" class="enclosing">
<tr>
<td>
<div align="center">
<center>
<table border="0" width="750" cellspacing="1" cellpadding="3" class="white">
<tr>
<td align="center" class="admin_white">
Hesk update to <?php echo HESK_NEW_VERSION; ?>
</td>
</tr>
</table>
</center>
</div>
</td>
</tr>
<tr>
<td>
<?php
} // End hesk_iHeader()


function hesk_iFooter() {
    global $hesk_settings;
?>
<p>&nbsp;</p>

</td>
</tr>
</table>
</center>
</div>

<p align="center"><font class="smaller">Powered by <a href="http://www.phpjunkyard.com/free-helpdesk-software.php" class="smaller" target="_blank">Help desk software Hesk</a> <?php echo HESK_NEW_VERSION; ?></font></p>

</body>
</html>
<?php
} // End hesk_iFooter()


/*
This function is from http://www.php.net/is_writable
and is a work-around for IIS bug which returns files as
writable by PHP when in fact they are not.
*/
function is__writable($path) {
//will work in despite of Windows ACLs bug
//NOTE: use a trailing slash for folders!!!
//see http://bugs.php.net/bug.php?id=27609
//see http://bugs.php.net/bug.php?id=30931

    if ($path{strlen($path)-1}=='/') // recursively return a temporary file path
        return is__writable($path.uniqid(mt_rand()).'.tmp');
    else if (is_dir($path))
        return is__writable($path.'/'.uniqid(mt_rand()).'.tmp');
    // check tmp file for read/write capabilities
    $rm = file_exists($path);
    $f = @fopen($path, 'a');
    if ($f===false)
        return false;
    fclose($f);
    if (!$rm)
        unlink($path);
    return true;
}
?>
