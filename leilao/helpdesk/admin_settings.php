<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect();

hesk_session_start();
hesk_isLoggedIn();
/* Must be administrator to access this page */
hesk_isAdmin();

$enable_save_settings=0;
$enable_use_attachments=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1>Settings</TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>

<?
/* Print header */
//require_once('inc/header.inc.php');

/* Print main manage users page */
//require_once('inc/show_admin_nav.inc.php');
?>

<tr>
<td>

<p><?php echo $hesklang['settings_intro'] . ' <b>' . $hesklang['all_req']; ?></b></p>

<script language="javascript" type="text/javascript"><!--
function hesk_checkFields() {
d=document.form1;
if (d.s_site_title.value=='') {alert('<?php echo $hesklang['err_sname']; ?>'); return false;}
if (d.s_site_url.value=='') {alert('<?php echo $hesklang['err_surl']; ?>'); return false;}

if (d.s_support_mail.value=='' || d.s_support_mail.value.indexOf(".") == -1 || d.s_support_mail.value.indexOf("@") == -1)
{alert('<?php echo $hesklang['err_supmail']; ?>'); return false;}
if (d.s_webmaster_mail.value=='' || d.s_webmaster_mail.value.indexOf(".") == -1 || d.s_webmaster_mail.value.indexOf("@") == -1)
{alert('<?php echo $hesklang['err_wmmail']; ?>'); return false;}
if (d.s_noreply_mail.value=='' || d.s_noreply_mail.value.indexOf(".") == -1 || d.s_noreply_mail.value.indexOf("@") == -1)
{alert('<?php echo $hesklang['err_nomail']; ?>'); return false;}

if (d.s_hesk_title.value=='') {alert('<?php echo $hesklang['err_htitle']; ?>'); return false;}
if (d.s_hesk_url.value=='') {alert('<?php echo $hesklang['err_hurl']; ?>'); return false;}
if (d.s_server_path.value=='') {alert('<?php echo $hesklang['err_spath']; ?>'); return false;}
if (d.s_max_listings.value=='') {alert('<?php echo $hesklang['err_max']; ?>'); return false;}
if (d.s_print_font_size.value=='') {alert('<?php echo $hesklang['err_psize']; ?>'); return false;}

if (d.s_database_host.value=='') {alert('<?php echo $hesklang['err_dbhost']; ?>'); return false;}
if (d.s_database_name.value=='') {alert('<?php echo $hesklang['err_dbname']; ?>'); return false;}
if (d.s_database_user.value=='') {alert('<?php echo $hesklang['err_dbuser']; ?>'); return false;}
if (d.s_database_pass.value=='') {alert('<?php echo $hesklang['err_dbpass']; ?>'); return false;}

if (d.s_use_custom[1].checked) {
    if (d.s_custom1_use.checked && d.s_custom1_name.value == '') {alert('<?php echo $hesklang['err_custname']; ?>'); return false;}
    if (d.s_custom2_use.checked && d.s_custom2_name.value == '') {alert('<?php echo $hesklang['err_custname']; ?>'); return false;}
    if (d.s_custom3_use.checked && d.s_custom3_name.value == '') {alert('<?php echo $hesklang['err_custname']; ?>'); return false;}
    if (d.s_custom4_use.checked && d.s_custom4_name.value == '') {alert('<?php echo $hesklang['err_custname']; ?>'); return false;}
    if (d.s_custom5_use.checked && d.s_custom5_name.value == '') {alert('<?php echo $hesklang['err_custname']; ?>'); return false;}
}

return true;
}
//-->
</script>

<form method="POST" action="admin_settings_save.php" name="form1" onSubmit="return hesk_checkFields()">

<table border="0" cellspacing="0" cellpadding="5" size="750">
<tr>
<td>

<!-- Check file and folder permissions -->
<table border="0">
<tr>
<td width="750"><?php echo $hesklang['check_status']; ?></tr>
</table>
<table border="0">
<tr>
<td align="left" width="200" valign="top"><b>/hesk_settings.inc.php</b></td>
<td align="left" width="550">
<?php
if (is_writable('hesk_settings.inc.php')) {
    $enable_save_settings=1;
    echo '<font class="success">'.$hesklang['exists'].'</font>, <font class="success">'.$hesklang['writable'].'</font>';
} else {
    echo '<font class="success">'.$hesklang['exists'].'</font>, <font class="error">'.$hesklang['not_writable'].'</font><br>'.$hesklang['e_settings'];
}
?>
</td>
</tr>
<tr>
<td align="left" width="200"><b>/attachments</b></td>
<td align="left" width="550">
<?php
if (!file_exists('attachments'))
{
    @mkdir('attachments', 0777);
}

if (is_dir('attachments')) {
    echo '<font class="success">'.$hesklang['exists'].'</font>, ';
    if (is_writable('attachments')) {
        $enable_use_attachments=1;
        echo '<font class="success">'.$hesklang['writable'].'</font>';
    } else {
        echo '<font class="error">'.$hesklang['not_writable'].'</font><br>'.$hesklang['e_attdir'];
    }
} else {
    echo '<font class="error">'.$hesklang['no_exists'].'</font>, <font class="error">'.$hesklang['not_writable'].'</font><br>'.$hesklang['e_attdir'];
}
?>
</td>
</tr>
</table>

<hr width="750">

<!-- Website info -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['wbst_title']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#1','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_site_title" size="40" maxlength="255" value="<?php echo $hesk_settings['site_title']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['wbst_url']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#2','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_site_url" size="40" maxlength="255" value="<?php echo $hesk_settings['site_url']; ?>"></td>
</tr>
</table>

<hr width="750">

<!-- E-mails -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['email_sup']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#3','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_support_mail" size="40" maxlength="255" value="<?php echo $hesk_settings['support_mail']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['email_wm']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#4','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_webmaster_mail" size="40" maxlength="255" value="<?php echo $hesk_settings['webmaster_mail']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['email_noreply']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#5','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_noreply_mail" size="40" maxlength="255" value="<?php echo $hesk_settings['noreply_mail']; ?>"></td>
</tr>
</table>

<hr width="750">

<!-- Helpdesk settings -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['hesk_title']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#6','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_hesk_title" size="40" maxlength="255" value="<?php echo $hesk_settings['hesk_title']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['hesk_url']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#7','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_hesk_url" size="40" maxlength="255" value="<?php echo $hesk_settings['hesk_url']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['hesk_path']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#8','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_server_path" size="40" maxlength="255" value="<?php
if ($hesk_settings['server_path'] == '/home/mysite/public_html/hesk') {
    echo getcwd();
} else {
    echo $hesk_settings['server_path'];
}
?>">
</td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['hesk_lang']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#9','400','500')"><b>?</b></a>]</td>
<td align="left" width="550">
<select name="s_language">
<?php
$dir = getcwd().'/language';
$path = opendir($dir);
$files = array();

while (false !== ($file = readdir($path))) {
    if(is_file($dir.'/'.$file) && substr($file, -8) == '.inc.php') {
        $files[]=$file;
    }
}

if(!empty($files)) {
    natcasesort($files);
    foreach ($files as $file) {
        $file=substr($file, 0, -8);
        if ($file == $hesk_settings['language']) {
            echo '<option value="'.$file.'" selected>'.ucfirst($file).'</option>';
        } else {
            echo '<option value="'.$file.'">'.ucfirst($file).'</option>';
        }
    }
}

closedir($path);
?>
</select>
</td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['max_listings']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#10','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_max_listings" size="5" maxlength="3" value="<?php echo $hesk_settings['max_listings']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['print_size']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#11','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_print_font_size" size="5" maxlength="3" value="<?php echo $hesk_settings['print_font_size']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['debug_mode']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#12','400','500')"><b>?</b></a>]</td>
<td align="left" width="550">
<?php
    $on = $hesk_settings['debug_mode'] ? 'checked' : '';
    $off = $hesk_settings['debug_mode'] ? '' : 'checked';
    echo '
    <label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_debug_mode" value="0" '.$off.'> '.$hesklang['off'].'</label> |
    <label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_debug_mode" value="1" '.$on.'> '.$hesklang['on'].'</label>';
?>
</td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['use_secimg']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#23','400','500')"><b>?</b></a>]</td>
<td align="left" width="550">
<?php
if(function_exists('imagecreate')) {
    $on = $hesk_settings['secimg_use'] ? 'checked' : '';
    $off = $hesk_settings['secimg_use'] ? '' : 'checked';
    echo '
    <label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_secimg_use" value="0" '.$off.'> '.$hesklang['off'].'</label> |
    <label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_secimg_use" value="1" '.$on.'> '.$hesklang['on'].'</label>';
}
else {
    echo $hesklang['secimg_no'];
}
?>
</td>
</tr>
</table>

<hr width="750">

<!-- Database settings -->
<?php /*?><table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['db_host']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#13','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_database_host" size="30" maxlength="255" value="<?php echo $hesk_settings['database_host']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['db_name']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#14','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_database_name" size="30" maxlength="255" value="<?php echo $hesk_settings['database_name']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['db_user']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#15','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_database_user" size="30" maxlength="255" value="<?php echo $hesk_settings['database_user']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['db_pass']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#16','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_database_pass" size="30" maxlength="255" value="<?php echo $hesk_settings['database_pass']; ?>"></td>
</tr>
</table>

<hr width="750">
<?php */?>
<input type="hidden" name="s_database_host" size="30" maxlength="255" value="<?php echo $hesk_settings['database_host']; ?>">
<input type="hidden" name="s_database_name" size="30" maxlength="255" value="<?php echo $hesk_settings['database_name']; ?>">
<input type="hidden" name="s_database_user" size="30" maxlength="255" value="<?php echo $hesk_settings['database_user']; ?>">
<input type="hidden" name="s_database_pass" size="30" maxlength="255" value="<?php echo $hesk_settings['database_pass']; ?>">
<!-- Attachments -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['attach_use']; $onload_status=''; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#17','400','500')"><b>?</b></a>]</td>
<td align="left" width="550">

<?php
if ($enable_use_attachments) {
?>
    <label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_attach_use" value="0" onClick="hesk_attach_disable(new Array('a1','a2','a3'))" <?php if(!$hesk_settings['attachments']['use']) {echo ' checked '; $onload_status=' disabled ';} ?>>
    <?php echo $hesklang['no']; ?></label> |
    <label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_attach_use" value="1" onClick="hesk_attach_enable(new Array('a1','a2','a3'))" <?php if($hesk_settings['attachments']['use']) {echo ' checked ';} ?>>
    <?php echo $hesklang['yes'];
} else {
    $onload_status=' disabled ';
    echo '<input type="hidden" name="s_attach_use" value="0"><font class="notice">'.$hesklang['e_attach'].'</font>';
}
?>
</label>
</td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['attach_num']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#18','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_max_number" size="5" maxlength="2" id="a1" value="<?php echo $hesk_settings['attachments']['max_number']; ?>" <?php echo $onload_status; ?>></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['attach_size']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#19','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_max_size" size="5" maxlength="5" id="a2" value="<?php echo $hesk_settings['attachments']['max_size']; ?>" <?php echo $onload_status; ?>></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['attach_type']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#20','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><input type="text" name="s_allowed_types" size="40" maxlength="255" id="a3" value="<?php echo implode(',',$hesk_settings['attachments']['allowed_types']); ?>" <?php echo $onload_status; ?>></td>
</tr>
</table>

<hr width="750">

<!-- Custom fields -->
<?php /*?><table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['custom_use']; $onload_status=''; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#21','400','500')"><b>?</b></a>]</td>
<td align="left" width="550"><label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_use_custom" value="0" <?php if(!$hesk_settings['use_custom']) {echo ' checked '; $onload_status=' disabled ';} ?>
onClick="hesk_attach_disable(new Array('cpos1','cpos2','c11','c12','c13','c14','c21','c22','c23','c24','c31','c32','c33','c34','c41','c42','c43','c44','c51','c52','c53','c54'))">
<?php echo $hesklang['no']; ?></label> |
<label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_use_custom" value="1" onClick="hesk_attach_enable(new Array('cpos1','cpos2','c11','c21','c31','c41','c51'))" <?php if($hesk_settings['use_custom']) {echo ' checked ';} ?>>
<?php echo $hesklang['yes']; ?></label></td>
</tr>

<tr>
<td align="right" width="200"><?php echo $hesklang['custom_place']; ?>: [<a href="Javascript:void(0)" onclick="Javascript:hesk_help('settings.html#22','400','500')"><b>?</b></a>]</td>
<td align="left" width="550">
<label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_custom_place" id="cpos1" value="0" <?php if($hesk_settings['custom_place']==0) {echo ' checked ';} echo $onload_status; ?>>
<?php echo $hesklang['place_after']; ?></label> |
<label><input style="background-color:#FFFFFF; border:none;" type="radio" name="s_custom_place" id="cpos2" value="1" <?php if($hesk_settings['custom_place']==1) {echo ' checked ';} echo $onload_status; ?>>
<?php echo $hesklang['place_before'];?></label>
</td>
</tr>

<?php
for ($i=1;$i<=5;$i++) {
    $this_field='custom' . $i;

    if ($onload_status==' disabled ') {
        $onload_locally=' disabled ';
    } elseif ($hesk_settings['custom_fields'][$this_field]['use']) {
        $onload_locally='';
    } else {
        $onload_locally=' disabled ';
    }

    if ($i==2 || $i==4) {
        $color='';
    } else {
        $color=' class="white" ';
    }

echo '
<tr>
<td align="right" width="200" valign="top" '.$color.'>'.$hesklang['custom_f'].' '.$i.':  [<a href="Javascript:void(0)" onclick="Javascript:hesk_help(\'settings.html#24\',\'400\',\'500\')"><b>?</b></a>]</td>
<td align="left" width="550" '.$color.'>
        <label><input style="background-color:#FFFFFF; border:none;" type="checkbox" name="s_custom'.$i.'_use" value="1" id="c'.$i.'1" '; if ($hesk_settings['custom_fields'][$this_field]['use']) {echo 'checked';} echo $onload_status.' onClick="hesk_attach_toggle(\'c'.$i.'1\',new Array(\'c'.$i.'2\',\'c'.$i.'3\',\'c'.$i.'4\'))"> '.$hesklang['custom_u'].'</label><br>
        <label><input style="background-color:#FFFFFF; border:none;" type="checkbox" name="s_custom'.$i.'_req" value="1" id="c'.$i.'2" '; if ($hesk_settings['custom_fields'][$this_field]['req']) {echo 'checked';} echo $onload_locally.'> '.$hesklang['custom_r'].'</label><br>
        '.$hesklang['custom_n'].': <input type="text" name="s_custom'.$i.'_name" size="30" maxlength="255" id="c'.$i.'3" value="'.$hesk_settings['custom_fields'][$this_field]['name'].'"'.$onload_locally.'><br>
        '.$hesklang['custom_l'].': <input type="text" name="s_custom'.$i.'_maxlen" size="5" maxlength="5" id="c'.$i.'4" value="'.$hesk_settings['custom_fields'][$this_field]['maxlen'].'"'.$onload_locally.'><br>
</td>
</tr>
';
} // End FOR
?>

</table>
<?php */?>
<p>&nbsp;</p>

<p align="center">
<?php
if ($enable_save_settings) {
    echo '<input type="submit" value="'.$hesklang['save_changes'].'" class="button">';
} else {
    echo '<input type="submit" value="'.$hesklang['save_changes'].' ('.$hesklang['disabled'].')" class="button" disabled><br><font class="error">'.$hesklang['e_save_settings'].'</font>';
}
?></p>



</td>
</tr>
</table>
</form>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
?>
