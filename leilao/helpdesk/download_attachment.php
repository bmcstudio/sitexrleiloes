<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');

$att_id=hesk_isNumber($_GET['att_id'],$hesklang['id_not_valid']);
$tic_id=hesk_input($_GET['track'],$hesklang['no_trackID']);

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

/* Get attachment info */
$sql = "SELECT * FROM `hesk_attachments` WHERE `att_id`=$att_id LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbNumRows($result) != 1) {hesk_error($hesklang['id_not_valid'].' (att_id)');}
$file = hesk_dbFetchAssoc($result);

/* Is ticket ID valid for this attachment? */
if ($file['ticket_id'] != $tic_id)
{
    hesk_error($hesklang['trackID_not_found']);
}

/* Send the file as an attachment to prevent malicious code from executing */
header ("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Length: ' . $file['size']);
header('Content-Disposition: attachment; filename=' . $file['real_name']);
readfile($hesk_settings['server_path'].'/attachments/'.$file['saved_name']);
?>
