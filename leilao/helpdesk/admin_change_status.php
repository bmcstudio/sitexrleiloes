<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/admin_common.inc.php');
hesk_session_start();
hesk_isLoggedIn();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1>Admin Change Status</TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>

<?
/* Print header */
//require_once('inc/header.inc.php');

$trackingID=strtoupper(hesk_input($_GET['track'],"$hesklang[int_error]: $hesklang[no_trackID]."));
$status=hesk_isNumber($_GET['s'],"$hesklang[int_error]: $hesklang[status_not_valid].");

if ($status==3) {$action=$hesklang['closed'];}
else {$action=$hesklang['opened'];}

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

$sql = "UPDATE `hesk_tickets` SET `status`='$status' WHERE `trackid`='$trackingID' LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[trackID_not_found].");}

$admin_trackingURL=$hesk_settings['hesk_url'].'/admin_ticket.php?track='.$trackingID.'&Refresh='.rand(10000,99999);

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');
?>

<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['ticket'].' '.$action; ?></h3>

<p>&nbsp;</p>

<p align="center"> <?php echo $hesklang['ticket_been'].' '.$action; ?>!</p>

<p align="center"><a href="<?php echo $admin_trackingURL; ?>"><?php echo $hesklang['view_ticket']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();

?>
