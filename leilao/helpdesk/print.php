<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
hesk_session_start();

$trackingID=strtoupper(hesk_input($_GET['track'],$lng_helptrackingnotfound));

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$lng_cant_connect_db $lng_contact_webmsater $hesk_settings[webmaster_mail]!");

/* Get ticket info */
$sql = "SELECT * FROM `hesk_tickets` WHERE `trackid`='$trackingID' LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$lng_cant_sql: $sql</p><p>$lng_mysql_said:<br>".mysql_error()."</p><p>$lng_contact_webmsater $hesk_settings[webmaster_mail]");
if (hesk_dbNumRows($result) != 1) {hesk_error($lng_helptrackingnotfound);}
$ticket = hesk_dbFetchAssoc($result);

/* Get category name and ID */
$sql = "SELECT *,".$lng_prefix_select."name as name FROM `hesk_categories` WHERE `id`=$ticket[category] LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$lng_cant_sql: $sql</p><p>$lng_mysql_said:<br>".mysql_error()."</p><p>$lng_contact_webmsater $hesk_settings[webmaster_mail]");
/* If this category has been deleted use the default category with ID 1 */
    if (hesk_dbNumRows($result) != 1)
    {
        $sql = "SELECT *,".$lng_prefix_select."name as name FROM `hesk_categories` WHERE `id`=1 LIMIT 1";
        $result = hesk_dbQuery($sql) or hesk_error("$lng_cant_sql: $sql</p><p>$lng_mysql_said:<br>".mysql_error()."</p><p>$lng_contact_webmsater $hesk_settings[webmaster_mail]");
    }
$category = hesk_dbFetchAssoc($result);

/* Get replies */
$sql = "SELECT * FROM `hesk_replies` WHERE `replyto`='$ticket[id]' ORDER BY `id` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$lng_cant_sql: $sql</p><p>$lng_mysql_said:<br>".mysql_error()."</p><p>$lng_contact_webmsater $hesk_settings[webmaster_mail]");
$replies = hesk_dbNumRows($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title><?php echo $hesk_settings['hesk_title']; ?></title>
<meta content="text/html; charset=<?php echo $lng_characset; ?>">
<style type="text/css">
body,p,td {
    color : black;
    font-family : Verdana, Geneva, Arial, Helvetica, sans-serif;
    font-size : <?php echo $hesk_settings['print_font_size']; ?>px;
}
</style>
</head>
<body onLoad="window.print()">


<?php
echo <<<EOC
<p>$lng_helpsubject: <b>$ticket[subject]</b><br>

$lng_helptrackingid: $trackingID<br>
$lng_helpticketstatus:

EOC;

$random=rand(10000,99999);

    switch ($ticket['status']) {
    case 0:
        $ticket['status']=$lng_helpopen;
        break;
    case 1:
        $ticket['status']=$lng_helpwaitingstaff;
        break;
    case 2:
        $ticket['status']=$lng_helpwaitingcust;
        break;
    default:
        $ticket['status']=$lng_closed;
    }

        if ($ticket['lastreplier']) {$ticket['lastreplier'] = $lng_staff;}
        else {$ticket['lastreplier'] = $lng_customer;}

echo <<<EOC
$ticket[status]<br>
$lng_helpcreateon: $ticket[dt]<br>
$lng_helplastupdate: $ticket[lastchange]<br>
$lng_helplastreplier: $ticket[lastreplier]<br>
$lng_helpcategory: $category[name]<br>
$lng_helpreplies: $replies<br>
$lng_helppriority:

EOC;
    if ($ticket['priority']==1) {echo "<b>$hesklang[high]</b>";}
    elseif ($ticket['priority']==2) {echo "$hesklang[medium]";}
    else {echo "$hesklang[low]";}

echo <<<EOC
<hr>
$lng_helpdate: $ticket[dt]<br>
$lng_helponlyname: $ticket[name]<br>
$lng_helpemail: $ticket[email]<br>
$lng_ip: $ticket[ip]<br>

EOC;

/* custom fields */
if ($hesk_settings['use_custom']) {

    foreach ($hesk_settings['custom_fields'] as $k=>$v) {
        if ($v['use']) {echo $v['name'].': '.$ticket[$k].'<br>';}
    }

}

echo <<<EOC
<b>$lng_helpmessage:</b><br>
$ticket[message]

<hr>

EOC;

while ($reply = hesk_dbFetchAssoc($result))
{
echo <<<EOC
$lng_helpdate: $reply[dt]<br>
$lng_helponlyname: $reply[name]<br>
<b>$lng_helpmessage:</b><br>
$reply[message]

<hr>

EOC;
}

echo $lng_helpend_ticket;
?>
</p>

</body>
</html>
