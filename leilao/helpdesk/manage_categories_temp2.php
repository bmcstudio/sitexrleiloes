<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link href="../style/UrTurn2Win.css" rel="stylesheet" type="text/css" />
<link href="../style/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="style/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div align="center" style="width:100%">
		<div align="center">
<?
//require_once('inc/header.inc.php');
include('header.php');

?>
<div id="account">
<div class="style1" id="title"><B>&nbsp;&nbsp;&nbsp;Admin</B></div><br />
	<div id="feedback_center_1">
		<div class="style17" id="title" style="margin-top: -0px;"><B>&nbsp;&nbsp;&nbsp;Show Ticket</B></div>
		<div id="feedback_username">
<?
/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

hesk_session_start();
hesk_isLoggedIn();
/* Must be administrator to access this page */
hesk_isAdmin();

/* Print header */
//require_once('inc/header.inc.php');

/* What should we do? */
$action=hesk_input($_REQUEST['a']);
if ($action == 'new') {new_cat();}
elseif ($action == 'rename') {rename_cat();}
elseif ($action == 'remove') {remove();}
elseif ($action == 'order') {order_cat();}

/* Print main manage users page */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<script language="Javascript" type="text/javascript"><!--
function confirm_delete()
{
if (confirm('<?php echo $hesklang['confirm_del_cat']; ?>')) {return true;}
else {return false;}
}
//-->
</script>

<h3 style="width:750px;" align="center"><?php echo $hesklang['manage_cat']; ?></h3>

<p style="width:750px;"><?php echo $hesklang['cat_intro']; ?>.</p>

<div style="width:750px;" align="center">
<table border="0" cellspacing="1" cellpadding="3" class="white">
<tr>
<td class="admin_white"><?php echo $hesklang['cat_name']; ?></td>
<td class="admin_white"><?php echo $hesklang['cat_order']; ?></td>
<td class="admin_white">&nbsp;</td>
</tr>

<?php
$sql = "SELECT * FROM `hesk_categories` ORDER BY `cat_order` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$options='';
$i=1;

while ($mycat=hesk_dbFetchAssoc($result))
{
    if ($i) {$color="admin_gray"; $i=0;}
    else {$color="admin_white"; $i=1;}

    /* Deleting category with ID 1 (default category) is not allowed */
    if ($mycat['id'] == 1)
    {
        $remove_code="&nbsp;";
    }
    else
    {
        $remove_code="<a href=\"manage_categories.php?a=remove&id=$mycat[id]\" onclick=\"return confirm_delete();\">$hesklang[remove]</a>";
    }

    $options .= "<option value=\"$mycat[id]\">$mycat[name]</option>\n";

echo <<<EOC
<tr>
<td class="$color">$mycat[name]</td>
<td class="$color" align="center"><a href="manage_categories.php?a=order&catid=$mycat[id]&move=-15">$hesklang[move_up]</a><br>
<a href="manage_categories.php?a=order&catid=$mycat[id]&move=15">$hesklang[move_dn]</a></td>
<td class="$color" align="center">$remove_code</td>
</tr>

EOC;
} // End while
?>
</table>
</div>

<p>&nbsp;</p>

<hr width="750">

<form action="manage_categories.php" method="POST">
<p style="width:750px;" align="center"><b><?php echo $hesklang['add_cat']; ?>:</b> (<?php echo $hesklang['max_chars']; ?>) <input type="text"
name="name" size="30" maxlength="40"><input type="hidden" name="a" value="new">
<input type="submit" value="<?php echo $hesklang['create_cat']; ?>" class="button"></p>
</form>

<hr width="750">

<form action="manage_categories.php" method="POST">
<p style="width:750px;" align="center"><?php echo $hesklang['ren_cat']; ?> <select name="catid"><?php
echo $options;
?></select> <?php echo $hesklang['to']; ?> <input type="text"
name="name" size="30" maxlength="40"><input type="hidden" name="a" value="rename">
<input type="submit" value="<?php echo $hesklang['ren_cat']; ?>" class="button"></p>
</form>

<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();


/*** START FUNCTIONS ***/

function new_cat() {
global $settings, $hesklang;

$catname=hesk_Input($_POST['name'],$hesklang['enter_cat_name']);

/* Get the latest cat_order */
$sql = "SELECT `cat_order` FROM `hesk_categories` ORDER BY `cat_order` DESC LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$row = hesk_dbFetchRow($result);
$my_order = $row[0]+10;

$sql = "INSERT INTO `hesk_categories` (`name`,`cat_order`) VALUES ('$catname','$my_order')";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 style="width:750px;"  align="center"><?php echo $hesklang['cat_added']; ?></h3>

<p>&nbsp;</p>

<p style="width:750px;"  align="center"><?php printf($hesklang['cat_name_added'],$catname); ?>!</p>

<p style="width:750px;"  align="center"><a href="manage_categories.php"><?php echo $hesklang['manage_cat']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End new_cat()


function rename_cat() {
global $settings, $hesklang;

$catid=hesk_isNumber($_POST['catid'],$hesklang['choose_cat_ren']);
$catname=hesk_Input($_POST['name'],$hesklang['cat_ren_name']);

$sql = "UPDATE `hesk_categories` SET `name`='$catname' WHERE `id`=$catid LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[cat_not_found].");}

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 style="width:750px;"  align="center"><?php echo $hesklang['cat_renamed']; ?></h3>

<p>&nbsp;</p>

<p style="width:750px;"  align="center"><?php echo $hesklang['cat_renamed_to'].' '.$catname; ?></p>

<p style="width:750px;"  align="center"><a href="manage_categories.php"><?php echo $hesklang['manage_cat']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End rename_cat()


function remove() {
global $settings, $hesklang;

$mycat=hesk_isNumber($_GET['id'],$hesklang['no_cat_id']);
if ($mycat == 1) {hesk_error($hesklang['cant_del_default_cat']);}

$sql = "DELETE FROM `hesk_categories` WHERE `id`=$mycat LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[cat_not_found].");}

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 style="width:750px;"  align="center"><?php echo $hesklang['cat_removed']; ?></h3>

<p>&nbsp;</p>

<p style="width:750px;"  align="center"><?php echo $hesklang['cat_removed_db']; ?>!</p>

<p style="width:750px;"  align="center"><a href="manage_categories.php"><?php echo $hesklang['manage_cat']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End remove()


function order_cat() {
global $settings, $hesklang;

$catid=hesk_isNumber($_GET['catid'],$hesklang['cat_move_id']);
$cat_move=intval($_GET['move']);

$sql = "UPDATE `hesk_categories` SET `cat_order`=`cat_order`+$cat_move WHERE `id`=$catid LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[cat_not_found].");}

/* Update all category fields with new order */
$sql = "SELECT `id` FROM `hesk_categories` ORDER BY `cat_order` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

$i = 10;
while ($mycat=hesk_dbFetchAssoc($result))
{
    $sql = "UPDATE `hesk_categories` SET `cat_order`=$i WHERE `id`=$mycat[id] LIMIT 1";
    hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
    $i += 10;
}

} // End order_cat()

?>
