<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
include("../functions.php");
$staticvar = "contact";
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('language/english.php');
require_once('inc/common.inc.php');

$hesk_error_buffer=array();

if ($hesk_settings['secimg_use']) {
        hesk_session_start();
        $mysecnum=hesk_isNumber($_POST['mysecnum']);
        if (empty($mysecnum)) {
                $hesk_error_buffer[]=$hesklang['sec_miss'];
        } else {
            require('secimg.inc.php');
            $sc=new PJ_SecurityImage($hesk_settings['secimg_sum']);
            if (!($sc->checkCode($mysecnum,$_SESSION['checksum']))) {
                    $hesk_error_buffer[]=$hesklang['sec_wrng'];
            }
        }
}

/* Print header */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<body>
<div id="main_div">
<?
	include("header.php");
?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$hesklang['submit_ticket'];?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2_static">
			<div class="staticbody">
			<div style="height: 20px;">&nbsp;</div>
			<?
				include("leftstatic.php");
			?>	
				<div class="staticright">
					<div style="text-align: right;"><a href="contact.html?a=add" class="alink"><?=$hesklang['backtomain'];?></a></div>
					<?
						$name=hesk_input($_POST['name']) or $hesk_error_buffer[]=$hesklang['enter_your_name'];
						$email=hesk_validateEmail($_POST['email'],'ERR',0) or $hesk_error_buffer[]=$hesklang['enter_valid_email'];
						$category=hesk_input($_POST['category']) or $hesk_error_buffer[]=$hesklang['sel_app_cat'];
						$priority=hesk_input($_POST['priority']) or $hesk_error_buffer[]=$hesklang['sel_app_priority'];
						$subject=hesk_input($_POST['subject']) or $hesk_error_buffer[]=$hesklang['enter_ticket_subject'];
						$message=hesk_input($_POST['message']) or $hesk_error_buffer[]=$hesklang['enter_message'];
						
						
						/* Custom fields */
						if ($hesk_settings['use_custom']) {
							foreach ($hesk_settings['custom_fields'] as $k=>$v) {
								if ($v['use']) {
									if ($v['req']) {$$k=hesk_input($_POST[$k]) or $hesk_error_buffer[]=$hesklang['fill_all'].': '.$v['name'];}
									else {$$k=hesk_input($_POST[$k]);}
									$_SESSION["c_$k"]=$_POST[$k];
								}
							}
						}
						
						
						/* If we have any errors lets store info in session to avoid re-typing everything */
						if (count($hesk_error_buffer)!=0) {
							$_SESSION['c_name']     = $_POST['name'];
							$_SESSION['c_email']    = $_POST['email'];
							$_SESSION['c_category'] = $_POST['category'];
							$_SESSION['c_priority'] = $_POST['priority'];
							$_SESSION['c_subject']  = $_POST['subject'];
							$_SESSION['c_message']  = $_POST['message'];
						
							$problem = '</p>
							<div align="center" class="normal_text">
						<table border="0" widht="80%"">
							<tr>
							<td align=left>
							<p align=left><b>'.$hesklang['submit_problems'].':</b></p>
							<ul type="none" style="float: left; margin-left: 10px; margin-top: 10px;">';
							foreach ($hesk_error_buffer as $error) {
								$problem .= "<li><img src='".$hesklang['imagepath']."arrow.gif'>&nbsp;&nbsp;$error</li>\n";
							}
							$problem .= '
							</ul>
							</td>
							</tr>
							</table>
							</div>
							';
							hesk_error($problem);
						}
						
						/*
						print_r($hesk_error_buffer);
						print_r($_SESSION);
						exit();
						*/
						
						$message=hesk_makeURL($message);
						$message=nl2br($message);
						
						/* Generate tracking ID */
						$useChars='AEUYBDGHJLMNPQRSTVWXZ123456789';
						$trackingID = $useChars{mt_rand(0,29)};
						for($i=1;$i<10;$i++)
						{
							$trackingID .= $useChars{mt_rand(0,29)};
						}
						$trackingURL=$hesk_settings['hesk_url'].'/ticket.html?track='.$trackingID;
						
						/* Attachments */
						if ($hesk_settings['attachments']['use']) {
							require_once('inc/attachments.inc.php');
							$attachments = array();
							for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
								$att = hesk_uploadFile($i);
								if (!empty($att)) {
									$attachments[$i] = $att;
								}
							}
						}
						$myattachments='';
						
						/* Add to database */
						require_once('inc/database.inc.php');
						hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
						
						if ($hesk_settings['attachments']['use'] && !empty($attachments)) {
							foreach ($attachments as $myatt) {
								$sql = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$trackingID', '$myatt[saved_name]', '$myatt[real_name]', '$myatt[size]')";
								$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
								$myattachments .= hesk_dbInsertID() . '#' . $myatt['real_name'] .',';
							}
						}
						
						$sql = "
						INSERT INTO `hesk_tickets` (
						`trackid`,`name`,`email`,`category`,`priority`,`subject`,`message`,`dt`,`lastchange`,`ip`,`status`,`attachments`,`custom1`,`custom2`,`custom3`,`custom4`,`custom5`
						)
						VALUES (
						'$trackingID','$name','$email','$category','$priority','$subject','$message',NOW(),NOW(),'$_SERVER[REMOTE_ADDR]','0','$myattachments','$custom1','$custom2','$custom3','$custom4','$custom5'
						)
						";
						
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						
						/* Get e-mail message for customer */
						$fp=fopen('emails/'.$hesklang['lng_prefix'].'new_ticket.txt','r');
						$message=fread($fp,filesize('emails/'.$hesklang['lng_prefix'].'new_ticket.txt'));
						fclose($fp);
						
						$message=str_replace('%%NAME%%',$name,$message);
						$message=str_replace('%%SUBJECT%%',$subject,$message);
						$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
						$message=str_replace('%%TRACK_URL%%',$trackingURL,$message);
						$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
						$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
						
						/* Send e-mail */
						$headers="From: $hesk_settings[noreply_mail]\n";
						$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
						@mail($email,$hesklang['ticket_received'],$message,$headers);
						
						/* Need to notify any admins? */
						$admins=array();
						$sql = "SELECT `email`,`isadmin`,`categories` FROM `hesk_users` WHERE `notify`='1'";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						while ($myuser=hesk_dbFetchAssoc($result))
						{
							/* Is this an administrator? */
							if ($myuser['isadmin']) {$admins[]=$myuser['email']; continue;}
							/* Not admin, is he allowed this category? */
							$cat=substr($myuser['categories'], 0, -1);
							$myuser['categories']=explode(',',$cat);
							if (in_array($category,$myuser['categories']))
							{
								$admins[]=$myuser['email']; continue;
							}
						}
						if (count($admins)>0)
						{
						$trackingURL_admin=$hesk_settings['hesk_url'].'/admin_ticket.php?track='.$trackingID;
						
						/* Get e-mail message for customer */
						$fp=fopen('emails/new_ticket_staff.txt','r');
						$message=fread($fp,filesize('emails/new_ticket_staff.txt'));
						fclose($fp);
						
						$message=str_replace('%%NAME%%',$name,$message);
						$message=str_replace('%%SUBJECT%%',$subject,$message);
						$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
						$message=str_replace('%%TRACK_URL%%',$trackingURL_admin,$message);
						$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
						$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
						
						/* Send e-mail to staff */
						$email=implode(',',$admins);
						$headers="From: $hesk_settings[noreply_mail]\n";
						$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
						@mail($email,$hesklang['new_ticket_submitted'],$message,$headers);
						} // End if
						
						?>
						
						<p>&nbsp;</p>
						
						<h3 class="darkblue-text-17-b" align="center"><?php echo $hesklang['ticket_submitted']; ?></h3>
						
						<p>&nbsp;</p>
						
						<p align="center" class="normal_text"><?php echo $hesklang['ticket_submitted_success'].': <b>'.$trackingID;?></b></p>
						<p align="center" style="margin-top: 10px;"><a href="<?php echo $trackingURL; ?>" class="alink"><?php echo $hesklang['view_your_ticket']; ?></a></p>
						
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
				
				</div>
		 </div>
		</div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
<?
	include("footer.php");
?>		
</div>
</body>
</html>
