<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/admin_common.inc.php');
hesk_session_start();
hesk_isLoggedIn();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1><?php echo $hesklang['profile_for'].' <b>'.$_SESSION['user']; ?></TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>

<?
/* Print header */
//require_once('inc/header.inc.php');

if (!empty($_POST['action'])) {update_profile();}

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');
?>
<tr>
<td>
<p><?php echo $hesklang['req_marked_with']; ?> <font class="important">*</font></p>

<form method="POST" action="profile.php" name="form1">

<!-- Contact info -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['name']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="name" size="25"
maxlength="50" value="<?php echo $_SESSION['name']; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['email']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="email" size="30"
maxlength="255" value="<?php echo $_SESSION['email']; ?>"></td>
</tr>
</table>

<hr>

<!-- Password -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['new_pass']; ?>: </td>
<td align="left" width="550"><input type="password" name="newpass" size="30"
maxlength="20"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['confirm_pass']; ?>: </td>
<td align="left" width="550"><input type="password" name="newpass2" size="30"
maxlength="20"></td>
</tr>
</table>

<hr>

<!-- signature -->
<table border="0">
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['signature_max']; ?>:</td>
<td align="left" width="550"><textarea name="signature" rows="6" cols="40"><?php echo $_SESSION['signature']; ?></textarea><br>
<?php echo $hesklang['sign_extra']; ?></td>
</tr>
</table>

<!-- Notify about new tickets and replies -->
<p style="width:748px;" align="center"><label><input style="border:none; background-color:#FFFFFF;" type="checkbox" name="notify" value="1" <?php
if ($_SESSION['notify']) {echo "checked";}
?> > <?php echo $hesklang['notify_new_posts']; ?>.</label></p>

<!-- Submit -->
<p style="width:675px;" align="center"><input type="hidden" name="action" value="update">
<input type="submit" value="<?php echo $hesklang['update_profile']; ?>" class="button"></p>

<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</form>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();


/*** START FUNCTIONS ***/

function update_profile() {
global $settings, $hesklang;

$_SESSION['name']=hesk_input($_POST['name'],$hesklang['enter_your_name']);
$_SESSION['email']=hesk_validateEmail($_POST['email'],$hesklang['enter_valid_email']);
$_SESSION['signature']=hesk_input($_POST['signature']);
if (hesk_input($_POST['notify'])) {$_SESSION['notify']=1;}
else {$_SESSION['notify']=0;}
if (strlen($_SESSION['signature'])>255) {hesk_error($hesklang['signature_long']);}

/* Change password? */
if (!empty($_POST['newpass']))
{
    $newpass=hesk_PasswordSyntax($_POST['newpass'],$hesklang['password_not_valid']);
    $newpass2=hesk_input($_POST['newpass2'],$hesklang['confirm_user_pass']);
    if ($newpass != $newpass2) {hesk_error($hesklang['passwords_not_same']);}
    $_SESSION['pass']=$newpass;
}

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

$sql = "UPDATE `hesk_users` SET `name`='$_SESSION[name]',`email`='$_SESSION[email]',
`signature`='$_SESSION[signature]',`pass`='$_SESSION[pass]',`notify`='$_SESSION[notify]' WHERE `id`='$_SESSION[id]' LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');
?>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['profile_updated']; ?></h3>

<p>&nbsp;</p>

<p align="center"><?php echo $hesklang['profile_updated_success']; ?>.</p>

<p align="center"><a href="profile.php"><?php echo $hesklang['view_profile']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End update_profile()

?>
