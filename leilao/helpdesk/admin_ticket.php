<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/admin_common.inc.php');
hesk_session_start();
hesk_isLoggedIn();

/* Print header */
//require_once('inc/header.inc.php');

$trackingID=strtoupper(hesk_input($_GET['track']));
if (empty($trackingID)) {print_form();}


/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1>Admin Ticket</TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>

<?
/* Get ticket info */
$sql = "SELECT * FROM `hesk_tickets` WHERE `trackid`='$trackingID' LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbNumRows($result) != 1) {hesk_error($hesklang['ticket_not_found']);}
$ticket = hesk_dbFetchAssoc($result);

/* Get category name and ID */
$sql = "SELECT * FROM `hesk_categories` WHERE `id`=$ticket[category] LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
/* If this category has been deleted use the default category with ID 1 */
    if (hesk_dbNumRows($result) != 1)
    {
        $sql = "SELECT * FROM `hesk_categories` WHERE `id`=1 LIMIT 1";
        $result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
    }
$category = hesk_dbFetchAssoc($result);

/* Is this user allowed to view tickets inside this category? */
hesk_okCategory($category['id']);

/* List of categories */
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
$sql = "SELECT `id`,`name` FROM `hesk_categories` ORDER BY `cat_order` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$categories_options='';
while ($row=hesk_dbFetchAssoc($result))
{
    if ($row['id'] == $ticket['category']) {continue;}
    $categories_options.='<option value="'.$row['id'].'">'.$row['name'].'</option>';
}

/* Get replies */
$sql = "SELECT * FROM `hesk_replies` WHERE `replyto`='$ticket[id]' ORDER BY `id` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$replies = hesk_dbNumRows($result);

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');

?>
<tr>
<td>
<h3 align="center"><?php echo $ticket['subject']; ?></h3>

<div>
<table border="0" width="750" cellspacing="1" cellpadding="5" class="white">
<tr>
<td class="grey">
<form style="margin-bottom:0;" action="admin_move_category.php" method="POST"><p>
<input type="hidden" name="track" value="<?php echo $trackingID; ?>">

<table border="0" cellspacing="1" cellpadding="2">
<?php
echo '
<tr>
<td><b>'.$hesklang['trackID'].':</b> </td>
<td>'.$trackingID.'</td>
</tr>
<tr class="white">
<td class="white"><b>'.$hesklang['ticket_status'].':</b> </td>
<td class="white">';
$random=rand(10000,99999);

    switch ($ticket['status']) {
    case 0:
        echo '<font class="open">'.$hesklang['open'].'</font> [<a
        href="admin_change_status.php?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>]';
        break;
    case 1:
        echo '<font class="waitingreply">'.$hesklang['wait_staff_reply'].'</font> [<a
        href="admin_change_status.php?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>]';
        break;
    case 2:
        echo '<font class="replied">'.$hesklang['wait_cust_reply'].'</font> [<a
        href="admin_change_status.php?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>]';
        break;
    default:
        echo '<font class="resolved">'.$hesklang['closed'].'</font> [<a
        href="admin_change_status.php?track='.$trackingID.'&s=1&Refresh='.$random.'">'.$hesklang['open_action'].'</a>]';
    }

echo '</td>
</tr>
<tr>
<td><b>'.$hesklang['created_on'].':</b> </td>
<td>'.$ticket['dt'].'</td>
</tr>
<tr class="white">
<td class="white"><b>'.$hesklang['last_update'].':</b> </td>
<td class="white">'.$ticket['lastchange'].'</td>
</tr>
<tr>
<td><b>'.$hesklang['last_replier'].':</b> </td>
<td>';
        if ($ticket['lastreplier']) {echo $hesklang['staff'];}
        else {echo $hesklang['customer'];}
echo '</td>
</tr>
<tr class="white">
<td class="white"><b>'.$hesklang['category'].':</b> </td>
<td class="white">'.$category['name'].'&nbsp;&nbsp;&nbsp;<i>'.$hesklang['move_to_catgory'].'</i> <select name="category">
    <option value="" selected>'.$hesklang['select'].'</option>'.$categories_options.'</select>
    <input type="submit" value="'.$hesklang['move'].'">
</td>
</tr>
<tr>
<td><b>'.$hesklang['replies'].':</b> </td>
<td>'.$replies.'</td>
</tr>
<tr class="white">
<td class="white"><b>'.$hesklang['priority'].':</b> </td>
<td class="white">';
        if ($ticket['priority']==1) {echo '<font class="important">'.$hesklang['high'].'</font>';}
        elseif ($ticket['priority']==2) {echo '<font class="medium">'.$hesklang['medium'].'</font>';}
        else {echo $hesklang['low'];}
echo '</td>
</tr>
<tr>
<td><b>'.$hesklang['archived'].':</b> </td>
<td>';
    if ($ticket['archive']) {
        echo $hesklang['yes'].' [<a href="archive.php?track='.$trackingID.'&archived=0">'.$hesklang['remove_archive'].'</a>]';
    } else {
        echo $hesklang['no'].' [<a href="archive.php?track='.$trackingID.'&archived=1">'.$hesklang['add_archive'].'</a>]';
    }
echo '</td>
</tr>';	
?>
<tr class="white">
<td class="white">&nbsp;</td>
<td class="white"><a href="print.php?track=<?php echo $trackingID; ?>" target="_blank"><?php echo $hesklang['printer_friendly']; ?></a></td>
</tr>
</table>

</form>
</td>
</tr>
<tr>
<td class="white">
    <table border="0" cellspacing="1">
    <tr>
    <td class="white"><b><?php echo $hesklang['date']; ?>:</b></td>
    <td class="white"><?php echo $ticket['dt']; ?></td>
    </tr>
    <tr>
    <td class="white"><b><?php echo $hesklang['name']; ?>:</b></td>
    <td class="white"><?php echo $ticket['name']; ?></td>
    </tr>
    <tr>
    <td class="white"><b><?php echo $hesklang['email']; ?>:</b></td>
    <td class="white"><a href="mailto:<?php echo $ticket['email']; ?>"><?php echo $ticket['email']; ?></a></td>
    </tr>
    <tr>
    <td class="white"><b><?php echo $hesklang['ip']; ?>:</b></td>
    <td class="white"><?php echo $ticket['ip']; ?></td>
    </tr>
    </table>

<?php
/* custom fields */
if ($hesk_settings['use_custom']) {

    $myclass='class="white"';
    echo '<table border="0" cellspacing="1">';

    foreach ($hesk_settings['custom_fields'] as $k=>$v) {
        if ($v['use']) {
            if ($myclass) {$myclass='class="grey"';}
            else {$myclass='class="white"';}
echo <<<EOC
    <tr>
    <td $myclass><b>$v[name]:</b></td>
    <td $myclass>$ticket[$k]</td>
    </tr>

EOC;
        }
    }

    echo '</table>';
}

?>

<p><b><?php echo $hesklang['message']; ?>:</b><p>
<p><?php echo $ticket['message']; ?><br>&nbsp;</p>

<?php
if ($hesk_settings['attachments']['use'] && !empty($ticket['attachments'])) {
    echo '<p><b>'.$hesklang['attachments'].':</b><br>';
    $att=explode(',',substr($ticket['attachments'], 0, -1));
    foreach ($att as $myatt) {
        list($att_id, $att_name) = explode('#', $myatt);
        echo '<img src="img/clip.gif" width="20" height="20"><a href="download_attachment.php?att_id='.$att_id.'&amp;track='.$trackingID.'">'.$att_name.'</a><br>';
    }
    echo '</p>';
}
?>

</td>
</tr>

<?php
$i=1;
while ($reply = hesk_dbFetchAssoc($result))
{
if ($i) {$color='class="grey"'; $i=0;}
else {$color='class="white"'; $i=1;}
echo <<<EOC
    <tr>
    <td $color>
        <table border="0" cellspacing="1">
        <tr>
        <td $color><b>$hesklang[date]:</b></td>
        <td $color>$reply[dt]</td>
        </tr>
        <tr>
        <td $color><b>$hesklang[name]:</b></td>
        <td $color>$reply[name]</td>
        </tr>
        </table>
    <p><b>$hesklang[message]:</b></p>
    <p>$reply[message]</p>

EOC;

if ($hesk_settings['attachments']['use'] && !empty($reply['attachments'])) {
    echo '<p><b>'.$hesklang['attachments'].':</b><br>';
    $att=explode(',',substr($reply['attachments'], 0, -1));
    foreach ($att as $myatt) {
        list($att_id, $att_name) = explode('#', $myatt);
        echo '<img src="img/clip.gif" width="20" height="20"><a href="download_attachment.php?att_id='.$att_id.'&amp;track='.$trackingID.'">'.$att_name.'</a><br>';
    }
    echo '</p>';
}

}
echo '</td>
</tr>';
?>
</table>
</div>

<hr align="left" width="750px">

<h3 align="center"><?php echo $hesklang['add_reply']; ?></h3>

<script language="javascript" type="text/javascript"><!--
var myMsgTxt = new Array();
myMsgTxt[0]='';

<?php
/* CANNED RESPONSES */
$trans = array_flip(get_html_translation_table(HTML_SPECIALCHARS));
$options='';
$sql = "SELECT * FROM `hesk_std_replies` ORDER BY `reply_order` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
while ($mysaved=hesk_dbFetchRow($result))
{
    $options .= "<option value=\"$mysaved[0]\">$mysaved[1]</option>\n";
    echo 'myMsgTxt['.$mysaved[0].']=\''.str_replace("\r\n","\\r\\n' + \r\n'", strtr(addslashes($mysaved[2]), $trans))."';\n";
    $i++;
}

?>

function setMessage(msgid) {
var myMsg=myMsgTxt[msgid];
myMsg = myMsg.replace(/%%HESK_NAME%%/g, '<?php echo str_replace('\'','\\\'',$ticket['name']); ?>');
myMsg = myMsg.replace(/%%HESK_EMAIL%%/g, '<?php echo str_replace('\'','\\\'',$ticket['email']); ?>');
myMsg = myMsg.replace(/%%HESK_custom1%%/g, '<?php echo str_replace('\'','\\\'',$ticket['custom1']); ?>');
myMsg = myMsg.replace(/%%HESK_custom2%%/g, '<?php echo str_replace('\'','\\\'',$ticket['custom2']); ?>');
myMsg = myMsg.replace(/%%HESK_custom3%%/g, '<?php echo str_replace('\'','\\\'',$ticket['custom3']); ?>');
myMsg = myMsg.replace(/%%HESK_custom4%%/g, '<?php echo str_replace('\'','\\\'',$ticket['custom4']); ?>');
myMsg = myMsg.replace(/%%HESK_custom5%%/g, '<?php echo str_replace('\'','\\\'',$ticket['custom5']); ?>');

    if (document.getElementById) {
        document.getElementById('HeskMsg').innerHTML='<textarea name="message" rows="12" cols="60">'+myMsg+'</textarea>';
    } else {
        document.form1.message.value=myMsg;
    }

}
//-->
</script>

<form method="POST" action="admin_reply_ticket.php" enctype="multipart/form-data" name="form1">

<p align="center"><?php echo $hesklang['select_saved']; ?>:<br>
<select name="saved_replies" onChange="setMessage(this.value)">
<option value="0"> - <?php echo $hesklang['select_empty']; ?> - </option>
<?php echo $options; ?>
</select>
</p>

<p align="center"><?php echo $hesklang['message']; ?>: <font class="important">*</font><br>
<div id="HeskMsg" align="center"><textarea name="message" rows="12" cols="60"></textarea></div></p>

<?php
/* attachments */
if ($hesk_settings['attachments']['use']) {

?>

<p align="center">

<?php
echo $hesklang['attachments'].':<br>';
    for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
        echo '<input type="file" name="attachment['.$i.']" size="50"><br>';
    }
?>

<?php echo$hesklang['accepted_types']; ?>: <?php echo '*'.implode(', *', $hesk_settings['attachments']['allowed_types']); ?><br>
<?php echo $hesklang['max_file_size']; ?>: <?php echo $hesk_settings['attachments']['max_size']; ?> Kb
(<?php echo sprintf("%01.2f",($hesk_settings['attachments']['max_size']/1024)); ?> Mb)

</p>

<?php
}
?>

<div align="center">
<table>
<tr>
<td align="left">
<?php
if ($ticket['status']==0||$ticket['status']==1||$ticket['status']==2)
{
echo <<<EOC
<label><input style="background-color:#FFFFFF; border:none;" type="checkbox" name="close" value="1"> $hesklang[close_this_ticket]</label><br>

EOC;
}
?>
<label><input style="background-color:#FFFFFF; border:none;" type="checkbox" name="set_priority" value="1"> <?php echo $hesklang['change_priority']; ?> </label>
 <select name="priority">
<option value="3" selected><?php echo $hesklang['low']; ?></option>
<option value="2"><?php echo $hesklang['medium']; ?></option>
<option value="1"><?php echo $hesklang['high']; ?></option>
</select><br>
<label><input style="background-color:#FFFFFF; border:none;" type="checkbox" name="signature" value="1" checked> <?php echo $hesklang['attach_sign']; ?></label>
 (<a href="profile.php"><?php echo $hesklang['profile_settings']; ?></a>)
</td>
</tr>
</table>
</div>

<p align="center"><input type="hidden" name="orig_id" value="<?php echo $ticket['id']; ?>">
<input type="hidden" name="orig_name" value="<?php echo $ticket['name']; ?>">
<input type="hidden" name="orig_email" value="<?php echo $ticket['email']; ?>">
<input type="hidden" name="orig_track" value="<?php echo $trackingID; ?>">
<input type="hidden" name="orig_subject" value="<?php echo $ticket['subject']; ?>">
<input type="submit" value="<?php echo $hesklang['submit_reply']; ?>" class="button"></p>

</form>

<!-- HR -->
<p>&nbsp;</p>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');

/*** START FUNCTIONS ***/

function print_form() {
global $hesk_settings, $hesklang;
?>
<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['view_ticket']; ?></h3>
<p>&nbsp;</p>

<form action="admin_ticket.php" method="GET">
<div align="center">
<center>
<table class="white" cellspacing="1" cellpadding="8">
<tr>
<td class="white">
<p align="center"><?php echo $hesklang['ticket_trackID']; ?>: <input type="text" name="track" maxlength="10"
size="12"></p>
<p align="center"><input type="hidden" name="Refresh" value="<?php echo rand(10000,99999); ?>">
<input type="submit" value="<?php echo $hesklang['view_ticket']; ?>" class="button"></p>
</td>
</tr>
</table>
</center>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End print_form()

?>
