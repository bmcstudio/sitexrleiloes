<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link href="../style/UrTurn2Win.css" rel="stylesheet" type="text/css" />
<link href="../style/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="style/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div align="center" style="width:100%">
		<div align="center">
<?
//require_once('inc/header.inc.php');
include('header.php');

?>
<div id="account">
<div class="style1" id="title"><B>&nbsp;&nbsp;&nbsp;Admin</B></div><br />
	<div id="feedback_center_1">
		<div class="style17" id="title" style="margin-top: -0px;"><B>&nbsp;&nbsp;&nbsp;Show Ticket</B></div>
		<div id="feedback_username">
<?
/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

hesk_session_start();
hesk_isLoggedIn();
/* Must be administrator to access this page */
hesk_isAdmin();

/* Print header */
//require_once('inc/header.inc.php');

/* What should we do? */
$action=hesk_input($_REQUEST['a']);
if ($action == 'new') {new_user();}
elseif ($action == 'edit') {edit_user();}
elseif ($action == 'save') {update_user();}
elseif ($action == 'remove') {remove();}
else {

/* Print main manage users page */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<script language="Javascript" type="text/javascript"><!--
function confirm_delete()
{
if (confirm('<?php echo $hesklang['sure_remove_user']; ?>')) {return true;}
else {return false;}
}
//-->
</script>

<h3 style="width:750px;" align="center"><?php echo $hesklang['manage_users']; ?></h3>

<p style="width:750px;"><?php echo $hesklang['users_intro']; ?></p>

<div style="width:750px;" align="center">
<table border="0" width="750" cellspacing="1" cellpadding="3" class="white">
<tr>
<td class="admin_white"><?php echo $hesklang['name']; ?></td>
<td class="admin_white"><?php echo $hesklang['email']; ?></td>
<td class="admin_white"><?php echo $hesklang['username']; ?></td>
<td class="admin_white"><?php echo $hesklang['pass']; ?></td>
<td class="admin_white"><?php echo $hesklang['administrator']; ?></td>
<td class="admin_white">&nbsp;</td>
<td class="admin_white">&nbsp;</td>
</tr>

<?php
$sql = "SELECT * FROM `hesk_users` WHERE 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

$i=1;

while ($myuser=hesk_dbFetchAssoc($result))
{
    if ($i) {$color="admin_gray"; $i=0;}
    else {$color="admin_white"; $i=1;}

    if ($myuser['isadmin']) {$myuser['isadmin']="<font class=\"open\">$hesklang[yes]</font>";}
    else {$myuser['isadmin']="<font class=\"resolved\">$hesklang[no]</font>";}

    /* Deleting user with ID 1 (default administrator) is not allowed */
    if ($myuser['id'] == $_SESSION['id'])
    {
        $edit_code="<a href=\"profile.php\">$hesklang[edit]</a>";
        $remove_code="&nbsp;";
    }
    else
    {
        $edit_code="<a href=\"manage_users.php?a=edit&id=$myuser[id]\">$hesklang[edit]</a>";
        if ($myuser['id'] == 1) {
            $remove_code="&nbsp;";
        } else {
            $remove_code="<a href=\"manage_users.php?a=remove&id=$myuser[id]\" onclick=\"return confirm_delete();\">$hesklang[remove]</a>";
        }
    }

echo <<<EOC
<tr>
<td class="$color">$myuser[name]</td>
<td class="$color"><a href="mailto:$myuser[email]">$myuser[email]</a></td>
<td class="$color">$myuser[user]</td>
<td class="$color">$myuser[pass]</td>
<td class="$color">$myuser[isadmin]</td>
<td class="$color" align="center">$edit_code</td>
<td class="$color" align="center">$remove_code</td>
</tr>

EOC;
} // End while
?>
</table>
</div>

<p>&nbsp;</p>

<hr width="750">

<h3 style="width:750px;" align="center"><?php echo $hesklang['add_user']; ?></h3>

<p style="width:750px;" align="left"><?php echo $hesklang['req_marked_with']; ?> <font class="important">*</font></p>

<form name="form1" action="manage_users.php" method="POST">

<!-- Contact info -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['real_name']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="name" size="25"
maxlength="50"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['email']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="email" size="30"
maxlength="255"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['username']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="user" size="25"
maxlength="20"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['pass']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="password" name="newpass" size="30"
maxlength="20"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['confirm_pass']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="password" name="newpass2" size="30"
maxlength="20"></td>
</tr>
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['administrator']; ?>: <font class="important">*</font></td>
<td align="left" valign="top" width="550"><label><input type="radio" name="isadmin" value="1">
<?php echo $hesklang['yes']; ?></label> |
<label><input type="radio" name="isadmin" value="0" checked> <?php echo $hesklang['no']; ?></label></td>
</tr>
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['allowed_cat']; ?>: <font class="important">*</font></td>
<td align="left" valign="top" width="550">
<?php
$sql_private = "SELECT * FROM `hesk_categories` ORDER BY `cat_order` ASC";
$result = hesk_dbQuery($sql_private) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

while ($row=hesk_dbFetchAssoc($result))
{
    echo "
    <label><input type=\"checkbox\" name=\"categories[]\" value=\"$row[id]\"> $row[name]</label><br>
    ";
}

?>
</td>
</tr>
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['signature_max']; ?>:</td>
<td align="left" width="550"><textarea name="signature" rows="6" cols="40"></textarea><br>
<?php echo $hesklang['sign_extra']; ?></td>
</tr>
</table>

<!-- Submit -->
<p style="width:750px;" align="center"><input type="hidden" name="a" value="new">
<input type="submit" value="<?php echo $hesklang['create_user']; ?>" class="button"></p>

</form>

<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();

} // End else


/*** START FUNCTIONS ***/

function edit_user()
{
global $hesk_settings, $hesklang;

$id=hesk_isNumber($_GET['id'],"$hesklang[int_error]: $hesklang[no_valid_id]");

$sql = "SELECT * FROM `hesk_users` WHERE `id`=$id LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$myuser=hesk_dbFetchAssoc($result);

/* Print main manage users page */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<h3 style="width:750px;" align="center"><?php echo $hesklang['editing_user'].' '.$myuser['user']; ?></h3>

<p style="width:750px;" align="left"><?php echo $hesklang['req_marked_with']; ?> <font class="important">*</font></p>

<form method="post" action="manage_users.php">

<!-- Contact info -->
<table border="0">
<tr>
<td align="right" width="200"><?php echo $hesklang['real_name']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="name" size="25"
maxlength="50" value="<?php echo $myuser[name]; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['email']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="email" size="30"
maxlength="255" value="<?php echo $myuser[email]; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['username']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="text" name="user" size="25"
maxlength="20" value="<?php echo $myuser[user]; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['pass']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="password" name="newpass" size="30"
maxlength="20" value="<?php echo $myuser[pass]; ?>"></td>
</tr>
<tr>
<td align="right" width="200"><?php echo $hesklang['confirm_pass']; ?>: <font class="important">*</font></td>
<td align="left" width="550"><input type="password" name="newpass2" size="30"
maxlength="20" value="<?php echo $myuser[pass]; ?>"></td>
</tr>
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['administrator']; ?>: <font class="important">*</font></td>
<td align="left" valign="top" width="550">
<?php
if ($myuser[isadmin]) {
    echo "
    <input type=\"radio\" name=\"isadmin\" value=\"1\" checked> $hesklang[yes] |
    <input type=\"radio\" name=\"isadmin\" value=\"0\"> $hesklang[no]"
    ;
} else {
    echo "
    <input type=\"radio\" name=\"isadmin\" value=\"1\"> $hesklang[yes] |
    <input type=\"radio\" name=\"isadmin\" value=\"0\" checked> $hesklang[no]"
    ;
}
?></td>
</tr>
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['allowed_cat']; ?>: <font class="important">*</font></td>
<td align="left" valign="top" width="550">
<?php
$sql_private = "SELECT * FROM `hesk_categories` ORDER BY `cat_order` ASC";
$result = hesk_dbQuery($sql_private) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

$cat=substr($myuser['categories'], 0, -1);
$myuser['categories']=explode(",",$cat);

while ($row=hesk_dbFetchAssoc($result))
{
    if (in_array($row[id],$myuser['categories'])) {
    echo "
    <input type=\"checkbox\" name=\"categories[]\" value=\"$row[id]\" checked>$row[name]<br>
    ";
    } else {
    echo "
    <input type=\"checkbox\" name=\"categories[]\" value=\"$row[id]\">$row[name]<br>
    ";
    }
}

?>
</td>
</tr>
<tr>
<td align="right" valign="top" width="200"><?php echo $hesklang['signature_max']; ?>:</td>
<td align="left" width="550"><textarea name="signature" rows="6" cols="40"><?php echo $myuser[signature]; ?></textarea><br>
<?php echo $hesklang['sign_extra']; ?>.</td>
</tr>
</table>

<!-- Submit -->
<p style="width:750px;" align="center"><input type="hidden" name="a" value="save">
<input type="hidden" name="userid" value="<?php echo $myuser[id]; ?>">
<input type="submit" value="<?php echo $hesklang['save_changes']; ?>" class="button"></p>

</form>

<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End edit_user()


function new_user() {
global $settings, $hesklang;

$myuser=hesk_validateUserInfo();

$sql = "INSERT INTO `hesk_users` (`user`,`pass`,`isadmin`,`name`,`email`,`signature`,`categories`)
VALUES ('$myuser[user]','$myuser[pass]','$myuser[isadmin]','$myuser[name]',
'$myuser[email]','$myuser[signature]','$myuser[categories]')";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 style="width:750px;" align="center"><?php echo $hesklang['user_added']; ?></h3>

<p>&nbsp;</p>

<p style="width:750px;" align="center"><?php printf($hesklang['user_added_success'],$myuser['user'],$myuser['pass']); ?>!</p>

<p style="width:750px;" align="center"><a href="manage_users.php"><?php echo $hesklang['manage_users']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End new_user()


function update_user() {
global $settings, $hesklang;

$myuser=hesk_validateUserInfo();
$myuser['id']=hesk_isNumber($_POST['userid'],"$hesklang[int_error]: $hesklang[no_valid_id]");

$sql = "UPDATE `hesk_users` SET `user`='$myuser[user]',`name`='$myuser[name]',`email`='$myuser[email]',
`signature`='$myuser[signature]',`pass`='$myuser[pass]',`categories`='$myuser[categories]',
`isadmin`='$myuser[isadmin]' WHERE `id`=$myuser[id] LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error($hesklang['user_not_found_nothing_edit']);}

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 style="width:750px;" align="center"><?php echo $hesklang['profile_updated']; ?></h3>

<p>&nbsp;</p>

<p style="width:750px;" align="center"><?php echo $hesklang['user_profile_updated_success']; ?>.</p>

<p style="width:750px;" align="center"><a href="manage_users.php"><?php echo $hesklang['manage_users']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End update_profile()

function hesk_validateUserInfo() {
global $hesklang;

$myuser['name']=hesk_input($_POST['name'],$hesklang['enter_real_name']);
$myuser['email']=hesk_validateEmail($_POST['email'],$hesklang['enter_valid_email']);
$myuser['user']=hesk_input($_POST['user'],$hesklang['enter_username']);
$myuser['signature']=hesk_input($_POST['signature']);
$myuser['isadmin']=hesk_isNumber($_POST['isadmin'],"$hesklang[int_error]: no valid isadmin");

$myuser['categories']='';
if (empty($myuser['isadmin'])) {
hesk_input($_POST['categories'],$hesklang['asign_one_cat']);
    foreach ($_POST['categories'] as $cat)
    {
        $myuser['categories'].="$cat,";
    }
}

if (strlen($myuser['signature'])>255) {hesk_error($hesklang['signature_long']);}

$newpass=hesk_PasswordSyntax($_POST['newpass'],$hesklang['password_not_valid']);
$newpass2=hesk_input($_POST['newpass2'],$hesklang['confirm_user_pass']);
if ($newpass != $newpass2) {hesk_error($hesklang['passwords_not_same']);}
$myuser['pass']=$newpass;

return $myuser;

} // End hesk_validateUserInfo()


function remove() {
global $settings, $hesklang;

$myuser=hesk_isNumber($_GET['id'],$hesklang['no_valid_id']);
if ($myuser == 1) {hesk_error($hesklang['cant_del_admin']);}
if ($myuser == $_SESSION['id']) {hesk_error($hesklang['cant_del_own']);}

$sql = "DELETE FROM `hesk_users` WHERE `id`=$myuser LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[user_not_found].");}

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 style="width:750px;" align="center"><?php echo $hesklang['user_removed']; ?></h3>

<p>&nbsp;</p>

<p style="width:750px;" align="center"><?php echo $hesklang['sel_user_removed']; ?>!</p>

<p style="width:750px;" align="center"><a href="manage_users.php"><?php echo $hesklang['manage_users']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End remove()

?>
