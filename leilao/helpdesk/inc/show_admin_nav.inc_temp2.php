<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

/* Check if this is a valid include */
if (!defined('IN_SCRIPT')) {die($hesklang['attempt']);}
?>

<div>
<table width="93%" border="0" cellspacing="1" cellpadding="3">
<tr>
<td align="center" class="admin_white">
<a href="admin_main.php" class="alinkauctions"><?php echo $hesklang['main_page']; ?></a>
| <a href="manage_users.php" class="alinkauctions"><?php echo $hesklang['manage_users']; ?></a>
| <a href="manage_categories.php" class="alinkauctions"><?php echo $hesklang['manage_cat']; ?></a>
| <a href="manage_canned.php" class="alinkauctions"><?php echo $hesklang['manage_saved']; ?></a>
| <a href="profile.php" class="alinkauctions"><?php echo $hesklang['profile']; ?></a>
| <a href="admin_settings.php" class="alinkauctions"><?php echo $hesklang['settings']; ?></a>
| <a href="admin.php?a=logout" class="alinkauctions"><?php echo $hesklang['logout']; ?></a>
</td>
</tr>
</table>
</div>
