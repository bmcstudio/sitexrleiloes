<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

/* Check if this is a valid include */
if (!defined('IN_SCRIPT')) {die($hesklang['attempt']);}

$sql = "SELECT * FROM `hesk_tickets` WHERE ";

if ($_GET['archive']) {$archive=1;$sql .= "`archive`='1' AND ";}
else {$archive=0;}

$sql .= hesk_myCategories();

/* Get all the SQL sorting preferences */
if (!isset($_GET['status'])) {$status=6; $sql .= " AND (`status`='0' OR `status`='1' OR `status`='2') ";}
else
{
    $status = hesk_isNumber($_GET['status']);
    if ($status==5) {$sql .= " AND (`status`='0' OR `status`='1') ";}
    elseif ($status==6) {$sql .= " AND (`status`='0' OR `status`='1' OR `status`='2') ";}
    elseif ($status!=4) {$sql .= " AND `status`='$status' ";}
}

$category = hesk_isNumber($_GET['category']) or $category=0;
if ($category) {$sql .= " AND `category`='$category' ";}

$sql_copy=$sql;

$maxresults = hesk_isNumber($_GET['limit']) or $maxresults=$hesk_settings['max_listings'];
$thispage = hesk_isNumber($_GET['page']) or $thispage=1;

$sort = hesk_input($_GET['sort']) or $sort='priority';
$sql .= " ORDER BY `$sort` ";

if (isset($_GET['asc']) && $_GET['asc']==0) {$sql .= " DESC ";$asc=0;$asc_rev=1;}
else {$sql .= " ASC ";$asc=1;$asc_rev=0;}

/* This query string will be used to browse pages */
$query = "status=$status&sort=$sort&category=$category&asc=$asc&limit=$maxresults&archive=$archive&page=";

/* Get number of tickets and page number */
$result = hesk_dbQuery($sql_copy) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$all = hesk_dbNumRows($result);

$thispages = ceil($all/$maxresults) or $thispages=1;
if ($thispage > $thispages) {$thispage=$thispages;}
$limit_down = (($thispage*$maxresults)-$maxresults);

    $prev_page = ($thispage-1 <= 0) ? 0 : $thispage-1;
    $next_page = ($thispage+1 > $thispages) ? 0 : $thispage+1;

    echo '<form name="HPage"><p style="width:750px;" align="center">'.sprintf($hesklang['tickets_on_pages'],$all,$thispages).' <select name="myHpage">';
    for ($i=1;$i<=$thispages;$i++) {
        echo '<option value="'.$i.'">'.$i.'</option>';
    }
    echo'</select> <input type="button" value="'.$hesklang['go'].'" onClick="Javascript:window.location=\'show_tickets.php?'.$query.'\'+document.HPage.myHpage.value"><br />';

    if ($thispages > 7 && $prev_page) {
        echo '
        <a href="show_tickets.php?'.$query.$i.'1">&lt;&lt;</a>
        &nbsp;
        <a href="show_tickets.php?'.$query.$prev_page.'">&lt;</a>
        &nbsp;
        ';
    }

    for ($i=1; $i<=$thispages; $i++) {
        if ($i <= ($thispage+5) && $i >= ($thispage-5)) {
           if($i == $thispage) {echo ' <b>'.$i.'</b> ';}
           else {echo ' <a href="show_tickets.php?'.$query.$i.'">'.$i.'</a> ';}
        }
    }

    if ($thispages > 7 && $next_page) {
        echo '
        &nbsp;
        <a href="show_tickets.php?'.$query.$next_page.'">&gt;</a>
        &nbsp;
        <a href="show_tickets.php?'.$query.$thispages.'">&gt;&gt;</a>
        ';
    }

    echo '</p></form>';


/* We have the full SQL query now, get tickets */
$sql .= " LIMIT $limit_down,$maxresults ";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* This query string will be used to order and reverse display */
$query = "status=$status&category=$category&asc=$asc_rev&limit=$maxresults&page=$thispage&archive=$archive&sort=";

/* Print the table with tickets */
$random=rand(10000,99999);
?>

<form name="form1" action="delete_tickets.php" method="POST">

<div>
<table border="0" width="750" cellspacing="1" cellpadding="3" class="white">
<tr>
<td class="admin_white">&nbsp;</td>
<td class="admin_white"><a href="show_tickets.php?<?php echo $query; ?>trackid"><?php echo $hesklang['trackID']; ?></a></td>
<td class="admin_white" align="center"><a href="show_tickets.php?<?php echo $query; ?>lastchange"><?php echo $hesklang['last_update']; ?></a></td>
<td class="admin_white"><a href="show_tickets.php?<?php echo $query; ?>name"><?php echo $hesklang['name']; ?></a></td>
<td class="admin_white"><a href="show_tickets.php?<?php echo $query; ?>subject"><?php echo $hesklang['subject']; ?></a></td>
<td class="admin_white" align="center"><a href="show_tickets.php?<?php echo $query; ?>status"><?php echo $hesklang['status']; ?></a></td>
<td class="admin_white" align="center"><a href="show_tickets.php?<?php echo $query; ?>lastreplier"><?php echo $hesklang['last_replier']; ?></a></td>
<td class="admin_white" align="center"><a href="show_tickets.php?<?php echo $query; ?>priority"><?php echo $hesklang['priority']; ?></a></td>
<td class="admin_white" align="center"><a href="show_tickets.php?<?php echo $query; ?>archive"><?php echo $hesklang['archived']; ?></a></td>
</tr>

<?php
while ($ticket=hesk_dbFetchAssoc($result))
{
    if ($i) {$color="admin_gray"; $i=0;}
    else {$color="admin_white"; $i=1;}

    switch ($ticket['status']) {
    case 0:
        $ticket['status']='<font class="open">'.$hesklang['open'].'</font>';
        break;
    case 1:
        $ticket['status']='<font class="waitingreply">'.$hesklang['wait_reply'].'</font>';
        break;
    case 2:
        $ticket['status']='<font class="replied">'.$hesklang['replied'].'</font>';
        break;
    default:
        $ticket['status']='<font class="resolved">'.$hesklang['closed'].'</font>';
    }

    switch ($ticket['priority']) {
    case 1:
        $ticket['priority']='<font class="important">'.$hesklang['high'].'</font>';
        break;
    case 2:
        $ticket['priority']='<font class="medium">'.$hesklang['medium'].'</font>';
        break;
    default:
        $ticket['priority']=$hesklang['low'];
    }

    $ticket['lastchange']=hesk_formatDate($ticket['lastchange']);

    if ($ticket['lastreplier']=='1') {$ticket['lastreplier']=$hesklang['staff'];}
    else {$ticket['lastreplier']=$hesklang['customer'];}

    if ($ticket['archive']) {$ticket['archive']=$hesklang['yes'];}
    else {$ticket['archive']=$hesklang['no'];}

echo <<<EOC
<tr>
<td class="$color" align="center"><input type="checkbox" name="id[]" value="$ticket[id]"></td>
<td class="$color"><a href="admin_ticket.php?track=$ticket[trackid]&Refresh=$random">$ticket[trackid]</a></td>
<td class="$color" align="center">$ticket[lastchange]</td>
<td class="$color">$ticket[name]</td>
<td class="$color"><a href="admin_ticket.php?track=$ticket[trackid]&Refresh=$random">$ticket[subject]</a></td>
<td class="$color" align="center">$ticket[status]</td>
<td class="$color" align="center">$ticket[lastreplier]</td>
<td class="$color" align="center">$ticket[priority]</td>
<td class="$color" align="center">$ticket[archive]</td>
</tr>

EOC;
} // End while
?>
</table>
</div>

<p>&nbsp; <a href="Javascript:void(0)" onClick="Javascript:hesk_changeAll()"><?php echo $hesklang['chg_all']; ?></a></p>

<p style="width:750px;" align="center"><select name="a">
<option value="delete" selected><?php echo $hesklang['del_selected']; ?></option>
<option value="close"><?php echo $hesklang['close_selected']; ?></option>
</select>
<input type="submit" value="<?php echo $hesklang['execute']; ?>" class="button"></p>

</form>
