<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

/* Check if this is a valid include */
if (!defined('IN_SCRIPT')) {die($hesklang['attempt']);}
?>

<div style="width:750px;">
<table border="0" width="750" cellspacing="1" cellpadding="5">
<tr>
<td valign="top" width="50%">
<form action="show_tickets.php" method="GET">
<h3 align="center"><?php echo $hesklang['show_tickets']; ?>:</h3>

<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td valign="top"><b><?php echo $hesklang['status']; ?></b>: &nbsp; </td>
<td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="50%"><label><input type="radio" name="status" value="0" style="border:none; background-color:#FFFFFF;" checked> <font class="open"><?php echo $hesklang['open']; ?></font></label></td>
<td width="50%"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="status" value="1"> <font class="waitingreply"><?php echo $hesklang['wait_reply']; ?></font></label></td>
</tr>
<tr>
<td width="50%"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="status" value="2"> <font class="replied"><?php echo $hesklang['replied']; ?></font></label></td>
<td width="50%"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="status" value="6"> <font class="replied"><?php echo $hesklang['all_not_closed']; ?></font></label></td>
</tr>
<tr>
<td width="50%"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="status" value="3"> <font class="resolved"><?php echo $hesklang['closed']; ?></label></td>
<td width="50%"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="status" value="4"> <?php echo $hesklang['any_status']; ?></label></td>
</tr>
</table>

</td>
</tr>
<tr>
<td class="white" valign="top"><b><?php echo $hesklang['sort_by']; ?></b>: &nbsp; </td>
<td class="white">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
<td width="50%" class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="sort" value="priority" checked> <?php echo $hesklang['priority']; ?></label></td>
<td width="50%" class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="sort" value="lastchange"> <?php echo $hesklang['last_update']; ?></label></td>
</tr>
<tr>
<td width="50%" class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="sort" value="name"> <?php echo $hesklang['name']; ?></label></td>
<td width="50%" class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="sort" value="subject"> <?php echo $hesklang['subject']; ?></label></td>
</tr>
<tr>
<td width="50%" class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="sort" value="status"> <?php echo $hesklang['status']; ?></label></td>
<td width="50%" class="white">&nbsp;</td>
</tr>
</table>

</td>
</tr>
<tr>
<td valign="center"><b><?php echo $hesklang['category']; ?></b>: &nbsp; </td>
<td><select name="category">
<option value="0"><?php echo $hesklang['any_cat']; ?></option>
<?php
$sql_private = "SELECT * FROM `hesk_categories` WHERE ";
$sql_private .= hesk_myCategories('id');
$sql_private .= ' ORDER BY `cat_order` ASC';

$result = hesk_dbQuery($sql_private) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
while ($row=hesk_dbFetchAssoc($result))
{
    echo '<option value="'.$row['id'].'">'.$row['name'].'</option>';
}

?>
</select></td>
</tr>
<tr>
<td class="white"><b><?php echo $hesklang['order']; ?></b>: &nbsp; </td>
<td class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="asc" value="1" checked> <?php echo $hesklang['ascending']; ?></label> |
<label><input style="border:none; background-color:#FFFFFF;" type="radio" name="asc" value="0"> <?php echo $hesklang['descending']; ?></label></td>
</tr>
<tr>
<td><b><?php echo $hesklang['display']; ?></b>: &nbsp; </td>
<td><input type="text" name="limit" value="<?php echo $hesk_settings['max_listings']; ?>" size="4"> <?php echo $hesklang['tickets_page']; ?></td>
</tr>
</table>

<p><label><input style="border:none; background-color:#FFFFFF;" type="checkbox" name="archive" value="1"> <?php echo $hesklang['disp_only_archived']; ?></label></p>

<p align="center"><input type="submit" value="<?php echo $hesklang['show_tickets']; ?>" class="button"></p>
</form>
</td>
<td valign="top" width="50%">
<form action="find_tickets.php" method="GET" name="findby">
<h3 align="center"><?php echo $hesklang['find_ticket_by']; ?>:</h3>

<table border="0" cellpadding="3" cellspacing="0" width="100%">
<tr>
<td><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="what" value="trackid" checked> <b><?php echo $hesklang['trackID']; ?></b></label>: &nbsp; </td>
<td><input type="text" name="trackid" size="12" maxlength="10" onFocus="Javascript:document.findby.what[0].checked=true;"></td>
</tr>
<tr>
<td class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="what" value="name"> <b><?php echo $hesklang['name']; ?></b></label>: &nbsp; </td>
<td class="white"><input type="text" name="name" size="20" onFocus="Javascript:document.findby.what[1].checked=true;"></td>
</tr>
<tr>
<td><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="what" value="dt"> <b><?php echo $hesklang['date_posted']; ?></b></label>: &nbsp; </td>
<td><input type="text" name="dt" size="12" maxlength="10" value="<?php echo $hesklang['yyyy_mm_dd']; ?>" onFocus="Javascript:document.findby.what[2].checked=true;"></td>
</tr>
<tr>
<td class="white"><label><input style="border:none; background-color:#FFFFFF;" type="radio" name="what" value="subject"> <b><?php echo $hesklang['subject']; ?></b></label>: &nbsp; </td>
<td class="white"><input type="text" name="subject" size="30" onFocus="Javascript:document.findby.what[3].checked=true;"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><b><?php echo $hesklang['display']; ?></b>: &nbsp; </td>
<td><input type="text" name="limit" value="<?php echo $hesk_settings['max_listings']; ?>" size="4"> <?php echo $hesklang['results_page']; ?></td>
</tr>
</table>

<p><label><input style="border:none; background-color:#FFFFFF;" type="checkbox" name="archive" value="1"> <?php echo $hesklang['search_only_archived']; ?></label></p>

<p align="center"><input type="submit" value="<?php echo $hesklang['find_ticket']; ?>" class="button"></p>
</form>
</td>
</tr>
</table>
</div>
