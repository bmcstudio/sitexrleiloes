<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

/* Check if this is a valid include */
if (!defined('IN_SCRIPT')) {die($hesklang['attempt']);}


/***************************
Function hesk_formatDate()
***************************/
function hesk_formatDate($dt)
{
    $dt=str_replace(' ','<br>',$dt);
    return($dt);
} // End hesk_formatDate()


/***************************
Function hesk_makeURL()
***************************/
function hesk_makeURL($strUrl)
{
    $myMsg = ' ' . $strUrl;
    $myMsg = preg_replace("#(^|[\n ])([\w]+?://[^ \"\n\r\t<]*)#is", "$1<a href=\"$2\" target=\"_blank\">$2</a>", $myMsg);
    $myMsg = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r<]*)#is", "$1<a href=\"http://$2\" target=\"_blank\">$2</a>", $myMsg);
    $myMsg = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $myMsg);
    $myMsg = substr($myMsg, 1);
    return($myMsg);
} // End hesk_makeURL()


/***************************
Function hesk_isNumber()
***************************/
function hesk_isNumber($in,$error=0) {

    $in = trim($in);

    if (preg_match("/\D/",$in) || $in=="")
    {
        if ($error)
        {
            hesk_error($error);
        }
        else
        {
            return '';
        }
    }

    return $in;

} // END hesk_isNumber()


/***************************
Function hesk_PasswordSyntax()
***************************/
function hesk_PasswordSyntax($password,$error,$checklength=1) {

    if (preg_match("/\W|\_|\./",$password) || empty($password))
    {
        hesk_error($error);
    }

    if ($checklength==1 && strlen($password) < 5)
    {
        hesk_error($error);
    }

    return $password;

} // END hesk_PasswordSyntax()


/***************************
Function hesk_validateURL()
***************************/
function hesk_validateURL($url,$error) {

    $url = trim($url);

    if (preg_match("/^(https?:\/\/+[\w\-]+\.[\w\-]+)/i",$url))
    {
        return $url;
    }

    hesk_error($error);

} // END hesk_validateURL()


/***************************
Function hesk_input()
***************************/
function hesk_input($in,$error=0) {

    $in = trim($in);

    if (strlen($in))
    {
        $in = htmlspecialchars($in);
        $in = preg_replace('/&amp;(\#[0-9]+;)/','&$1',$in);
    }
    elseif ($error)
    {
        hesk_error($error);
    }

    if (!ini_get('magic_quotes_gpc'))
    {
        if (!is_array($in))
            $in = addslashes($in);
        else
            $in = hesk_slashArray($in);
    }

    return $in;

} // END hesk_input()


/***************************
Function hesk_input_index() This function is for main page
***************************/
function hesk_input_index($in,$error=0) {

    $in = trim($in);

    if (strlen($in))
    {
        $in = htmlspecialchars($in);
        $in = preg_replace('/&amp;(\#[0-9]+;)/','&$1',$in);
    }
    elseif ($error)
    {
        hesk_error_index($error);
    }

    if (!ini_get('magic_quotes_gpc'))
    {
        if (!is_array($in))
            $in = addslashes($in);
        else
            $in = hesk_slashArray($in);
    }

    return $in;

} // END hesk_input()

/***************************
Function hesk_validateEmail()
***************************/
function hesk_validateEmail($address,$error,$required=1) {

    $address = trim($address);

    if (preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$address))
    {
        return $address;
    }

    if ($required) {
        hesk_error($error);
    } else {
        return '';
    }

} // END hesk_validateEmail()


/***************************
Function hesk_myCategories()
***************************/
function hesk_myCategories($what='category') {

    if (!empty($_SESSION['isadmin']))
    {
        return '1';
    }
    else
    {
        $mycat_sql='(';
        $i=1;
        foreach ($_SESSION['categories'] as $mycat)
        {
            if ($i) {
                $mycat_sql.=" `$what`=$mycat ";
            } else {
                $mycat_sql.=" OR `$what`=$mycat ";
            }
            $i=0;
        }
        $mycat_sql.=')';
        return $mycat_sql;
    }

} // END hesk_myCategories()


/***************************
Function hesk_okCategory()
***************************/
function hesk_okCategory($cat) {

    if (!empty($_SESSION['isadmin']))
    {
        return true;
    }
    elseif (in_array($cat,$_SESSION['categories']))
    {
        return true;
    }
    else
    {
        global $hesklang;
        hesk_error($hesklang['not_authorized_tickets']);
    }

} // END hesk_okCategory()


/***************************
Function hesk_session_regenerate_id()
***************************/
function hesk_session_regenerate_id() {

    if (version_compare(phpversion(),"4.3.3",">=")) {
       session_regenerate_id();
    } else {
        $randlen = 32;
        $randval = '0123456789abcdefghijklmnopqrstuvwxyz';
        $random = '';
        $randval_len = 35;
        for ($i = 1; $i <= $randlen; $i++) {
            $random .= substr($randval, rand(0,$randval_len), 1);
        }

        if (session_id($random)) {
            setcookie(
                session_name('HESK'),
                $random,
                ini_get("session.cookie_lifetime"),
                "/"
            );
            return true;
        } else {
            return false;
        }
    }

} // END hesk_session_regenerate_id()


/***************************
Function hesk_isAdmin()
***************************/
function hesk_isAdmin() {

    if (empty($_SESSION['isadmin']))
    {
        global $hesklang;
        hesk_error("$hesklang[must_be_admin]<p>&nbsp;</p>
        <p align=\"center\"><a href=\"admin.php\">$hesklang[click_login]</a>");
    }
    else
    {
        hesk_session_regenerate_id();
        return true;
    }

} // END hesk_isAdmin()


/***************************
Function hesk_isLoggedIn()
***************************/
function hesk_isLoggedIn($ticket_id=0) {

    if (empty($_SESSION['id']))
    {
        $referer = hesk_input($_SERVER['REQUEST_URI']);
        $referer = str_replace('&amp;','&',$referer);

        if (strpos($referer,'admin_reply_ticket.php')!== false) {
            $referer = 'admin_main.php';
        }

        $url='admin.php?a=login&notice=1&goto='.urlencode($referer);
        Header("Location: $url");
        exit();
    }
    else
    {
        hesk_session_regenerate_id();
        return true;
    }

} // END hesk_isLoggedIn()


/***************************
Function hesk_session_start()
***************************/
function hesk_session_start() {

    session_name('HESK');

    if (session_start()) {
        header ('P3P: CP="CAO DSP COR CURa ADMa DEVa OUR IND PHY ONL UNI COM NAV INT DEM PRE"');
        return true;
    } else {
        global $hesk_settings, $hesklang;
        hesk_error("$hesklang[no_session] $hesklang[contact_webmaster] $hesk_settings[webmaster_mail]");
    }

} // END hesk_session_start()


/***************************
Function hesk_session_stop()
***************************/
function hesk_session_stop() {


    session_unset();
    session_destroy();
    return true;

} // END hesk_session_stop()


/***************************
Function hesk_slashArray()
***************************/
function youraff_slashArray ($a)
{
    while (list($k,$v) = each($a))
    {
        if (!is_array($v))
            $a[$k] = addslashes($v);
        else
            $a[$k] = hesk_slashArray($v);
    }

    reset ($a);
    return ($a);
} // END hesk_slashArray()


/***************************
Function hesk_error()
***************************/
function hesk_error($error) {
global $hesk_settings, $hesklang;

//require_once('inc/header.inc.php');
?>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['error']; ?></h3>
<p>&nbsp;</p>

<p align="center"><?php echo $error; ?></p>
<p>&nbsp;</p>
<p align="center"><a href="javascript:history.go(-1)"><?php echo $hesklang['back']; ?></a></p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<?php
//require_once('inc/footer.inc.php');
exit();
} // END hesk_error()

/***************************
Function hesk_error()
***************************/
function hesk_error_index($error) {
global $hesk_settings, $hesklang;

?>
<html>
<head><title>WelCome To AuctionAvenue Support Centre</title>
<link href = "style/main.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginheight="0" mariginwidth="0">
<TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
<tr>
<td>
<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
<TBODY>
<TR >
      <td class="H1" height=15 bgColor="#FFFFFF" COLSPAN=2 align=center>Welcome to AuctionAvenue Support Centre</td></TR>
<TR bgColor="#484848">
	<TD height=15></TD>
</TR>
<TR bgColor="#CCCCCC">
	<TD height=15></TD>
</TR>
</TBODY></TABLE>
<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
<TBODY>
<TR >
      <td align="center">
<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['error']; ?></h3>
<p>&nbsp;</p>

<p align="center"><?php echo $error; ?></p>
<p>&nbsp;</p>
<p align="center"><a href="javascript:history.go(-1)"><?php echo $hesklang['back']; ?></a></p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</td>
</TR>
</TBODY>
</TABLE>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // END hesk_error()
?>
