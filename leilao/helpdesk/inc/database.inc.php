<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

/* Check if this is a valid include */
if (!defined('IN_SCRIPT')) {die($lng_attempt);}

/***************************
Function hesk_dbConnect()
***************************/
function hesk_dbConnect() {
global $hesk_settings;
global $hesk_db_link;

    $hesk_db_link = mysql_connect($hesk_settings['database_host'],
    $hesk_settings['database_user'], $hesk_settings['database_pass']);

    if (@mysql_select_db ($hesk_settings['database_name'], $hesk_db_link))
    return $hesk_db_link;
    else
    global $hesklang;
    hesk_error("$lng_cant_connect_db $lng_contact_webmsater $hesk_settings[webmaster_mail]!");

} // END hesk_dbConnect()


/***************************
Function hesk_dbClose()
***************************/
function hesk_dbClose() {
global $hesk_db_link;

    return @mysql_close($hesk_db_link);

} // END hesk_dbClose()


/***************************
Function hesk_dbQuery()
***************************/
function hesk_dbQuery($query)
{
    global $hesk_last_query;
    global $hesk_db_link;

    if (!$hesk_db_link && !hesk_dbConnect())
        return false;

    $hesk_last_query = $query;
    return @mysql_query ($query, $hesk_db_link);
} // END hesk_dbQuery()


/***************************
Function hesk_dbFetchAssoc()
***************************/
function hesk_dbFetchAssoc($res) {

    return @mysql_fetch_assoc($res);

} // END hesk_FetchAssoc()


/***************************
Function hesk_dbFetchRow()
***************************/
function hesk_dbFetchRow($res) {

    return @mysql_fetch_row($res);

} // END hesk_FetchRow()


/***************************
Function hesk_dbResult()
***************************/
function hesk_dbResult($res, $row, $column) {

    return @mysql_result($res, $row, $column);

} // END hesk_dbResult()


/***************************
Function hesk_dbInsertID()
***************************/
function hesk_dbInsertID() {
global $hesk_db_link;

    if ($lastid = @mysql_insert_id($hesk_db_link))
    {
        return $lastid;
    }

} // END hesk_dbInsertID()


/***************************
Function hesk_dbFreeResult()
***************************/
function hesk_dbFreeResult($res) {

    return mysql_free_result($res);

} // END hesk_dbFreeResult()


/***************************
Function hesk_dbNumRows()
***************************/
function hesk_dbNumRows($res) {

    return @mysql_num_rows($res);

} // END hesk_dbNumRows()


/***************************
Function hesk_dbAffectedRows()
***************************/
function hesk_dbAffectedRows() {
global $hesk_db_link;

    return @mysql_affected_rows($hesk_db_link);

} // END hesk_dbAffectedRows()

?>
