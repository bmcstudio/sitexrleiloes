<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
define('INSTALL',1);
define('HESK_NEW_VERSION','0.94.1');

/* Get all the required files and functions */
require('../hesk_settings.inc.php');
require('../inc/common.inc.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>Install Hesk <?php echo HESK_NEW_VERSION; ?></title>
<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
<link href="../hesk_style.css" type="text/css" rel="stylesheet">
<script language="Javascript" type="text/javascript" src="hesk_javascript.js"><!--
// -->
</script>
</head>
<body marginwidth="0" leftmargin="0">


<div align="center">
<center>
<table border="0" cellspacing="0" cellpadding="5" width="770" class="enclosing">
<tr>
<td>
<div align="center">
<center>
<table border="0" width="750" cellspacing="1" cellpadding="3" class="white">
<tr>
<td align="center" class="admin_white">
Hesk <?php echo HESK_NEW_VERSION; ?> installation script
</td>
</tr>
</table>
</center>
</div>
</td>
</tr>
<tr>
<td>
<h3 align="center"><br>Welcome to Hesk!</h3>

<p>Hello and thank you for downloading the Hesk helpdesk script! This tool will help you install and configure Hesk on your server.</p>

<p><font color="#FF0000">PLEASE MAKE SURE YOU READ INSTRUCTIONS IN THE README.HTM FILE
<b>BEFORE</b> RUNNING THIS INSTALLATION SCRIPT ANY FURTHER!</font></p>


<hr width="750">

<form method="GET" action="install.php">
<p align="center"><input type="submit" value="New install"></p>
<p align="center">Installs a new copy of Hesk to your server</p>
</form>

<hr width="750">

<form method="GET" action="update.php">
<p align="center"><input type="submit" value="Update existing install"></p>
<p align="center">Updates your existing Hesk to version <?php echo HESK_NEW_VERSION; ?></p>
</form>

<hr width="750">
<p>&nbsp;</p>

</td>
</tr>
</table>
</center>
</div>

<p align="center"><font class="smaller">Powered by <a href="http://www.phpjunkyard.com/free-helpdesk-software.php" class="smaller" target="_blank">Help desk software Hesk</a> <?php echo HESK_NEW_VERSION; ?></font></p>

</body>
</html>
