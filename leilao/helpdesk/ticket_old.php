<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');

/* Print header */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$AllPageTitle;?></title>
<link href="style/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="wrapper">
<!-- header Start -->
<?
	include("header.php");
?>
<!-- header End -->

<div id="container">
<!-- Banner Start -->
<?
	include("banner.php");
?>
<!-- Banner End -->
	<div id="home-container">
	<div id="left-side">
		<!-- Left Start -->
		<?
			include("leftside.php");
		?>
		<!-- Left End -->
	</div>
	  <div id="right-side">
	  	<div class="page-title">
			<div class="left">About AuctionsAvenue</div>
		</div>
		<div class="inner-container">
			<div class="larg-box">
				<div class="title-bar">
					<div class="left">About us</div>
					<div class="right"></div>
					<div class="cleaner"></div>				
				</div>
				<div class="body">
				<div id="feedback_center_1">
						<div class="style17" id="title" style="margin-top: -0px;"><B>&nbsp;&nbsp;&nbsp;View Ticket</B></div>
						<div id="feedback_username" width="750">
				<?
				$trackingID=strtoupper(hesk_input($_GET['track']));
				if (empty($trackingID)) {print_form();}
				
				/* Connect to database */
				require_once('inc/database.inc.php');
				hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
				
				/* Get ticket info */
				$sql = "SELECT * FROM `hesk_tickets` WHERE `trackid`='$trackingID' LIMIT 1";
				$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
				if (hesk_dbNumRows($result) != 1) {hesk_error($hesklang['ticket_not_found']);}
				$ticket = hesk_dbFetchAssoc($result);
				
				/* Get category name and ID */
				$sql = "SELECT * FROM `hesk_categories` WHERE `id`=$ticket[category] LIMIT 1";
				$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
				/* If this category has been deleted use the default category with ID 1 */
					if (hesk_dbNumRows($result) != 1)
					{
						$sql = "SELECT * FROM `hesk_categories` WHERE `id`=1 LIMIT 1";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
					}
				$category = hesk_dbFetchAssoc($result);
				
				/* Get replies */
				$sql = "SELECT * FROM `hesk_replies` WHERE `replyto`='$ticket[id]' ORDER BY `id` ASC";
				$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
				$replies = hesk_dbNumRows($result);
				?>
				</td>
				</tr>
				<tr>
				<td>
				
				<h3 align="center" style="width:750px;"><?php echo $ticket['subject']; ?></h3>
				
				
				
				<div width="750">
				<table border="0" width="750" cellspacing="1" cellpadding="5" class="white">
				<tr>
				<td class="grey">
				
				<table border="0" cellspacing="1" cellpadding="1">
				<?php
				echo '
				<tr>
				<td>'.$hesklang['trackID'].': </td>
				<td>'.$trackingID.'</td>
				</tr>
				<tr class="white">
				<td class="white">'.$hesklang['ticket_status'].': </td>
				<td class="white">';
				$random=rand(10000,99999);
				
					switch ($ticket['status']) {
					case 0:
						echo '<font class="open">'.$hesklang['open'].'</font> [<a class="alinkauctions" href="change_status.php?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>]';
						break;
					case 1:
						echo '<font class="replied">'.$hesklang['wait_staff_reply'].'</font> [<a class="alinkauctions" href="change_status.php?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>]';
						break;
					case 2:
						echo '<font class="waitingreply">'.$hesklang['wait_cust_reply'].'</font> [<a class="alinkauctions" href="change_status.php?track='.$trackingID.'&s=3&Refresh='.$random.'">'.$hesklang['close_action'].'</a>]';
						break;
					default:
						echo '<font class="resolved">'.$hesklang['closed'].'</font> [<a class="alinkauctions"
						href="change_status.php?track='.$trackingID.'&s=1&Refresh='.$random.'">'.$hesklang['open_action'].'</a>]';
					}
				
				echo '</td>
				</tr>
				<tr>
				<td>'.$hesklang['created_on'].': </td>
				<td>'.$ticket['dt'].'</td>
				</tr>
				<tr class="white">
				<td class="white">'.$hesklang['last_update'].': </td>
				<td class="white">'.$ticket['lastchange'].'</td>
				</tr>
				<tr>
				<td>'.$hesklang['last_replier'].': </td>
				<td>';
						if ($ticket['lastreplier']) {echo $hesklang['staff'];}
						else {echo $hesklang['customer'];}
				echo '</td>
				</tr>
				<tr class="white">
				<td class="white">'.$hesklang['category'].': </td>
				<td class="white">'.$category['name'].'</td>
				</tr>
				<tr>
				<td>'.$hesklang['replies'].': </td>
				<td>'.$replies.'</td>
				</tr>
				<tr class="white">
				<td class="white">'.$hesklang['priority'].': </td>
				<td class="white">';
						if ($ticket['priority']==1) {echo '<font class="important">'.$hesklang['high'].'</font>';}
						elseif ($ticket['priority']==2) {echo '<font class="medium">'.$hesklang['medium'].'</font>';}
						else {echo $hesklang['low'];}
				echo '</td>
				</tr>';		
				?>
				<tr>
				<td>&nbsp;</td>
				<td><a href="print.php?track=<?php echo $trackingID; ?>" class="alinkauctions" target="_blank"><?php echo $hesklang['printer_friendly']; ?></a></td>
				</tr>
				</table>
				
				</td>
				</tr>
				<tr>
				<td class="white">
					<table border="0" cellspacing="1">
					<tr>
					<td class="white"><?php echo $hesklang['date']; ?>:</td>
					<td class="white"><?php echo $ticket['dt']; ?></td>
					</tr>
					<tr>
					<td class="white"><?php echo $hesklang['name']; ?>:</td>
					<td class="white"><?php echo $ticket['name']; ?></td>
					</tr>
					<tr>
					<td class="white"><?php echo $hesklang['email']; ?>:</td>
					<td class="white"><a class="alinkauctions" href="mailto:<?php echo $ticket['email']; ?>"><?php echo $ticket['email']; ?></a></td>
					</tr>
					<?php /*?><tr>
					<td class="white"><?php echo $hesklang['ip']; ?>:</td>
					<td class="white"><?php echo $ticket['ip']; ?></td>
					</tr><?php */?>
					</table>
				
				<?php
				/* custom fields */
				if ($hesk_settings['use_custom']) {
				
					$myclass='class="white"';
					echo '<table border="0" cellspacing="1">';
				
					foreach ($hesk_settings['custom_fields'] as $k=>$v) {
						if ($v['use']) {
							if ($myclass) {$myclass='';}
							else {$myclass='class="white"';}
				echo <<<EOC
					<tr>
					<td $myclass>$v[name]:</td>
					<td $myclass>$ticket[$k]</td>
					</tr>
				
				EOC;
						}
					}
				
					echo '</table>';
				}
				
				?>
				
				<p><b><?php echo $hesklang['message']; ?>:</b><p>
				<p class="style15"><?php echo $ticket['message']; ?><br>&nbsp;</p>
				
				<?php
				if ($hesk_settings['attachments']['use'] && !empty($ticket['attachments'])) {
					echo '<p><b>'.$hesklang['attachments'].':</b><br>';
					$att=explode(',',substr($ticket['attachments'], 0, -1));
					foreach ($att as $myatt) {
						list($att_id, $att_name) = explode('#', $myatt);
						echo '<img src="img/clip.gif" width="20" height="20"><a class="alinkauctions"  href="download_attachment.php?att_id='.$att_id.'&amp;track='.$trackingID.'">'.$att_name.'</a><br>';
					}
					echo '</p>';
				}
				?>
				
				</td>
				</tr>
				
				<?php
				$i=1;
				while ($reply = hesk_dbFetchAssoc($result))
				{
				if ($i) {$color='class="grey"'; $i=0;}
				else {$color='class="white"'; $i=1;}
				echo <<<EOC
					<tr>
					<td $color>
						<table border="0" cellspacing="1">
						<tr>
						<td $color>$hesklang[date]:</td>
						<td $color>$reply[dt]</td>
						</tr>
						<tr>
						<td $color>$hesklang[name]:</td>
						<td $color>$reply[name]</td>
						</tr>
						</table>
					<p><b>$hesklang[message]:</b></p>
					<p class="style15">$reply[message]</p>
				
				EOC;
				
				if ($hesk_settings['attachments']['use'] && !empty($reply['attachments'])) {
					echo '<p><b>'.$hesklang['attachments'].':</b><br>';
					$att=explode(',',substr($reply['attachments'], 0, -1));
					foreach ($att as $myatt) {
						list($att_id, $att_name) = explode('#', $myatt);
						echo '<img src="img/clip.gif" width="20" height="20"><a class="alinkauctions" href="download_attachment.php?att_id='.$att_id.'&amp;track='.$trackingID.'">'.$att_name.'</a><br>';
					}
					echo '</p>';
				}
				
				echo '</td></tr>';
				}
				?>
				</table>
				</div>
				
				<hr width="750">
				
				<h3 width="750" align="center"><?php echo $hesklang['add_reply']; ?></h3>
				
				<form method="POST" action="reply_ticket.php" enctype="multipart/form-data">
				<p width="750" align="center"><?php echo $hesklang['message']; ?>: <font class="important">*</font><br>
				<textarea name="message" rows="12" cols="60"></textarea></p>
				
				<?php
				/* attachments */
				if ($hesk_settings['attachments']['use']) {
				
				?>
				
				<p width="750" align="center">
				
				<?php
				echo $hesklang['attachments'].':<br>';
					for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
						echo '<input type="file" name="attachment['.$i.']" size="50"><br>';
					}
				?>
				
				<?php echo$hesklang['accepted_types']; ?>: <?php echo '*'.implode(', *', $hesk_settings['attachments']['allowed_types']); ?><br>
				<?php echo $hesklang['max_file_size']; ?>: <?php echo $hesk_settings['attachments']['max_size']; ?> Kb
				(<?php echo sprintf("%01.2f",($hesk_settings['attachments']['max_size']/1024)); ?> Mb)
				
				</p>
				
				<?php
				}
				?>
				
				<p width="750" align="center"><input type="hidden" name="orig_id" value="<?php echo $ticket['id']; ?>">
				<input type="hidden" name="orig_name" value="<?php echo $ticket['name']; ?>">
				<input type="hidden" name="orig_track" value="<?php echo $trackingID; ?>">
				<input type="submit" value="<?php echo $hesklang['submit_reply']; ?>" class="button"></p>
				
				</form>
				
				</div>
				</div>
				</div>
				<div style="height: 20px; clear:both">&nbsp;</div>
				</div>
				</div>
	    </div>
		<div class="cleaner"></div>	
	</div>
</div>
</div>
<!-- footer Start -->
<?
	include("footer.php");
?>
<!-- footer End -->

<div class="cleaner"></div>	
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');

/*** START FUNCTIONS ***/

function print_form() {
global $hesk_settings, $hesklang;
?>
<p class="smaller"><a href="<?php echo $hesk_settings['site_url']; ?>"
class="smaller"><?php echo $hesk_settings['site_title']; ?></a> &gt;
<a href="index.php?a=start" class="smaller"><?php echo $hesk_settings['hesk_title']; ?></a>
&gt; <?php echo $hesklang['view_ticket']; ?><br>&nbsp;</p>
</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['view_ticket']; ?></h3>
<p>&nbsp;</p>

<form action="ticket.php" method="GET">
<div align="center">
<center>
<table class="white" cellspacing="1" cellpadding="8">
<tr>
<td class="white">
<p align="center"><?php echo $hesklang['ticket_trackID']; ?>: <input type="text" name="track" maxlength="10"
size="12"></p>
<p align="center"><input type="hidden" name="Refresh" value="<?php echo rand(10000,99999); ?>">
<input type="submit" value="<?php echo $hesklang['view_ticket']; ?>" class="button"></p>
</td>
</tr>
</table>
</center>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
//require_once('footer.php');
exit();
} // End print_form()

?>
