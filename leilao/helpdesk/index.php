<?
	include("config/connect.php");
	include("../functions.php");
	$staticvar = "contact";
	$incpath = "";
	define('IN_SCRIPT',1);
	include($incpath."hesk_settings.inc.php");
	include($incpath.'inc/common.inc.php');
	require_once('language/'.$hesk_settings['language'].'.inc.php');
	if($_POST['send']=="" || $_POST["mysecnum"]=="")
	{
		if ($hesk_settings['secimg_use']) {
			hesk_session_start();
			$_SESSION['secnum']=rand(10000,99999);
			$_SESSION['checksum']=crypt($_SESSION['secnum'],$hesk_settings['secimg_sum']);
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<body>
<div id="main_div">
<?
	include("header.php");
?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$hesklang['submit_ticket'];?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2_static">
			<div class="staticbody">
			<div style="height: 20px;">&nbsp;</div>
			<?
				include("leftstatic.php");
			?>	
				<div class="staticright">
		<form method="POST" action="" name="form1" enctype="multipart/form-data">
		<? 	if($_POST['send']!="")
			{
				$hesk_error_buffer=array();

				if ($hesk_settings['secimg_use']) {
						hesk_session_start();
						$mysecnum=hesk_isNumber($_POST['mysecnum']);
						if (empty($mysecnum)) {
								$hesk_error_buffer[]=$hesklang['sec_miss'];
						} else {
							require('secimg.inc.php');
							$sc=new PJ_SecurityImage($hesk_settings['secimg_sum']);
							if (!($sc->checkCode($mysecnum,$_SESSION['checksum']))) {
									$hesk_error_buffer[]=$hesklang['sec_wrng'];
							}
						}
				}
				
				$name=hesk_input($_POST['name']) or $hesk_error_buffer[]=$hesklang['enter_your_name'];
						$email=hesk_validateEmail($_POST['email'],'ERR',0) or $hesk_error_buffer[]=$hesklang['enter_valid_email'];
						$category=hesk_input($_POST['category']) or $hesk_error_buffer[]=$hesklang['sel_app_cat'];
						$priority=hesk_input($_POST['priority']) or $hesk_error_buffer[]=$hesklang['sel_app_priority'];
						$subject=hesk_input($_POST['subject']) or $hesk_error_buffer[]=$hesklang['enter_ticket_subject'];
						$message=hesk_input($_POST['message']) or $hesk_error_buffer[]=$hesklang['enter_message'];
						
						
						/* Custom fields */
						if ($hesk_settings['use_custom']) {
							foreach ($hesk_settings['custom_fields'] as $k=>$v) {
								if ($v['use']) {
									if ($v['req']) {$$k=hesk_input($_POST[$k]) or $hesk_error_buffer[]=$hesklang['fill_all'].': '.$v['name'];}
									else {$$k=hesk_input($_POST[$k]);}
									$_SESSION["c_$k"]=$_POST[$k];
								}
							}
						}
						
						
						/* If we have any errors lets store info in session to avoid re-typing everything */
						if (count($hesk_error_buffer)!=0) {
							$_SESSION['c_name']     = $_POST['name'];
							$_SESSION['c_email']    = $_POST['email'];
							$_SESSION['c_category'] = $_POST['category'];
							$_SESSION['c_priority'] = $_POST['priority'];
							$_SESSION['c_subject']  = $_POST['subject'];
							$_SESSION['c_message']  = $_POST['message'];
						
							$problem = '</p>
							<div align="center" class="normal_text">
						<table border="0" widht="80%"">
							<tr>
							<td align=left>
							<p align=left><b>'.$hesklang['submit_problems'].':</b></p>
							<ul type="none" style="float: left; margin-left: 10px; margin-top: 10px;">';
							foreach ($hesk_error_buffer as $error) {
								$problem .= "<li><img src='".$hesklang['imagepath']."arrow.gif'>&nbsp;&nbsp;$error</li>\n";
							}
							$problem .= '
							</ul>
							</td>
							</tr>
							</table>
							</div>
							';
							hesk_error_contact($problem);
						}
						else
						{
						
						/*
						print_r($hesk_error_buffer);
						print_r($_SESSION);
						exit();
						*/
						
						$message=hesk_makeURL($message);
						$message=nl2br($message);
						
						/* Generate tracking ID */
						$useChars='AEUYBDGHJLMNPQRSTVWXZ123456789';
						$trackingID = $useChars{mt_rand(0,29)};
						for($i=1;$i<10;$i++)
						{
							$trackingID .= $useChars{mt_rand(0,29)};
						}
						$trackingURL=$hesk_settings['hesk_url'].'/ticket.html?track='.$trackingID;
						
						/* Attachments */
						if ($hesk_settings['attachments']['use']) {
							require_once('inc/attachments.inc.php');
							$attachments = array();
							for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++)
							{
								$att = hesk_uploadFile($i);
								if (!empty($att)) {
									$attachments[$i] = $att;
								}
							}
						}
						$myattachments='';
						
						/* Add to database */
						require_once('inc/database.inc.php');
						hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
						
						if ($hesk_settings['attachments']['use'] && !empty($attachments)) {
							foreach ($attachments as $myatt) {
								$sql = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$trackingID', '$myatt[saved_name]', '$myatt[real_name]', '$myatt[size]')";
								$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
								$myattachments .= hesk_dbInsertID() . '#' . $myatt['real_name'] .',';
							}
						}
						
						$sql = "
						INSERT INTO `hesk_tickets` (
						`trackid`,`name`,`email`,`category`,`priority`,`subject`,`message`,`dt`,`lastchange`,`ip`,`status`,`attachments`,`custom1`,`custom2`,`custom3`,`custom4`,`custom5`
						)
						VALUES (
						'$trackingID','$name','$email','$category','$priority','$subject','$message',NOW(),NOW(),'$_SERVER[REMOTE_ADDR]','0','$myattachments','$custom1','$custom2','$custom3','$custom4','$custom5'
						)
						";
						
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						
						/* Get e-mail message for customer */
						$fp=fopen('emails/'.$hesklang['lng_prefix'].'new_ticket.txt','r');
						$message=fread($fp,filesize('emails/'.$hesklang['lng_prefix'].'new_ticket.txt'));
						fclose($fp);
						
						$message=str_replace('%%NAME%%',$name,$message);
						$message=str_replace('%%SUBJECT%%',$subject,$message);
						$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
						$message=str_replace('%%TRACK_URL%%',$trackingURL,$message);
						$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
						$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
						
						/* Send e-mail */
						$headers="From: $hesk_settings[noreply_mail]\n";
						$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
						@mail($email,$hesklang['ticket_received'],$message,$headers);
						
						/* Need to notify any admins? */
						$admins=array();
						$sql = "SELECT `email`,`isadmin`,`categories` FROM `hesk_users` WHERE `notify`='1'";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						while ($myuser=hesk_dbFetchAssoc($result))
						{
							/* Is this an administrator? */
							if ($myuser['isadmin']) {$admins[]=$myuser['email']; continue;}
							/* Not admin, is he allowed this category? */
							$cat=substr($myuser['categories'], 0, -1);
							$myuser['categories']=explode(',',$cat);
							if (in_array($category,$myuser['categories']))
							{
								$admins[]=$myuser['email']; continue;
							}
						}
						if (count($admins)>0)
						{
						$trackingURL_admin=$hesk_settings['hesk_url'].'/admin_ticket.php?track='.$trackingID;
						
						/* Get e-mail message for customer */
						$fp=fopen('emails/new_ticket_staff.txt','r');
						$message=fread($fp,filesize('emails/new_ticket_staff.txt'));
						fclose($fp);
						
						$message=str_replace('%%NAME%%',$name,$message);
						$message=str_replace('%%SUBJECT%%',$subject,$message);
						$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
						$message=str_replace('%%TRACK_URL%%',$trackingURL_admin,$message);
						$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
						$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
						
						/* Send e-mail to staff */
						$email=implode(',',$admins);
						$headers="From: $hesk_settings[noreply_mail]\n";
						$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
						@mail($email,$hesklang['new_ticket_submitted'],$message,$headers);
						} // End if

		
		?>
		
		<p>&nbsp;</p>
						
						<h3 class="darkblue-text-17-b" align="center"><?php echo $hesklang['ticket_submitted']; ?></h3>
						
						<p>&nbsp;</p>
						
						<p align="center" class="normal_text"><?php echo $hesklang['ticket_submitted_success'].': <b>'.$trackingID;?></b></p>
						<p align="center" style="margin-top: 10px;"><a href="<?php echo $trackingURL; ?>" class="alink"><?php echo $hesklang['view_your_ticket']; ?></a></p>
						
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
				
				</div>
		 </div>
		</div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		
<?
	session_unset($_SESSION['c_name']);     
    session_unset($_SESSION['c_email']);
    session_unset($_SESSION['c_category']);
    session_unset($_SESSION['c_priority']);
    session_unset($_SESSION['c_subject']);
    session_unset($_SESSION['c_message']);
	include("footer.php");
	exit;
	}
?>	

<? } ?>	
		<table border="0" cellspacing="0" cellpadding="0" size="750" style="margin-top: 10px;">
			<tr>
				<td width="750" colspan="2" align="right"><a href="viewticket.html" class="alink_big"><?=$hesklang['view_ticket'];?></a></td>
			</tr>
			<tr>
				<Td>&nbsp;</Td>
			</tr>
			<tr>
				<td align="left" class="normal_text"><?=$hesklang['use_form_below'];?>.</p></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>		
		<table border="0" cellspacing="2" cellpadding="2" size="750" >
		  <tr>
			<td width="750">
			<!-- Contact info -->
		 	 <table border="0" cellpadding="2" cellspacing="2">
			  <tr>
				<td align="right" nowrap="nowrap" class="normal_text"><?=$hesklang['name'];?>: <font class="red-text-12-b">*</font></td>
				<td align="left" width="600"><input type="text" name="name" size="25" maxlength="30" value="<?php echo stripslashes(hesk_input($_SESSION['c_name']));?>" class="textboxclas"></td>
		 	  </tr>
			  <tr>
				<td align="right" nowrap="nowrap" class="normal_text"><?=$hesklang['email'];?>: <font class="red-text-12-b">*</font></td>
				<td align="left" width="600"><input type="text" name="email" size="30" maxlength="50" value="<?php echo stripslashes(hesk_input($_SESSION['c_email']));?>" class="textboxclas"></td>
	 		  </tr>

			 
			<tr>
				<td align="right" nowrap="nowrap" class="normal_text"><?=$hesklang['category'];?>: <font class="red-text-12-b">*</font></td>
				<td align="left" width="600" style="padding-left: 5px;"><select name="category">
				<?php
				require_once($incpath.'inc/database.inc.php');
				hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
				$sql = "SELECT *,".$hesklang['lng_prefix']."name as name FROM `hesk_categories` ORDER BY `cat_order` ASC";
				$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
				while ($row=hesk_dbFetchAssoc($result))
				{
					if ($_SESSION['c_category'] == $row['id']) {$selected = ' selected';}
					else {$selected = '';}
					echo '<option value="'.$row['id'].'"'.$selected.'>'.$row['name'].'</option>';
				}
				?>
				</select>
				</td>
			</tr>
			<tr>	
				<td align="right" nowrap="nowrap" class="normal_text"><?=$hesklang['priority'];?>: <font class="red-text-12-b">*</font></td>
				<td align="left" width="600"><select name="priority">
				<option <?php if($_SESSION['c_priority']==3) {echo 'selected';} ?> value="3"><?php echo $hesklang['low'];?></option>
				<option <?php if($_SESSION['c_priority']==2) {echo 'selected';} ?> value="2"><?php echo $hesklang['medium'];?></option>
				<option <?php if($_SESSION['c_priority']==1) {echo 'selected';} ?> value="1"><?php echo $hesklang['high'];?></option>
				</select></td>
			</tr>
		 
			<tr>
				<td align="right" nowrap="nowrap" class="normal_text"><?=$hesklang['subject'];?>: <font class="red-text-12-b">*</font></td>
				<td align="left" width="580"><input type="text" name="subject" size="40" maxlength="40" value="<?=hesk_input($_SESSION['c_subject']);?>" class="textboxclas"></td>
			</tr>
			<tr>
				<td align="right" valign="top" nowrap="nowrap" class="normal_text"><?=$hesklang['message'];?>: <font class="red-text-12-b">*</font></td>
				<td align="left" width="580"><textarea name="message" rows="12" cols="60"><?=stripslashes(hesk_input($_SESSION['c_message']));?></textarea></td>
			</tr>
		
			<tr>
				<td align="right" nowrap="nowrap" valign="top" class="normal_text"><?=$hesklang['attachments'];?>:</td>
				<td align="left" width="580">
				<?php
					for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
						echo '<input type="file" name="attachment['.$i.']" size="50" class="textboxclas"><br>';
					}
				?>
				<span class="normal_text"><?=$hesklang['accepted_types'];?>:</span><?php echo '*'.implode(', *', $hesk_settings['attachments']['allowed_types']); ?><br>
				<?=$hesklang['max_file_size'];?>: <?php echo $hesk_settings['attachments']['max_size']; ?> Kb
				(<?php echo sprintf("%01.2f",($hesk_settings['attachments']['max_size']/1024)); ?> Mb)</p>
				</td>
			</tr>

		</table>
		</td>
		  </tr>
		 </table>
		<table border="0" width="710" cellpadding="2" cellspacing="1">
			<tr>
				<td align="center" style="padding-right: 250px;">
				<?php
					if ($hesk_settings['secimg_use']) {
						echo '<p>&nbsp;<img src="'.$incpath.'print_sec_img.php?'.rand(10000,99999).'" width="100" height="20" alt="Security image" border="1"><br><br></p><p><span class="normal_text" style="margin-left: 82px;">'.
						$hesklang['sec_enter'].'</span>: <input type="text" name="mysecnum" size="10" maxlength="5" class="textboxclas" style="width: 75px;"></p>
						<p>';
					} else {
					echo '<p>&nbsp;<br>';
				}
				?><br />
				<b class="normal_text" style="margin-left: 62px;"><?=$hesklang['before_submit'];?></b>
				<br /><br />
					<li style="text-align: left; margin-left: 75px;" class="normal_text"><?=$hesklang['all_info_in'];?>.</li>
					<li style="text-align: left;margin-left: 75px;" class="normal_text"><?=$hesklang['all_error_free'];?>.</li></p>
				</td>
			</tr>
		</table>
	   
<div align="left" style="padding-top: 15px; padding-left: 125px;"><input type="image" value="Submit Ticket" src="<?=$hesklang['imagepath'];?>Submit ticket.png" onmouseout="this.src='<?=$hesklang['imagepath'];?>Submit ticket.png'" onmouseover="this.src='<?=$hesklang['imagepath'];?>Submit ticket-hover.png'"></div>
		<input type="hidden" value="submit" name="submit" />
		<input type="hidden" name="send" value="send" />
	<div id="footerspace" style="clear:both; height: 20px;"></div>
				</form>
	</div>
				
				</div>
		 </div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
<?
	include("footer.php");
?>		
</div>
</body>
</html>
