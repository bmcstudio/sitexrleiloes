<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect();

hesk_session_start();
hesk_isLoggedIn();
/* Must be administrator to access this page */
hesk_isAdmin();

$set=array();

$set['site_title']=hesk_input($_POST['s_site_title'],$hesklang['err_sname']);
$set['site_title']=str_replace('\\&quot;','&quot;',$set['site_title']);
$set['site_url']=hesk_validateURL($_POST['s_site_url'],$hesklang['err_surl']);

$set['support_mail']=hesk_validateEmail($_POST['s_support_mail'],$hesklang['err_supmail']);
$set['webmaster_mail']=hesk_validateEmail($_POST['s_webmaster_mail'],$hesklang['err_wmmail']);
$set['noreply_mail']=hesk_validateEmail($_POST['s_noreply_mail'],$hesklang['err_nomail']);

$set['hesk_title']=hesk_input($_POST['s_hesk_title'],$hesklang['err_htitle']);
$set['hesk_title']=str_replace('\\&quot;','&quot;',$set['hesk_title']);
$set['hesk_url']=hesk_validateURL($_POST['s_hesk_url'],$hesklang['err_hurl']);
$set['server_path']=hesk_input($_POST['s_server_path'],$hesklang['err_spath']);
$set['language']=hesk_input($_POST['s_language'],$hesklang['err_lang']);
$set['language']=str_replace('.inc.php','',$set['language']);
if (!file_exists('language/'.$set['language'].'.inc.php')) {hesk_error($hesklang['err_nolang']);}
$set['max_listings']=hesk_input($_POST['s_max_listings'],$hesklang['err_max']);
$set['print_font_size']=hesk_input($_POST['s_print_font_size'],$hesklang['err_psize']);
$set['debug_mode']=$_POST['s_debug_mode'] ? 1 : 0;
$set['secimg_use']=$_POST['s_secimg_use'] ? 1 : 0;
$set['secimg_sum']='';
for ($i=1;$i<=10;$i++) {
    $set['secimg_sum'] .= substr('AEUYBDGHJLMNPQRSTVWXZ123456789', rand(0,29), 1);
}

$set['database_host']=hesk_input($_POST['s_database_host'],$hesklang['err_dbhost']);
$set['database_name']=hesk_input($_POST['s_database_name'],$hesklang['err_dbname']);
$set['database_user']=hesk_input($_POST['s_database_user'],$hesklang['err_dbuser']);
$set['database_pass']=hesk_input($_POST['s_database_pass'],$hesklang['err_dbpass']);
$set_link = @mysql_connect($set['database_host'],$set['database_user'],$set['database_pass']) or hesk_error($hesklang['err_dbconn']);
if (!(@mysql_select_db($set['database_name'],$set_link))) {hesk_error($hesklang['err_dbsele']);}
mysql_close($set_link);

$set['attachments']['use']=$_POST['s_attach_use'] ? 1 : 0;
if ($set['attachments']['use']) {
    $set['attachments']['max_number']=hesk_isNumber($_POST['s_max_number']) ? $_POST['s_max_number'] : 2;
    $set['attachments']['max_size']=hesk_isNumber($_POST['s_max_size']) ? $_POST['s_max_size'] : 512;
    $set['attachments']['allowed_types']=hesk_input($_POST['s_allowed_types']);
    if (empty($set['attachments']['allowed_types'])) {
        $set['attachments']['allowed_types']=array('.gif','.jpg','.jpeg','.zip','.rar','.csv','.doc','.txt','.pdf');
    } else {
        $set['attachments']['allowed_types']=explode(',',str_replace(' ','',$set['attachments']['allowed_types']));
    }
} else {
    $set['attachments']['max_number']=2;
    $set['attachments']['max_size']=512;
    $set['attachments']['allowed_types']=array('.gif','.jpg','.jpeg','.zip','.rar','.csv','.doc','.txt','.pdf');
}

$set['use_custom']=$_POST['s_use_custom'] ? 1 : 0;
$set['custom_place']=$_POST['s_custom_place'] ? 1 : 0;
if ($set['use_custom']) {

    for ($i=1;$i<=5;$i++) {
        $this_field='custom' . $i;

        $set['custom_fields'][$this_field]['use']=$_POST['s_custom'.$i.'_use'] ? 1 : 0;
        if ($set['custom_fields'][$this_field]['use']) {
            $set['custom_fields'][$this_field]['req']=$_POST['s_custom'.$i.'_req'] ? 1 : 0;
            $set['custom_fields'][$this_field]['name']=hesk_input($_POST['s_custom'.$i.'_name'],$hesklang['err_custname']);
            $set['custom_fields'][$this_field]['maxlen']=hesk_isNumber($_POST['s_custom'.$i.'_maxlen']) ? $_POST['s_custom'.$i.'_maxlen'] : 255;
        } else {
            $set['custom_fields'][$this_field] = array('use'=>0,'req'=>0,'name'=>'Custom field '.$i,'maxlen'=>255);
        }
    }

} else {
    $set['custom_fields']=array (
        'custom1'  => array('use'=>0,'req'=>0,'name'=>'Custom field 1','maxlen'=>255),
        'custom2'  => array('use'=>0,'req'=>0,'name'=>'Custom field 2','maxlen'=>255),
        'custom3'  => array('use'=>0,'req'=>0,'name'=>'Custom field 3','maxlen'=>255),
        'custom4'  => array('use'=>0,'req'=>0,'name'=>'Custom field 4','maxlen'=>255),
        'custom5'  => array('use'=>0,'req'=>0,'name'=>'Custom field 5','maxlen'=>255)
    );
}

$settings_file_content='<?php
/* Settings file for Hesk ' . $hesk_settings['hesk_version'] . ' */
/*** Please read the README.HTM file for more information on these settings ***/

/* Website settings */
$hesk_settings[\'site_title\']=\'' . $set['site_title'] . '\';
$hesk_settings[\'site_url\']=\'' . $set['site_url'] . '\';

/* Contacts */
$hesk_settings[\'support_mail\']=\'' . $set['support_mail'] . '\';
$hesk_settings[\'webmaster_mail\']=\'' . $set['webmaster_mail'] . '\';
$hesk_settings[\'noreply_mail\']=\'' . $set['noreply_mail'] . '\';

/* Help desk settings */
$hesk_settings[\'hesk_url\']=\'' . $set['hesk_url'] . '\';
$hesk_settings[\'hesk_title\']=\'' . $set['hesk_title'] . '\';
$hesk_settings[\'server_path\']=\'' . $set['server_path'] . '\';
$hesk_settings[\'language\']=\'' . $set['language'] . '\';
$hesk_settings[\'max_listings\']=' . $set['max_listings'] . ';
$hesk_settings[\'print_font_size\']=' . $set['print_font_size'] . ';
$hesk_settings[\'debug_mode\']=' . $set['debug_mode'] . ';
$hesk_settings[\'secimg_use\']=' . $set['secimg_use'] . ';
$hesk_settings[\'secimg_sum\']=\'' . $set['secimg_sum'] . '\';

/* Database settings */
$hesk_settings[\'database_host\']=\'' . $set['database_host'] . '\';
$hesk_settings[\'database_name\']=\'' . $set['database_name'] . '\';
$hesk_settings[\'database_user\']=\'' . $set['database_user'] . '\';
$hesk_settings[\'database_pass\']=\'' . $set['database_pass'] . '\';

/* File attachments */
$hesk_settings[\'attachments\']=array (
    \'use\' =>  ' . $set['attachments']['use'] . ',
    \'max_number\'  =>  ' . $set['attachments']['max_number'] . ',
    \'max_size\'    =>  ' . $set['attachments']['max_size'] . ', // kb
    \'allowed_types\'   =>  array(\'' . implode('\',\'',$set['attachments']['allowed_types']) . '\')
);

/* Custom fields */
$hesk_settings[\'use_custom\']=' . $set['use_custom'] . ';
$hesk_settings[\'custom_place\']=' . $set['custom_place'] . ';
$hesk_settings[\'custom_fields\']=array (
';

for ($i=1;$i<=5;$i++) {
    $settings_file_content.='\'custom'.$i.'\'=>array(\'use\'=>'.$set['custom_fields']['custom'.$i]['use'].',\'req\'=>'.$set['custom_fields']['custom'.$i]['req'].',\'name\'=>\''.$set['custom_fields']['custom'.$i]['name'].'\',\'maxlen\'=>'.$set['custom_fields']['custom'.$i]['maxlen'].')';
    if ($i!=5) {$settings_file_content.=',
';}
}

$settings_file_content.='
);

#############################
#     DO NOT EDIT BELOW     #
#############################
$hesk_settings[\'hesk_version\']=\'' . $hesk_settings['hesk_version'] . '\';
if ($hesk_settings[\'debug_mode\']) {
    error_reporting(E_ALL ^ E_NOTICE);
} else {
    ini_set(\'display_errors\', 0);
    ini_set(\'log_errors\', 1);
}
if (!defined(\'IN_SCRIPT\')) {die(\'Invalid attempt!\');}
if (is_dir(\'install\') && !defined(\'INSTALL\')) {die(\'Please delete the <b>install</b> folder from your server for security reasons then refresh this page!\');}
?>';

/*echo $settings_file_content;
exit();*/

$fp=@fopen('hesk_settings.inc.php','w') or hesk_error($hesklang['err_openset']);
fputs($fp,$settings_file_content);
fclose($fp);

/* Print header */
require_once('inc/header.inc.php');

/* Print main manage users page */
//require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<link href = "style/main.css" rel="stylesheet" type="text/css">
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['set_saved']; ?></h3>

<p>&nbsp;</p>

<p align="center"><?php echo $hesklang['set_were_saved']; ?></p>

<p align="center"><a href="admin_settings.php"><?php echo $hesklang['settings']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>

<?php
//require_once('inc/footer.inc.php');
exit();
?>
