<?
	include("config/connect.php");
	include("../functions.php");
	$incpath = "";
	$staticvar = "contact";
	define('IN_SCRIPT',1);
	include($incpath."hesk_settings.inc.php");
	include($incpath.'inc/common.inc.php');
	require_once('language/'.$hesk_settings['language'].'.inc.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>

<body>
<div id="main_div">
<?
	include("header.php");
?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$hesklang['forgot_tid'];?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2_static">
			<div class="staticbody">
			<div style="height: 20px;">&nbsp;</div>
			<?
				include("leftstatic.php");
			?>	
				<div class="staticright">
					<div style="text-align: right;"><a href="contact.html?a=add" class="alink"><?=$hesklang['backtomain'];?></a></div>
					<?
	
$email=hesk_validateEmail($_POST['email'],$hesklang['enter_valid_email']);

/* Prepare ticket statuses */
$my_status = array(
    0 => $hesklang['open'],
    1 => $hesklang['wait_staff_reply'],
    2 => $hesklang['wait_cust_reply'],
    3 => $hesklang['closed']
);

/* Get ticket(s) from database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

$sql = "SELECT * FROM `hesk_tickets` WHERE `email` LIKE '$email'";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$num=hesk_dbNumRows($result);
if ($num < 1) {
    //hesk_error($hesklang['tid_not_found']);
}
if($num>0)
{
$tid_list='';
$name='';
while ($my_ticket=hesk_dbFetchAssoc($result))
{
$name = $name ? $name : $my_ticket['name'];
$tid_list .= "
$hesklang[trackID]: $my_ticket[trackid]
$hesklang[subject]: $my_ticket[subject]
$hesklang[status]: ".$my_status[$my_ticket['status']]."
$hesk_settings[hesk_url]/ticket.html?track=$my_ticket[trackid]
";
}
/* Get e-mail message for customer */
$fp=fopen('emails/'.$hesklang['lng_prefix'].'forgot_ticket_id.txt','r');
$message=fread($fp,filesize('emails/'.$hesklang['lng_prefix'].'forgot_ticket_id.txt'));
fclose($fp);

$message=str_replace('%%NAME%%',$name,$message);
$message=str_replace('%%NUM%%',$num,$message);
$message=str_replace('%%LIST_TICKETS%%',$tid_list,$message);
$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
/* Send e-mail */
$headers="From: $hesk_settings[noreply_mail]\n";
$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
@mail($email,$hesklang['tid_email_subject'],$message,$headers);

?>
		  	<div id="hrsetfeedback" align="center">
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<h3 class="darkblue-text-17-b" align="center"><?=$hesklang['tid_sent'];?></h3>
			<p>&nbsp;</p>
			<p align="center" class="normal_text"><?=$hesklang['tid_sent2'];?></p>
			<p>&nbsp;</p>
			<p align="center" class="normal_text"><?=$hesklang['check_spambox'];?></p>
			<p>&nbsp;</p>
			<p align="center"><a class="alink" href="contact.html?a=add"><?=$hesklang['snabidsupport'];?></a></p>
			
			<p>&nbsp;</p>
			</div>
			<div>
				<p style="height:100px;"></p>
			</div>
			<?
}
else
{
?>
<div id="hrsetfeedback" align="center">
			<p>&nbsp;</p>
			
			<h3 align="center" class="normal_text"><?=$hesklang['error'];?></h3>
			
			<p>&nbsp;</p>
			
			<p align="center" class="normal_text"><?=$hesklang['tid_not_found'];?></p>
			<p>&nbsp;</p>
			<p align="center"><a class="alink" href="javascript:history.go(-1)"><?=$hesklang['back'];?></a></p>
			
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			</div>
			<div>
				<p style="height:100px;"></p>
			</div>
		</div>
<?
}
?>
				
				</div>
		 </div>
		</div>
		<div id="cleaner"></div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
</div>
<!-- footer Start -->
<?
	include("footer.php");
?>
<!-- footer End -->

<div class="cleaner"></div>	
</div>
</body>
</html>
