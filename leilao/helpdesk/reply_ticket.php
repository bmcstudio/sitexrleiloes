<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
include("../functions.php");
$staticvar = "contact";
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('language/english.php');
require_once('inc/common.inc.php');

/* Print header */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>

<body>
<div id="main_div">
<?
	include("header.php");
?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$hesklang['ticketreply'];?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2_static">
			<div class="staticbody">
			<div style="height: 20px;">&nbsp;</div>
			<?
				include("leftstatic.php");
			?>	
				<div class="staticright">
					<div style="text-align: right;"><a href="contact.html?a=add" class="alink"><?=$hesklang['backtomain'];?></a></div>
					<?
						$message=hesk_input($_POST['message'],$hesklang['enter_message']);
						$message=hesk_makeURL($message);
						$message=nl2br($message);
						$orig_name=hesk_input($_POST['orig_name'],"$hesklang[int_error]: No orig_name");
						$replyto=hesk_isNumber($_POST['orig_id'],"$hesklang[int_error]: No or invalid orig_id");
						$trackingID=hesk_input($_POST['orig_track'],"$hesklang[int_error]: No orig_track");
						$trackingURL=$hesk_settings['hesk_url'].'/ticket.html?track='.$trackingID.'&Refresh='.rand(10000,99999);
						
						/* Attachments */
						if ($hesk_settings['attachments']['use']) {
							require_once('inc/attachments.inc.php');
							$attachments = array();
							for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
								$att = hesk_uploadFile($i);
								if (!empty($att)) {
									$attachments[$i] = $att;
								}
							}
						}
						$myattachments='';
						
						/* Connect to database */
						require_once('inc/database.inc.php');
						hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
						
						if ($hesk_settings['attachments']['use'] && !empty($attachments)) {
							foreach ($attachments as $myatt) {
								$sql = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$trackingID', '$myatt[saved_name]', '$myatt[real_name]', '$myatt[size]')";
								$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
								$myattachments .= hesk_dbInsertID() . '#' . $myatt['real_name'] .',';
							}
						}
						
						/* Make sure the ticket is open */
						$sql = "UPDATE `hesk_tickets` SET `status`='1',`lastreplier`='0',`lastchange`=NOW() WHERE `id`=$replyto LIMIT 1";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						
						/* Add reply */
						$sql = "
						INSERT INTO `hesk_replies` (
						`replyto`,`name`,`message`,`dt`,`attachments`
						)
						VALUES (
						'$replyto','$orig_name','$message',NOW(),'$myattachments'
						)
						";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						
						$sql = "SELECT `subject`,`category` FROM `hesk_tickets` WHERE `id`=$replyto LIMIT 1";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						$ticket = hesk_dbFetchAssoc($result);
						$category=$ticket['category'];
						
						/* Need to notify any admins? */
						$admins=array();
						$sql = "SELECT `email`,`isadmin`,`categories` FROM `hesk_users` WHERE `notify`='1'";
						$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
						while ($myuser=hesk_dbFetchAssoc($result))
						{
							/* Is this an administrator? */
							if ($myuser['isadmin']) {$admins[]=$myuser['email']; continue;}
							/* Not admin, is he allowed this category? */
							$cat=substr($myuser['categories'], 0, -1);
							$myuser['categories']=explode(",",$cat);
							if (in_array($category,$myuser['categories']))
							{
								$admins[]=$myuser['email']; continue;
							}
						}
						if (count($admins)>0)
						{
						$trackingURL_admin=$hesk_settings['hesk_url'].'/admin_ticket.php?track='.$trackingID;
						
						/* Get e-mail message for customer */
						$fp=fopen('emails/new_reply_by_customer.txt','r');
						$message=fread($fp,filesize('emails/new_reply_by_customer.txt'));
						fclose($fp);
						
						$message=str_replace('%%NAME%%',$orig_name,$message);
						$message=str_replace('%%SUBJECT%%',$ticket['subject'],$message);
						$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
						$message=str_replace('%%TRACK_URL%%',$trackingURL_admin,$message);
						$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
						$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);
						
						/* Send e-mail to staff */
						$email=implode(',',$admins);
						$headers="From: $hesk_settings[noreply_mail]\n";
						$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
						@mail($email,$hesklang['new_reply_ticket'],$message,$headers);
						} // End if
						
						?>
						</td>
						</tr>
						<tr>
						<td>
						
						<p>&nbsp;</p>
						
						<h3 class="darkblue-text-17-b" align="center"><?php echo $hesklang['reply_submitted']; ?></h3>
						
						<p>&nbsp;</p>
						
						<p align="center" class="normal_text"><?php echo $hesklang['reply_submitted_success']; ?>!</p>
						<p align="center" style="margin-top: 10px;"><a href="<?php echo $trackingURL; ?>" class="alink"><?php echo $hesklang['view_your_ticket']; ?></a></p>
						
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
				
				</div>
		 </div>
		 </div>
		 <div id="cleaner"></div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
<?
	include("footer.php");
?>		
</div>
						
						
						<div class="cleaner"></div>	
						</div>
						</body>
						</html>
						<?php
						//require_once('inc/footer.inc.php');
						
						/*** START FUNCTIONS ***/
						
						function print_form() {
						global $hesk_settings, $hesklang;
						?>
						<?php /*?><p class="smaller"><a href="<?php echo $hesk_settings['site_url']; ?>"
						class="smaller"><?php echo $hesk_settings['site_title']; ?></a> &gt;
						<a href="index.php?a=start" class="smaller"><?php echo $hesk_settings['hesk_title']; ?></a>
						&gt; <?php echo $hesklang['view_ticket']; ?><br>&nbsp;</p><?php */?>
						<p>&nbsp;</p>
						<h3 align="center" class="normal_text"><?php echo $hesklang['view_ticket']; ?></h3>
						<p>&nbsp;</p>
						
						<form action="ticket.html" method="GET">
						<div align="center">
						<center>
						<table class="white" cellspacing="1" cellpadding="8">
						<tr>
						<td class="white">
						<p align="center" class="normal_text"><?php echo $hesklang['ticket_trackID']; ?>: <input type="text" name="track" maxlength="10"
						size="12" class="textboxclas"></p>
						<p align="center" style="margin-top: 15px;"><input type="hidden" name="Refresh" value="<?php echo rand(10000,99999); ?>">
						<input type="submit" value="<?php echo $hesklang['view_ticket']; ?>" class="button"></p>
						</td>
						</tr>
						</table>
						</center>
						</div>
						
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						</div>
				</div>
				</div>
				<div style="height: 20px; clear:both">&nbsp;</div>
				</div>
				</div>
	    </div>
		<div class="cleaner"></div>	
	</div>
</div>
</div>

<div class="cleaner"></div>	
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
//require_once('footer.php');
exit();
} // End print_form()

?>
