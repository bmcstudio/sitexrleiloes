<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/admin_common.inc.php');
hesk_session_start();
hesk_isLoggedIn();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1>Admin Reply Ticket</TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>
<?
/* Print header */
//require_once('inc/header.inc.php');

$message=hesk_input($_POST['message'],$hesklang['enter_message']);
/* Attach signature to the message? */
if (!empty($_POST['signature']))
{
    $message .= "<br><br>$_SESSION[signature]<br>&nbsp;";
}
$message=hesk_makeURL($message);
$message=nl2br($message);
$orig_name=hesk_input($_POST['orig_name'],"$hesklang[int_error]: No orig_name");
$orig_email=hesk_validateEmail($_POST['orig_email'],"$hesklang[int_error]: No valid orig_email");
$orig_subject=hesk_input($_POST['orig_subject'],"$hesklang[int_error]: No orig_subject");
$replyto=hesk_isNumber($_POST['orig_id'],"$hesklang[int_error]: No or invalid orig_id");
$trackingID=hesk_input($_POST['orig_track'],"$hesklang[int_error]: No orig_track");
$trackingURL=$hesk_settings['hesk_url'].'/ticket.php?track='.$trackingID.'&Refresh='.rand(10000,99999);
$admin_trackingURL=$hesk_settings['hesk_url'].'/admin_ticket.php?track='.$trackingID.'&Refresh='.rand(10000,99999);

/* Attachments */
if ($hesk_settings['attachments']['use']) {
    require_once('inc/attachments.inc.php');
    $attachments = array();
    for ($i=1;$i<=$hesk_settings['attachments']['max_number'];$i++) {
        $att = hesk_uploadFile($i);
        if (!empty($att)) {
            $attachments[$i] = $att;
        }
    }
}
$myattachments='';

/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

if ($hesk_settings['attachments']['use'] && !empty($attachments)) {
    foreach ($attachments as $myatt) {
        $sql = "INSERT INTO `hesk_attachments` (`ticket_id`,`saved_name`,`real_name`,`size`) VALUES ('$trackingID', '$myatt[saved_name]', '$myatt[real_name]', '$myatt[size]')";
        $result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        $myattachments .= hesk_dbInsertID() . '#' . $myatt['real_name'] .',';
    }
}

/* Add reply */
$sql = "
INSERT INTO `hesk_replies` (
`replyto`,`name`,`message`,`dt`,`attachments`
)
VALUES (
'$replyto','$_SESSION[name]','$message',NOW(),'$myattachments'
)
";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* Change the status of priority? */
if (!empty($_POST['set_priority']))
{
    $priority=hesk_input($_POST['priority'],$hesklang['select_priority']);
    $priority_sql = ",`priority`='$priority'";
}
else
{
    $priority_sql = "";
}

/* Update the original ticket */
if (!empty($_POST['close']))
{
    $sql = "UPDATE `hesk_tickets` SET `status`='3',`lastreplier`='1',`lastchange`=NOW() $priority_sql WHERE `id`=$replyto LIMIT 1";
}
else
{
    $sql = "UPDATE `hesk_tickets` SET `status`='2',`lastreplier`='1',`lastchange`=NOW() $priority_sql WHERE `id`=$replyto LIMIT 1";
}
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");


/*** Send "New reply added" e-mail ***/
/* Get e-mail message */
$fp=fopen('emails/gr_new_reply_by_staff.txt','r');
$message=fread($fp,filesize('emails/gr_new_reply_by_staff.txt'));
fclose($fp);

$message=str_replace('%%NAME%%',$orig_name,$message);
$message=str_replace('%%SUBJECT%%',$orig_subject,$message);
$message=str_replace('%%TRACK_ID%%',$trackingID,$message);
$message=str_replace('%%TRACK_URL%%',$trackingURL,$message);
$message=str_replace('%%SITE_TITLE%%',$hesk_settings['site_title'] ,$message);
$message=str_replace('%%SITE_URL%%',$hesk_settings['site_url'] ,$message);

/* Send the e-mail */
$headers="From: $hesk_settings[noreply_mail]\n";
$headers.="Reply-to: $hesk_settings[noreply_mail]\n";
@mail($orig_email,$hesklang['new_reply_staff'],$message,$headers);

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');

?>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['reply_added']; ?></h3>
<p>&nbsp;</p>
<p align="center"><?php
echo $hesklang['reply_submitted'].'. ';
if (!empty($_POST['close']))
{
    echo $hesklang['ticket_marked']." <font class=\"open\">$hesklang[close]</font>.";
}
?></p>
<p align="center"><a href="<?php echo $admin_trackingURL; ?>"><?php echo $hesklang['view_ticket']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</td>
</tr>
</TABLE>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();

?>
