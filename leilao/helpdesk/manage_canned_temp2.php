<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link href="../style/UrTurn2Win.css" rel="stylesheet" type="text/css" />
<link href="../style/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="style/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div align="center" style="width:100%">
		<div align="center">
<?
//require_once('inc/header.inc.php');
include('header.php');

?>
<div id="account">
<div class="style1" id="title"><B>&nbsp;&nbsp;&nbsp;Admin</B></div><br />
	<div id="feedback_center_1">
		<div class="style17" id="title" style="margin-top: -0px;"><B>&nbsp;&nbsp;&nbsp;Show Ticket</B></div>
		<div id="feedback_username">
<?
/* Connect to database */
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

hesk_session_start();
hesk_isLoggedIn();
/* Must be administrator to access this page */
hesk_isAdmin();

/* Print header */
require_once('inc/header.inc.php');

/* What should we do? */
$action=hesk_input($_REQUEST['a']);
if ($action == 'new') {new_saved();}
elseif ($action == 'edit') {edit_saved();}
elseif ($action == 'remove') {remove();}
elseif ($action == 'order') {order_saved();}

/* Print main manage users page */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<script language="Javascript" type="text/javascript"><!--
function confirm_delete()
{
if (confirm('<?php echo $hesklang['delete_saved']; ?>')) {return true;}
else {return false;}
}

function hesk_insertTag(tag) {
var text_to_insert = '%%'+tag+'%%';
hesk_insertAtCursor(document.form1.msg, text_to_insert);
document.form1.msg.focus();
}

function hesk_insertAtCursor(myField, myValue) {
if (document.selection) {
myField.focus();
sel = document.selection.createRange();
sel.text = myValue;
}
else if (myField.selectionStart || myField.selectionStart == '0') {
var startPos = myField.selectionStart;
var endPos = myField.selectionEnd;
myField.value = myField.value.substring(0, startPos)
+ myValue
+ myField.value.substring(endPos, myField.value.length);
} else {
myField.value += myValue;
}
}
//-->
</script>

<h3 align="center"><?php echo $hesklang['manage_saved']; ?></h3>

<p><?php echo $hesklang['manage_intro']; ?></p>

<div align="center">
<center>
<table border="0" cellspacing="1" cellpadding="3" class="white">
<tr>
<td class="admin_white"><?php echo $hesklang['saved_title']; ?></td>
<td class="admin_white"><?php echo $hesklang['reply_order']; ?></td>
<td class="admin_white">&nbsp;</td>
</tr>

<?php
$sql = "SELECT * FROM `hesk_std_replies` ORDER BY `reply_order` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$options='';
$javascript_messages='';
$javascript_titles='';
$trans = array_flip(get_html_translation_table(HTML_SPECIALCHARS));

$i=1;

if (hesk_dbNumRows($result) < 1) {
    echo '
    <tr>
        <td>'.$hesklang['no_saved'].'</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td> 
    </tr>
    ';
} else {
    while ($mysaved=hesk_dbFetchAssoc($result))
    {
        if ($i) {$color="admin_gray"; $i=0;}
        else {$color="admin_white"; $i=1;}

        $options .= "<option value=\"$mysaved[id]\">$mysaved[title]</option>\n";
        $javascript_messages.='myMsgTxt['.$mysaved['id'].']=\''.str_replace("\r\n","\\r\\n' + \r\n'", strtr(addslashes($mysaved['message']), $trans))."';\n";
        $javascript_titles.='myTitle['.$mysaved['id'].']=\''.strtr(addslashes($mysaved['title']), $trans)."';\n";

    echo <<<EOC
    <tr>
    <td class="$color">$mysaved[title]</td>
    <td class="$color" align="center"><a href="manage_canned.php?a=order&replyid=$mysaved[id]&move=-15">$hesklang[move_up]</a><br>
    <a href="manage_canned.php?a=order&replyid=$mysaved[id]&move=15">$hesklang[move_dn]</a></td>
    <td class="$color" align="center">
    <a href="manage_canned.php?a=remove&id=$mysaved[id]" onclick="return confirm_delete();">$hesklang[remove]</a>
    </td>
    </tr>

EOC;
    } // End while
}

?>
</table>
</center>
</div>

<script language="javascript" type="text/javascript"><!--
var myMsgTxt = new Array();
myMsgTxt[0]='';
var myTitle = new Array();
myTitle[0]='';

<?php
echo $javascript_titles;
echo $javascript_messages;
?>

function setMessage(msgid) {
    if (document.getElementById) {
        document.getElementById('HeskMsg').innerHTML='<textarea name="msg" rows="15" cols="70">'+myMsgTxt[msgid]+'</textarea>';
        document.getElementById('HeskTitle').innerHTML='<input type="text" name="name" size="40" maxlength="50" value="'+myTitle[msgid]+'">';
    } else {
        document.form1.msg.value=myMsgTxt[msgid];
        document.form1.name.value=myTitle[msgid];
    }

    if (msgid==0) {
        document.form1.a[0].checked=true;
    } else {
        document.form1.a[1].checked=true;
    }
}
//-->
</script>

<p>&nbsp;</p>

<hr width="750">

<form action="manage_canned.php" method="POST" name="form1">
<p align="center"><b><?php echo $hesklang['new_saved']; ?></b></p>

<div align="center"><center>
<table border="0">
<tr>
<td>

<p><label><input type="radio" name="a" value="new" checked> <?php echo $hesklang['canned_add']; ?></label><br>
<label><input type="radio" name="a" value="edit"> <?php echo $hesklang['canned_edit']; ?></label>:

<select name="saved_replies" onChange="setMessage(this.value)">
<option value="0"> - <?php echo $hesklang['select_empty']; ?> - </option>
<?php echo $options; ?>
</select>

<p><b><?php echo $hesklang['saved_title']; ?>:</b> <div id="HeskTitle"><input type="text" name="name" size="40" maxlength="50"></div></p>
<p><b><?php echo $hesklang['message']; ?>:</b><br>
<div id="HeskMsg"><textarea name="msg" rows="15" cols="70"></textarea></div><br>

<?php echo $hesklang['insert_special']; ?>:<br>
<a href="javascript:void(0)" onClick="hesk_insertTag('HESK_NAME')"><?php echo $hesklang['name']; ?></a> |
<a href="javascript:void(0)" onClick="hesk_insertTag('HESK_EMAIL')"><?php echo $hesklang['email']; ?></a>
<?php
if ($hesk_settings['use_custom']) {
    foreach ($hesk_settings['custom_fields'] as $k=>$v) {
        if ($v['use']) {
            echo '| <a href="javascript:void(0)" onClick="hesk_insertTag(\'HESK_'.$k.'\')">'.$v['name'].'</a>';
        }
    }
}
?>
</p>

</td>
</tr>
</table>
</center></div>

<p align="center"><input type="submit" value="<?php echo $hesklang['save_reply']; ?>" class="button"></p>
</form>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();


/*** START FUNCTIONS ***/

function edit_saved() {
global $settings, $hesklang;

$savename=hesk_Input($_POST['name'],$hesklang['ent_saved_title']);
$msg=hesk_Input($_POST['msg'],$hesklang['ent_saved_msg']);
$id=hesk_isNumber($_POST['saved_replies'],$hesklang['id_not_valid']);

$sql = "UPDATE `hesk_std_replies` SET `title`='$savename',`message`='$msg' WHERE `id`=$id LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['saved']; ?></h3>

<p>&nbsp;</p>

<p align="center"><?php echo $hesklang['your_saved']; ?>!</p>

<p align="center"><a href="manage_canned.php"><?php echo $hesklang['saved_replies']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End edit_saved()


function new_saved() {
global $settings, $hesklang;

$savename=hesk_Input($_POST['name'],$hesklang['ent_saved_title']);
$msg=hesk_Input($_POST['msg'],$hesklang['ent_saved_msg']);

/* Get the latest reply_order */
$sql = "SELECT `reply_order` FROM `hesk_std_replies` ORDER BY `reply_order` DESC LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$row = hesk_dbFetchRow($result);
$my_order = $row[0]+10;

$sql = "INSERT INTO `hesk_std_replies` (`title`,`message`,`reply_order`) VALUES ('$savename','$msg','$my_order')";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['saved']; ?></h3>

<p>&nbsp;</p>

<p align="center"><?php echo $hesklang['your_saved']; ?>!</p>

<p align="center"><a href="manage_canned.php"><?php echo $hesklang['saved_replies']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End new_saved()

function remove() {
global $settings, $hesklang;

$mysaved=hesk_isNumber($_GET['id'],$hesklang['id_not_valid']);

$sql = "DELETE FROM `hesk_std_replies` WHERE `id`=$mysaved LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[reply_not_found].");}

/* Print admin navigation */
require_once('inc/show_admin_nav.inc.php');
?>

</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['saved_removed']; ?></h3>

<p>&nbsp;</p>

<p align="center"><?php echo $hesklang['saved_rem_full']; ?>!</p>

<p align="center"><a href="manage_canned.php"><?php echo $hesklang['saved_replies']; ?></a> |
<a href="admin_main.php"><?php echo $hesklang['main_page']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<!-- HR -->
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End remove()


function order_saved() {
global $settings, $hesklang;

$replyid=hesk_isNumber($_GET['replyid'],$hesklang['reply_move_id']);
$reply_move=intval($_GET['move']);

$sql = "UPDATE `hesk_std_replies` SET `reply_order`=`reply_order`+$reply_move WHERE `id`=$replyid LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
if (hesk_dbAffectedRows() != 1) {hesk_error("$hesklang[int_error]: $hesklang[reply_not_found].");}

/* Update all category fields with new order */
$sql = "SELECT `id` FROM `hesk_std_replies` ORDER BY `reply_order` ASC";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

$i = 10;
while ($myreply=hesk_dbFetchAssoc($result))
{
    $sql = "UPDATE `hesk_std_replies` SET `reply_order`=$i WHERE `id`=$myreply[id] LIMIT 1";
    hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
    $i += 10;
}

} // End order_saved()

?>