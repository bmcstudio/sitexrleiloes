<?
	include("config/connect.php");
	include("../functions.php");
	$staticvar = "contact";
	$incpath = "";
	define('IN_SCRIPT',1);
	include($incpath."hesk_settings.inc.php");
	include($incpath.'inc/common.inc.php');
	require_once('language/'.$hesk_settings['language'].'.inc.php');
	if ($hesk_settings['secimg_use'] && $_REQUEST['a']=='add') {
        hesk_session_start();
        $_SESSION['secnum']=rand(10000,99999);
        $_SESSION['checksum']=crypt($_SESSION['secnum'],$hesk_settings['secimg_sum']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script language="Javascript" type="text/javascript" src="hesk_javascript.js"></script>
<script language="javascript">
	function check()
	{
		if(document.ticket.track.value=="")
		{
			return false;
		}	
		else
		{
			return true;
		}
	}	
</script>
</head>

<body>
	<div id="main_div">
<?
	include("header.php");
?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$hesklang['view_ticket'];?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2_static">
			<div class="staticbody">
			<div style="height: 20px;">&nbsp;</div>
			<?
				include("leftstatic.php");
			?>	
				<div class="staticright">
					<div style="text-align: right;"><a href="contact.html?a=add" class="alink"><?=$hesklang['backtomain'];?></a></div>
					<form method="GET" name="ticket" action="ticket.html">
						<p align="left"><span class="darkblue-text-17-b"><?=$hesklang['excisting'];?></span><br /><br /><b><?=$hesklang['trackID'];?>:</b>
						<table border="0">
						<tr>
						 <td width="272px">
						<input type="text" name="track" class="textboxclas" />
						<input type="hidden" name="Refresh" value="<?php echo rand(10000,99999); ?>">
						 </td>
						 <td>
						<input type="image" src="<?=$hesklang['imagepath'];?>View ticket.png" onmouseover="this.src='<?=$hesklang['imagepath'];?>View ticket-hover.png'" onmouseout="this.src='<?=$hesklang['imagepath'];?>View ticket.png'" value="View Ticket" class="button" onclick="return check();">
						 </td>
						</tr>
					    </table>
						<br /><a href="Javascript:void(0)" class="alink" onClick="Javascript:hesk_toggleLayerDisplay('forgot')"><?=$hesklang['forgot_tid'];?></a></p>
						</form>
						<div id="forgot" style="display: none; margin-top: 10px;">
						<script language="javascript" type="text/javascript"><!--
						function hesk_checkEmail() {
						d=document.form1;
						if (d.email.value=='' || d.email.value.indexOf(".") == -1 || d.email.value.indexOf("@") == -1)
						{alert('<?=$hesklang['enter_valid_email'];?>'); return false;}
						
						return true;
						}
						//-->
						</script>
						<form action="forgetID.html" method="POST" name="form1" onSubmit="return hesk_checkEmail()">
						<p style="margin-top: 5px;"><b><?=$hesklang['tid_mail'];?>:
			</b><br><br />
			<table border="0" cellspacing="0">
				<tr>
					<td width="275px">
						<input type="text" name="email" size="30" maxlength="50" class="textboxclas">
						<input type="hidden" name="a" value="forgot_tid">
					</td>
					<td>
						<input type="image" value="Send me my tracking ID" src="<?=$hesklang['imagepath'];?>Send me my tracking ID.png" onmouseout="this.src='<?=$hesklang['imagepath'];?>Send me my tracking ID.png'" onmouseover="this.src='<?=$hesklang['imagepath'];?>Send me my tracking ID_hover.png'">
					</td>
				</tr>
			</table></p>
						</form>
						</div>
				</div>
		 </div>
		 </div>
		 <div id="cleaner"></div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
<?
	include("footer.php");
?>		
</div>
</body>
</html>
