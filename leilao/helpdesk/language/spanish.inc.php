<?php
/*
* Language file for Helpdesk software Hesk
* Language: ENGLISH
* HESK Version: 0.94 @ April 23, 2007
* LANGUAGE FILE VERSION: 0.94.1 @ April 25, 2007
* Author: Klemen Stirn (http://www.PHPJunkyard.com)
*/

$hesklang['ENCODING']='ISO-8859-1';
$hesklang['lng_prefix']= "sp_";
$hesklang['imagepath'] = "images/spanish/";

/* ERROR MESSAGES */
$hesklang['cant_connect_db']='Can\'t connect to database!';
$hesklang['invalid_action']='Invalid action';
$hesklang['select_username']='Please select your username';
$hesklang['enter_pass']='Please enter your password';
$hesklang['cant_sql']='Can\'t execute SQL';
$hesklang['contact_webmsater']='Please notify webmaster at';
$hesklang['mysql_said']='MySQL said';
$hesklang['wrong_pass']='Wrong password.';
$hesklang['session_expired']='Your session has expired, please login using the form below.';
$hesklang['attempt']='Invalid attempt!';
$hesklang['not_authorized_tickets']='You are not authorized to view tickets inside this category!';
$hesklang['must_be_admin']='You are not authorized to view this page! To view this page you must be logged in as an administrator.';
$hesklang['no_session']='Can\'t start a new session!';
$hesklang['error']='Error';
$hesklang['int_error']='Interior error de secuencia de comandos';
$hesklang['no_trackID']='No hay n�mero de seguimiento';
$hesklang['status_not_valid']='estatus no v�lido';
$hesklang['trackID_not_found']='N�mero de seguimiento no ha sido encontrado';
$hesklang['enter_message']='Por favor, introduzca su mensaje';
$hesklang['select_priority']='Por favor, seleccione prioridad';
$hesklang['ticket_not_found']='Venta de entradas No se ha encontrado! Por favor, aseg�rese de que ha introducido el ID de seguimiento correcto!';
$hesklang['no_selected']='No tickets selected, nothing to delete';
$hesklang['id_not_valid']='Esto no es un n�mero de identificaci�n v�lido';
$hesklang['enter_id']='Por favor, introduzca el seguimiento de ID';
$hesklang['enter_name']='Please enter customer name';
$hesklang['enter_date']='Please enter the date you want to search in';
$hesklang['date_not_valid']='This is not a valid date. Please enter date in <b>YYYY-MM-DD</b> format.';
$hesklang['enter_subject']='Please enter ticket subject';
$hesklang['invalid_search']='Invalid search action';
$hesklang['choose_cat_ren']='Please choose a category to be renamed';
$hesklang['cat_ren_name']='Please write new category name';
$hesklang['cat_not_found']='Category not found';
$hesklang['enter_cat_name']='Please enter category name';
$hesklang['no_cat_id']='No category ID';
$hesklang['cant_del_default_cat']='You cannot delete the default category, you can only rename it';
$hesklang['no_valid_id']='No valid user ID';
$hesklang['user_not_found']='User not found';
$hesklang['enter_real_name']='Please enter user real name';
$hesklang['enter_valid_email']='Por favor, introduzca una direcci�n de correo electr�nico v�lida';
$hesklang['enter_username']='Please enter username (login)';
$hesklang['asign_one_cat']='Please asign user to al least one category!';
$hesklang['signature_long']='User signature is too long! Please limit the signature to 255 chars';
$hesklang['password_not_valid']='Password must be at least 5 chars long and can only consist of letters (a-z,A-Z) and digits (0-9)';
$hesklang['confirm_user_pass']='Please confirm password';
$hesklang['passwords_not_same']='The two passwords entered are not the same!';
$hesklang['cant_del_admin']='You cannot delete the default administrator!';
$hesklang['cant_del_own']='You cannot delete the profile you are logged in as!';
$hesklang['enter_your_name']='Por favor, introduzca su nombre';
$hesklang['enter_message']='Por favor, introduzca su mensaje';
$hesklang['sel_app_cat']='Por favor, seleccione la categor�a apropiada';
$hesklang['sel_app_priority']='Por favor seleccione la prioridad';
$hesklang['enter_ticket_subject']='Por favor, introduzca su billete de materia';
$hesklang['user_not_found_nothing_edit']='User not found or nothing to change';

/* ADMIN PANEL */
$hesklang['admin_login']='Admin login';
$hesklang['administrator']='Administrator';
$hesklang['login']='Login';
$hesklang['user']='User';
$hesklang['username']='Username';
$hesklang['pass']='Password';
$hesklang['confirm_pass']='Confirm password';
$hesklang['logged_out']='Logged out';
$hesklang['logout']='Logout';
$hesklang['logout_success']='You have been successfully logged out!';
$hesklang['click_login']='Click here to login';
$hesklang['back']='Volver atr�s';
$hesklang['displaying_pages']='Displaying <b>%d</b> tickets. Pages:';
$hesklang['trackID']='N�mero de seguimiento';
$hesklang['timestamp']='Timestamp';
$hesklang['name']='Nombre';
$hesklang['subject']='Asunto';
$hesklang['status']='Situaci�n';
$hesklang['priority']='Prioridad';
$hesklang['open']='Abierto'; // Open STATUS
$hesklang['open_action']='Abierto Ticket'; // Open ACTION
$hesklang['close']='Cerrado'; // Close STATUS
$hesklang['close_action']='Cerrado Ticket'; // Close ACTION
$hesklang['any_status']='Any status';
$hesklang['high']='Alta';
$hesklang['medium']='Media';
$hesklang['low']='Baja';
$hesklang['del_selected']='Delete selected tickets';
$hesklang['main_page']='Main page';
$hesklang['manage_users']='Manage users';
$hesklang['manage_cat']='Manage categories';
$hesklang['profile']='Your profile';
$hesklang['show_tickets']='Show tickets';
$hesklang['sort_by']='Sort by';
$hesklang['date_posted']='Date posted';
$hesklang['category']='Categor�a';
$hesklang['any_cat']='Any category';
$hesklang['order']='Order';
$hesklang['ascending']='ascending';
$hesklang['descending']='descending';
$hesklang['display']='Display';
$hesklang['tickets_page']='tickets per page';
$hesklang['find_ticket']='Find ticket';
$hesklang['find_ticket_by']='Find a ticket by';
$hesklang['yyyy_mm_dd']='YYYY-MM-DD';
$hesklang['results_page']='results per page';
$hesklang['opened']='Abierto'; // The ticket has been OPENED
$hesklang['closed']='Cerrado'; // The ticked has been CLOSED
$hesklang['ticket']='Ticket';
$hesklang['tickets']='Tickets';
$hesklang['ticket_been']='This ticket has been';
$hesklang['view_ticket']='Ver la venta de entradas';

$hesklang['excisting']	= "ver los tickets";
$hesklang['open_tickets']='Abrir tickets';
$hesklang['remove_statement']='Remove &quot;Powered by&quot; statement';
$hesklang['support_remove']='Support PHPJunkyard, buy a Hesk license and you can remove the &quot;Powered by Help desk software Hesk&quot; copyright statement from your helpdesk';
$hesklang['click_info']='Click here for more info';
$hesklang['stay_updated']='Stay updated';
$hesklang['join_news']='Join PHPJunkyard FREE newsletter and you will be notified about new scripts, new versions of the existing scripts and other important news from PHPJunkYard';
$hesklang['rate_script']='Rate this script';
$hesklang['please_rate']='If you like this script please rate it or even write a review at';
$hesklang['new_reply_staff']='New reply to your support ticket';
$hesklang['reply_added']='Reply added';
$hesklang['reply_submitted']='Su respuesta ha sido transmitido';
$hesklang['ticket_marked']='This ticket has been marked';
$hesklang['ticket_status']='Estatus del ticket';
$hesklang['replies']='Responder';
$hesklang['date']='Fecha';
$hesklang['email']='E-mail';
$hesklang['ip']='IP';
$hesklang['message']='Informaci�n adicional';
$hesklang['add_reply']='A�adir respuesta';
$hesklang['close_this_ticket']='Close this ticket';
$hesklang['change_priority']='Change priority to';
$hesklang['attach_sign']='Attach signature';
$hesklang['profile_settings']='Profile settings';
$hesklang['submit_reply']='Submit reply';
$hesklang['support_panel']='Support panel';
$hesklang['ticket_trackID']='Venta de entradas de seguimiento de ID';
$hesklang['c2c']='Click to continue';
$hesklang['tickets_deleted']='Tickets deleted';
$hesklang['num_tickets_deleted']='<b>%d</b> tickets have been deleted';
$hesklang['tickets_found']='Tickets found';
$hesklang['found_num_tickets']='Found <b>%d</b> tickets. Pages:';
$hesklang['confirm_del_cat']='Are you sure you want to remove this category?';
$hesklang['cat_intro']='Here you are able to manage categories. Categories are useful
for categorizing tickets by relevance (for example &quot;Sales&quot;,
&quot;Hardware problems&quot;, &quot;PHP/MySQL problems&quot; etc) and for
assigning users to categories (for example that your sales person can only view
tickets posted to &quot;Sales&quot; category)';
$hesklang['cat_name']='Category name';
$hesklang['remove']='Remove';
$hesklang['add_cat']='Add new category';
$hesklang['max_chars']='max 40 chars';
$hesklang['create_cat']='Create category';
$hesklang['ren_cat']='Rename category';
$hesklang['to']='to';
$hesklang['cat_added']='Category added';
$hesklang['cat_name_added']='Category %s has been successfully added';
$hesklang['cat_renamed']='Category renamed';
$hesklang['cat_renamed_to']='Selected category has been successfully renamed to';
$hesklang['cat_removed']='Category removed';
$hesklang['cat_removed_db']='Selected category has been successfully removed from the database';
$hesklang['sure_remove_user']='Are you sure you want to remove this user?';
$hesklang['manage_users']='Manage users';
$hesklang['users_intro']='Here you are able to manage users who can login to the admin panel and
answer tickets. Administrators can view/edit tickets in any category and have access
to all functions of the admin panel (manage users, manage categories, ...) while
other users may only view and reply to tickets within their categories.';
$hesklang['yes']='YES';
$hesklang['no']='NO';
$hesklang['edit']='Edit';
$hesklang['add_user']='Add new user';
$hesklang['req_marked_with']='Required fields are marked with';
$hesklang['real_name']='Real name';
$hesklang['allowed_cat']='Allowed categories';
$hesklang['signature_max']='Signature (max 255 chars)';
$hesklang['sign_extra']='HTML code is not allowed. Links will be clickable.';
$hesklang['create_user']='Create user';
$hesklang['editing_user']='Editing user';
$hesklang['user_added']='User added';
$hesklang['user_added_success']='New user %s with password %s has been successfully added';
$hesklang['profile_updated']='Profile updated';
$hesklang['profile_updated_success']='User profile has been successfully updated';
$hesklang['user_removed']='User removed';
$hesklang['sel_user_removed']='Selected user has been successfully removed from the database';
$hesklang['profile_for']='Profile for';
$hesklang['new_pass']='New password';
$hesklang['update_profile']='Update profile';
$hesklang['notify_new_posts']='Notify me of new tickets and posts within my categories';
$hesklang['profile_updated']='Profile updated';
$hesklang['profile_updated_success']='Your profile has been successfully updated';
$hesklang['view_profile']='View profile';
$hesklang['new_reply_ticket']='Nueva respuesta a la incidencia';
$hesklang['new_ticket_submitted']='Nuevo billete de apoyo presentado';
$hesklang['user_profile_updated_success']='This user profile has been updated successfully';
$hesklang['printer_friendly']='Versi�n para imprimir';
$hesklang['end_ticket']='--- End of ticket ---';

/* CUSTOMER INTERFACE */
$hesklang['your_ticket_been']='Su ticket ha sido';
$hesklang['view_your_ticket']='Ver su ticket';
$hesklang['submit_ticket']='Enviar una solicitud de venta de entradas';
$hesklang['sub_ticket']='Enviar billete';
$hesklang['sub_support']='Enviar una incidencia';
$hesklang['use_form_below']='Por favor, utilice el siguiente formulario para enviarnos una solicitud con n�mero de ticket. Los campos obligatorios est�n marcados con <font class="red-text-12-b">*</font>';
$hesklang['before_submit']='Antes de enviar, por favor aseg�rese de que';
$hesklang['all_info_in']='toda la informaci�n necesaria se haya llenado';
$hesklang['all_error_free']='toda la informaci�n sea correcta y libre de errores';
$hesklang['we_have']='Hemos';
$hesklang['recorded_ip']='registrada como su direcci�n IP';
$hesklang['recorded_time']='registrada el momento de su presentaci�n';
$hesklang['open_ticket']='Abra un nuevo billete';
$hesklang['view_existing']='ver los tickets';
$hesklang['save_changes']='Save changes';
$hesklang['reply_submitted']='Responder presentado';
$hesklang['reply_submitted_success']='Su respuesta a este ticket ha sido transmitido con �xito.';
$hesklang['view_your_ticket']='Ver su ticket';
$hesklang['ticket_received']='Su apoyo recibido boleto';
$hesklang['ticket_submitted']='Venta de entradas presentado';
$hesklang['ticket_submitted_success']='Su billete se ha presentado con �xito! Venta de entradas ID';
$hesklang['your_ticket']='Your ticket';


/* ADDED IN HESK VERSION 0.94 */
$hesklang['check_updates']='Make sure you always have installed the latest version of Hesk!';
$hesklang['check4updates']='Check for updates';
$hesklang['open']='New';
$hesklang['wait_reply']='Esperando respuesta';
$hesklang['wait_staff_reply']='Se espera la respuesta del empleado';
$hesklang['wait_cust_reply']='Se espera la respuesta del cliente';
$hesklang['replied']='Respondi�';
$hesklang['closed']='Resuelta';
$hesklang['last_update']='�ltima actualizaci�n';
$hesklang['last_replier']='�ltima respuesta';
$hesklang['staff']='Personal';
$hesklang['customer']='Customer';
$hesklang['archived']='Archived';
$hesklang['close_selected']='Mark selected tickets Resolved';
$hesklang['execute']='Execute';
$hesklang['saved_replies']='Canned responses';
$hesklang['manage_saved']='Canned responses';
$hesklang['manage_intro']='Here you can add and manage canned responses. These are commonly used replies which are more or less the same for every customer. You should use canned responses to avoid typing the same reply to different customers numerous times.';
$hesklang['saved_title']='Canned response title';
$hesklang['no_saved']='No canned responses';
$hesklang['delete_saved']='Are you sure you want to delete this canned response?';
$hesklang['new_saved']='Add or Edit a canned response';
$hesklang['canned_add']='Create a new canned response';
$hesklang['canned_edit']='Edit selected canned response';
$hesklang['saved_title']='Title (max 50 chars)';
$hesklang['save_reply']='Save response';
$hesklang['saved']='Response saved';
$hesklang['your_saved']='Your canned response has been saved for future use';
$hesklang['ent_saved_title']='Please enter reply title';
$hesklang['ent_saved_msg']='Please enter reply message';
$hesklang['saved_removed']='Canned response removed';
$hesklang['saved_rem_full']='Selected canned response has been removed from the database';
$hesklang['clip_alt']='This post has attachments';
$hesklang['attachments']='Ap�ndice';
$hesklang['accepted_types']='Aceptada archivo';
$hesklang['max_file_size']='Tama�o m�ximo';
$hesklang['fill_all']='Issing campo obligatorio';
$hesklang['file_too_large']='Your file %s is too large';
$hesklang['type_not_allowed']='Invalid file type (%s)';
$hesklang['cannot_move_tmp']='Cannot move file to the attachments folder, please check your server path settings';
$hesklang['created_on']='Creado en';
$hesklang['add_archive']='Add to archive';
$hesklang['remove_archive']='Remove from archive';
$hesklang['added_archive']='Added to archive';
$hesklang['removed_archive']='Removed from archive';
$hesklang['added2archive']='Ticket has been added to archive';
$hesklang['removedfromarchive']='Ticket has been removed from archive';
$hesklang['tickets_closed']='Tickets closed';
$hesklang['num_tickets_closed']='<b>%d</b> tickets have been closed';
$hesklang['select_saved']='Select a canned response';
$hesklang['select_empty']='Select / Empty';
$hesklang['insert_special']='Insert special tag (will be replaced with customer info)';
$hesklang['move_to_catgory']='Move ticket to';
$hesklang['move']='Move';
$hesklang['select']=' - Select - ';
$hesklang['moved']='Ticket moved';
$hesklang['moved_to']='This ticket has been moved to the new category';
$hesklang['licensed']='Licensed version';
$hesklang['licensed_to']='This copy of Hesk is licensed to';
$hesklang['url']='URL';
$hesklang['all_not_closed']='All but closed';
$hesklang['disp_only_archived']='Display only archived tickets';
$hesklang['search_only_archived']='Search only within archived tickets';
$hesklang['chg_all']='Change all';
$hesklang['settings']='Settings';
$hesklang['settings_intro']='Use this tool to configure your help desk. For more information about all settings and options click the help sign or refer to the readme.htm file.';
$hesklang['all_req']='All fields (except disabled ones) are required!';
$hesklang['wbst_title']='Website title';
$hesklang['wbst_url']='Website URL';
$hesklang['email_sup']='Support e-mail';
$hesklang['email_wm']='Webmaster e-mail';
$hesklang['email_noreply']='No reply e-mail';
$hesklang['hesk_url']='Hesk URL';
$hesklang['hesk_title']='Hesk title';
$hesklang['max_listings']='Listings per page';
$hesklang['hesk_lang']='Language';
$hesklang['print_size']='Print font size';
$hesklang['debug_mode']='Debug mode';
$hesklang['on']='ON';
$hesklang['off']='OFF';
$hesklang['use_secimg']='Use anti-SPAM image';
$hesklang['secimg_no']='Not available';
$hesklang['attach_use']='Use attachments';
$hesklang['attach_num']='Number per post';
$hesklang['attach_size']='File size limit (Kb)';
$hesklang['attach_type']='Allowed file types';
$hesklang['custom_use']='Enable custom fields';
$hesklang['custom_place']='Custom field location';
$hesklang['place_after']='After Message';
$hesklang['place_before']='Before Message';
$hesklang['custom_f']='Custom field';
$hesklang['custom_u']='Use this field';
$hesklang['custom_r']='Required field (uncheck for optional field)';
$hesklang['custom_n']='Field name';
$hesklang['custom_l']='Maximum length (chars)';
$hesklang['hesk_path']='System/root path';
$hesklang['db_host']='Database host';
$hesklang['db_name']='Database name';
$hesklang['db_user']='Database username';
$hesklang['db_pass']='Database password';
$hesklang['err_sname']='Please enter your website title';
$hesklang['err_surl']='Please enter your website URL. Make sure it is a valid URL (start with http:// or https://)';
$hesklang['err_supmail']='Please enter a valid support e-mail';
$hesklang['err_wmmail']='Please enter a valid webmaster e-mail';
$hesklang['err_nomail']='Please enter a valid noreply e-mail';
$hesklang['err_htitle']='Please enter the title of your support desk';
$hesklang['err_hurl']='Please enter your Hesk folder url. Make sure it is a valid URL (start with http:// or https://)';
$hesklang['err_spath']='Please enter the system (root, server) path to Hesk folder';
$hesklang['err_lang']='Please select Hesk language';
$hesklang['err_nolang']='The language file specified doesn\'t exist in the languag folder! Make sure the file is uploaded before changing the language setting.';
$hesklang['err_max']='Please enter maximum listings displayed per page';
$hesklang['err_psize']='Please enter the print font size';
$hesklang['err_dbhost']='Please enter your MySQL database host';
$hesklang['err_dbname']='Please enter your MySQL database name';
$hesklang['err_dbuser']='Please enter your MySQL database username';
$hesklang['err_dbpass']='Please enter your MySQL database password';
$hesklang['err_dbconn']='Could not connect to MySQL database using provided information! Please double-check your database settings';
$hesklang['err_dbsele']='Could not select MySQL database, please double-check database NAME';
$hesklang['err_custname']='Please enter name(s) for selected optional field(s)';
$hesklang['err_openset']='Can\'t open file <b>hesk_settings.inc.php</b> for writing. Please CHMOD this file to 666 (rw-rw-rw-)';
$hesklang['set_saved']='Settings saved';
$hesklang['set_were_saved']='Your settings have been successfully saved';
$hesklang['sec_img']='Security image';
$hesklang['sec_enter']='Por favor, introduce el n�mero de arriba';
$hesklang['sec_miss']='Por favor, introduzca el n�mero de la seguridad';
$hesklang['sec_wrng']='�Por favor, introduce el c�digo de seguridad!';
$hesklang['submit_problems']='Por favor, volver atr�s y corregir los siguientes problemas';
$hesklang['cat_order']='Category order';
$hesklang['reply_order']='Canned response order';
$hesklang['move_up']='Move up';
$hesklang['move_dn']='Move down';
$hesklang['cat_move_id']='Missing category ID';
$hesklang['reply_move_id']='Missing canned response ID';
$hesklang['forgot_tid']='�Olvidado n�mero de seguimiento?';
$hesklang['tid_mail']='Por favor, introduzca su direcci�n de correo electr�nico';
$hesklang['tid_send']='Send me my tracking ID';
$hesklang['tid_not_found']='No hay tickets con su direcci�n de correo electr�nico';
$hesklang['tid_email_subject']='Support ticket reminder';
$hesklang['tid_sent']='N�mero de seguimiento ha sido enviado';
$hesklang['tid_sent2']='Un e-mail con detalles acerca de sus entradas se ha enviado a su direcci�n';
$hesklang['check_spambox']='<b>�Por favor, compruebe si el correo electr�nico aterriz� accidentalmente en la carpeta de spam!</b>';
$hesklang['reply_not_found']='Canned response not found';
$hesklang['check_status']='Checking status of required files and folders';
$hesklang['exists']='Exists';
$hesklang['no_exists']='Doesn\'t exist';
$hesklang['writable']='Writable';
$hesklang['not_writable']='Not writable';
$hesklang['disabled']='disabled';
$hesklang['e_settings']='You will not be able to save your settings unless this file is writable by the script. Please refer to the readme file for further instructions!';
$hesklang['e_attdir']='You will not be able to file attachments unless this exists and is writable by the script. Please refer to the readme file for further instructions!';
$hesklang['e_save_settings']='Unable to save your settings because <b>hesk_settings.inc.php</b> file is not writable by the script.';
$hesklang['e_attach']='Disabled because your <b>attachments</b> directory is not writable by the script.';
$hesklang['tickets_on_pages']='%d tickets displayed on %d pages. Jump to page'; // First %d is replaced with number of tickets, second %d with number of pages
$hesklang['go']='Ir';
$hesklang['prefix_select'] = "sp_";
$hesklang['about_foot'] = "Sobre nosotros";
$hesklang['contact_foot'] = "Contactar con Snabid";
$hesklang['privacy_foot'] = "Pol�tica de privacidad";
$hesklang['jobs_foot'] = "Jobs";
$hesklang['termscondi_foot'] = "T�rminos y condiciones";
$hesklang['help_foot'] = "Ayuda";
$hesklang['paysafe_foot'] = "Paga con seguridad en Snabid";

$hesklang['ticketreply']	= "Responder Ticket";
$hesklang['snabidsupport']	= "Snabid ayuda & Supportcenter";


/* This is left side menu */
$hesklang['leftmenu_aboutus']	= "Sobre nosotros";
$hesklang['leftmenu_contact']	= "Contactar con Snabid";
$hesklang['leftmenu_jobs']		= "Jobs";
$hesklang['leftmenu_termscondi']= "T�rminos y condiciones";
$hesklang['leftmenu_privacy']	= "Pol�tica de privacidad";
$hesklang['leftmenu_tabhelp']	= "Ayuda";
$hesklang['leftmenu_howitwork'] = "�C�mo funciona?";


/* This is header menu */
$hesklang['headermenu_tabhome']		= "Inicio";
$hesklang['headermenu_tabmyavenue']	= "Mi Snabid";
$hesklang['headermenu_logout'] 		= "Cerrar";
$hesklang['headermenu_welcome']		= "Bienvenido";

$hesklang['regforfree2'] = "<a href='../registration.html' class='register'>Reg�strate gratis</a>&nbsp;o&nbsp;<a href='../login.html' class='register'>Entrar</a>";
$hesklang['availablebids']	= "Ofertas disponibles";

/* This is view category menu*/
$hesklang['allliveauc']		= "Todas las subastas activas";
$hesklang['futureaucs']		= "Futuras subastas";
$hesklang['endedauc']		= "Subastas concluidas";

/* This is footer*/
$hesklang['paysafe']		= "Paga con seguridad en Snabid";
$hesklang['backtomain']		= 'Volver a la p�gina principal';



/* DO NOT CHANGE BELOW */



if (!defined('IN_SCRIPT')) {echo "No syntax errors detected in $_SERVER[PHP_SELF]";exit();}
?>
