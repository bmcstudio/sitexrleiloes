<?php
/*
* Language file for Helpdesk software Hesk
* Language: ENGLISH
* HESK Version: 0.94 @ April 23, 2007
* LANGUAGE FILE VERSION: 0.94.1 @ April 25, 2007
* Author: Klemen Stirn (http://www.PHPJunkyard.com)
*/


$hesklang['ENCODING']			='ISO-8859-1';
$hesklang['lng_prefix']			= "gr_";
$hesklang['imagepath'] = "images/german/";

/* ERROR MESSAGES */
$hesklang['cant_connect_db']='Can\'t connect to database!';
$hesklang['invalid_action']='Invalid action';
$hesklang['select_username']='Please select your username';
$hesklang['enter_pass']='Please enter your password';
$hesklang['cant_sql']='Can\'t execute SQL';
$hesklang['contact_webmsater']='Please notify webmaster at';
$hesklang['mysql_said']='MySQL said';
$hesklang['wrong_pass']='Wrong password.';
$hesklang['session_expired']='Your session has expired, please login using the form below.';
$hesklang['attempt']='Invalid attempt!';
$hesklang['not_authorized_tickets']='You are not authorized to view tickets inside this category!';
$hesklang['must_be_admin']='You are not authorized to view this page! To view this page you must be logged in as an administrator.';
$hesklang['no_session']='Can\'t start a new session!';
$hesklang['error']='Fehler';
$hesklang['int_error']='Interne Skript-Fehler';
$hesklang['no_trackID']='Keine Tracking-ID';
$hesklang['status_not_valid']='Status ung�ltig';
$hesklang['trackID_not_found']='Tracking-ID wurde nicht gefunden';
$hesklang['enter_message']='Bitte geben Sie Ihre Nachricht ein';
$hesklang['select_priority']='Bitte w�hlen Sie Priorit�t';
$hesklang['ticket_not_found']='Ticket nicht gefunden! Bitte stellen Sie sicher, Sie haben die richtigen Tracking-ID!';
$hesklang['no_selected']='No tickets selected, nothing to delete';
$hesklang['id_not_valid']='Dies ist keine g�ltige ID';
$hesklang['enter_id']='Bitte geben Sie Tracking-ID';
$hesklang['enter_name']='Please enter customer name';
$hesklang['enter_date']='Please enter the date you want to search in';
$hesklang['date_not_valid']='This is not a valid date. Please enter date in <b>YYYY-MM-DD</b> format.';
$hesklang['enter_subject']='Bitte geben Sie unterliegt Fahrkarte';
$hesklang['invalid_search']='Invalid search action';
$hesklang['choose_cat_ren']='Please choose a category to be renamed';
$hesklang['cat_ren_name']='Please write new category name';
$hesklang['cat_not_found']='Category not found';
$hesklang['enter_cat_name']='Please enter category name';
$hesklang['no_cat_id']='No category ID';
$hesklang['cant_del_default_cat']='You cannot delete the default category, you can only rename it';
$hesklang['no_valid_id']='No valid user ID';
$hesklang['user_not_found']='User not found';
$hesklang['enter_real_name']='Please enter user real name';
$hesklang['enter_valid_email']='Bitte geben Sie eine g�ltige E-Mail-Adresse';
$hesklang['enter_username']='Please enter username (login)';
$hesklang['asign_one_cat']='Please asign user to al least one category!';
$hesklang['signature_long']='User signature is too long! Please limit the signature to 255 chars';
$hesklang['password_not_valid']='Password must be at least 5 chars long and can only consist of letters (a-z,A-Z) and digits (0-9)';
$hesklang['confirm_user_pass']='Please confirm password';
$hesklang['passwords_not_same']='The two passwords entered are not the same!';
$hesklang['cant_del_admin']='You cannot delete the default administrator!';
$hesklang['cant_del_own']='You cannot delete the profile you are logged in as!';
$hesklang['enter_your_name']='Bitte geben Sie Ihren Namen';
$hesklang['enter_message']='Bitte geben Sie Ihre Nachricht ein';
$hesklang['sel_app_cat']='Bitte w�hlen Sie die entsprechende Kategorie';
$hesklang['sel_app_priority']='Bitte w�hlen Sie die entsprechende Priorit�t';
$hesklang['enter_ticket_subject']='Bitte geben Sie Ihre Tickets unter';
$hesklang['user_not_found_nothing_edit']='User not found or nothing to change';

/* ADMIN PANEL */
$hesklang['admin_login']='Admin login';
$hesklang['administrator']='Administrator';
$hesklang['login']='Login';
$hesklang['user']='User';
$hesklang['username']='Username';
$hesklang['pass']='Password';
$hesklang['confirm_pass']='Confirm password';
$hesklang['logged_out']='Logged out';
$hesklang['logout']='Logout';
$hesklang['logout_success']='You have been successfully logged out!';
$hesklang['click_login']='Click here to login';
$hesklang['back']='Gehen Sie zur�ck';
$hesklang['displaying_pages']='Displaying <b>%d</b> tickets. Pages:';
$hesklang['trackID']='Tracking-ID';
$hesklang['timestamp']='Timestamp';
$hesklang['name']='Name';
$hesklang['subject']='Betreff';
$hesklang['status']='Status';
$hesklang['priority']='Priorit�t';
$hesklang['open']='Ge�ffnet'; // Open STATUS
$hesklang['open_action']='Open ticket'; // Open ACTION
$hesklang['close']='Closed'; // Close STATUS
$hesklang['close_action']='Schlie�en Ticket'; // Close ACTION
$hesklang['any_status']='Any status';
$hesklang['high']='Hoch';
$hesklang['medium']='Mittel';
$hesklang['low']='Niedrig';
$hesklang['del_selected']='Delete selected tickets';
$hesklang['main_page']='Main page';
$hesklang['manage_users']='Manage users';
$hesklang['manage_cat']='Manage categories';
$hesklang['profile']='Your profile';
$hesklang['show_tickets']='Show tickets';
$hesklang['sort_by']='Sort by';
$hesklang['date_posted']='Date posted';
$hesklang['category']='Kategorie';
$hesklang['any_cat']='Any category';
$hesklang['order']='Order';
$hesklang['ascending']='ascending';
$hesklang['descending']='descending';
$hesklang['display']='Display';
$hesklang['tickets_page']='tickets per page';
$hesklang['find_ticket']='Find ticket';
$hesklang['find_ticket_by']='Find a ticket by';
$hesklang['yyyy_mm_dd']='YYYY-MM-DD';
$hesklang['results_page']='results per page';
$hesklang['opened']='Ge�ffnet'; // The ticket has been OPENED
$hesklang['closed']='Geschlossen'; // The ticked has been CLOSED
$hesklang['ticket']='Ticket';
$hesklang['tickets']='Tickets';
$hesklang['ticket_been']='This ticket has been';
$hesklang['view_ticket']='Ticket ansehen';
$hesklang['excisting']	= "Bestehende Tickets ansehen";
$hesklang['open_tickets']='Open tickets';
$hesklang['remove_statement']='Remove &quot;Powered by&quot; statement';
$hesklang['support_remove']='Support PHPJunkyard, buy a Hesk license and you can remove the &quot;Powered by Help desk software Hesk&quot; copyright statement from your helpdesk';
$hesklang['click_info']='Click here for more info';
$hesklang['stay_updated']='Stay updated';
$hesklang['join_news']='Join PHPJunkyard FREE newsletter and you will be notified about new scripts, new versions of the existing scripts and other important news from PHPJunkYard';
$hesklang['rate_script']='Rate this script';
$hesklang['please_rate']='If you like this script please rate it or even write a review at';
$hesklang['new_reply_staff']='New reply to your support ticket';
$hesklang['reply_added']='Reply added';
$hesklang['reply_submitted']='Ihre Antwort wurde �bermittelt';
$hesklang['ticket_marked']='This ticket has been marked';
$hesklang['ticket_status']='Ticket Status';
$hesklang['replies']='Antworten';
$hesklang['date']='Datum';
$hesklang['email']='E-mail';
$hesklang['ip']='IP';
$hesklang['message']='Nachricht';
$hesklang['add_reply']='Antwort hinzuf�gen';
$hesklang['close_this_ticket']='Close this ticket';
$hesklang['change_priority']='Change priority to';
$hesklang['attach_sign']='Attach signature';
$hesklang['profile_settings']='Profile settings';
$hesklang['submit_reply']='Submit reply';
$hesklang['support_panel']='Support panel';
$hesklang['ticket_trackID']='Ticket-Tracking-ID';
$hesklang['c2c']='Click to continue';
$hesklang['tickets_deleted']='Tickets deleted';
$hesklang['num_tickets_deleted']='<b>%d</b> tickets have been deleted';
$hesklang['tickets_found']='Tickets found';
$hesklang['found_num_tickets']='Found <b>%d</b> tickets. Pages:';
$hesklang['confirm_del_cat']='Are you sure you want to remove this category?';
$hesklang['cat_intro']='Here you are able to manage categories. Categories are useful
for categorizing tickets by relevance (for example &quot;Sales&quot;,
&quot;Hardware problems&quot;, &quot;PHP/MySQL problems&quot; etc) and for
assigning users to categories (for example that your sales person can only view
tickets posted to &quot;Sales&quot; category)';
$hesklang['cat_name']='Category name';
$hesklang['remove']='Remove';
$hesklang['add_cat']='Add new category';
$hesklang['max_chars']='max 40 chars';
$hesklang['create_cat']='Create category';
$hesklang['ren_cat']='Rename category';
$hesklang['to']='to';
$hesklang['cat_added']='Category added';
$hesklang['cat_name_added']='Category %s has been successfully added';
$hesklang['cat_renamed']='Category renamed';
$hesklang['cat_renamed_to']='Selected category has been successfully renamed to';
$hesklang['cat_removed']='Category removed';
$hesklang['cat_removed_db']='Selected category has been successfully removed from the database';
$hesklang['sure_remove_user']='Are you sure you want to remove this user?';
$hesklang['manage_users']='Manage users';
$hesklang['users_intro']='Here you are able to manage users who can login to the admin panel and
answer tickets. Administrators can view/edit tickets in any category and have access
to all functions of the admin panel (manage users, manage categories, ...) while
other users may only view and reply to tickets within their categories.';
$hesklang['yes']='YES';
$hesklang['no']='NO';
$hesklang['edit']='Edit';
$hesklang['add_user']='Add new user';
$hesklang['req_marked_with']='Required fields are marked with';
$hesklang['real_name']='Real name';
$hesklang['allowed_cat']='Allowed categories';
$hesklang['signature_max']='Signature (max 255 chars)';
$hesklang['sign_extra']='HTML code is not allowed. Links will be clickable.';
$hesklang['create_user']='Create user';
$hesklang['editing_user']='Editing user';
$hesklang['user_added']='User added';
$hesklang['user_added_success']='New user %s with password %s has been successfully added';
$hesklang['profile_updated']='Profile updated';
$hesklang['profile_updated_success']='User profile has been successfully updated';
$hesklang['user_removed']='User removed';
$hesklang['sel_user_removed']='Selected user has been successfully removed from the database';
$hesklang['profile_for']='Profile for';
$hesklang['new_pass']='New password';
$hesklang['update_profile']='Update profile';
$hesklang['notify_new_posts']='Notify me of new tickets and posts within my categories';
$hesklang['profile_updated']='Profile updated';
$hesklang['profile_updated_success']='Your profile has been successfully updated';
$hesklang['view_profile']='View profile';
$hesklang['new_reply_ticket']='Neue Antwort auf Support-Ticket';
$hesklang['new_ticket_submitted']='Neue Support-Ticket vorgelegt';
$hesklang['user_profile_updated_success']='This user profile has been updated successfully';
$hesklang['printer_friendly']='Druckversion';
$hesklang['end_ticket']='--- End of ticket ---';

/* CUSTOMER INTERFACE */
$hesklang['your_ticket_been']='Ihr Ticket wurde';
$hesklang['view_your_ticket']='Ihr Ticket ansehen';
$hesklang['submit_ticket']='Eine Ticketanfrage senden';
$hesklang['sub_ticket']='Submit ticket';
$hesklang['sub_support']='Senden ein Support-Ticket';
$hesklang['use_form_below']='Bitte verwenden Sie das unten stehende Formular, um uns eine Anfrage mit Ticketnummer zu senden. Plichtfelder sind markiert mit einem <font class="red-text-12-b">*</font>';
$hesklang['before_submit']='Bevor Sie das Formular absenden, vergewissern Sie sich bitte noch einmal dass';
$hesklang['all_info_in']='Alle ben�tigten Informationen ausgef�llt wurden';
$hesklang['all_error_free']='Alle Informationen richtig sind und keine Fehler beinhalten';
$hesklang['we_have']='Wir haben';
$hesklang['recorded_ip']='als Ihre IP-Adresse';
$hesklang['recorded_time']='erfasst den Zeitpunkt Ihrer Vorlage';
$hesklang['open_ticket']='�ffnen Sie ein neues Ticket';
$hesklang['view_existing']='Bestehende Tickets ansehen';
$hesklang['save_changes']='Save changes';
$hesklang['reply_submitted']='Antwort vorgelegt';
$hesklang['reply_submitted_success']='Ihre Antwort zu diesem Ticket wurde erfolgreich �bermittelt!';
$hesklang['view_your_ticket']='Ihr Ticket ansehen';
$hesklang['ticket_received']='Ihr Support-Ticket erhalten';
$hesklang['ticket_submitted']='Ticket vorgelegt';
$hesklang['ticket_submitted_success']='Ihr Ticket wurde erfolgreich gesendet! Ticket-ID';
$hesklang['your_ticket']='Your ticket';


/* ADDED IN HESK VERSION 0.94 */
$hesklang['check_updates']='Make sure you always have installed the latest version of Hesk!';
$hesklang['check4updates']='Check for updates';
$hesklang['open']='New';
$hesklang['wait_reply']='Warten Antwort';
$hesklang['wait_staff_reply']='Antwort vom Mitarbeiter erwartet';
$hesklang['wait_cust_reply']='Antwort vom Kunden erwartet';
$hesklang['replied']='Antwortete';
$hesklang['closed']='Gel�st';
$hesklang['last_update']='Letztes Update';
$hesklang['last_replier']='Letzte Antwort';
$hesklang['staff']='Personal';
$hesklang['customer']='Customer';
$hesklang['archived']='Archived';
$hesklang['close_selected']='Mark selected tickets Resolved';
$hesklang['execute']='Execute';
$hesklang['saved_replies']='Canned responses';
$hesklang['manage_saved']='Canned responses';
$hesklang['manage_intro']='Here you can add and manage canned responses. These are commonly used replies which are more or less the same for every customer. You should use canned responses to avoid typing the same reply to different customers numerous times.';
$hesklang['saved_title']='Canned response title';
$hesklang['no_saved']='No canned responses';
$hesklang['delete_saved']='Are you sure you want to delete this canned response?';
$hesklang['new_saved']='Add or Edit a canned response';
$hesklang['canned_add']='Create a new canned response';
$hesklang['canned_edit']='Edit selected canned response';
$hesklang['saved_title']='Title (max 50 chars)';
$hesklang['save_reply']='Save response';
$hesklang['saved']='Response saved';
$hesklang['your_saved']='Your canned response has been saved for future use';
$hesklang['ent_saved_title']='Please enter reply title';
$hesklang['ent_saved_msg']='Please enter reply message';
$hesklang['saved_removed']='Canned response removed';
$hesklang['saved_rem_full']='Selected canned response has been removed from the database';
$hesklang['clip_alt']='This post has attachments';
$hesklang['attachments']='Anh�nge';
$hesklang['accepted_types']='Akzeptierte Dateien';
$hesklang['max_file_size']='Max. Dateigr��e';
$hesklang['fill_all']='Fehlende Felder m�ssen ausgef�llt werden';
$hesklang['file_too_large']='Your file %s is too large';
$hesklang['type_not_allowed']='Invalid file type (%s)';
$hesklang['cannot_move_tmp']='Cannot move file to the attachments folder, please check your server path settings';
$hesklang['created_on']='Erstellt am';
$hesklang['add_archive']='Add to archive';
$hesklang['remove_archive']='Remove from archive';
$hesklang['added_archive']='Added to archive';
$hesklang['removed_archive']='Removed from archive';
$hesklang['added2archive']='Ticket has been added to archive';
$hesklang['removedfromarchive']='Ticket has been removed from archive';
$hesklang['tickets_closed']='Tickets closed';
$hesklang['num_tickets_closed']='<b>%d</b> tickets have been closed';
$hesklang['select_saved']='Select a canned response';
$hesklang['select_empty']='Select / Empty';
$hesklang['insert_special']='Insert special tag (will be replaced with customer info)';
$hesklang['move_to_catgory']='Move ticket to';
$hesklang['move']='Move';
$hesklang['select']=' - Select - ';
$hesklang['moved']='Ticket moved';
$hesklang['moved_to']='This ticket has been moved to the new category';
$hesklang['licensed']='Licensed version';
$hesklang['licensed_to']='This copy of Hesk is licensed to';
$hesklang['url']='URL';
$hesklang['all_not_closed']='All but closed';
$hesklang['disp_only_archived']='Display only archived tickets';
$hesklang['search_only_archived']='Search only within archived tickets';
$hesklang['chg_all']='Change all';
$hesklang['settings']='Settings';
$hesklang['settings_intro']='Use this tool to configure your help desk. For more information about all settings and options click the help sign or refer to the readme.htm file.';
$hesklang['all_req']='All fields (except disabled ones) are required!';
$hesklang['wbst_title']='Website title';
$hesklang['wbst_url']='Website URL';
$hesklang['email_sup']='Support e-mail';
$hesklang['email_wm']='Webmaster e-mail';
$hesklang['email_noreply']='No reply e-mail';
$hesklang['hesk_url']='Hesk URL';
$hesklang['hesk_title']='Hesk title';
$hesklang['max_listings']='Listings per page';
$hesklang['hesk_lang']='Language';
$hesklang['print_size']='Print font size';
$hesklang['debug_mode']='Debug mode';
$hesklang['on']='ON';
$hesklang['off']='OFF';
$hesklang['use_secimg']='Use anti-SPAM image';
$hesklang['secimg_no']='Not available';
$hesklang['attach_use']='Use attachments';
$hesklang['attach_num']='Number per post';
$hesklang['attach_size']='File size limit (Kb)';
$hesklang['attach_type']='Allowed file types';
$hesklang['custom_use']='Enable custom fields';
$hesklang['custom_place']='Custom field location';
$hesklang['place_after']='After Message';
$hesklang['place_before']='Before Message';
$hesklang['custom_f']='Custom field';
$hesklang['custom_u']='Use this field';
$hesklang['custom_r']='Required field (uncheck for optional field)';
$hesklang['custom_n']='Field name';
$hesklang['custom_l']='Maximum length (chars)';
$hesklang['hesk_path']='System/root path';
$hesklang['db_host']='Database host';
$hesklang['db_name']='Database name';
$hesklang['db_user']='Database username';
$hesklang['db_pass']='Database password';
$hesklang['err_sname']='Please enter your website title';
$hesklang['err_surl']='Please enter your website URL. Make sure it is a valid URL (start with http:// or https://)';
$hesklang['err_supmail']='Please enter a valid support e-mail';
$hesklang['err_wmmail']='Please enter a valid webmaster e-mail';
$hesklang['err_nomail']='Please enter a valid noreply e-mail';
$hesklang['err_htitle']='Please enter the title of your support desk';
$hesklang['err_hurl']='Please enter your Hesk folder url. Make sure it is a valid URL (start with http:// or https://)';
$hesklang['err_spath']='Please enter the system (root, server) path to Hesk folder';
$hesklang['err_lang']='Please select Hesk language';
$hesklang['err_nolang']='The language file specified doesn\'t exist in the languag folder! Make sure the file is uploaded before changing the language setting.';
$hesklang['err_max']='Please enter maximum listings displayed per page';
$hesklang['err_psize']='Please enter the print font size';
$hesklang['err_dbhost']='Please enter your MySQL database host';
$hesklang['err_dbname']='Please enter your MySQL database name';
$hesklang['err_dbuser']='Please enter your MySQL database username';
$hesklang['err_dbpass']='Please enter your MySQL database password';
$hesklang['err_dbconn']='Could not connect to MySQL database using provided information! Please double-check your database settings';
$hesklang['err_dbsele']='Could not select MySQL database, please double-check database NAME';
$hesklang['err_custname']='Please enter name(s) for selected optional field(s)';
$hesklang['err_openset']='Can\'t open file <b>hesk_settings.inc.php</b> for writing. Please CHMOD this file to 666 (rw-rw-rw-)';
$hesklang['set_saved']='Settings saved';
$hesklang['set_were_saved']='Your settings have been successfully saved';
$hesklang['sec_img']='Security image';
$hesklang['sec_enter']='Bitte geben Sie die oben angezeigte Nummer ein';
$hesklang['sec_miss']='Bitte geben Sie die Anzahl der Sicherheit';
$hesklang['sec_wrng']='Bitte geben Sie korrekte Sicherheits-Code';
$hesklang['submit_problems']='Bitte gehen Sie zur�ck und korrigieren die folgenden Probleme';
$hesklang['cat_order']='Category order';
$hesklang['reply_order']='Canned response order';
$hesklang['move_up']='Move up';
$hesklang['move_dn']='Move down';
$hesklang['cat_move_id']='Missing category ID';
$hesklang['reply_move_id']='Missing canned response ID';
$hesklang['forgot_tid']='Tracking-ID vergesssen?';
$hesklang['tid_mail']='Bitte geben Sie Ihre Email-Adresse ein. Wir senden Ihnen die Tracking-ID`s noch einmal zu';
$hesklang['tid_send']='Send me my tracking ID';
$hesklang['tid_not_found']='Es wurden keine Tickets mit Ihrer Email-Adresse gefunden';
$hesklang['tid_email_subject']='Support ticket reminder';
$hesklang['tid_sent']='Tracking-ID gesendet';
$hesklang['tid_sent2']='Eine Email mit allen Angaben zur Ihren Tickets wurde an Ihre Email-Adresse gesendet.';
$hesklang['check_spambox']='<b>Bitte �berpr�fen Sie auch, ob die Email versehentlich in Ihren Spamordner gelandet ist!</b>';
$hesklang['reply_not_found']='Canned response not found';
$hesklang['check_status']='Checking status of required files and folders';
$hesklang['exists']='Exists';
$hesklang['no_exists']='Doesn\'t exist';
$hesklang['writable']='Writable';
$hesklang['not_writable']='Not writable';
$hesklang['disabled']='disabled';
$hesklang['e_settings']='You will not be able to save your settings unless this file is writable by the script. Please refer to the readme file for further instructions!';
$hesklang['e_attdir']='You will not be able to file attachments unless this exists and is writable by the script. Please refer to the readme file for further instructions!';
$hesklang['e_save_settings']='Unable to save your settings because <b>hesk_settings.inc.php</b> file is not writable by the script.';
$hesklang['e_attach']='Disabled because your <b>attachments</b> directory is not writable by the script.';
$hesklang['tickets_on_pages']='%d tickets displayed on %d pages. Jump to page'; // First %d is replaced with number of tickets, second %d with number of pages

$hesklang['go']='Go';
$hesklang['prefix_select'] = "";
$hesklang['about_foot'] = "�ber uns";
$hesklang['contact_foot'] = "Kontakt";
$hesklang['privacy_foot'] = "Datenschutz";
$hesklang['jobs_foot'] = "Jobs";
$hesklang['termscondi_foot'] = "Willkommen";
$hesklang['help_foot'] = "Hilfe";
$hesklang['paysafe_foot'] = "Sicher zahlen bei Snabid";

$hesklang['ticketreply']	= "Ticket beantworten";
$hesklang['snabidsupport']	= "Snabid Hilfe & Supportcenter";

/* This is left side menu */
$hesklang['leftmenu_aboutus']	= "�ber uns";
$hesklang['leftmenu_contact']	= "Kontakt";
$hesklang['leftmenu_jobs']		= "Jobs";
$hesklang['leftmenu_termscondi']= "Willkommen";
$hesklang['leftmenu_privacy']	= "Datenschutz";
$hesklang['leftmenu_tabhelp']	= "Hilfe";
$hesklang['leftmenu_howitwork'] = "Wie es funktioniert?";


/* This is header menu */
$hesklang['headermenu_tabhome']		= "Home";
$hesklang['headermenu_tabmyavenue']	= "Mein Snabid";
$hesklang['headermenu_logout'] 		= "Logout";
$hesklang['headermenu_welcome']		= "Willkommen";

$hesklang['regforfree2'] = "<a href='../registration.html' class='register'>Registrieren Sie sich f�r Freie</a>&nbsp;oder&nbsp;<a href='../login.html' class='register'>Melden Sie sich in</a>";
$hesklang['availablebids']	= "Verf�gbar Gebote";

/* This is view category menu*/
$hesklang['allliveauc']		= "Alle Live Auktionen";
$hesklang['futureaucs']		= "Zuk�nftige Auktionen";
$hesklang['endedauc']		= "Beendete Auktionen";

/* This is footer*/
$hesklang['paysafe']		= "Sicher zahlen bei Snabid";
$hesklang['backtomain']		= "Zur�ck zur Hauptseite";

/* DO NOT CHANGE BELOW */
if (!defined('IN_SCRIPT')) {echo "No syntax errors detected in $_SERVER[PHP_SELF]";exit();}
?>
