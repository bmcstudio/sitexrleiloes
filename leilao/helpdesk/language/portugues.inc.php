<?
//Common variables used in website
	
	$lng_imagepath		= "images/";
	$lng_imageext		= "gif";
	$lng_characset		= "iso-8859-1";
	$lng_prefix			= "";

	$lng_tabhome 		= "Home";
	$lng_tabmyavenue 	= "Minha Conta";
	$lng_tabregister	= "Registre-se";
	$lng_tabhelp		= "Ajuda";
	$lng_tabbuybids		= "Compre Bids";
	
	$lng_category		= "Categorias";
	$lng_allliveauc		= "Todos Leil�es Ativos";
	$lng_futureaucs		= "Pr�ximos Leil�es";
	$lng_futureauc		= "Pr�ximo Leil�o";
	$lng_endedauc		= "Leil�es Finalizados";

	$lng_news			= "Novidades";
	$lng_morenews		= "Mais Novidades";

	$lng_startdate		= "Data de In�cio";
	$lng_starttime		= "Hora de In�cio";
	$lng_enddate		= "Data Final";
	$lng_winner			= "Ganhador";
	$lng_ended			= "Finalizado";
	$lng_username		= "Nome de Usu�rio";
	$lng_password		= "Senha";
	$lng_paysafe		= "Pague com Seguran�a no Barato de Lance";
	$lng_termscondi		= "Termos de Uso";
	$lng_aboutus		= "Sobre n�s";
	$lng_contact		= "Fale conosco";
	$lng_privacy		= "Pol�tica de Privacidade";
	$lng_jobs			= "Trabalhe Conosco";
	$lng_welcome		= "Bem-Vindo";
	$lng_bids			= "Bids";
	$lng_availablebids	= "Bids Dispon�veis";
	$lng_price			= "Pre�o";
	$lng_bidder			= "Usu�rio";
	$lng_auctionid		= "ID do Leil�o";
	$lng_endprice		= "Pre�o Final";
	$lng_linkmore		= "mais";
	$lng_itemvalue		= "Valor do Item: ";
	
	//Left side of user account
	
	$lng_myauctionsavenue = "Meu Barato de Lance";
	$lng_accview		= "Ver";
	$lng_accmain		= "PRINCIPAL";//*****
	$lng_acchome		= "Home";
	$lng_accauctions	= "LEIL�ES";
	$lng_myauctions		= "Meus Leil�es";
	$lng_mybidbuddy		= "Meu Auto Lance";
	$lng_watchauctions	= "Lista de Preferidos";
	$lng_wonauctions	= "Leil�es Ganhos";
	$lng_accaccount		= "CONTA";
	$lng_accbidaccount	= "Carteira de Bid";
	$lng_accvouchers	= "Cupons";
	$lng_accreferral	= "Indique Amigos";
	$lng_accdetails		= "DETALHES";
	$lng_mydetails		= "Meus Detalhes";
	$lng_changepassword	= "Alterar Senha";
	$lng_closeaccount	= "Finalizar Conta";
	$lng_accnewsletter	= "Newsletter";
	$lng_accsmssetting	= "Configura��es SMS";
	$lng_myaddresses	= "Meu Endere�o";

	$lng_image			= "Imagem";
	$lng_description	= "Descri��o";
	$lng_pricebidder	= "Pre�o por Bid";
	$lng_countdown		= "Cronometro";
	$lng_placebid		= "Bid Local";
	
	$lng_inclvatexcl	= "Taxa de Entrega n�o inclusa";
	$lng_inclvatexcl2	= "Mais Imposto.";
	$lng_deliverycharge = "Taxa de Entrega";
	
	$lng_nextpage		= "Pr�xima P�gina";
	$lng_previouspage	= "P�gina Anterior";

	//end left side of user account
	

//end common variables


//Language variables for index page
	$lng_lastwinner		= "�ltimo Ganhador";
	$lng_finalprice		= "Pre�o Final";
	$lng_memlogin		= "Member Login";
	$lng_forgotpass		= "Perdeu a Senha?";
	$lng_signup			= "Novo Usu�rio, Cadastre-se";
	$lng_sendmessage1	= "Enviar a Mensagem BID ";
	$lng_sendmessage2	= " para ".$SMSsendnumber.". SMS o pre�o � 100 dinars + imposto"; //*****
	$lng_aucavenueid	= "ID Barato de Lance";
	$lng_actualprice	= "Pre�o Atual";
	$lng_readmore		= "Leia<br>Mais";
	
//end index page variables

//Language variables for login page
	
	$lng_login 			= "Login";
	$lng_bidnow 		= "Participe J� - Estes leil�es est�o prestes a terminar";
	$lng_aucavenuelogin = "Login Barato de Lance";
	$lng_newtoaucavenue = "Novo no Barato de Lance";
	$lng_registernow	= "Registre J�!";
	$lng_exciteauctions	= "Baratodelance.com.br - O site dos leil�es mais emocionantes da Internet";
	$lng_newestproduct	= "Produtos novos e pre�os Surpreendentes";
	$lng_loginvoucher	= $Currency."20 - cupom v�lido para o primeiro leil�o que voc� ganhar";
	$lng_freeregister	= "Cadastro Gr�tis";
	$lng_alreadyregister= "J� � registrado?";
	$lng_logindata		= "Por favor, digite seus dados de login aqui";
	$lng_forgotdata		= "<a href='forgotpassword.html' class='blue_link'>Perdeu</a> usuario ou senha?";
	$lng_invaliddata	= "Nome de Usu�rio ou Senha Inv�lidos.";
	$lng_accountsuspend = "Sua conta est� suspensa no Barato de Lance.";
	$lng_accountdelete	= "Sua conta foi deletada do Barato de Lance.";
	$lng_enterdata		= "Por Favor, informe seu nome de usuario e senha";
	$lng_enterpassword	= "Por Favor, informe sua senha.";
	

//end login page variable

//Language variables for registration page
	
	$lng_frregistration = "Cadstro Gratis";
	$lng_vouchermessage = $Currency."20 cupons, voc� ganhou para o primeiro leil�o";
	$lng_amazingproducts= "A chance de ganhar incr�veis produtos a pre�os surpreendentes";
	$lng_registrationdata= "Para se cadastrar no Barato de Lance, por favor preencha o seguinte";
	$lng_personalinfo	= "Informa��es Pessoais";
	$lng_firstname		= "Primeiro Nome";
	$lng_lastname		= "�ltimo Nome";
	$lng_birthdate		= "Data de Nascimento";
	$lng_gender			= "Sexo";
	$lng_male			= "Masculino";
	$lng_female			= "Feminino";
	$lng_addressline1   = "Endere�o";
	$lng_addressline2	= "Complemento";
	$lng_towncity		= "Cidade-Estado";
	$lng_country		= "Pa�s";
	$lng_postcode		= "CEP";
	$lng_phoneno		= "Telefone";
	$lng_logininfo		= "Informa��es de Login";
	$lng_characterlong	= "(no m�nimo 6 caracteres)";
	$lng_mobilenumber	= "Celular";
	$lng_retypepassword = "Confirme sua Senha";
	$lng_passsecurity	= "Senha";
	$lng_emailaddress	= "Email";
	$lng_confirmemail	= "Confirme seu endere�o de email";
	$lng_picturecharct  = "Digite os caracteres que aparecem na figura abaixo.";
	$lng_casesensitive  = "Indiferente a letras mai�sculas e min�sculas";
	$lng_acceptterms	= "Eu li, entendi e aceito os termos e condi��es de servi�o do Barato de Lance.";
	$lng_acceptprivacy	= "Eu li, entendi e aceito a pol�tica de privacidade do Baradodelance.";
	$lng_acceptnewsletter= "Sim, eu gostaria de receber a Newsletter .";
	$lng_emailexists	= "Este endere�o de email j� existe!!!";
	$lng_usernameexists	= "Este nome de usu�rio j� existe!!!";
	$lng_confirmregister= "Por Favor, confirme seu registro!";
	$lng_sentemailto	= "N�s enviamos um email para ";
	$lng_registernote	= "Enviamos um e-mail de verifica��o contendo seus detalhes de login e um link de confirma��o. Para confirmar sua inscri��o Barato de Lance, por favor clique no link no e-mail e ative sua conta. Por favor note que este link s� � v�lido por 48 horas.";
	$lng_importantword	= "Importante:";
	$lng_dontreceivenote= "Se voc� n�o receber este e-mail, tente verificar o seu filtro de spam. Se isso n�o funcionar e voc� ainda n�o tiver recebido o e-mail, ou o endere�o indicado estiver incorreto, <a href='helpdesk/contact.html?a=add' class='blue_link'>clique aqui</a>.";
	$lng_correctcode	= "Por favor Entre C�digo de Seguran�a Correto!!!";
	
//end registration page variable

// Language variables for myaccount/myauctions
	
	$lng_aucbiddingon	= "Leil�es em Andamento";
	$lng_notbiddingany	= "Voce n�o deu lance em nenhum leil�o";
	$lng_choosebidpack	= "Por favor, escolha BidPack que voc� gostaria de comprar";
	$lng_currentselect	= "Voc� Selecionou: ";

// end myaccount page variagle

//Language variables for mybidbutler page
	
	$lng_activebidbuddy	 = "Meus Auto Lances Ativos";
	$lng_noactivebidbuddy="Nenhum Auto Lances ativo para mostrar";
	$lng_bidbutlerbid	 = "Bids Feitos";
	$lng_deletebutlerbid = "Deletar";
	$lng_butlerstartprice= "Pre�o Inicial";
	
//end variables for mybidbutler page

//Language variables for watchedauctions page
	
	$lng_notwatchingany	= "Nenhum leil�o assistido para mostrar";
	$lng_deleteselected	= "Excluir leil�es selecionados";
	$lng_deletewatchauc	= "Deletar";

//end variables for watchedauctions page

//Language variables for wonauctions page
	
	$lng_winprice		= "Pre�o Ganho";
	$lng_acceptdenied	= "Aceitar/Negar";
	$lng_payment		= "Pagamento";
	$lng_wonaccepted	= "Aceito";
	$lng_wondenied		= "Negado";
	$lng_clickhere		= "Clique Aqui";
	$lng_lastdateaccept	= "�ltima Data para Aceitar ";
	$lng_acceptperiodover="Per�odos de Aceita��o do Leil�o Acabou";
	$lng_paymentperiodover="Leil�es Pagos <br />periodo finalizado";
	$lng_nowonauctions	= "Nenhum Leil�o ganho para mostrar";
		
//end variables for wonauctions page

//Language variables for acceptordenied page
		
	$lng_wonauctionaccept	= "Leil�es Ganhos Aceitos";
	$lng_pleaseselect		= "Por Favor, selecione";
	
//end variables for acceptordenied page

//Language variables for buybids page
	
	$lng_buybidpack		= "Compar um BidPack";
	$lng_youchoosen1	= "Voc� Selcionou";
	$lng_youchoosen2	= "Ap�s confirmar o pagamento os Bids estar�o dispon�veis";
	$lng_paymentmethod	= "Por Favor selecione uma forma de pagamento:";
	$lng_bidsfor		= "bids por";
	$lng_bidpackchar	= "Bidpack: ";
	$lng_bidreffbonus	= "Remessa de Bonus";

//end variables for buybids page

//Language variables for vouchers page
	
	$lng_voucherdate	= "Data";
	$lng_voucherlabel	= "Voucher";
	$lng_voucheramount	= "Pre�o/Bids";
	$lng_vouchercombinable = "Combin�veis";
	$lng_voucherauction	= "Leil�es";
	$lng_voucherstatus	= "Status";
	$lng_vouchervalidto	= "V�lido para";
	$lng_voucherused	= "Usado";
	$lng_voucherexpired	= "Expirado";
	$lng_voucherrunning	= "Execu��o";
	$lng_novoucher		= "Voc� n�o tem nenhum Voucher AINDA";
	$lng_vouvalidfor	= " (valido por ";
	$lng_vouvaliddays	= " dias)";
	

//end variables for vouchers page

//Language variables for affiliate page
	
	$lng_invitationsent	= "Convite Enviado!";
	$lng_referralurl	= "Sua referencia URL :";
	$lng_referralmessage = "Digite o endere�o de e-mail da pessoa que voc� deseja<br> enviar ";
	$lng_separatecoma	= "(separados por ;) :";


//end variables for affiliate page

//Language variables for smssetting page

	$lng_receivesms		= "Receba uma mensagem SMS quando um novo leil�o come�ar";
	$lng_yesreceivesms	= "Sim, me informar quando um leil�o come�ar.";
	$lng_smsturnon		= "Seu alerta por SMS foi ativado!";
	$lng_smsturnoff		= "Seu alerta por SMS foi desativado!";
	$lng_plsentermobile	= "Por favor, indique o seu celular!";

//end variables for smssetting page

//Language variables for mydetails page

	$lng_customerid		= "Cliente ID :";
	$lng_detailmobileno	= "Numero do Celular :";
	$lng_mydetailnote	= "Para evitar abusos, n�o permitimos que voc� edite seus detalhes, uma vez registada. Voc� s� pode editar o seu n�mero m�vel.";
	$lng_mobileupdate	= "Numero de telefone altaerado com sucesso!";
	$lng_yourdata		= "Seus Dados : ";

//end variables for mydetails page

//Language variables for changepassword page
	
	$lng_yourpassword	= "Altere sua Senha";
	$lng_enternewpassword = "Por favor, digite sua nova senha";
	$lng_retypenewpassword= "Por favor, redigite sua nova senha";
	$lng_passwordchanged= "Sua senha foi alterada!";

//end variables for changepassword page

//Language variables for closeaccount page
	
	$lng_closeavenueacc	= "Finalizar sua conta";
	$lng_closeimportantnote	= "Importante: Se voc� fechar sua conta, voc� n�o ser� capaz de voltar a registar com o Barato de Lance usando esse endere�o de e-mail por dois meses. Observe tamb�m que voc� perder� todos os lances que voc� tem atualmente em sua conta.";
	$lng_yescloseaccount	= "Sim, eu quero fechar minha conta";

//end variables for closeaccount page

//Language variables for newsletter page

	$lng_wanttoknow		= "Want us to let you know about new acutions and special offers?";
	$lng_newslettersub	= "Inscrever em newsletter :";
	$lng_youremailadd	= "Seu Email :";
	$lng_dontwanttoknow	= "N�o gostaria de receber mais detalhes sobre nossas ofertas em seu email?";
	$lng_newsletterunsub= "Descadastrar em newsletter :";
	$lng_thankyounewslet= "Obrigado por se cadastrar em nossa newsletter.";
	$lng_funandgoodluck	= "Divirta-se e boa sorte!";
	$lng_youraucaveteam	= "Seu Baradodelance - Equipe";
	$lng_unsubscribenews= "Seu descadastramento em nossa newsletter ...";
	$lng_plsentersubemail = "Por favor, infome endere�o de email para se Cadastrar!";
	$lng_plsenterunsubemail = "Por favor, infome endere�o de email para se Descadastrar!";
	$lng_plscheckemailadd = "Verifique seu e-mail. Foi digitado incorretamente.";

//end variables for newsletter page

//Language variables for help page

	$lng_helptopics		= "Topicos de Ajuda";
	$lng_quicklinks		= "Links R�pidos";
	$lng_lostpassword	= "Senha Perdida";
	$lng_subunsubnewslet= "Cadastrar/Descadastrar em nossa Newsletter";
	$lng_lostuserdata	= "Perdeu os dados do usuario";
	$lng_changeaddress	= "Alterar Endere�o";
	$lng_confirmwon		= "Confirme um leil�o ganho";
	$lng_reportabuse	= "Informar Abuso";
	$lng_emailaucavenue	= "Email Baradodelance";
	$lng_contactbyemail	= "Envie-nos um email<br>atrav�s do centro de apoio ao cliente:";
	

//end variables for help page

//Language variables for productdetails page

	$lng_smallauctions	= "Leil�es";
	$lng_addtowatchlist	= "Adicionar � Lista de Preferidos ";
	$lng_addedtowatchlist= "Adicionado � Lista de Preferidos";
	$lng_fixedpriceauc= "Pre�o Fixo ";
	$lng_fixedpriceauction="Leil�o de Pre�o Fixo";
	$lng_offauction		= "100% off";
	$lng_centauction	= "Leil�o de 1c";
	$lng_nightauction	= "Leil�o Noturdo";
	$lng_nailbiterauction="Leil�o NailBiter";
	$lng_clicktoenlarge	= "Clique na imagem para ampli�-la";
	$lng_bidhistory		= "Hist�rico de Bids";
	$lng_mybids			= "Meus Bids";
	$lng_bidhistorybid	= "BID";
	$lng_bidhistorybidder= "USU�RIO";
	$lng_bidhistorytype	= "TIPO";
	$lng_biddingsinglebid = "bid simples";
	$lng_biddingsmsbid	= "SMS Bid";
	$lng_biddingbutlerbid="Auto Lance";
	$lng_biddingtime	= "TEMPO";
	$lng_witheachbid	= "Cada BID aumentar� o valor do item em ";
	$lng_endlatest1		= "Este leil�o terminara em ";
	$lng_endlatest2		= " �s ";
	$lng_savings		= "Economia";
	$lng_worthupto		= "No valor de At�:";
	$lng_placedbids		= "Bids Feitos";
	$lng_fixedprice		= "Valor de Mercado:";
	$lng_typicalworthup	= "O 'Valor de Mercado' reflete o valor recomendado pelo fabricante ou o pre�o t�pico em lojas de varejo.";
	$lng_auctionended1	= "Este leil�o foi encerrado em ";
	$lng_auctionended2	=  " �s ";
	$lng_congratulations= "Parab�ns,";
	$lng_bookbidbuddy	= "Auto Lances";
	$lng_bidbuddyoverview="Meus Auto Lances";
	$lng_bidfrom		= "BID DE";
	$lng_bidto			= "BID PARA";
	$lng_butlerbids		= "BIDS";
	$lng_bidbuddyadded	= "Auto lance Adicionado";
	$lng_noactivebidbuddy="Nenhum Auto Lances Ativo";
	$lng_bidbuddy		= "Auto lance";
	$lng_youcantplace	= "Este leil�o � Lance Simples. Voc� n�o pode colocar Auto Lances";
	$lng_nowliveonauc	= "Agora no Barato de Lance";
	$lng_aboutaucave	= "Sobre o Barato de Lance";
	$lng_newtoaucave	= "Novo no Shock Bee?";
	$lng_getfirstauction= "Registre-se agora e ganhe R$20 de desconto no seu primeiro leil�o! ";
	$lng_productdetails	= "Detalhes do produto";
	$lng_noliveauction	= "Nenhum Leil�o para mostrar";

//end variables for productdetails page

//Language variables for allliveauction/futureauctions/endedauctions page

	$lng_liveauctions	= "Leil�es Ativos";
	$lng_allauctions	= "Todos os Leil�es";
	$lng_insteadof		= "(inves de ";
	$lng_noliveauctioncat="Nenhum Leil�o ativo nesta categoria";
	
	$lng_nofutureauction= "Nenhum Leil�o futuro para exibir";
	$lng_nofutureauctioncat="Nenhum Leil�o futuro para exibir nesta categoria";

	$lng_endtime		= "Finalizado";
	$lng_noendedauction	= "Nenhum leil�o finalizado para exibir";
	$lng_noendedauctioncat="Nenhum leil�o finalizado para exibir nesta categoria";
//end variables for allliveauction page

//Language variables for aboutus page

	$lng_aboutaucave	= "Sobre o Baradodelance";

//end variables for allliveauction page

//Language variables for allnews page
	
	$lng_aucavenews		= "Novidades Baradodelance";
	$lng_newsdate		= "Data:";
	$lng_nonewstodisp	= "Nenhuma novidade para exibir";
	
//end variables for allnews page

//Language variables for bidhistory page
	
	$lng_purchasebids	= "Comprado / Gratis";
	$lng_bidsdate		= "Data";
	$lng_historybids	= "Bids Dados";
	$lng_biddingstart	= "Inicio";
	$lng_biddingstatus	= "Status";
	$lng_bidliveauction	= "Leil�es ativos";
	$lng_bidfutureauction= "Proximos Leil�es";
	$lng_donthaveanybid	= "Voc� ainda n�o tem nenhum bid em sua conta";
	$lng_biddingsmsbids	= "Bids SMS";
	$lng_backbooking	= "Voltar";

//end variables for bidhistory page


//Language variables for buybidsunsuccess page

	$lng_buybidspayment	= "Pagamento de Bids";
	$lng_sorrygoback	= "Desculpe seu pagamento n�o foi processado<br /><a href='buybids.html' class='alink'>Clique aqui</a> para voltar.";

//end variables for buybidsunsuccess page

//Language variables for emailconfirmsuccess page

	$lng_emailconfirmsucc= "Confirma��o de email";
	$lng_regsuccess		= "Rigistradc com sucesso";
	$lng_regcongratulat	= "Parab�ns!!";
	$lng_yourregsuccess	= "Seu e-mail de verifica��o f confirmada com sucesso, por favor <a href='myaccount.html' class='alink'>Clique aqui</a> para continuar...";

//end variables for emailconfirmsuccess page


//Language variables for forgotpassword page

	$lng_titleforgot	= "Perdeu a Senha?";
	$lng_didnotfindemail= "Desculpe, N�o encontramos o seu endere�o de e-mail em nosso sistema!";
	$lng_accountverify	= "Essa conta n�o foi verificada ainda. Por favor, verifique primeiro.";
	$lng_emailsentto	= "Um e-mail foi enviado para :";
	$lng_forgottendata	= "Voc� esqueceu seus dados de login?";
	$lng_noproblementer	= "N�o tem problema, basta inserir o seu email e enviaremos as informa��es para a sua conta de e-mail.";
	$lng_enteryouremail	= "Seu Email : ";

//end variables for forgotpassword

//Language variables for jobs page
	
	$lng_aucavejobs	= "Trabalhe no Baradodelance";

//end variables for forgotpassword

//Language variables for paymentsuccess/paymentunsuccess page

	$lng_wonauctionpay	= "Pagamento de Leil�es Ganhos";
	$lng_paymentreceive	= "Pagamento de leil�o ganho recebido com sucesso<br/>ID DO LEIL�O: ";
	$lng_paymentnotreceive="Desculpe!<br />Alguns problemas ocorreram no processo de pagamento.";
	$lng_paymentunsuccess="Desculpe, seu processo de pagamento n�o foi concluido<br /><a href='wonauctions.html' class='alink'>Clique Aqui</a> para voltar.";

//end variables for paymentsuccess


//Language variables for privacy page
	
	$lng_aucaveprivacy	= "Politica de Privacidade do Barato de Lance";

//end variables for privacy

//Language variables for smsbid static page
	
	$lng_smsbidaucave	= "Baratodelance SMS Bid";

//end variables for smsbid static page

//Language variables for terms static page
	
	$lng_aucaveterms	= "Baratodelance Termos &amp; Condi��es";

//end variables for terms static page

//Language variables for thankyou page
	
	$lng_youpurchased1	= "Parab�ns! Voc� Comprou ";
	$lng_youpurchased2	= ". Sua transa��o foi conclu�da com sucesso.";
	$lng_youcanbid		= "Voc� pode dar Bid";
	$lng_thankyoumessage= "<a href='index.html' class='alink'>Clique aqui</a> para encontrar pre�os incriveis";
	$lng_aucaveteam		= "Equipe Barato de Lance.";

//end variables for thankyou page

//Language variables for addresses page
	
	$lng_addresses		= "Endere�os";
	$lng_hereeditadd	= "Aqui voc� pode adicionar ou editar seus endere�os: ";
	$lng_enternewadd	= "Informe seu novo endere�o";
	$lng_addresschanged	= "Endere�o alterado com sucesso!";
	$lng_deliveryaddress= "Endere�o de Entrega";
	$lng_onlyname		= "Nome :";
	$lng_mandatoryfield	= "Campos Obrigat�rios";
	$lng_billingaddress	= "Endere�o de Cobran�a";
	
//end variables for addresses page

//Language variables for wonauctionaccept page
	
	$lng_wonauctionaccept="Por Favor, confirme seu leil�o ganho";
	$lng_alreadyacceptdenied="Voc� j� aceitou/negou este Bid!";
	$lng_onlyaccept		= "Aceitar";
	$lng_onlydenied		= "Negar";

//end variables for wonauctionaccept page

//Language variables for choosevoucher page

	$lng_aucavevouchers	= "Vouchers Barato de Lance";
	$lng_makepayment	= "Efetuar pagamento";
	$lng_plsselvoucher	= "Por favor selecione um voucher";
	$lng_selectone		= "selecione um";
	$lng_dontwantvou	= "Se voc� n�o quiser usar seu cupom, assinale a op��o.";
	$lng_auctiondetails	= "Detalhes do Leil�o";
	$lng_shippingcharge	= "Custo de Entrega";
	$lng_vouchermaximum	= "Valor do Bonus(M�ximo";
	$lng_totalpayment	= "Pagamento Total";
	$lng_vouchernote1	= "Note: ";
	$lng_vouchernote2	= "Estes s�o Vale-Bids. Ap�s pagamento eles ser�o creditados em sua conta.";
	
	
//Language variables for choosevoucher page


//Language variables for helpdesk support centre

	//language variables for contact page
	
		$lng_helponlyname	= "Nome";
		$lng_aucavesupport	= "Centro de Suporte Barato de Lance";
		$lng_submitticket	= "Enviar um Ticket";
		$lng_viewticket		= "Ver Tickets";
		$lng_pleaseuseform	= "Utilize o formul�rio abaixo para enviar um ticket. Os campos obrigat�rios est�o marcados com <font class='red-text-12-b'>*</font>";
		$lng_helpemail		= "E-mail";
		$lng_helpcategory	= "Categoria";
		$lng_helppriority	= "Prioridade";
		$lng_helpsubject	= "Assunto";
		$lng_helpmessage	= "Mensagem";
		$lng_helpattachment	= "Anexos";
		$lng_helpacceptedfiles= "Arquivos Aceitos";
		$lng_helpmaxfilesize= "Tamanho M�x.";
		$lng_helpenternumber="Por favor, informe o n�mero exibido acima";
		$lng_helpsubmittingsure="Antes de enviar certifique-se da seguinte";
		$lng_helpallnecessary	= "Toda a informa��o necess�ria foi preenchida";
		$lng_helpallcorrect	= "Toda a informa��o est� correta e livre de erros";
		$lng_helplow		= "Baixa";
		$lng_helpmedium		= "M�dia";
		$lng_helphigh		= "Alta";

	//end variables for contact page
	
	//language variables for viewtickets page
		
		$lng_helpexcisting	= "Ver Tickets Abertos";
		$lng_helptrackingid	= "ID de rastreamento";
		$lng_helpforgottid	= "Perdeu ID de rastreamento?";
		$lng_helpplsenteremail="Por favor digite seu endere�o de email e as IDs dos seus tickets ser� enviados para voc�";
		
	//end variables for ticket page

	//language variables for ticket page
	
		$lng_helpticketstatus="Status do Ticket";
		$lng_helpcreateon	= "Criado em";
		$lng_helplastupdate	= "Ultima atualiza��o";
		$lng_helplastreplier= "Ultima resposta";
		$lng_helpreplies	= "Respostas";
		$lng_helpprinterfriend="Vers�o para Impress�o";
		$lng_helpdate		= "Data";
		$lng_helpaddreply	= "Add. resposta";

	//end variables for viewtickets page

	//language variables for reply_ticket page
		
		$lng_helpticketreply	= "Ticket Respondidos";
		$lng_helpentermessage="Por Favor, digite uma mensagem";
		$lng_helpreplysubmit = "Sua resposta foi enviada";
		$lng_helpyourreplysubmit="Sua resposta para este ticket foi enviado com sucesso!";
		$lng_helpnotrackingid	= "Sem ID de rastreamento";
		$lng_helpstatusnotvalid	= "Status inv�lido";
		$lng_helpclosed			= "Fechado";
		$lng_helpopened			= "Aberto";
		$lng_helptrackingnotfound	= "ID de rastreamento n�o econtrado";
		$lng_helponlyticket		= "Ticket";
		$lng_helpyourticketbeen	= "Seus ticket foram";
		$lng_helpviewyourticket	= "Ver seus ticket";
		$lng_helpforgotonlyid	= "Perdeu ID de rastreamento";
		$lng_helpwaitingstaff	= "'Aguarando responsta da equipe";
		$lng_helpwaitingcust	= "Aguardando resposta do cliente";

	//end variables for viewtickets page
	
	//language variables for forgotID page
		
		$lng_helptrackingidsent	= "ID de rastreamento enviado";
		$lng_helpemaildeatilsent= "Um e-mail com detalhes sobre o seu ticket foi enviado                                   para o endere�o";
		$lng_helpalsocheckemail	= "<b>N�o deixe de verificar o e-mail dentro de sua                                   caixa de lixo eletr�nico!</b>";
		$lng_helperror			= "Erro";
		$lng_helpnoemailfound	="Nenhum ticket com o seu endere�o de e-mail foram encontrados";
		$lng_helpgoback			= "Voltar";
		
		
		
	//end variables for forgotID page

//end variables for  helpdesk support centre

//For all pages JAVASCRIPT ALERTS variable
		
		//acceptordenied.php
		$lng_plsselaccordenied = "Por favor selecione aceito ou negado!";	
		
		//addresses.php

		$lng_plsentername 	    = "Por Favor, informe o nome!";
		$lng_plsenteradd 	    = "Por Favor, informe o endere�o!";
		$lng_plsentercity		= "Por Favor, informe a cidade!";
		$lng_plsenterpostcode	= "Por Favor, informe o CEP!";
		$lng_plsenterphoneno	= "Por Favor, informe o Telefone!";

		//affiliate.php
		
		$lng_plsenteremailadd	= "Por Favor, Informe o Endere�o de email!";
		
		//buybids.php
		
		$lng_plschoosemethod	= "Por favor, escolha como voc� gostaria de comprar!";	
		
		//forgotpassword.php
		
		$lng_entervalidemail	= "Por Favor, informe um endere�o de email v�lido";

		//choosevoucher.php
		
		$lng_plschoosevoucher		= "Por Favor, selecion um Voucher!";	

		//editpassword.php
		
		$lng_plsenternewpass	= "Por Favor, Informe Sua Nova Senha!";
		$lng_passtooshort		= "A senha � muito curta!";
		$lng_plsenterconfpass	= "Por Favor, Informe a Confirma��o de Senha!";
		$lng_passmismatch		= "Incompatibilidade de senha!";
	
		//watchauction.php
		
		$lng_areyousuretodel	= "Tem certeza de eliminar este leil�o participado";
	
			//unsubscribeuser.php
	
		$lng_plstickcheckbox	= "Por Favor, Marque a caixa de verifica��o para confirmar o fechamento da sua conta";
		
		//registration.php

		$lng_regenterfirstname	= "Por Favor, Informe Primeiro Nome!";	
		$lng_regenterlastname	= "Por Favor, Informe Lastname!";
		$lng_regselbirthdate	= "Por Favor, Selecione Data de Nascimento!";
		$lng_regenteraddress	= "Por Favor, Informe o Endere�o!";
		$lng_regentercity		= "Por Favor, Informe a Cidade!";
		$lng_regenterpostcode	= "Por Favor, Informe CEP!";
		$lng_regenterphoneno	= "Por Favor, Informe telefone!";
		$lng_regusernametooshort= "Nome de usu�rio � muito curto!";
		$lng_regenterpassword	= "Por Favor, Informe Senha!";
		$lng_regpasstooshort	= "Senha Muito Curta!";
		$lng_regconfpassword	= "Por Favor, Informe Confirma��o de Senha!";
		$lng_regpassmismatch	= "Senhas Diferentes!";
		$lng_regenteremailadd	= "Por Favor, Informe Endere�o de Email";
		$lng_regenterconfemail	= "Por Favor, Informe Confirma��o de Email!";
		$lng_regemailmismatch	= "Emails N�o Conferem!";
		$lng_regreadterms		= "Por Favor, aceite os temos e condi��es!";
		$lng_regreadprivacy		= "Por Favor, aceite a politica de privacidade!";
		$lng_regenterseccode	= "Por Favor, Informe o Codigo de Seguran�a!";
		
	
		//productdetails.php
		
		$lng_plsbidstartprice	= "Por Favor, informe o pre�o inicial doAuto Lances !";
		$lng_plsbidendprice		= "Por Favor, informe o pre�o final doAuto Lances!";
		$lng_plsenterbid		= "Por Favor, informeAuto Lances bids!";
		$lng_plsmorethanone		= "You palceAuto Lances for more than one bid!";
		$lng_bidstartnotgreater	= "BidBuddy start price not greater than end price!";
		$lng_bidendpricemust	= "BidBuddy end price is must greater than start price!";
		$lng_bidfromvalueismust	= "BID FROM Value needs to be greater than the Current Leil�o Price!";
		$lng_youbidisrunning	= "Your Auto Lances is running you can't delete it!";
		
		//mydetails.php
		
		$lng_plsentermobileno	= "Por Favor, Informe o numero de celular!";
		
		//commonused
		
		$lng_pleaserechargebid	= "Por Favor, compre bids!";		
		
// End all pages javascript alerts

// For All pages EMAIL variables start
	
	//wonauction information email
		
		$lng_mailwonaucinfo		= "Informa��o do Leil�o ganho";
		$lng_mailhi				= "Ol� ";
		$lng_mailcongratulation	= "Parab�ns!";
		$lng_mailyouhavewon		= "Voc� ganhou o item seguinte na Barato de Lance.";
		$lng_mailauctionid		= "Leil�o ID";
		$lng_mailname			= "Nome";
		$lng_mailprice			= "Pre�o";
		$lng_mailclosingdate	= "Data de Fechamento";
		$lng_mailclosingnote	= "Os leil�es ganhos devem ser aceite / recusado no prazo de 7 dias a contar da data de encerramento";
		$lng_mailaccdenclick1	= "Para aceitar / negar o seu leil�o ganho, clique aqui  ";
		$lng_mailaccdenclick2	= "clique aqui";
		$lng_mailsubjectauctionwon	= "Leil�o Fechado - Item Ganho!";
	
	//end

	//affiliate email
	
		$lng_mailheythere		= "Ola,";
		$lng_mailyoureceived1	= "Voc� recebeu este e-mail porque ";
		$lng_mailyoureceived2	= " convidou voc� para participar ";
		$lng_mailthesiteconst	= "O site consiste de uma leilano on-line abrangente (muitos produtos exclusivos) onde os usu�rios (tais como a voc�!) efetuam lances.";
		$lng_mailclicktoregister= "Clique no link a seguir e registar-se no Barato de Lance";
		$lng_mailsubjectaffiliate = "Enviar Registro ao baratodelance.com.br";
		
	// This is emailcontent of affiliate.php start
		
		$lng_emailcontent_affiliate='';
		$lng_emailcontent_affiliate.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>".$lng_mailheythere."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'></p>"."<br>".	
		
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailyoureceived1."%1\$s".$lng_mailyoureceived2." @.<a href='".$SITE_URL."registration.php?ref=%2\$s'>".$SITE_URL."registration.php?ref=%2\$s</a></td>
		</tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailthesiteconst."</td>
		</tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailclicktoregister."</td>
		</tr>";

		$lng_emailcontent_affiliate.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>URL: <a href='".$SITE_URL."registration.php?ref=%2\$s'>".$SITE_URL."registration.php?ref=%2\$s</a></td></tr>
		</table>";
		
	// This is emailcontent of affiliate.php end 
	
		
	//end
	
	//registration confirmation/forgotpassword  email
		
		$lng_mailaccountinfo	= "Informa��es da conta";
		$lng_mailreghi			= "Ol� ";
		$lng_mailreglogininfo	= "Aqui est� a informa��o de login que voc� pediu";
		$lng_mailaucaveusername	= "login";
		$lng_mailaucaveuserid	= "ID de usuario";
		$lng_mailtoresetyourpass1= "Para resetar sua senha ".
		$lng_mailtoenableyouracc= "Para ativar sua conta ";
		$lng_mailtoresetyourpass2= "clique aqui";
		$lng_mailhappywinning	= "Felizes ganhadores no baratodelance.com.br!";
		$lng_mailsubjectaccinfo	= "Informa��es da conta @ baratodelance.com.br";
	
	//end
	
	// This is emailcontent for forgotpassword.php start
		
		$lng_emailcontent_forgotpassword='';
		$lng_emailcontent_forgotpassword.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailaccountinfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreghi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreglogininfo.":</td>
		</tr>";
		
		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveusername.": %2\$s</td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveuserid.": %3\$s</td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtoresetyourpass1.": <a href='".$SITE_URL."password.php?ud=%4\$s&pd=%5\$s&key=%6\$s'>".$lng_mailtoresetyourpass2."</a></td>
		</tr>";

		$lng_emailcontent_forgotpassword.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td><br>".$lng_mailhappywinning."<br></td>
		</tr>
		</table>";
		
	// This is emailcontent for forgotpassword.php end
	
	// This is emailcontent for registration.php start
		
		$lng_emailcontent_registraion='';
		$lng_emailcontent_registraion.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailaccountinfo."</p>"."<br>".	
	
	"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreghi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailreglogininfo.":</td>
		</tr>";
		
		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveusername.": %2\$s</td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaucaveuserid.": %3\$s</td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtoenableyouracc." <a href='".$SITE_URL."password.html?auc_key=%4\$s'>".$lng_mailtoresetyourpass2."</a></td>
		</tr>";

		$lng_emailcontent_registraion.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailhappywinning."</td>
		</tr>
		</table>";
		
	// This is emailcontent for registration.php end
	
	//acceptordenied wonauction information email
		
		$lng_mailauctioninfo	= "Informa��es do Leil�o Ganho";
		$lng_mailaccepthi		= "Ola ";
		$lng_mailwonandaccept	= "Voc� ganhou e aeitou o seguinte item.";
		$lng_mailwonanddenied	= "Voc� ganhou e negou o seguinte item.";
		$lng_mailwonacceptid	= "Leil�o ID";
		$lng_mailwonname		= "Nome";
		$lng_mailwonprice		= "Pre�o";
		$lng_mailaccedenieddate	= "Data de Aceita��o / nega��o";
		$lng_mailauctionpaid	= "O pre�o de leil�o deve ser pago no prazo de 7 dias a contar da data de aceita��o.";
		$lng_mailtomakepay1		= "Para fazer o pagamento ";
		$lng_mailtomakepay2		= "clique aqui";
		$lng_mailsubjectacceptordenied = "Leil�o Aceito / Negado - Item ganho!";
	
	//This email content for acceptordenied.php's Accept start
		
		$lng_emailcontent_acceordeaccept='';
		
		$lng_emailcontent_acceordeaccept.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_acceordeaccept .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_acceordeaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		"</table>";
		
	//This email content for acceptordenied.php's Accept end
	
	//This email content for acceptordenied.php's Denied start
		
		$lng_emailcontent_acceordedenied='';
		
		$lng_emailcontent_acceordedenied.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_acceordedenied .= $lng_mailwonanddenied;
		"</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_acceordedenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		"</table>";
		
	//This email content for acceptordenied.php's Denied end
	
	//This email content for acceptordenied.php's Accept and mailflag = 0 start
		
		$lng_emailcontent_acceordeflag='';
		
		$lng_emailcontent_acceordeflag.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_acceordeflag .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailauctionpaid."</td>
		</tr>";
		
		$lng_emailcontent_acceordeflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtomakepay1."<a href='".$SITE_URL."login.php?wid=%6\$s'>".$lng_mailtomakepay2."</a></td>
		</tr>";
		"</table>";
		
	//This email content for acceptordenied.php's Accept and mailflag = 0 end	
	
	//This email content for wonauctionaccept.php's Accept start
		
		$lng_emailcontent_wonaccept='';
		
		$lng_emailcontent_wonaccept.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td> ".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_wonaccept .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_wonaccept.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>
		</table>";
					
	//This email content for wonauctionaccept.php's Accept end
	
	//This email content for wonauctionaccept.php's Denied start
		
		$lng_emailcontent_wondenied='';
		
		$lng_emailcontent_wondenied.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td> ".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_wondenied .= $lng_mailwonanddenied;
		"</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_wondenied.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>
		</table>";
					
	//This email content for wonauctionaccept.php's Denied end
	
	//This email content for wonauctionaccept.php's Accept and mailflag = 0 start
		
		$lng_emailcontent_wonacceptflag='';
		
		$lng_emailcontent_wonacceptflag.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'>".$lng_mailauctioninfo."</p>"."<br>".	
	
		"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td> ".$lng_mailaccepthi."%1\$s, </td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>";
		$lng_emailcontent_wonacceptflag .= $lng_mailwonandaccept;
		"</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonacceptid.": %2\$s</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonname.": %3\$s</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwonprice.": %4\$s</td>
		</tr>";

		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailaccedenieddate.": %5\$s</td>
		</tr>";
		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailauctionpaid."</td>
		</tr>";
		
		$lng_emailcontent_wonacceptflag.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailtomakepay1."<a href='".$SITE_URL."login.php?wid=%5\$s'>".$lng_mailtomakepay2."</a></td>
		</tr>
		</table>";
			
		
	//This email content for wonauctionaccept.php's Accept and mailflag = 0 end
		
	//end

	//subscribe for newsletter email
		
		$lng_mailnewsletterhi	= "Ola,";
		$lng_mailthanksinterest	= "Obrigado pelo seu interesse em nossa newsletter.";
		$lng_mailyouarenowsub	= "Est� cadastrado para receber a newsletter do Barato de Lance.";
		$lng_mailwhydonttake1	= "Por que voc� n�o d� uma olhada ";
		$lng_mailwhydonttake2	= " para ver o que ofertas incr�veis que temos no Barato de Lance.";
		$lng_mailhavefun		= "Divirta-se e boa Sorte!";
		$lng_mailformhere		= "De todos no Barato de Lance";
		$lng_mailnewslettersubject = "Obrigado por se cadastar em nossa newsletter";
		
	//This is emailcontent for newsletter.php start
		
		$lng_emailcontent_newsletter ='';
		$lng_emailcontent_newsletter.= "<font style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>"."</font><br>"."<br>"."<p align='center' style='font-size: 14px;font-family: Arial, Helvetica, sans-serif;font-weight:bold;'></p>"."<br>".	
	
	"<table border='0' cellpadding='3' cellspacing='0' width='500' align='left' class='style13'>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailnewsletterhi."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailthanksinterest."</td>
		</tr>";
		
		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailyouarenowsub."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailwhydonttake1."<a href='".$SITE_URL."index.php'>".$SITE_URL."index.php</a>".$lng_mailwhydonttake2."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailhavefun."</td>
		</tr>";

		$lng_emailcontent_newsletter.="<tr style='font-size: 10px;font-family: Arial, Helvetica, sans-serif;color: #333333;'>
		<td>".$lng_mailformhere."</td>
		</tr>
		</table>";
		
	//This is emailcontent for newsletter.php end
	//end
	
// end email variables

//ERROR Messages for smsbidplace page
	
	$lng_wrongparameter	= "ERRO : Parametro errado informado";
	$lng_userdoesnotex	= "ERRO : Usu�rio n�o existe";
	$lng_auctionnotrunn	= "ERRO : Este leil�o ainda n�o come�ou";
	$lng_auctioncurrpause="ERRO : Este leil�o esta em pausa";
	$lng_aucdoesnotex	= "ERRO : Este leil�o n�o existe";
	$lng_okmessage1		= "OK : Seu Lance foi Dado com Sucesso. O pre�o ap�s o lance � ";
	$lng_okmessage2		= " R$ e Tempo restante � ";
	$lng_invalidmessage	= "ERRO : Dados de mensagens inv�lidas";

//smsbulknotify page variables
	
	$lng_bulkmessage1	= "Leil�o ";
	$lng_bulkmessage2	= " est� apenas come�ando";

//end smsbulknotify page variables

//end messages

// New variables define for SnaBid start
	
	$lng_openauction = "Leil�es Ativos";
	$lng_moreliveauction = "Mais Leil�es Ativos";
	$lng_howitwork   = "Como Funciona?";
	$lng_regforfree	 = "<a href='registration.html' class='register'>Registre-se</a>&nbsp;ou&nbsp;<a href='login.html' class='register'>Efetue Login</a>";
	$lng_regforfree2 = "<a href='../registration.html' class='register'>Registre-se</a>&nbsp;ou&nbsp;<a href='../login.html' class='register'>Efetue Login</a>";
	$lng_logout = "Logout";
	$lng_paymethod = "Money bookers";

// New variables define for SnaBid end

	$lng_pause		= 	"Pausado";
	$lng_nobidspalced = "Nenhum Lance Feito";
	$lng_subtotal   = "Subtotal";
?>