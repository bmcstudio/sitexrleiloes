<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/admin_common.inc.php');
hesk_session_start();
hesk_isLoggedIn();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1>Delete Tickets</TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>
<?
$random=rand(10000,99999);

$ids=hesk_input($_POST['id'],"$hesklang[no_selected].</p><p>&nbsp;</p>
<p align=\"center\"><a href=\"admin_main.php?Refresh=$random\">$hesklang[c2c]</a>");

require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

$i=0;

/* DELETE */
if ($_POST['a']=="delete") {
$title = $hesklang['tickets_deleted'];
$msg = $hesklang['num_tickets_deleted'];

    foreach ($_POST['id'] as $this_id)
    {
        $this_id=hesk_isNumber($this_id,$hesklang['id_not_valid']);
        $sql="SELECT `trackid` FROM `hesk_tickets` WHERE `id`=$this_id";
        $result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        $trackingID = hesk_dbResult($result,0,0);
        $sql="DELETE FROM `hesk_attachments` WHERE `ticket_id`='$trackingID'";
        hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        $sql="DELETE FROM `hesk_tickets` WHERE `id`=$this_id LIMIT 1";
        hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        $sql="DELETE FROM `hesk_replies` WHERE `replyto`=$this_id";
        hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        $i++;
    }
} else {
/* JUST CLOSE */
$title = $hesklang['tickets_closed'];
$msg = $hesklang['num_tickets_closed'];

    foreach ($_POST['id'] as $this_id)
    {
        $this_id=hesk_isNumber($this_id,$hesklang['id_not_valid']);
        $sql="UPDATE `hesk_tickets` SET `status`='3' WHERE `id`=$this_id LIMIT 1";
        hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
        $i++;
    }
}
/* Print header */
//require_once('inc/header.inc.php');

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');

?>
<tr>
<td>

<p>&nbsp;</p>

<h3 align="center"><?php echo $title; ?></h3>

<p>&nbsp;</p>

<p align="center"><b><?php printf($msg,$i); ?>.</b></p>
<p align="center"><a href="admin_main.php?Refresh=<?php echo $random; ?>"><?php echo $hesklang['c2c']; ?></a></p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<hr width="750">
</td>
</tr>
</TABLE>
</body>
</html>


<?php

/* Print footer */
//require_once('inc/footer.inc.php');
exit();

?>
