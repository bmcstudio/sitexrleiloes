<?php
/* Settings file for Hesk 0.94.1 */
/*** Please read the README.HTM file for more information on these settings ***/

$hesk_settings['site_title']='My lovely website';
$hesk_settings['site_url']='http://www.mywebsite.com';

/* Contacts */
$hesk_settings['support_mail']='support@mywebsite.com';
$hesk_settings['webmaster_mail']='webmaster@mywebsite.com';
$hesk_settings['noreply_mail']='NOREPLY@mywebsite.com';

/* Help desk settings */
$hesk_settings['hesk_url']='http://www.mywebsite.com/hesk';
$hesk_settings['hesk_title']='My company support system';
$hesk_settings['server_path']='/home/mysite/public_html/hesk';
$hesk_settings['language']='english';
$hesk_settings['max_listings']=15;
$hesk_settings['print_font_size']=12;
$hesk_settings['debug_mode']=0;
$hesk_settings['secimg_use']=1;
$hesk_settings['secimg_sum']='H29PEW9SQR';

/* File attachments */
$hesk_settings['attachments']=array (
    'use'           =>  0,
    'max_number'    =>  2,
    'max_size'      =>  1024, // kb
    'allowed_types' =>  array('.gif','.jpg','.jpeg','.zip','.rar','.csv','.doc','.txt','.pdf')
);

/* Custom fields */
$hesk_settings['use_custom']=0;
$hesk_settings['custom_place']=0;
$hesk_settings['custom_fields']=array (
    'custom1'  => array('use'=>0,'req'=>0,'name'=>'Custom field 1','maxlen'=>255),
    'custom2'  => array('use'=>0,'req'=>0,'name'=>'Custom field 2','maxlen'=>255),
    'custom3'  => array('use'=>0,'req'=>0,'name'=>'Custom field 3','maxlen'=>255),
    'custom4'  => array('use'=>0,'req'=>0,'name'=>'Custom field 4','maxlen'=>255),
    'custom5'  => array('use'=>0,'req'=>0,'name'=>'Custom field 5','maxlen'=>255)
);

/* Database settings */
$hesk_settings['database_host']='localhost';
$hesk_settings['database_name']='wwwbara_hdesk';
$hesk_settings['database_user']='wwwbara_hdesk';
$hesk_settings['database_pass']='Wg480C';
#############################
#     DO NOT EDIT BELOW     #
#############################
$hesk_settings['hesk_version']='0.94.1';
if ($hesk_settings['debug_mode']) {
    error_reporting(E_ALL ^ E_NOTICE);
} else {
    ini_set('display_errors', 0);
    ini_set('log_errors', 1);
}
if (!defined('IN_SCRIPT')) {die('Invalid attempt!');}
if (is_dir('install') && !defined('INSTALL')) {die('Please delete the <b>install</b> folder from your server for security reasons then refresh this page!');}
?>