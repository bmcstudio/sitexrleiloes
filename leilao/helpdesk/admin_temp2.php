<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);
include("config/connect.php");
/* Get all the required files and functions */
require_once('hesk_settings.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
hesk_session_start();
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link href="../style/UrTurn2Win.css" rel="stylesheet" type="text/css" />
<link href="../style/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="style/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body>
	<div align="center" style="width:100%">
		<div align="center">
<?
/* What should we do? */
$action=hesk_input($_REQUEST['a']) or $action='login';
if ($action == 'login') {print_login();}
elseif ($action == 'do_login') {do_login();}
elseif ($action == 'logout') {logout();}
else {hesk_error($hesklang['invalid_action']);}

/* Print footer */
?>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?
//require_once('inc/footer.inc.php');
exit();

/*** START FUNCTIONS ***/

function do_login() {
global $hesklang;
$user=hesk_isNumber($_POST['user'],$hesklang['select_username']);
$pass=hesk_input($_POST['pass'],$hesklang['enter_pass']);

$sql = "SELECT * FROM `hesk_users` WHERE `id`=$user LIMIT 1";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$res=hesk_dbFetchAssoc($result);
foreach ($res as $k=>$v) {
    $_SESSION[$k]=$v;
}

/* Check password */
if ($pass != $_SESSION['pass']) {
hesk_session_stop();
hesk_error($hesklang['wrong_pass']);
}

/* Regenerate session ID (security) */
hesk_session_regenerate_id();

/* Get allowed categories */
if (empty($_SESSION['isadmin'])) {
$cat=substr($_SESSION['categories'], 0, -1);
$_SESSION['categories']=explode(",",$cat);
}

session_write_close();
if ($url=hesk_input($_REQUEST['goto'])) {
    $url = str_replace('&amp;','&',$url);
    Header('Location: '.$url);
} else {
    Header('Location: admin_main.php');
}
exit();
} // End do_login()


function print_login() {
global $hesk_settings, $hesklang;
//require_once('inc/header.inc.php');
include('header.php');
?>
<div id="account">
<div class="style1" id="title"><B>&nbsp;&nbsp;&nbsp;Admin Login</B></div><br />
	<div id="feedback_center_1">
		<div class="style17" id="title" style="margin-top: -0px;"><B>&nbsp;&nbsp;&nbsp;Login</B></div>
		<div id="feedback_username" width="750">
</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>

<?php
if ($_REQUEST['notice']) {
echo '<p align="center" class="important">'.$hesklang['session_expired'].'</p>';
}
?>

<form action="admin.php" method="POST">

<div align="center">
<center>
<table border="0" cellspacing="1" cellpadding="5">
<tr>
<td align="right"><?php echo $hesklang['user']; ?>: </td>
<td align="left"><select name="user">
<?php
$sql = "SELECT * FROM `hesk_users`";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
while ($row=hesk_dbFetchAssoc($result))
{
    echo '<option value="'.$row['id'].'">'.$row['user'].'</option>';
}

?>
</select></td>
</tr>
<tr>
<td align="right"><?php echo $hesklang['pass']; ?>: </td>
<td><input type="password" name="pass"></td>
</tr>
</table>
</center>
</div>

<p align="center"><input type="hidden" name="a" value="do_login">
<?php
if ($url=hesk_input($_REQUEST['goto'])) {
echo '<input type="hidden" name="goto" value="'.$url.'">';
}
?>
<input type="submit" value="<?php echo $hesklang['login']; ?>" class="button"></p>

</form>
<?php
} // End print_login()

function logout() {
global $hesk_settings, $hesklang;
require_once('inc/header.inc.php');
hesk_session_stop();
?>
<p class="smaller"><a href="<?php echo $hesk_settings['site_url']; ?>"
class="smaller"><?php echo $hesk_settings['site_title']; ?></a> &gt;
<?php echo $hesklang['logged_out']; ?><br>&nbsp;</p>
</td>
</tr>
<tr>
<td>

<p>&nbsp;</p>
<h3 align="center"><?php echo $hesklang['logout']; ?></h3>
<p>&nbsp;</p>

<p align="center"><?php echo $hesklang['logout_success']; ?></p>
<p>&nbsp;</p>
<p align="center"><a href="admin.php"><?php echo $hesklang['click_login']; ?></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
</div>
<div id="footerspace" style="clear:both; height: 25px;"></div>
<?
	require_once("footer.php");
?>
</div>
</div>
</body>
</html>
<?php
//require_once('inc/footer.inc.php');
exit();
} // End logout()

?>
