<?php
/*******************************************************************************
*  Title: Helpdesk software Hesk
*  Version: 0.94.1 @ October 25, 2007
*  Author: Klemen Stirn
*  Website: http://www.phpjunkyard.com
********************************************************************************
*  COPYRIGHT NOTICE
*  Copyright 2005-2007 Klemen Stirn. All Rights Reserved.
*
*  This script may be used and modified free of charge by anyone
*  AS LONG AS COPYRIGHT NOTICES AND ALL THE COMMENTS REMAIN INTACT.
*  By using this code you agree to indemnify Klemen Stirn from any
*  liability that might arise from it's use.
*
*  Selling the code for this program, in part or full, without prior
*  written consent is expressly forbidden.
*
*  Obtain permission before redistributing this software over the Internet
*  or in any other medium. In all cases copyright and header must remain
*  intact. This Copyright is in full effect in any country that has
*  International Trade Agreements with the United States of America or
*  with the European Union.
*
*  Removing any of the copyright notices without purchasing a license
*  is illegal! To remove PHPJunkyard copyright notice you must purchase a
*  license for this script. For more information on how to obtain a license
*  please visit the site below:
*  http://www.phpjunkyard.com/copyright-removal.php
*******************************************************************************/

define('IN_SCRIPT',1);

/* Get all the required files and functions */
require_once('hesk_settings_admin.inc.php');
require_once('language/'.$hesk_settings['language'].'.inc.php');
require_once('inc/common.inc.php');
hesk_session_start();
hesk_isLoggedIn();
require_once('inc/database.inc.php');
hesk_dbConnect() or hesk_error("$hesklang[cant_connect_db] $hesklang[contact_webmsater] $hesk_settings[webmaster_mail]!");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="style/main.css" type="text/css" rel="stylesheet">
</head>

<body bgcolor="#ffffff" style="padding-left:10px">
<TABLE cellSpacing=10 cellPadding=0  border=0 width="100%">
		<TR>
			<TD class=H1>Find Ticket</TD>
		</TR>
		<TR>
			<TD background="images/vdots.gif"><IMG height=1 
			  src="images/spacer.gif" width=1 border=0></TD>
		</TR>
<?
/* Print header */
//require_once('inc/header.inc.php');

$sql = "SELECT * FROM `hesk_tickets` WHERE ";

if ($_GET['archive']) {$archive=1;$sql .= "`archive`='1' AND ";}
else {$archive=0;}

$sql .= hesk_myCategories();
$sql .= " AND ";

/* Get all the SQL sorting preferences */
$what=hesk_input($_GET['what'],"$hesklang[int_error]: no what defined");

switch ($what) {
case "trackid":
    $extra=hesk_input($_GET['trackid'],$hesklang['enter_id']);
    $sql .= "`trackid` = '$extra' ";
    break;
case "name":
    $extra=hesk_input($_GET['name'],$hesklang['enter_name']);
    $sql .= "`name` LIKE '%$extra%' ";
    break;
case "dt":
    $extra=hesk_input($_GET['dt'],$hesklang['enter_date']);
        if (!preg_match("/\d{4}-\d{2}-\d{2}/",$extra))
        {
            hesk_error($hesklang['date_not_valid']);
        }
    $sql .= "`dt` LIKE '$extra%' ";
    break;
case "subject":
    $extra=hesk_input($_GET['subject'],$hesklang['enter_subject']);
    $sql .= "`subject` LIKE '%$extra%' ";
    break;
default:
    hesk_error($hesklang['invalid_search']);
}

/* Print admin navigation */
//require_once('inc/show_admin_nav.inc.php');

?>
<tr>
<td>

<h3 align="center"><?php echo $hesklang['tickets_found']; ?></h3>

<?php
$maxresults = hesk_isNumber($_GET['limit']) or $maxresults=$hesk_settings['max_listings'];
$thispage = hesk_isNumber($_GET['page']) or $thispage=1;

$sort = hesk_input($_GET['sort']) or $sort='priority';
$sql .= " ORDER BY `$sort` ";

if (isset($_GET['asc']) && $_GET['asc']==0) {$sql .= " DESC ";$asc=0;$asc_rev=1;}
else {$sql .= " ASC ";$asc=1;$asc_rev=0;}

$query = "what=$what&trackid=$extra&name=$extra&date=$extra&subject=$extra&limit=$maxresults&archive=$archive&asc=$asc&sort=$sort&page=";

/* Get number of tickets and page number */
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");
$all = hesk_dbNumRows($result);

$thispages = ceil($all/$maxresults) or $thispages=1;
if ($thispage > $thispages) {$thispage=$thispages;}
$limit_down = (($thispage*$maxresults)-$maxresults);

    $prev_page = ($thispage-1 <= 0) ? 0 : $thispage-1;
    $next_page = ($thispage+1 > $thispages) ? 0 : $thispage+1;


    echo '<form name="HPage"><p align="center">'.sprintf($hesklang['tickets_on_pages'],$all,$thispages).' <select name="myHpage">';
    for ($i=1;$i<=$thispages;$i++) {
        echo '<option value="'.$i.'">'.$i.'</option>';
    }
    echo'</select> <input type="button" value="'.$hesklang['go'].'" onClick="Javascript:window.location=\'show_tickets.php?'.$query.'\'+document.HPage.myHpage.value"><br />';

    if ($thispages > 7 && $prev_page) {
        echo '
        <a href="find_tickets.php?'.$query.$i.'1">&lt;&lt;</a>
        &nbsp;
        <a href="find_tickets.php?'.$query.$prev_page.'">&lt;</a>
        &nbsp;
        ';
    }

    for ($i=1; $i<=$thispages; $i++) {
        if ($i <= ($thispage+5) && $i >= ($thispage-5)) {
           if($i == $thispage) {echo ' <b>'.$i.'</b> ';}
           else {echo ' <a href="find_tickets.php?'.$query.$i.'">'.$i.'</a> ';}
        }
    }

    if ($thispages > 7 && $next_page) {
        echo '
        &nbsp;
        <a href="find_tickets.php?'.$query.$next_page.'">&gt;</a>
        &nbsp;
        <a href="find_tickets.php?'.$query.$thispages.'">&gt;&gt;</a>
        ';
    }

    echo '</p></form>';

/* We have the full SQL query now, get tickets */
$sql .= " LIMIT $limit_down,$maxresults ";
$result = hesk_dbQuery($sql) or hesk_error("$hesklang[cant_sql]: $sql</p><p>$hesklang[mysql_said]:<br>".mysql_error()."</p><p>$hesklang[contact_webmsater] $hesk_settings[webmaster_mail]");

$query = "what=$what&trackid=$extra&name=$extra&date=$extra&subject=$extra&limit=$maxresults&archive=$archive&page=$thispage&asc=$asc_rev&sort=";

/* Print the table with tickets */
$random=rand(10000,99999);
?>

<form name="form1" action="delete_tickets.php" method="POST">

<div align="center">
<center>
<table border="0" width="750" cellspacing="1" cellpadding="3" class="t-a">
<tr class="th-a">
<td>&nbsp;</td>
<td><a href="find_tickets.php?<?php echo $query; ?>trackid"><?php echo $hesklang['trackID']; ?></a></td>
<td align="center"><a href="find_tickets.php?<?php echo $query; ?>lastchange"><?php echo $hesklang['last_update']; ?></a></td>
<td ><a href="find_tickets.php?<?php echo $query; ?>name"><?php echo $hesklang['name']; ?></a></td>
<td><a href="find_tickets.php?<?php echo $query; ?>subject"><?php echo $hesklang['subject']; ?></a></td>
<td align="center"><a href="find_tickets.php?<?php echo $query; ?>status"><?php echo $hesklang['status']; ?></a></td>
<td align="center"><a href="find_tickets.php?<?php echo $query; ?>lastreplier"><?php echo $hesklang['last_replier']; ?></a></td>
<td align="center"><a href="find_tickets.php?<?php echo $query; ?>priority"><?php echo $hesklang['priority']; ?></a></td>
<td align="center"><a href="find_tickets.php?<?php echo $query; ?>archive"><?php echo $hesklang['archived']; ?></a></td>
</tr>

<?php
while ($ticket=hesk_dbFetchAssoc($result))
{
    if ($i) {$color="admin_gray"; $colorname="#F4F4F4"; $i=0;}
    else {$color="admin_white"; $colorname="#FFFFFF"; $i=1;}

    switch ($ticket['status']) {
    case 0:
        $ticket['status']='<font class="open">'.$hesklang['open'].'</font>';
        break;
    case 1:
        $ticket['status']='<font class="waitingreply">'.$hesklang['wait_reply'].'</font>';
        break;
    case 2:
        $ticket['status']='<font class="replied">'.$hesklang['replied'].'</font>';
        break;
    default:
        $ticket['status']='<font class="resolved">'.$hesklang['closed'].'</font>';
    }

    switch ($ticket['priority']) {
    case 1:
        $ticket['priority']='<font class="important">'.$hesklang['high'].'</font>';
        break;
    case 2:
        $ticket['priority']='<font class="medium">'.$hesklang['medium'].'</font>';
        break;
    default:
        $ticket['priority']=$hesklang['low'];
    }

    $ticket['lastchange']=hesk_formatDate($ticket['lastchange']);

    if ($ticket['lastreplier']=='1') {$ticket['lastreplier']=$hesklang['staff'];}
    else {$ticket['lastreplier']=$hesklang['customer'];}

    if ($ticket['archive']) {$ticket['archive']=$hesklang['yes'];}
    else {$ticket['archive']=$hesklang['no'];}

    if ($ticket['attachments']) {$ticket['attachments']='<img src="img/clip.gif" width="20" height="20" alt="'.$hesklang['clip_alt'].'">';}

echo <<<EOC
<tr>
<td class="$color" align="center"><input style="border:none; background-color:$colorname" type="checkbox" name="id[]" value="$ticket[id]"></td>
<td class="$color"><a href="admin_ticket.php?track=$ticket[trackid]&Refresh=$random">$ticket[trackid]</a></td>
<td class="$color" align="center">$ticket[lastchange]</td>
<td class="$color">$ticket[name]</td>
<td class="$color">$ticket[attachments]<a href="admin_ticket.php?track=$ticket[trackid]&Refresh=$random">$ticket[subject]</a></td>
<td class="$color" align="center">$ticket[status]</td>
<td class="$color" align="center">$ticket[lastreplier]</td>
<td class="$color" align="center">$ticket[priority]</td>
<td class="$color" align="center">$ticket[archive]</td>
</tr>

EOC;
} // End while
?>
</table>
</center>
</div>

<p align="center"><select name="a">
<option value="delete" selected><?php echo $hesklang['del_selected']; ?></option>
<option value="close"><?php echo $hesklang['close_selected']; ?></option>
</select>
<input type="submit" value="<?php echo $hesklang['execute']; ?>" class="button"></p>

</form>

<hr>

<?php
require_once('inc/show_search_form.inc.php');
?>


<hr width="750">
<p>&nbsp;</p>
</td>
</tr>
</TABLE>
</body>
</html>
<?php

/* Print footer */
//require_once('inc/footer.inc.php');
exit();

?>
