<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />



<style type="text/css">

    .clocks {

        height: 92px;

        position: relative;

        width: 92px;

    }

</style>



<script lang="javascript">

    $(document).ready(function() {

        $('#password-clear').show();

        $('#password-password').hide();



        $('#password-clear').focus(function() {

            $('#password-clear').hide();

            $('#password-password').show();

            $('#password-password').focus();

        });

        $('#password-password').blur(function() {

            if($('#password-password').val() == '') {

                $('#password-clear').show();

                $('#password-password').hide();

            }

        });



        $('#default-value').each(function() {

            var default_value = this.value;

            $(this).focus(function() {

                if(this.value == default_value) {

                    this.value = '';

                }

            });

            $(this).blur(function() {

                if(this.value == '') {

                    this.value = default_value;

                }

            });

        });

    });

</script>



<script type="text/javascript">

    var _gaq = _gaq || [];



    _gaq.push(['_setAccount', 'UA-16911433-1']);

    _gaq.push(['_trackPageview']);



    (function() {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

    })();

</script>



<script language="javascript" type="text/javascript">

    // inner variables

    var canvas, ctx;

    var clockRadius = 120;

    var clockImage;



    // draw functions :

    function clear() { // clear canvas function

        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    }



    function drawScene() { // main drawScene function

        clear(); // clear canvas



        // get current time

        var date = new Date();

        var hours = date.getHours();

        var minutes = date.getMinutes();

        var seconds = date.getSeconds();

        hours = hours > 12 ? hours - 12 : hours;

        var hour = hours + minutes / 60;

        var minute = minutes + seconds / 60;



        // save current context

        ctx.save();



        // draw clock image (as background)

        ctx.drawImage(clockImage, 0, 0, 92, 92);

        ctx.translate(canvas.width / 2, canvas.height / 2);

        ctx.beginPath();



        // draw numbers

        ctx.font = '36px Arial';

        ctx.fillStyle = '#000';

        ctx.textAlign = 'center';

        ctx.textBaseline = 'middle';



        for (var n = 1; n <= 12; n++) {

            var theta = (n - 3) * (Math.PI * 2) / 12;

            var x = clockRadius * 1.7 * Math.cos(theta);

            var y = clockRadius * 1.7 * Math.sin(theta);

            ctx.fillText(n, x, y);

        }



        // draw hour

        ctx.save();

        var theta = (hour - 3) * 2 * Math.PI / 12;

        ctx.rotate(theta);

        ctx.beginPath();

        ctx.moveTo(-15, -5);

        ctx.lineTo(-15, 5);

        ctx.lineTo(clockRadius * 0.2, 1);

        ctx.lineTo(clockRadius * 0.2, -1);

        ctx.fillStyle = '#eddd4a';

        ctx.fill();

        ctx.restore();



        // draw minute

        ctx.save();

        var theta = (minute - 15) * 2 * Math.PI / 60;

        ctx.rotate(theta);

        ctx.beginPath();

        ctx.moveTo(-15, -4);

        ctx.lineTo(-15, 4);

        ctx.lineTo(clockRadius * 0.3, 1);

        ctx.lineTo(clockRadius * 0.3, -1);

        ctx.fillStyle = '#eddd4a';

        ctx.fill();

        ctx.restore();



        // draw second

        ctx.save();

        var theta = (seconds - 15) * 2 * Math.PI / 60;

        ctx.rotate(theta);

        ctx.beginPath();

        ctx.moveTo(-15, -3);

        ctx.lineTo(-15, 3);

        ctx.lineTo(clockRadius * 0.35, 1);

        ctx.lineTo(clockRadius * 0.35, -1);

        ctx.fillStyle = '#0f0';

        ctx.fill();

        ctx.restore();

        ctx.restore();

    }



    // initialization

    function startClock(){

        canvas = document.getElementById('canvas');

        ctx = canvas.getContext('2d');



        // var width = canvas.width;

        // var height = canvas.height;



        clockImage = new Image();

        clockImage.src = 'img/relogio.png';



        setInterval(drawScene, 1000); // loop drawScene

    }



	function CheckSearch() {

		if(document.searchform.searchtext.value!="") {

			return true;

		} else {

			return false;

		}

	}



	function ChangeLanguage(id) {

		window.location.href = 'changelanguage.html?lng=' + id;

	}



	var plsrechargebid = "<?=$lng_pleaserechargebid;?>";

	var printended = "<?=$lng_ended;?>";

	var printpause = "<?=$lng_pause;?>";

	var allimagepath = "<?=$lng_imagepath;?>";

</script>



<?

    setlocale(LC_ALL, NULL);

	setlocale(LC_ALL, 'pt_BR'); 



    if($_SESSION['url'] && $_SESSION['url']!=$SITE_URL) {

    	session_destroy();

    	echo "<script language='javascript'>window.location.href='index.html';</script>";

    	exit;

    }

?>



<?

    function checkaucstatus($status) {

		$st = $status;



		if($st=="2") {

			$qryauc = "select * from auction a left join auc_due_table adt on a.auctionID=adt.auction_id where adt.auc_due_time!=0 and auc_status='".$st."'";

		} else {

    		$qryauc = "select * from auction where auc_status='".$st."'";

		}



		$rsauc = mysql_query($qryauc);

		$totalauc = mysql_num_rows($rsauc);



		return $totalauc;

	}



	function getcatname($category) {

		$qrysel = "select *,c.".$lng_prefix."name as catname from auction a left join products p on a.productID=p.productID left join categories c on a.categoryID=c.categoryID where a.categoryID=".$category." and auc_status='2'";

		$rssel = mysql_query($qrysel);

		$totalsel = mysql_num_rows($rssel);



		return $totalsel;

	}

?>



<link href="css/style.css" rel="stylesheet" type="text/css" />



<div style="background: #fbd437;">

    <div class="topo_new">

    <div class="topo2">

    <div class="logo" >

      <a href="index.html">

        <img src="images/logo_bee_2.png" alt="Shock Bee" title="Shock Bee" />

      </a>

    </div>



    <div class="toolbar_new">

        <table class="login" style="width: 260px;margin-right: 50px;">

            <tbody>

                <tr>

                    <td><a href="myaccount.html">Minha conta</a></td>

                    <td><a href="registration.html">Registre-se</a></td>

                    <td><a href="help.html">Ajuda</a></td>

                </tr>

            </tbody>

        </table>

    <!--table>

        <tr>

            <td><!-img src="img/relogio.png"/->

            </td>

            <td>

                <span style="color: #FFF;" class="horas">

                <?php

                    echo date('G:i');

                ?>

                </span>

            </td>

        </tr>

        <tr>

            <td colspan="2">

                <span style="color: #FFF;">

                    <?php

                        dataCorrente();

                    ?>

                </span>

            </td>

        </tr>

    </table-->

      <!--form method="post" id="formLogin1" action="/Login">

        <p class="opcoes">

          <label for="login"> <strong>Login:</strong>

            <input type="text" class="campoM" name="login" id="login" value="" rel="send"/>

          </label>

          <label for="senha"> <strong>Senha:</strong>

            <input type="password" class="campoP" name="senha" id="senha" rel="send"/>

          </label>

          <input type="hidden" name="submitted" value="login" rel="send"/>

          <input type="image" class="botao" src="img/bt-ok.png"/>

        </p>

      </form>

      <p class="opcoes"> <a href="/Cadastro"> <img src="img/cadastrese.png"/> <strong>Cadastre-se</strong> </a> <a href="/EsqueciMinhaSenha" class="esqueci_minhasenha123"><img src="img/esquecesenha.png"/> <strong>Esqueci minha senha</strong></a> <a href="/ComprarLances"><img src="img/comprarlances.png"/><strong>Comprar Lances</strong></a> </p-->

    </div>



    <div class="data_topo">

      <strong><?php dataCorrente(); ?></strong>

    </div>



    <? if(!isset($_SESSION["userid"])) { ?>

     <div class="toolbar_login">

      <form method="post" id="formLogin1" action="password.html">

        <div class="opcoes">
        <table  border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><label for="login">

            <input id="default-value" type="text" class="campoM" name="username" id="login" rel="send" value="digite seu login"/>

          </label></td>
    <td><label for="senha">

            <input id="password-clear" type="text" class="campoP" id="senha" autocomplete="off" value="senha"/>

            <input id="password-password" type="password" class="campoP" name="password" id="senha" rel="send" autocomplete="off" />

          </label></td>
    <td><input type="hidden" name="submitted" value="login" rel="send"/>

          <input type="image" class="botao" src="images/enter.png"/></td>
  </tr>
</table>     
        </div>

      </form>

    </div>

    <?

    } else {

    ?>

    <div class="toolbar2" style="float:right;padding-top: 50px;">

        <strong style="font-size: 20px;color:#FFF">Seja bem vindo <?=$_SESSION["username"]?>, <a href="logout.html">Sair</a></strong> 

    </div>

    <? } ?>

    <!--div class="logado">

        <p class="botoes"> 

            <a href="/ComprarLances" title="comprar lances"> 

                <img alt="comprar lances" src="img/bt-comprar-lances.jpg"/> 

            </a> 

            <a href="/Login/logout" title="sair"> 

                <img alt="sair" src="img/bt-sair.jpg"/> 

            </a> 

            <a href="/MeuPerfil" title="sair"> 

                <img alt="sair" src="img/meuperfil.jpg"/> 

            </a> 

        </p>

        <!- lances ->

        <p class="cont-lances"> 

            <span class="foreground" id="ln1"></span> 

            <span class="shadow" id="ln2"></span> 

        </p>

    </div-->

    <!--<div class="section">

      <div class="headerBar">

        <!-div id="relogio">

            <span class="dia">

                <?php

               //     echo date('d') . " de " . date("M") . " de " . date("Y");

                ?>

            </span> 

            <span id="horas" class="hora">

                <?php

                 //   echo "<script language='javascript' type='text/javascript'>

//    						document.getElementById('horas').innerHTML = getTime();

//    					</script>";

                ?>

            </span>

        </div->

        <div>

            <ul class="menu">

              <li><a href="promocoes.html">PROMO&Ccedil;&Otilde;ES</a></li>

              <li> <a href="buybids.html">COMPRE LAN&Ccedil;ES</a></li>

              <li><a href="depoimentos.html">DEPOIMENTOS</a></li>

              <li><a href="how_it_works.html">COMO FUNCIONA</a></li>

            </ul>

        </div>



        <table class="login">

            <tr>

                <td><a href="myaccount.html">Minha conta</a></td>

                <td><a href="registration.html">Registre-se</a></td>

                <td><a href="help.html">Ajuda</a></td>

            </tr>

        </table>

      </div>

    </div>-->



    <!--div class="sectionFooter"></div-->

    <!--div class="section">

      <div class="frases"> <span class="frase">

        Lances comprados e pagos não tem prazo para usar.          </span> </div>

    </div>

    <div class="sectionFooter"></div-->

    <!--div id="esqueci_minhasenha" style="display:none;">

      <div id="msgSenha" style="background:#fff;padding:5px; font-size:11px;"></div>

      <div class="section">

        <div class="dialogs">

          <p><strong>Recuperar senha</strong></p>

          <form id="formEsqueciSenha" action="javascript:void(0);" method="post">

            <p>

              <label>Digite seu e-mail:</label>

              <input type="text" name="email_senha" id="email_senha" class="campoM" value=""/>

            </p>

            <p>

              <button type="button" id="solicitar_senha">Solicitar nova senha.</button>

            </p>

          </form>

        </div>

      </div>

      <div class="sectionFooter"></div>

    </div-->

    <!--div class="wrapCadastro">

      <div class="section">

        <h2 class="leiloesCadastro">Faça já o seu cadastro e comece dar seus lances agora mesmo!</h2>

        <!- div erro ->

        <div id="msgError"></div>

        <!- Esqueci minha senha ->

      </div>

      <div class="sectionFooter"></div>

    </div-->

</div>

            <div class="section2">

                <div class="content">

                    <div class="headerBar">

                        <div>

                            <ul class="menu">

                                <li><a href="promocoes.html">PROMO&Ccedil;&Otilde;ES</a></li>

                                <li> <a href="buybids.html">COMPRE LAN&Ccedil;ES</a></li>

                                <li><a href="depoimentos.html">DEPOIMENTOS</a></li>

                                <li><a href="how_it_works.html">COMO FUNCIONA</a></li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<?

	echo "<script language='javascript'>UpdateLoginLogout();</script>";

?>