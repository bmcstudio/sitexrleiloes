<nav class="menu2">
    <ul class="menu_galeria">
    <li><a style="cursor:pointer" class="active" onclick="ChangeActiveGalery(this,'_fotos', '_videos');">Fotos</a></li>
    <li><a style="cursor:pointer" onclick="ChangeActiveGalery(this,'_videos', '_fotos');">Vídeos</a></li>
 </ul>
    </nav>
<div class="galeria">
  <div class="conteudo_galeria" style="boder: 2px solid #000;">
    <div class="middlepart" style="width: 353px; margin: 25px 0;" id="_fotos">
        <div class="bigimage" style="margin-top: 5px;">
            <div id="mainimage1" onmousedown="ZoomIn(1)">
                <img id="imgZoom1" src="uploads/products/<?=$obj->picture1;?>" width="350" height="233"/>
            </div>
            <div id="mainimage2" onmousedown="ZoomIn(2)" style="display:none;"><? if($obj->picture2!=""){ ?><img id="imgZoom2" src="uploads/products/<?=$obj->picture2;?>" width="350" height="233"/><? }   ?></div>
            <div id="mainimage3" onmousedown="ZoomIn(3)" style="display: none"><? if($obj->picture3!=""){ ?><img id="imgZoom3" src="uploads/products/<?=$obj->picture3;?>" width="350" height="233"/><? } ?></div>
            <div id="mainimage4" onmousedown="ZoomIn(4)" style="display: none"><? if($obj->picture4!=""){ ?><img id="imgZoom4" src="uploads/products/<?=$obj->picture4;?>" width="350" height="233"/><? } ?></div>
        </div>
        <div class="thumb">
            <div style="padding-top: 15px; text-align: left; padding-left: 10px;">
            <? if($obj->picture1!=""){ ?>
            <img src="uploads/products/thumbs/thumb_<?=$obj->picture1;?>" width="79" height="60" onclick="changeimage(1);" id="otherimageprd_1" style="cursor: pointer" />
            <? } else { ?><img id="otherimageprd_1" src="<?=$lng_imagepath;?>no_image.gif"  width="79" height="60" style="cursor: pointer" /><? } ?>

            <? if($obj->picture2!=""){ ?>
            <img id="otherimageprd_2" src="uploads/products/thumbs/thumb_<?=$obj->picture2;?>" width="79" height="60" onclick="changeimage(2);" style="cursor: pointer" /><? } else { ?><img src="<?=$lng_imagepath;?>no_image.gif" style="cursor: pointer" width="79" height="60"/><? } ?>

            <? if($obj->picture3!=""){ ?>
            <img src="uploads/products/thumbs/thumb_<?=$obj->picture3;?>" width="79" height="60" style="cursor: pointer"  onclick="changeimage(3);" id="otherimageprd_3" />
            <? } else { ?><img src="<?=$lng_imagepath;?>no_image.gif" style="cursor: pointer" width="79" height="60" /><? } ?>

            <? if($obj->picture4!=""){ ?>
            <img id="otherimageprd_4" src="uploads/products/thumbs/thumb_<?=$obj->picture4;?>" onclick="changeimage(4);" width="79" height="60" style="cursor: pointer" /><? } else { ?><img src="<?=$lng_imagepath;?>no_image.gif" style="cursor: pointer" width="79" height="60"/><? } ?>
            </div>
        </div>
    </div>
    <div class="middlepart" id="_videos" style="display:none;">
        <div class="youtube_player bigvideo" style="margin: 25px 0;">
            <iframe id="mainvideo1" width="353" height="233" src="//www.youtube.com/embed/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video1,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>?rel=0&wmode=Opaque&enablejsapi=1" frameborder="0" allowfullscreen></iframe>
            <? if($obj->video2!=""){ ?><iframe id="mainvideo2" style="display:none;" width="353" height="233" src="//www.youtube.com/embed/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video2,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>?rel=0&wmode=Opaque&enablejsapi=1" frameborder="0" allowfullscreen></iframe><? }  ?></span>
            <? if($obj->video3!=""){ ?><iframe id="mainvideo3" style="display: none;" width="353" height="233" src="//www.youtube.com/embed/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video3,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>?rel=0&wmode=Opaque&enablejsapi=1" frameborder="0" allowfullscreen></iframe><? } ?></span>
            <? if($obj->video4!=""){ ?><iframe id="mainvideo4" style="display: none" width="353" height="233" src="//www.youtube.com/embed/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video4,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>?rel=0&wmode=Opaque&enablejsapi=1" frameborder="0" allowfullscreen></iframe><? } ?></span>
        </div>
        <div class="thumb">
            <div style="margin-top: -27px; text-align: left; padding-left: 10px;">
            <? if($obj->video1!=""){ ?>
            <img src="http://i1.ytimg.com/vi/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video1,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>/0.jpg" width="79" height="60" onclick="changevideo(1);" id="otherimageprd_1" style="cursor: pointer" />
            <? } else { ?><img id="othervideo_1" src="<?=$lng_imagepath;?>no_image.gif"  width="79" height="60" style="cursor: pointer" /><? } ?>

            <? if($obj->video2!=""){ ?>
            <img id="othervideo_2" src="http://i1.ytimg.com/vi/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video2,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>/0.jpg" width="79" height="60" onclick="changevideo(2);" style="cursor: pointer" /><? } else { ?><img src="<?=$lng_imagepath;?>no_image.gif" style="cursor: pointer" width="79" height="60"/><? } ?>

            <? if($obj->video3!=""){ ?>
            <img src="http://i1.ytimg.com/vi/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video3,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>/0.jpg" width="79" height="60" style="cursor: pointer"  onclick="changevideo(3);" id="othervideo_3" />
            <? } else { ?><img src="<?=$lng_imagepath;?>no_image.gif" style="cursor: pointer" width="79" height="60" /><? } ?>

            <? if($obj->video4!=""){ ?>
            <img id="othervideo_4" src="http://i1.ytimg.com/vi/<?php
                    preg_match(
                        '/[\\?\\&]v=([^\\?\\&]+)/',
                        $obj->video4,
                        $matches
                    );
                    // $matches[1] should contain the youtube id
                    echo $matches[1];

                ?>/0.jpg" onclick="changevideo(4);" width="79" height="60" style="cursor: pointer" /><? } else { ?><img src="<?=$lng_imagepath;?>no_image.gif" style="cursor: pointer" width="79" height="60"/><? } ?>
            </div>
        </div>
    </div>
</div>
<style>
    #divZoomIn > div[id^='mlens']{
        margin: 0 auto !important;

    }
</style>
