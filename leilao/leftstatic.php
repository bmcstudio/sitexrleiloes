<div id="left-side">
    <div class="my-account-left">
        <div class="title">
            <h3>MENU</h3>
        </div>
        <div class="leftnav" align="center">
            <div class="staticleft">
                <? if($staticvar!="about"){?>
                    <a href="about_us.html"><?=$lng_aboutus;?></a>
                <? } else{ ?>
                    <span class="red-text-12-b"><?=$lng_aboutus;?></span>
                <? } ?>
            </div>
            <div class="staticdevider"></div>
            <div class="staticleft">
                <? if($staticvar!="contact"){?>
                    <a href="contact_us.html"><?=$lng_contact;?></a>
                <? } else { ?>
                    <span class="red-text-12-b"><?=$lng_contact;?></span>
                <? } ?>
            </div>
            <div class="staticdevider"></div>
            <!--<div class="staticleft"><? if($staticvar!="jobs"){?><a href="jobs.html"><?=$lng_jobs;?></a><? } else { ?><span class="red-text-12-b"><?=$lng_jobs;?></span><? } ?></div>
            <div class="staticdevider"></div>-->
            <div class="staticleft">
                <? if($staticvar!="terms"){?>
                    <a href="terms_and_conditions.html"><?=$lng_termscondi;?></a>
                <? } else {?>
                    <span class="red-text-12-b"><?=$lng_termscondi;?></span>
                <? } ?>
            </div>
            <div class="staticdevider"></div>
            <div class="staticleft">
                <? if($staticvar!="privacy"){?>
                    <a href="privacy_policy.html"><?=$lng_privacy;?></a>
                <? } else { ?>
                    <span class="red-text-12-b"><?=$lng_privacy;?></span>
                <? } ?>
            </div>
            <div class="staticdevider"></div>
            <div class="staticleft">
                <? if($staticvar!="help"){?>
                    <a href="help.html"><?=$lng_tabhelp;?></a>
                <? } else { ?>
                    <span class="red-text-12-b"><?=$lng_tabhelp;?></span>
                <? } ?>
            </div>
            <div class="staticdevider"></div>
            <div class="staticleft" style="padding-bottom: 10px;">
                <? if($staticvar!="howit"){?>
                    <a href="how_it_works.html"><?=$lng_howitwork;?></a>
                <? } else { ?>
                    <span class="red-text-12-b"><?=$lng_howitwork;?></span>
                <? } ?>
            </div>
        </div>
    </div>
</div>