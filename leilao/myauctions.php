<?
	include("config/connect.php");
	include("language/portugues.php");
	include("session.php");
	include("functions.php");
	$uid = $_SESSION["userid"];
	if(!$_GET['pgno']) {
		$PageNo = 1;
	} else {
		$PageNo = $_GET['pgno'];
	}

	$qryselauc = "select *,p.".$lng_prefix."name as name,".$lng_prefix."short_desc as short_desc from bid_account ba left join auction a on ba.auction_id=a.auctionID left join products p on ba.product_id=p.productID left join auc_due_table adt on a.auctionID=adt.auction_id where a.auc_status='2' and adt.auc_due_time!=0 and ba.user_id=$uid group by ba.auction_id order by auc_due_time";
	$resselauc = mysql_query($qryselauc);
	$totalauc = mysql_num_rows($resselauc);
	$totalpage=ceil($totalauc/$PRODUCTSPERPAGE_MYACCOUNT);

	if($totalpage>=1) {
    	$startrow=$PRODUCTSPERPAGE_MYACCOUNT*($PageNo-1);
    	$qryselauc.=" LIMIT $startrow,$PRODUCTSPERPAGE_MYACCOUNT";
    	//echo $sql;
    	$resselauc=mysql_query($qryselauc);
    	$totalauc=mysql_num_rows($resselauc);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" src="function.js"></script>
</head>
<body class="admin_page" onload="OnloadPage();">
    <? include("header.php"); ?>
    <div id="main_div">
		<div id="middle_div">
            <div id="cleaner"></div>
			<? include("leftside.php"); ?>
			<div class="inner-container content-center">
				<div class="titlebar">
					<div class="leftbar"></div>
					<div class="middlebar"><div class="page_title_font"><?=$lng_myauctionsavenue;?> - <?=$lng_aucbiddingon;?></div></div>
					<div class="rightbar"></div>
				</div>
				<div class="bodypart">	
					<div style="height: 15px;">&nbsp;</div>
					<?
					if($totalauc>0)
					{
					?>
					<?
						$counter = 1;
						while($obj = mysql_fetch_array($resselauc))
						{
							if($counter==1)
							{
								$arr = $obj["auctionID"];
								$prr = $obj["auc_due_price"];
							}
							else
							{
								$arr .= ",".$obj["auctionID"];
								$prr .= ",".$obj["auc_due_price"];
							}
					 ?>	
					 <? if($counter==1){ ?>
						<div class="strip strip-title">
							<div class="imagetext"><?=$lng_image;?></div>
							<div class="descriptiontext"><?=$lng_description;?></div>
							<div class="pricetext">Lance</div>
							<div class="biddertext" style="width: 105px; text-align: right;"><?=$lng_countdown;?></div>
						</div>
						<div style="height: 15px;">&nbsp;</div>
					<? } ?>
					  <div class="auction-item" style="display: none" title="<?=$obj["auctionID"];?>" id="auction_<?=$obj["auctionID"];?>"></div>
						<div class="even-row">
							<div class="evenproductimage">
							<a href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html">
							<img src="uploads/products/thumbs_big/thumbbig_<?=$obj["picture1"];?>" border="0"></a></div>
							<div class="evenproductdesc"><div class="auction_decription"><a href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html" class="black_link"><?=stripslashes($obj["name"]);?></a><br /><br /><?=stripslashes(choose_short_desc($obj["short_desc"],160));?><a href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html" class="black_link"><?=$lng_linkmore;?></a></div></div>
							<div class="evenproductprice">
								<span class="body_inner_box_price">
									<span style="font-size:15px; font-weight: bold;" id="currencysymbol_<?=$obj["auctionID"]?>"></span><span style="font-size:15px; font-weight: bold;" id="price_index_page_<?=$obj["auctionID"];?>">---</span><br />
									<div style="padding-top: 5px;" class="bidderfont">
										<span id="product_bidder_<?=$obj["auctionID"];?>"></span>
									</div>
								</span>
							</div>
							<div class="evenproductbidder">
								<span id="counter_index_page_<?=$obj["auctionID"];?>">
									<?
										echo "<script language=javascript>
											  document.getElementById('counter_index_page_".$obj["auctionID"]."').innerHTML = calc_counter_from_time('".$obj["auc_due_time"]."');
											 </script>";
									?>
								</span>					
							</div>							
						</div>
					
					<?
							$counter++;	
						}
					?>
					<div style="height: 10px;"></div>
					<div class="strip">
						<div style="padding-top: 2px;">
					<?
					if($PageNo>1)
					{
					  $PrevPageNo = $PageNo-1;
					?>
						  <a class="alink" href="myauctions_<?=$PrevPageNo; ?>.html">&lt; <?=$lng_previouspage;?></a>
					<?
						if($totalpage>2 && $totalpage!=$PageNo)
						{
					 ?>
						<span class="paging">&nbsp;|</span>
					 <?
						}
					  }
				     ?>&nbsp;
				     <?php
			 		  if($PageNo<$totalpage)
					  {
						 $NextPageNo = 	$PageNo + 1;
					  ?>
						  <a class="alink" id="next" href="myauctions_<?=$NextPageNo;?>.html"><?=$lng_nextpage;?> &gt;</a>
					  <?
					   }
					  ?>
					     </div>
					</div>	
					<?
					}
					else
					{
					?>		
					<div>&nbsp;</div>
					<div class="noauction_message" align="center"><?=$lng_notbiddingany;?></div>
					<div>&nbsp;</div>
					<?
					}
					?>
				</div>
			</div>
            <div id="cleaner"></div>
		</div>
    </div>
    <? include("footer.php"); ?>
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="effect.js"></script>
<script language="javascript" src="default.js"></script>
<script language="javascript" src="jqModal.js"></script>
</body>
</html>
