<?
	include("config/connect.php");
	include("language/portugues.php");
	include("session.php");
	include("functions.php");

	$uid = $_SESSION["userid"];
	
	if($_POST["submit"]!="" && $_POST["unsubscribecheck"]!="")
	{
		$qryupd = "update registration set account_status='2' where id='$uid'";
		mysql_query($qryupd) or die(mysql_error());
		header("location: logout.html");
		exit;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript" src="function.js"></script>
<script language="javascript">
	function check()
	{
		if(document.unsubscribe.unsubscribecheck.checked==false)
		{
			alert("<?=$lng_plstickcheckbox;?>");
			return false;
		}
	}
</script>
</head>


<body class="admin_page">
    <? include("header.php"); ?>
    <div id="main_div">
		<div id="middle_div">
            <div id="cleaner"></div>
			<? include("leftside.php"); ?>
			<div class="inner-container content-center">
				<div class="titlebar">
					<div class="leftbar"></div>
					<div class="middlebar"><div class="page_title_font"><?=$lng_myauctionsavenue;?> - <?=$lng_closeaccount;?></div></div>
					<div class="rightbar"></div>
				</div>
				<div class="bodypart" style="text-align: left;">	
				<form name="unsubscribe" action="" method="post" onsubmit="return check();">
					<div id="middle">
						<h4 class="grayboldfont"><?=$lng_closeavenueacc;?></h4>
						<div style="width: 600px; margin-top: 10px;" class="normal_text"><?=$lng_closeimportantnote;?></div>
						<div style="margin-top: 10px;" class="normal_text"><input type="checkbox" name="unsubscribecheck" value="unsubscribe" class="checkbox" />&nbsp;&nbsp;<?=$lng_yescloseaccount;?></div>
						<div style="margin-top: 25px;"><input type="image" name="submitimage" value="Close" src="<?=$lng_imagepath;?>close.jpg" onmouseover="this.src='<?=$lng_imagepath;?>close.jpg'" onmouseout="this.src='<?=$lng_imagepath;?>close.jpg'" /></div>
						<input type="hidden" name="submit" value="submit" />
						<div style="height: 20px; clear:both">&nbsp;</div>
					</div>
				</form>				
				</div>
			</div>
            <div id="cleaner"></div>
		</div>
    </div>
    <? include("footer.php"); ?>		
</body>
</html>
