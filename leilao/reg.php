<?php
header('Content-type: application/rtf, Content-Disposition: inline, filename=Regulamento_v1.rtf'); /* aqui você muda o mime type para msword, avisando para o browser que o tipo de conteúdo do arquivo vi ser um doc. */

$arquivo_caminho = "regulamento/regulamento/Regulamento_v1.rtf"; //criei uma variável que armazena o meu arquivo doc.

$fp = fopen($arquivo_caminho, 'r'); //executo a função fopen para abrir meu arquivo em mode leitura apenas
$arquivo_saida = fread($fp, filesize($arquivo_caminho));  /* aqui crio uma variável arquivo_saida para receber o arquivo aberto para leitura pegando um total de conteúdo para ler, o total do conteúdo pode ser informado em bytes, mas como eu quero ler o conteúdo inteiro então pego a função filesize para retornar o tamanho total do arquivo. */
fclose($fp);

$arquivo_final = $arquivo_saida;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>XRLielões</title>
</head>
<body>
	<?=$arquivo_final?>
</body>
</html>