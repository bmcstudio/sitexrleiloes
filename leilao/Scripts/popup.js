      $(document).ready(function() {
		$(".item_enter, .item_view, .item_image, .evenproductimage a, .showpop").on('click', function(e){
		    e.preventDefault();
		    $.get($(this).attr('href'), {}, function(response){
		        el = $(response).find('#product_details_main').html();
		        el = $('<div id="product_details_main" />').html(el);
		        $.fancybox({
		            content     : el,
		            maxWidth	: '790px',
		            // maxHeight	: '450px',
                maxHeight : '90%',
		            fitToView	: false,
		            width		: '790px',
		            // height		: '450px',
                height  : '90%',
		            autoSize	: false,
		            closeClick	: false,
		            openEffect	: 'none',
		            closeEffect	: 'none'
	            });
	        }, 'html');
	    });
	});