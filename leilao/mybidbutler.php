<?
	include("config/connect.php");
	include("session.php");
	include("functions.php");
	$uid = $_SESSION["userid"];

	if(!$_GET['pgno'])
	{
		$PageNo = 1;
	}
	else
	{
		$PageNo = $_GET['pgno'];
	}

	$qrysel = "select *,p.".$lng_prefix."name as name,".$lng_prefix."short_desc as short_desc from bidbutler bb left join auction a on bb.auc_id=a.auctionID left join products p on a.productID=p.productID where bb.user_id='$uid' and a.auc_status='2' and butler_status='0'";
	$ressel = mysql_query($qrysel);
	$total = mysql_num_rows($ressel);
	$totalpage=ceil($total/$PRODUCTSPERPAGE_MYACCOUNT);
	if($totalpage>=1)
	{
	$startrow=$PRODUCTSPERPAGE_MYACCOUNT*($PageNo-1);
	$qrysel.=" LIMIT $startrow,$PRODUCTSPERPAGE_MYACCOUNT";
	//echo $sql;
	$ressel=mysql_query($qrysel);
	$total=mysql_num_rows($ressel);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" src="jquery.js"></script>
<script language="javascript" src="effect.js"></script>
<script language="javascript" src="default.js"></script>
<script language="javascript" src="jqModal.js"></script>
<script language="javascript" src="function.js"></script>
<script language="javascript" type="text/javascript">
function DeleteBidButler1(id)
{
//		alert(id);
		var url = "deletebutler.php?delid=" + id; 
		$.ajax({
			url: url = "deletebutler.php?delid=" + id,
			dataType: 'json',
			success: function(data){
			$.each(data, function(i, item){
			result = item.result;
			if(result=="unsuccess")
			{
				alert("<?=$lng_youbidisrunning;?>");
			}
			else
			{
				window.location.href='mybidbutler.html';
			}
			});
		}
		});
}
</script>
</head>


<body class="admin_page">
    <? include("header.php"); ?>
    <div id="main_div">
		<div id="middle_div">
            <div id="cleaner"></div>
			<? include("leftside.php"); ?>
			<div class="inner-container">
				<div class="titlebar">
					<div class="leftbar"></div>
					<div class="middlebar"><div class="page_title_font"><?=$lng_myauctionsavenue;?> - <?=$lng_activebidbuddy;?></div></div>
					<div class="rightbar"></div>
				</div>
				<div class="bodypart">	
					<div style="height: 15px;">&nbsp;</div>
					<?
					if($total>0)
					{
						$counter = 1;
						while($obj = mysql_fetch_array($ressel))
						{
				    ?>	
					 <? if($counter==1){ ?>
						<div class="strip">
							<div class="watchimagetext"><?=$lng_image;?></div>
							<div class="watchdescriptiontext"><?=$lng_description;?></div>
							<div class="watchpricetext"><?=$lng_butlerstartprice;?></div>
							<div class="watchbiddertext"><?=$lng_endprice;?></div>
							<div class="watchcountdowntext"><?=$lng_bids;?></div>
							<div class="watchaucdelete" style="text-align: right; width: 50px;"><?=$lng_deletebutlerbid;?></div>
						</div>
						<div style="height: 15px;">&nbsp;</div>
					<? } ?>
						<div class="even-row">
							<div class="watchevenproductimage"><a href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html"><img src="uploads/products/thumbs_big/thumbbig_<?=$obj["picture1"];?>" style="border: medium none ;"></a></div>
							<div class="watchevenproductdesc"><div class="watchauction_decription"><a href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html" class="black_link"><?=stripslashes($obj["name"]);?></a><br /><br /><?=stripslashes(choose_short_desc($obj["short_desc"],130));?><a href="auction<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html" class="black_link"><?=$lng_linkmore;?></a></div></div>
							<div class="watchevenproductprice"><b><?=$Currency;?><?=$obj["butler_start_price"];?></b></div>
							<div class="butlerendprice"><b><?=$Currency;?><?=$obj["butler_end_price"];?></b></div>
							<div class="watchevenproductcountdown"><b><?=$obj["butler_bid"];?></b></div>
							<div class="watchdelete"><img src="<?=$lng_imagepath;?>btn_closezoom.png" style="cursor: pointer;" onclick="DeleteBidButler1('<?=$obj["id"]?>');"/></div>
						</div>
					
					<?
							$counter++;	
						}
					?>
					<div style="height: 10px;"></div>
					<div class="strip">
						<div style="padding-top: 2px;">
					<?
					if($PageNo>1)
					{
					  $PrevPageNo = $PageNo-1;
					?>
						  <a class="alink" href="mybidbutler_<?=$PrevPageNo;?>.html">&lt; <?=$lng_previouspage;?></a>
					<?
						if($totalpage>2 && $totalpage!=$PageNo)
						{
					 ?>
						<span class="paging">&nbsp;|</span>
					 <?
						}
					  }
				     ?>&nbsp;
				     <?php
			 		  if($PageNo<$totalpage)
					  {
						 $NextPageNo = 	$PageNo + 1;
					  ?>
						  <a class="alink" id="next" href="mybidbutler_<?=$NextPageNo;?>.html"><?=$lng_nextpage;?> &gt;</a>
					  <?
					   }
					  ?>
					     </div>
						<div id="cleaner"></div>
					</div>	
					<?
					}
					else
					{
					?>
					<div class="noauction_message"><?=$lng_noactivebidbuddy;?></div>
					<div style="height: 15px;">&nbsp;</div>
					<?
					}
					?>
				</div>
				<div class="bottomline">
					<div class="leftsidecorner"></div>
					<div class="middlecorner"></div>
					<div class="rightsidecorner"></div>
				</div>
			</div>
            <div id="cleaner"></div>
		</div>
    </div>
    <? include("footer.php"); ?>
</form>
</body>
</html>
