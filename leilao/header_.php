<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<style type="text/css">
    .clocks {
        height: 92px;
        position: relative;
        width: 92px;
    }
</style>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-16911433-1']);
    _gaq.push(['_trackPageview']);
    
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
<div style="display: none;">
<img src="images/input_box.jpg" />
<img src="images/All_categories_hover1.jpg" />
<img src="images/All_categories_hover.jpg" />
<img src="images/All_categories_last.jpg" />
<img src="<?=$lng_imagepath;?>registerbgsnap1.gif" />
<img src="<?=$lng_imagepath;?>all_cate_bg_new.jpg" />
</div>
<script language="javascript" type="text/javascript">
    // inner variables
    var canvas, ctx;
    var clockRadius = 120;
    var clockImage;
    
    // draw functions :
    function clear() { // clear canvas function
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }
    
    function drawScene() { // main drawScene function
        clear(); // clear canvas
        
        // get current time
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        hours = hours > 12 ? hours - 12 : hours;
        var hour = hours + minutes / 60;
        var minute = minutes + seconds / 60;
        
        // save current context
        ctx.save();
        
        // draw clock image (as background)
        ctx.drawImage(clockImage, 0, 0, 92, 92);
        
        ctx.translate(canvas.width / 2, canvas.height / 2);
        ctx.beginPath();
        
        // draw numbers
        ctx.font = '36px Arial';
        ctx.fillStyle = '#000';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        
        for (var n = 1; n <= 12; n++) {
            var theta = (n - 3) * (Math.PI * 2) / 12;
            var x = clockRadius * 1.7 * Math.cos(theta);
            var y = clockRadius * 1.7 * Math.sin(theta);
            ctx.fillText(n, x, y);
        }
        
        // draw hour
        ctx.save();
        var theta = (hour - 3) * 2 * Math.PI / 12;
        ctx.rotate(theta);
        ctx.beginPath();
        ctx.moveTo(-15, -5);
        ctx.lineTo(-15, 5);
        ctx.lineTo(clockRadius * 0.2, 1);
        ctx.lineTo(clockRadius * 0.2, -1);
        ctx.fillStyle = '#eddd4a';
        ctx.fill();
        ctx.restore();
        
        // draw minute
        ctx.save();
        var theta = (minute - 15) * 2 * Math.PI / 60;
        ctx.rotate(theta);
        ctx.beginPath();
        ctx.moveTo(-15, -4);
        ctx.lineTo(-15, 4);
        ctx.lineTo(clockRadius * 0.3, 1);
        ctx.lineTo(clockRadius * 0.3, -1);
        ctx.fillStyle = '#eddd4a';
        ctx.fill();
        ctx.restore();
        
        // draw second
        ctx.save();
        var theta = (seconds - 15) * 2 * Math.PI / 60;
        ctx.rotate(theta);
        ctx.beginPath();
        ctx.moveTo(-15, -3);
        ctx.lineTo(-15, 3);
        ctx.lineTo(clockRadius * 0.35, 1);
        ctx.lineTo(clockRadius * 0.35, -1);
        ctx.fillStyle = '#0f0';
        ctx.fill();
        ctx.restore();
        
        ctx.restore();
    }
    
    // initialization
    (function(){
        canvas = document.getElementById('canvas');
        ctx = canvas.getContext('2d');
        
        // var width = canvas.width;
        // var height = canvas.height;
        
        clockImage = new Image();
        clockImage.src = 'img/relogio.png';
        
        setInterval(drawScene, 1000); // loop drawScene
    });

	function CheckSearch() {
		if(document.searchform.searchtext.value!="") {
			return true;
		} else {
			return false;
		}
	}
	function ChangeLanguage(id)	{
		window.location.href = 'changelanguage.html?lng=' + id;
	}
	var plsrechargebid = "<?=$lng_pleaserechargebid;?>";
	var printended	   = "<?=$lng_ended;?>";
	var printpause		= "<?=$lng_pause;?>";
	var allimagepath   = "<?=$lng_imagepath;?>";
</script>
<?
setlocale(LC_ALL, NULL);
	setlocale(LC_ALL, 'pt_BR'); 
if($_SESSION['url'] && $_SESSION['url']!=$SITE_URL)
{
	session_destroy();
	echo "<script language='javascript'>window.location.href='index.html';</script>";
	exit;
}
?>
<?
function checkaucstatus($status)
	{
		$st = $status;
		if($st=="2")
		{
			$qryauc = "select * from auction a left join auc_due_table adt on a.auctionID=adt.auction_id where adt.auc_due_time!=0 and auc_status='".$st."'";
		}
		else
		{
		$qryauc = "select * from auction where auc_status='".$st."'";
		}
		$rsauc = mysql_query($qryauc);
		$totalauc = mysql_num_rows($rsauc);
		return $totalauc;
	}
	
	function getcatname($category)
	{
			$qrysel = "select *,c.".$lng_prefix."name as catname from auction a left join products p on a.productID=p.productID left join categories c on a.categoryID=c.categoryID where a.categoryID=".$category." and auc_status='2'";
			$rssel = mysql_query($qrysel);
			$totalsel = mysql_num_rows($rssel);
			return $totalsel;
	}
?>

<div id="header_top">
		<div style="display: none;">
			<img src="<?=$lng_imagepath;?>BID_btn.png" />
			<img src="<?=$lng_imagepath;?>BID_btn_hover.png" />
			<img src="<?=$lng_imagepath;?>BID_btn_hover2.png" />
			<img src="<?=$lng_imagepath;?>LOGIN_btn-2.png" />
			<img src="<?=$lng_imagepath;?>LOGIN_btn-over.png" />
			<img src="<?=$lng_imagepath;?>LOGIN_btn.png" />
			<img src="<?=$lng_imagepath;?>login_btn3.png" />
			<img src="<?=$lng_imagepath;?>register_btn-hover1.gif" />
			<img src="<?=$lng_imagepath;?>register_btn-hover.gif" />
			<img src="<?=$lng_imagepath;?>buy bids_hover.png" />
			<img src="<?=$lng_imagepath;?>close_hover_btn.png" />
			<img src="<?=$lng_imagepath;?>edit_hover_btn.png" />
			<img src="<?=$lng_imagepath;?>make a payment_hover_btn.png" />
			<img src="<?=$lng_imagepath;?>send_hover_btn.png" />
			<img src="<?=$lng_imagepath;?>SOLD_btn.png" />
			<img src="<?=$lng_imagepath;?>submit_hover_btn.png" />
			<img src="<?=$lng_imagepath;?>subscribe_hover_btn.png" />
			<img src="<?=$lng_imagepath;?>unsubscribe_hover_btn.png" />
		</div>
		<div class="logo_new">
		<div id="logo_maindiv_left">
			<div id="logo_maindiv"><a href="index.html"><img src="<?=$lng_imagepath;?>logo.jpg" border="0" /></a></div>
		</div>
		<div id="logo_maindiv_right">
			<div class="div_menu">
				<!--<div class="allmaps_new"><img src="<?=$lng_imagepath;?>map1.png" />&nbsp;<? if($_SESSION["language_name"]=="GR"){ ?><img src="<?=$lng_imagepath;?>map2.png" /><? } else { ?><img src="<?=$lng_imagepath;?>map2.png" border="0" style="cursor: pointer;" onclick="ChangeLanguage('gr');" /><? } ?>&nbsp;<? if($_SESSION["language_name"]=="SP"){ ?><img src="<?=$lng_imagepath;?>map3.png" /><? } else { ?><img src="<?=$lng_imagepath;?>map3.png" border="0" onclick="ChangeLanguage('sp');" style="cursor: pointer;" /></a><? } ?>&nbsp;<img src="<?=$lng_imagepath;?>map4.png" /></div>-->
				<? if($_SESSION["userid"]==""){ ?>
				<div class="menu_new2"><span class="top_menu"><div class="home"><a href="index.html"><?=$lng_tabhome;?></a></div><div class="acclink"><a href="myaccount.html"><?=$lng_tabmyavenue;?></a></div><div class="reglink"><a href="registration.html"><?=$lng_tabregister;?></a></div><div class="helplink"><a href="help.html"><?=$lng_tabhelp;?></a></div></span></div>
				<? } else { ?>
				<div class="menu_new2"><span class="top_menu"><div class="home"><a href="index.html"><?=$lng_tabhome;?></a></div><div class="acclink"><a href="myaccount.html" ><?=$lng_tabmyavenue;?></a></div><div class="helplink2"><a href="help.html"><?=$lng_tabhelp;?></a></div><div class="logoutlink"><a href="logout.html"><?=$lng_logout;?></a></div></span></div>
				<? } ?>
			</div>
			
            <div class="folow">
            <a href="http://www.orkut.com.br/Main#Profile?uid=13251088210323862923" target="_blank"><img src="images/orkuticon.png" width="50" border="0" /></a>&nbsp;<a href="http://twitter.com/baratodelance" target="_blank"><img src="images/twittericon.jpg" width="50" border="0" /></a>&nbsp;<a href="http://www.facebook.com/profile.php?id=100001022327473&ref=profile" target="_blank"><img src="images/facebookicon.png" width="50" border="0" /></a>
      </div><br />
            <!--<div class="clokdate">
            <? //echo ucfirst(gmstrftime('%A, %d de %B de %Y'));?>
            </div>-->
		<? if($_SESSION["userid"]==""){ ?>
			<div class="welcome_register"><?=$lng_welcome;?>!&nbsp;<?=$lng_regforfree;?></div>
		<? } else { ?>
			<div class="welcome_register2"><?=$lng_welcome;?>&nbsp;<?=getUserName($_SESSION["userid"]);?>&nbsp;<span>|</span>&nbsp;<?=$lng_availablebids;?> : <span id="bids_count" style="color: #000000"><?=GetUserFinalBids($_SESSION["userid"]);?></span></div>
		<? } ?>
        
		</div>
		</div>
		<div id="all_cate_bar">        
			<div id="all_cate_btn" style="background: url(<?=$lng_imagepath;?>all_cate_bg_new.jpg) no-repeat right;">
			<div class="menu">
				<ul>
				<li><a class="hide" href="all_live_auctions_2.html"></a> 
				<!--[if lte IE 6]>
				<a href="#">
				<table><tr><td>
				<![endif]-->
				<ul>
				<li><a href="all_live_auctions_2.html"><?=$lng_allliveauc;?> (<?=checkaucstatus(2);?>)</a></li>
			
						
		<?
				$qryc = "select *,".$lng_prefix."name as name from categories where status='1' ORDER BY name";
				$resc = mysql_query($qryc);
				$totalc = mysql_num_rows($resc);

				while($cat = mysql_fetch_array($resc))
				{
					echo '<li style="margin-top: -5px;"><a href="all_auctions_'.$cat["categoryID"].'.html">&nbsp;&nbsp;&nbsp;'.stripslashes($cat["name"]).' ('.getcatname($cat["categoryID"]).')</a></li>';
				}
		?>
				<li style="margin-top: -5px;"><a href="all_future_auctions_1.html"><?=$lng_futureaucs;?> (<?=checkaucstatus(1);?>)</a></li>
				<li style="margin-top: -5px;"><a href="all_ended_auctions_3.html"><?=$lng_endedauc;?> (<?=checkaucstatus(3);?>)</a></li>
		        <li id="ddf" style="margin-top: -5px;"><a></a></li>
				</ul>
					<!--[if lte IE 6]>
					</td></tr></table>
					</a>
					<![endif]-->
				 </li>
			 </ul>
		</div>
		</div>
		<form name="searchform" action="allauctions.html" onsubmit="return CheckSearch();" method="post">
		<div id="input_textbox"><input type="text" name="searchtext" class="searchbox" value="<?=$searchdata!=""?$searchdata:"";?>"/></div>
		<div class="search_btn"><input type="image" name="searchbutton" src="<?=$lng_imagepath;?>search_btn.jpg" /></div>
		<div id="how_it_btn"><a href="promocoes.html"><img src="<?=$lng_imagepath;?>promocoes.png"/></a><a href="buybids.html"><img src="<?=$lng_imagepath;?>comprebids.png"/></a><a href="depoimentos.html"><img src="<?=$lng_imagepath;?>depimentos.png"/></a><a href="how_it_works.html"><img src="<?=$lng_imagepath;?>comofunciona.png"/></a></div>
		</div>	
		<input type="hidden" name="search" value="search" id="search" />
		</form>
		</div>
	<?
		echo "<script language='javascript'>
		UpdateLoginLogout();
	</script>";
	?>
    
