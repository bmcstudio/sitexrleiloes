<?
	header("Access-Control-Allow-Credentials: true");
    header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
    header("Access-Control-Allow-Headers: *, X-Requested-With, Content-Type");
    header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT");

	include("../config/connect.php");
	include("../language/portugues.php");
	include("../functions.php");

	$postdata = file_get_contents("php://input");
	$request = json_decode($postdata);

	function mascara_string($mascara,$string)
	{
	   // $string = str_replace(" ","",$string);
	   for($i=0;$i<strlen($string);$i++)
	   {
	      $mascara[strpos($mascara,"#")] = $string[$i];
	   }
	   return $mascara;
	}

	$info_nome = $request->info->nome;
	$info_cpf = mascara_string( "###.###.###-##", $request->info->cpf);
	$info_rg = $request->info->rg;
	$info_nascimento = date("d/m/Y", strtotime( $request->info->nascimento ) );
	$info_sexo = $request->info->sexo;
	$info_endereco = $request->info->endereco;
	$info_complemento = $request->info->complemento;
	$info_cep = $request->info->cep;
	$info_cidade = $request->info->cidade;
	$info_estado = $request->info->estado;
	$info_pais = $request->info->pais;

	if ( strlen($request->info->tel1) == 10 ) $info_tel1 = mascara_string( "(##) ####-####", $request->info->tel1 );
	else if ( strlen($request->info->tel1) == 11 ) $info_tel1 = mascara_string( "(##) #####-####", $request->info->tel1 );
	if ( strlen($request->info->tel2) == 10 ) $info_tel2 = mascara_string( "(##) ####-####", $request->info->tel2 );
	else if ( strlen($request->info->tel2) == 11 ) $info_tel2 = mascara_string( "(##) #####-####", $request->info->tel2 );

	$profissional_profissao = $request->profissional->profissao;
	$profissional_empresa = $request->profissional->empresa;
	$profissional_cnpj = mascara_string( "##.###.###/####-##", $request->profissional->cnpj );
	$profissional_autonomo = $request->profissional->autonomo;
	$profissional_renda = $request->profissional->renda;
	$profissional_endereco = $request->profissional->endereco;
	$profissional_complemento = $request->profissional->complemento;
	$profissional_cep = $request->profissional->cep;
	$profissional_cidade = $request->profissional->cidade;
	$profissional_estado = $request->profissional->estado;
	$profissional_pais = $request->profissional->pais;
	if ( strlen($request->profissional->tel1) == 10 ) $profissional_tel1 = mascara_string( "(##) ####-####", $request->profissional->tel1 );
	else if ( strlen($request->profissional->tel1) == 11 ) $profissional_tel1 = mascara_string( "(##) #####-####", $request->profissional->tel1 );
	if ( strlen($request->profissional->tel2) == 10 ) $profissional_tel2 = mascara_string( "(##) ####-####", $request->profissional->tel2 );
	else if ( strlen($request->profissional->tel2) == 11 ) $profissional_tel2 = mascara_string( "(##) #####-####", $request->profissional->tel2 );

	$refbancaria_banco = $request->refbancaria->banco;
	$refbancaria_agencia = $request->refbancaria->agencia;
	$refbancaria_conta = $request->refbancaria->conta;
	if ( strlen($request->refbancaria->telefone) == 10 ) $refbancaria_telefone = mascara_string( "(##) ####-####", $request->refbancaria->telefone );
	else if ( strlen($request->refbancaria->telefone) == 11 ) $refbancaria_telefone = mascara_string( "(##) #####-####", $request->refbancaria->telefone );

	$refcomercial_nomeloja1 = $request->refcomercial->nomeloja1;
	$refcomercial_nomeloja2 = $request->refcomercial->nomeloja2;
	if ( strlen($request->refcomercial->telefone) == 10 ) $refcomercial_telefone = mascara_string( "(##) ####-####", $request->refcomercial->telefone );
	else if ( strlen($request->refcomercial->telefone) == 11 ) $refcomercial_telefone = mascara_string( "(##) #####-####", $request->refcomercial->telefone );
	if ( strlen($request->refcomercial->telefone2) == 10 ) $refcomercial_telefone2 = mascara_string( "(##) ####-####", $request->refcomercial->telefone2 );
	else if ( strlen($request->refcomercial->telefone2) == 11 ) $refcomercial_telefone2 = mascara_string( "(##) #####-####", $request->refcomercial->telefone2 );

	$refpessoal_nome1 = $request->refpessoal->nome1;
	$refpessoal_nome2 = $request->refpessoal->nome2;
	if ( strlen($request->refpessoal->telefone) == 10 ) $refpessoal_telefone = mascara_string( "(##) ####-####", $request->refpessoal->telefone );
	else if ( strlen($request->refpessoal->telefone) == 11 ) $refpessoal_telefone = mascara_string( "(##) #####-####", $request->refpessoal->telefone );
	if ( strlen($request->refpessoal->telefone2) == 10 ) $refpessoal_telefone2 = mascara_string( "(##) ####-####", $request->refpessoal->telefone2 );
	else if ( strlen($request->refpessoal->telefone2) == 11 ) $refpessoaltelefone2 = mascara_string( "(##) #####-####", $request->refpessoal->telefone2 );

	$login_usuario = $request->login->usuario;
	$login_senha = sha1($request->login->senha);
	$login_email = $request->login->email;
	$login_celular = $request->login->celular;
	$login_newsletter = $request->login->newsletter;
	$login_comproucavalo = $request->refcomercial->comproucavalo;
	$login_onde = $request->refcomercial->onde;

	$qrysel = "SELECT cpf from registration where cpf='".$info_cpf."'";
	$res = mysql_query($qrysel) or die(mysql_error());
	$totalcpf = mysql_num_rows($res);

	$qrysel = "SELECT email from registration where email='".$login_email."'";
	$res = mysql_query($qrysel) or die(mysql_error());
	$totalemail = mysql_num_rows($res);

	if( $totalcpf > 0 || $totalemail > 0 )
	{
		header('HTTP/1.1 409 Conflict');
		if($totalcpf > 0)
			die("CPF Já cadastrado!");
		else if($totalemail > 0)
			die("Email Já cadastrado!");
	}

	$created = date('Y-m-d H:i:s');

	$qryins = "INSERT into registration (account_status,member_status,username, firstname, sex,birth_date, email, password, terms_condition, privacy, newslatter,mobile_no,register_date,country, country_emp,full_mobileno,addressline1,adress_emp,city, city_emp,state, state_emp,phone, phone2,phoneno_emp, phoneno2_emp,postcode, postcode_emp,cpf,rg, cnpj, empresa,atividade, renda, autonomo, bairro, banco, conta_corrente, agencia, phone_b, nome1_ref, nome2_ref, phone1_ref, phone2_ref, nomeloja1_ref, nomeloja2_ref, phoneloja1_ref, phoneloja2_ref, ondecomprou, buy_horse)
                values('1','1','$username','$info_nome','$info_sexo','$info_nascimento','$login_email','$login_senha','1','1','$login_newsletter','$login_celular',NOW(),'$info_pais','$profissional_pais','$login_celular','$info_endereco','$profissional_endereco','$info_cidade','$profissional_cidade','$info_estado','$profissional_estado','$info_tel1','$info_tel2','$profissional_tel1','$profissional_tel2','$info_cep','$profissional_cep','$info_cpf','$info_rg','$profissional_cnpj','$profissional_empresa','$profissional_profissao','$profissional_renda','$profissional_autonomo','$profissional_complemento','$refbancaria_banco','$refbancaria_conta','$refbancaria_agencia','$refbancaria_telefone','$refpessoal_nome1','$refpessoal_nome2','$refpessoal_telefone','$refpessoal_telefone2','$refcomercial_nomeloja1','$refcomercial_nomeloja2','$refcomercial_telefone','$refcomercial_telefone2','$login_onde','$login_comproucavalo')";
	mysql_query($qryins) or die(mysql_error());

	date_default_timezone_set('GMT-3');

	$userid = mysql_insert_id();

    $datetime = new DateTime();
    $atualTime = $datetime->format("H:i:s");
    $atualDate = $datetime->format("d/m/Y");

    $login_comproucavalo = $login_comproucavalo==1 ? "Sim" : "Não";

    $mailcont2 = sprintf($lng_emailcad,$userid,$login_usuario,$info_nome,'',$info_sexo,$info_nascimento,$login_email,$login_senha,'Sim','Sim', $login_newsletter, $_SESSION["refid"], $login_celular, $atualDate, $info_pais, $profissional_pais, $login_celular, $info_endereco, $profissional_endereco, $info_cidade, $profissional_cidade, $info_estado, $profissional_estado, $info_tel1, $info_tel2, $profissional_tel1, $profissional_tel2, $info_cep, $profissional_cep, '', $info_cpf, $info_rg, $profissional_cnpj, $profissional_empresa, $profissional_profissao, $profissional_renda, $profissional_renda, $profissional_complemento, $refbancaria_banco, $refbancaria_conta, $refbancaria_agencia, $refbancaria_telefone, $refpessoal_nome1, $refpessoal_nome2, $refpessoal_telefone, $refpessoal_telefone2, $refcomercial_nomeloja1 , $refcomercial_nomeloja2 , $refcomercial_telefone , $refcomercial_telefone2 , $login_onde , $login_comproucavalo );
    SendHTMLMail($info_nome,"Cadastro para aprovação.",$mailcont2,$email,$info_nome);

?>