<?
	include("connect.php");
	include("admin.config.inc.php");
	include("security.php");


	if($_REQUEST["updatepausetime"])
	{
		$pshour = $_REQUEST["pshour"];
		$psmin = $_REQUEST["psmin"];
		$pssec = $_REQUEST["pssec"];
		$pehour = $_REQUEST["pehour"];
		$pemin = $_REQUEST["pemin"];
		$pesec = $_REQUEST["pesec"];

		$pstime = $pshour.":".$psmin.":".$pssec;
		$petime = $pehour.":".$pemin.":".$pesec;

		$sqlchkmanage = "select * from auction_pause_management where id=1";
		$reschkmanage = mysql_query($sqlchkmanage) or die(mysql_error());
		if(0<mysql_num_rows($reschkmanage))
		{
			$updatemanage = "update auction_pause_management set pause_start_time='$pstime', pause_end_time='$petime' where id='1'";
			mysql_query($updatemanage) or die(mysql_error());
			header("location: message.php?msg=40");
			exit;
		}
		else
		{
			$insertmanage = "insert into auction_pause_management (pause_start_time,pause_end_time) values('".$pstime."','".$petime."')";
			mysql_query($insertmanage) or die(mysql_error());
			header("location: message.php?msg=40");
			exit;
		}
	}
$sqlmanage = "select * from auction_pause_management where id=1";
$resmanage = mysql_query($sqlmanage) or die(mysql_error());
if(0<mysql_num_rows($resmanage))
{
	$rowmanage = mysql_fetch_array($resmanage);
	$pshour = substr($rowmanage["pause_start_time"],0,2);
	$psmin = substr($rowmanage["pause_start_time"],3,2);
	$pssec = substr($rowmanage["pause_start_time"],6);
	$pehour = substr($rowmanage["pause_end_time"],0,2);
	$pemin = substr($rowmanage["pause_end_time"],3,2);
	$pesec = substr($rowmanage["pause_end_time"],6);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<LINK href="main.css" type=text/css rel=stylesheet>
<script language="javascript">
function Check(f1)
{
	if(document.f1.pshour.value=="none")
	{
		alert("Please select start hour!!!");
		document.f1.pshour.focus();
		return false;
	}
	if(document.f1.psmin.value=="none")
	{
		alert("Please select start minute!!!");
		document.f1.psmin.focus();
		return false;
	}
	if(document.f1.pssec.value=="none")
	{
		alert("Please select start second!!!");
		document.f1.pssec.focus();
		return false;
	}
	if(document.f1.pehour.value=="none")
	{
		alert("Please select end hour!!!");
		document.f1.pehour.focus();
		return false;
	}
	if(document.f1.pemin.value=="none")
	{
		alert("Please select end minute!!!");
		document.f1.pemin.focus();
		return false;
	}
	if(document.f1.pesec.value=="none")
	{
		alert("Please select end second!!!");
		document.f1.pesec.focus();
		return false;
	}
}
</script>
</head>

<BODY bgColor=#ffffff>   
<form name="f1" id="f1" action="manageauctionpause.php" method="post" onsubmit="return Check(this)">
<table width="100%" cellpadding="0" cellSpacing="10">
  <tr>
	<td class="H1">Administrar tempo de pausa de leil&otilde;es</td>
  </tr>
  <tr>
	<td background="<?=DIR_WS_ICONS?>vdots.gif"><IMG height=1 src="<?=DIR_WS_ICONS?>spacer.gif" width=1 border=0></td>
  </tr>
  <tr>
	<td class="a" align="right" colspan=2 >* Campos Obrigatorios</td>
  </tr>
  <tr>
 	<td>
 	  <table cellpadding="1" cellspacing="2">
	    <tr valign="middle">
          <td class=f-c align=right valign="middle" width="191"><font class=a>*</font>Horario inicial da pausa:</td>
		  <td>
		  	<select name="pshour">
				<option value="none">hh</option>
				<?
					for($i=0;$i<=23;$i++)
					{
						if($i<10)
						{
							$i = "0".$i;
						}
				?>
				<option <?=$pshour==$i?"selected":"";?> value="<?=$i;?>"><?=$i;?></option>
				<?
					}
				?>
			</select>
		  	<select name="psmin">
				<option value="none">mm</option>
				<?
					for($i=0;$i<=59;$i++)
					{
						if($i<10)
						{
							$i = "0".$i;
						}
				?>
				<option <?=$psmin==$i?"selected":"";?> value="<?=$i;?>"><?=$i;?></option>
				<?
					}
				?>
			</select>
		  	<select name="pssec">
				<option value="none">ss</option>
				<?
					for($i=0;$i<=59;$i++)
					{
						if($i<10)
						{
							$i = "0".$i;
						}
				?>
				<option <?=$pssec==$i?"selected":"";?> value="<?=$i;?>"><?=$i;?></option>
				<?
					}
				?>
			</select>
		  </td>
		</tr>
	    <tr valign="middle">
          <td class=f-c align=right valign="middle" width="191"><font class=a>*</font> Horario final da pausa :</td>
		  <td>
		  	<select name="pehour">
				<option value="none">hh</option>
				<?
					for($i=0;$i<=23;$i++)
					{
						if($i<10)
						{
							$i = "0".$i;
						}
				?>
				<option <?=$pehour==$i?"selected":"";?> value="<?=$i;?>"><?=$i;?></option>
				<?
					}
				?>
			</select>
		  	<select name="pemin">
				<option value="none">mm</option>
				<?
					for($i=0;$i<=59;$i++)
					{
						if($i<10)
						{
							$i = "0".$i;
						}
				?>
				<option <?=$pemin==$i?"selected":"";?> value="<?=$i;?>"><?=$i;?></option>
				<?
					}
				?>
			</select>
		  	<select name="pesec">
				<option value="none">ss</option>
				<?
					for($i=0;$i<=59;$i++)
					{
						if($i<10)
						{
							$i = "0".$i;
						}
				?>
				<option <?=$pesec==$i?"selected":"";?> value="<?=$i;?>"><?=$i;?></option>
				<?
					}
				?>
			</select>
		  </td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</table>
	<table>
		<tr>
			<td colspan="2">Observa&ccedil;&atilde;o: Sua mudan&ccedil;a na gest&atilde;o leil&atilde;o reflete pausa ap&oacute;s dez minutos.</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr valign="middle">
			<td colspan="2" align="center">
			<input type="submit" name="updatepausetime" value="Update" class="bttn" />
			</td>
		</tr>
	 </table>
</table>
</form>
</body>
</html>
