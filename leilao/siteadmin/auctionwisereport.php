<?
	include_once("admin.config.inc.php");
	include("connect.php");
	include("functions.php");
	include("security.php");
	$PRODUCTSPERPAGE = 10;

	if($_POST["submit"]!="" || $_GET["sdate"]!="")
	{
		if(!$_GET['pgno'])
		{
			$PageNo = 1;
		}
		else
		{
			if($_POST["submit"]!="")
			{
			$PageNo = 1;
			}
			else
			{
			$PageNo = $_GET['pgno'];
			}
		}

			if($_POST["datefrom"]!="")
			{
				$startdate = ChangeDateFormat($_POST["datefrom"]);
				$enddate = ChangeDateFormat($_POST["dateto"]);
				$auctionstatus = $_POST["auctionstatus"];
				$auctiontype = $_POST["auctiontype"];
				$cid = $_POST["category"];
			}
			else
			{
				$startdate = ChangeDateFormat($_GET["sdate"]);
				$enddate = ChangeDateFormat($_GET["edate"]);
				$auctionstatus = $_GET["stat"];
				$auctiontype = $_GET["type"];
				$cid = $_GET["catid"];
			}

			$urldata = "sdate=".ChangeDateFormatSlash($startdate)."&edate=".ChangeDateFormatSlash($enddate)."&stat=".$auctionstatus."&type=".$auctiontype."&catid=".
			$cid;
		
		if($auctiontype!="none")
		{
			if($auctiontype=="digital") { $auctype = "and c.digital='1'"; }							
			if($auctiontype=="presencial") { $auctype = "and c.presencial='1'"; }							
			if($auctiontype=="virtual") { $auctype = "and c.virtual='1'"; }														
		}

		if($auctionstatus!="")
		{
			if( $startdate == "" )
			{
				$startdate = "01/01/1900";
			}

			if( $enddate == "" )
			{
				$enddate = "01/01/2500";
			}
			// if($auctionstatus==2)
			// {	
			// 	$qrysel = "select * from categories c left join auction a on a.categoryID=c.categoryID left join products p on a.productID=p.productID where (a.auc_status='$auctionstatus') and c.cat_start_date>='$startdate' and c.cat_end_date<='$enddate' ".$auctype;
			// }
			// else
			// {
				$qrysel = "select * from categories c left join auction a on a.categoryID=c.categoryID left join products p on a.productID=p.productID where a.auc_status='$auctionstatus' and cat_start_date>='$startdate' and cat_end_date<='$enddate' ".$auctype;
			// }
		}
		else
		{
				$qrysel = "select * from categories c left join auction a on a.categoryID=c.categoryID left join products p on a.productID=p.productID where cat_start_date>='$startdate' and cat_end_date<='$enddate' ".$auctype;
		}
		if($cid!="")
		{
				$qrysel .= " and a.categoryID='$cid'";
		}
		$ressel = mysql_query($qrysel);
		$total = mysql_num_rows($ressel);
		$totalpage=ceil($total/$PRODUCTSPERPAGE);

		if($totalpage>=1)
		{
			$startrow=$PRODUCTSPERPAGE*($PageNo-1);
			$qrysel.=" LIMIT $startrow,$PRODUCTSPERPAGE";
			//echo $sql;
			$ressel=mysql_query($qrysel);
			$total=mysql_num_rows($ressel);
		}
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; UTF-8" />
<META content="MSHTML 6.00.2600.0" name=GENERATOR>
<link rel="stylesheet" href="main.css" type="text/css">
<link href="zpcal/themes/aqua.css" rel="stylesheet" type="text/css" media="all" title="Calendar Theme - aqua.css" />
<script type="text/javascript" src="zpcal/src/utils.js"></script>
<script type="text/javascript" src="zpcal/src/calendar.js"></script>
<script type="text/javascript" src="zpcal/lang/calendar-en.js"></script>
<script type="text/javascript" src="zpcal/src/calendar-setup.js"></script>
<title><? echo $ADMIN_MAIN_SITE_NAME." - Auctionwise Report"; ?></title>
<script language="javascript">
	function Check(f1)
	{	
		if(document.f1.datefrom.value=="")
		{
			// alert("Por favor, escolha uma data de in�cio!!!");
			// return false;
			// document.f1.datefrom.focus();
			document.f1.datefrom.value="01/01/1900";
		}
		if(document.f1.dateto.value=="")
		{
			// alert("Por favor, escolha uma data de t�rmino!!!");
			// return false;
			// document.f1.dateto.focus();
			document.f1.dateto.value="01/01/2500";
		}
	}
</script>
<script language="javascript">
// USE FOR AJAX //
function GetXmlHttpObject()
{
var xmlHttp=null;
try
  {
  // Firefox, Opera 8.0+, Safari
  xmlHttp=new XMLHttpRequest();
  }
catch (e)
  {
  // Internet Explorer
  try
    {
    xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
  catch (e)
    {
    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
return xmlHttp;
}

function PauseAuction(aucid)
{
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
	  {
	  alert ("Your browser does not support AJAX!");
	  return;
	  } 
	var url="pauseauction.php";
	url=url+"?aucid="+aucid
	xmlHttp.onreadystatechange=changeStatus;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}
function changeStatus()
{
	if (xmlHttp.readyState==4)
	{ 
		var temp=xmlHttp.responseText;
		redata = temp.split('|');
		if(redata[0]=="success")
		{
			document.getElementById('pause_' + redata[1]).style.display = 'none';
			document.getElementById('resume_' + redata[1]).style.display = 'block';
			document.getElementById('auctionstatus_' + redata[1]).style.color = 'green';
			document.getElementById('auctionstatus_' + redata[1]).innerHTML = 'Paused';
		}
	}
}
function StartAuction(aucid1)
{
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)
	  {
	  alert ("Your browser does not support AJAX!");
	  return;
	  } 
	var url="pauseauction.php";
	url=url+"?aucidstart="+aucid1
	xmlHttp.onreadystatechange=changeStatus1;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}
function changeStatus1()
{
	if (xmlHttp.readyState==4)
	{ 
		var temp=xmlHttp.responseText;
		redata = temp.split('|');
		if(redata[0]=="success")
		{
			document.getElementById('pause_' + redata[1]).style.display = 'block';
			document.getElementById('resume_' + redata[1]).style.display = 'none';
			document.getElementById('auctionstatus_' + redata[1]).style.color = '#FF0000';
			document.getElementById('auctionstatus_' + redata[1]).innerHTML = 'Active';
		}
	}
}
</script>
</head>

<body>
<TABLE width="100%"  border=0 cellPadding=0 cellSpacing=10>
  <!--DWLayoutTable-->
    <TR> 
      <TD width="100%" class="H1">Relat&oacute;rio por Lote</TD>
    </TR>
  	<TR>
    <TD background="images/vdots.gif"><IMG height=1 
      src="images/spacer.gif" width=1 border=0></TD>
	</TR>
	<tr>
		<td height="10"></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td align="center" class="h2"><b>Por Favor Selcione a Data</b></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<form action="" method="post" name="f1" onsubmit="return Check(this)">	
	<tr>
		<td align="center"><b>De</b> : <input class="textbox" type="text" name="datefrom" id="datefrom" size="12" value="<?=$startdate!=""?ChangeDateFormatSlash($startdate):"";?>">&nbsp;&nbsp;<img src="images/pmscalendar.gif" align="absmiddle" width="20" height="20" id="vfrom">&nbsp;&nbsp;-&nbsp;&nbsp; <b><? echo utf8_encode('�')?></b> : <input class="textbox" type="text" name="dateto" size="12" id="dateto" value="<?=$enddate!=""?ChangeDateFormatSlash($enddate):"";?>">&nbsp;&nbsp;<img src="images/pmscalendar.gif" align="absmiddle" width="20" height="20" id="zfrom">&nbsp;&nbsp;</font></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td align="center">
			<b>Leil&atilde;o Tipo :&nbsp;</b>
			<select name="auctiontype" class="solidinput">
				<option value="none">Selecione ...</option>
				<option <?=$auctiontype=="digital"?"selected":"";?> value="digital">Digital</option>
				<option <?=$auctiontype=="presencial"?"selected":"";?> value="presencial">Presencial</option>
				<option <?=$auctiontype=="Virtual"?"selected":"";?> value="virtual">Virtual</option>
		  </select>
			&nbsp;&nbsp;&nbsp;
			<b>Status do Leil&atilde;o:&nbsp;</b>
			<select name="auctionstatus" class="solidinput">
				<option value="">Selecione ...</option>
				<option <?=$auctionstatus=="2"?"selected":"";?> value="2">Ativo</option>
				<option <?=$auctionstatus=="3"?"selected":"";?> value="3">Vendido</option>
				<option <?=$auctionstatus=="1"?"selected":"";?> value="1">Pendente</option>
			</select>
	  </td>
	</tr>	
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td align="center"><input type="submit" name="submit" value="Buscar" class="bttn-s"></td>
	</tr>
	</form>
	<TR>
    	<TD><!--content-->
		<? if(isset($total))
		{
			if($total==0)
			{
			?>
			<table width="70%" border="0" cellspacing="1" cellpadding="1" align="center"> 
			<tr>
				<td height="8"></td>
			</tr>
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center" bgcolor="#000000">
						<tr> 
						  <td> 
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <tr> 
								<td class=th-a > 
								  <div align="center">Sem Informa&ccedil;&otilde;es Para Exibir.</div>
								</td>
							  </tr>
							</table>
						  </td>
						</tr>
					</table>
				</td>
			</tr>			
	      </table>
		 <?
		 	}
			else
			{
		?>
          <TABLE width="100%" border=1 cellSpacing=0 class=t-a>
            <!--DWLayoutTable-->
              <TR class=th-a> 
				<!--<TD nowrap width="5%">User Id</TD>-->
				<TD width="7%" align="left" nowrap="nowrap">Lote  ID</TD>
				<TD width="50%" nowrap="nowrap" align="center">Nome</TD>
				<TD nowrap="nowrap" align="left">Pre&ccedil;o Inicial</TD>
				<TD align="left" nowrap="nowrap">Pre&ccedil;o Atual</TD>
				<TD nowrap="nowrap" align="left">Status do Lote</TD>
				<TD nowrap="nowrap" align="left">&Uacute;ltimo lance por:</TD>
				<TD width="35%" align="center" nowrap="nowrap">Op&ccedil;&otilde;es</TD>
			 </TR>
		<?
			while($obj = mysql_fetch_object($ressel))
			{
	
				if($obj->auc_status=="2") {$status = "<font color='red'>Active</font>";}
				if($obj->auc_status=="3" && $obj->buy_user != 0) { $status = "<font color='blue'>Vendido</font>"; }
				if($obj->auc_status=="3" && $obj->buy_user == 0) { $status = "<font color='dark blue'>N�o Vendido</font>"; }
				if($obj->auc_status=="1") { $status = "<font color='green'>Pendente</font>"; }
				

				if ($colorflg==1){
					$colorflg=0;
					echo "<TR bgcolor=\"#f4f4f4\"> ";
				}else{
					$colorflg=1;
				  	echo "<TR> ";
				}

				$qr = "SELECT auc_due_price from auc_due_table where auction_id=".$obj->auctionID."";
				$rs = mysql_query($qr) or die(mysql_error());
				$objcdt = mysql_fetch_object($rs);

				$qr2 = "SELECT username from registration where id=".$obj->buy_user."";
				$rs2 = mysql_query($qr2) or die(mysql_error());
				$objuser = mysql_fetch_object($rs2);
				$totalrs2 = mysql_num_rows($rs2);
		?>
				<TD align="center" nowrap="nowrap"><?=$obj->auctionID;?></TD>
				<TD nowrap="nowrap" align="center"><?=utf8_encode( $obj->name );?></TD>
				<TD nowrap="nowrap" align="center"><?=$Currency.$obj->auc_start_price;?></TD>
				<TD align="center" nowrap="nowrap"><?=$Currency.($objcdt->auc_due_price + $obj->auc_start_price);?></TD>
				<TD nowrap="nowrap" align="center"><span id="auctionstatus_<?=$obj->auctionID;?>"><?=utf8_encode($status);?></span></TD>
				<TD nowrap="nowrap" align="center">
				<? 
				if($totalrs2 == 0) 
				{ 
					echo utf8_encode("---"); 
				} 
				else 
				{ 
					echo utf8_encode($objuser->username);
				}
				?>
				</TD>
				<TD align="center" nowrap="nowrap">
					<div style="width: 70px;">
					<input type="button" name="details" class="bttn" value="Detalhes" onclick="javascript: window.location.href='auctiondetails.php?aid=<?=$obj->auctionID;?>'" />
				</div>
					<!-- <div style="width: 80px; float:right; clear:left">
				<? if($obj->pause_status=="0" && $obj->auc_status=="2"){ ?>
					<input type="button" id="pause_<?=$obj->auctionID;?>" name="pause" class="bttn" value="Pause" onclick="PauseAuction('<?=$obj->auctionID;?>');" />
					<input type="button" id="resume_<?=$obj->auctionID;?>" name="resume" class="bttn" value="Resumo" onclick="StartAuction('<?=$obj->auctionID;?>');" style="display: none;" />
				<? } elseif($obj->pause_status=="1" && $obj->auc_status=="2") { ?>
					<input type="button" id="pause_<?=$obj->auctionID;?>" name="pause" class="bttn" value="Pause" onclick="PauseAuction('<?=$obj->auctionID;?>');" style="display: none;" />
					<input type="button" id="resume_<?=$obj->auctionID;?>" name="resume" class="bttn" value="Resumo" onclick="StartAuction('<?=$obj->auctionID;?>');"/>
				<? } ?> -->
				<!-- </div> -->
				</TD>
			</tr>
		<?
			}
		?>		
		 </TABLE>
		  <?
			if($PageNo>1)
			{
			  $PrevPageNo = $PageNo-1;
		  ?>
		  <A class='paging' href="auctionwisereport.php?pgno=<?=$PrevPageNo;?>&<?=$urldata;?>">&lt; P&aacute;gina Anterior</A>
		  <?
		   }
		  ?> &nbsp;&nbsp;&nbsp;
		  <?php
			if($PageNo<$totalpage)
			{
			 $NextPageNo = 	$PageNo + 1;
		  ?>
		  <A class='paging' id='next' href="auctionwisereport.php?pgno=<?=$NextPageNo;?>&<?=$urldata;?>">Pr&oacute;xima P&aacute;gina &gt;</A>
		  <?
		   }
		  ?>
	<?
		
		}
	}
	 ?>
	 </TD>
	 </TR>
	</TABLE>	 
<script type="text/javascript">
var cal = new Zapatec.Calendar.setup({ 
inputField:"datefrom",
ifFormat:"%d/%m/%Y",
button:"vfrom",
showsTime:false 
});
</script>
<script type="text/javascript">
var cal = new Zapatec.Calendar.setup({ 
inputField:"dateto",
ifFormat:"%d/%m/%Y",
button:"zfrom",
showsTime:false 
});
</script>
</body>
</html>
