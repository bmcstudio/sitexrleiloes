<?
	include_once("admin.config.inc.php");
	include("connect.php");
	include("functions.php");
	include("security.php");
	
	$aid = $_GET["aid"];

	if($aid!="")
	{	
		$qrysel = "select * from auction a left join products p on a.productID=p.productID left join registration r on a.buy_user=r.id  where a.auctionID='$aid'";
		$ressel = mysql_query($qrysel);
		$total = mysql_num_rows($ressel);
		$obj = mysql_fetch_object($ressel);

		if($obj->digital=="1") { $auctype = "Online"; }
		if($obj->virtual=="1") { $auctype = "Virtual"; }
		if($obj->presencial=="1") { $auctype = "Presencial"; }
		// if($obj->offauction=="1") { if($auctype==""){ $auctype = "100% Off Auction"; } else { $auctype .= ", 100% Off Auction"; } }
		// if($obj->nightauction=="1") { if($auctype=="") { $auctype = "Night Auction";  } else { $auctype .= ", Night Auction"; } }
		// if($obj->openauction=="1") { if($auctype=="") { $auctype = "Open Auction"; } else { $auctype .= ", Open Auction"; } }
		
		// if($obj->time_duration=="none") { $duration = "Default"; } 
		// elseif($obj->time_duration=="10sa") { $duration = "10 Second Auction"; } 
		// elseif($obj->time_duration=="15sa") { $duration = "15 Second Auction"; } 
		// elseif($obj->time_duration=="20sa") { $duration = "20 Second Auction"; } 
		
		if($obj->auc_status=="2") { $status = "<font color='red'>Ativo</font>"; }
		elseif($obj->auc_status=="1") { $status = "<font color='green'>Pendente</font>"; }
		elseif($obj->auc_status=="3") { $status = "<font color='blue'>Vendido</font>"; }

		$numberbids = explode("|",GetBidsDetails($aid));
		$biddingprice = $numberbids[0]*0.50;

		// if($obj->fixedpriceauction=="1")
		// {
		// 	$priceauc = $obj->auc_fixed_price;
		// 	$prodprice = $obj->price;
		// 	$prloss = $priceauc + $biddingprice - $prodprice;
		// }
		// elseif($obj->offauction=="1")
		// {
		// 	$priceauc = 0.01;
		// 	$prodprice = $obj->price;
		// 	$prloss = $priceauc + $biddingprice - $prodprice;
		// }
		// else
		// {
		// 	$priceauc = $obj->auc_final_price;
		// 	$prodprice = $obj->price;
		// 	$prloss = $priceauc + $biddingprice - $prodprice;
		// }
	
		if($prloss<0)
		{
			$prloss1 = "<font color='red'>-".$Currency.number_format(substr($prloss,1),2)."</font>";
		}
		else
		{
			$prloss1 = "<font color='green'>".$Currency.number_format($prloss,2)."</font>";
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="main.css" type="text/css">
<title><? echo $ADMIN_MAIN_SITE_NAME." - Auction Details"; ?></title>

</head>

<body>
<TABLE width="100%"  border=0 cellPadding=0 cellSpacing=10>
  <!--DWLayoutTable-->
    <TR> 
      <TD width="100%" class="H1">Detalhes Do Lote</TD>
    </TR>
  	<TR>
    <TD background="images/vdots.gif"><IMG height=1 
      src="images/spacer.gif" width=1 border=0></TD>
	</TR>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td>
			<table border="0">
				<tr>
					<td class="f-c"  align="right">Nome :</td>
					<td align="left">&nbsp;&nbsp;<?=stripslashes($obj->name);?></td>
				</tr>
				<tr>
					<td class="f-c"  align="right">Tipo :</td>
					<td>&nbsp;&nbsp;<?=$auctype;?></td>
				</tr>
				<tr>
					<td class="f-c"  align="right">Status :</td>
					<td>&nbsp;&nbsp;<?=$status;?></td>
				</tr>
				<!-- <tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Dura&ccedil;&atilde;o :</td>
					<td>&nbsp;&nbsp;<?=$duration;?></td>
				</tr> -->
				<!-- <tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Data de In&iacute;cio :</td>
					<td>&nbsp;&nbsp;<?=ChangeDateFormatSlash($obj->auc_start_date);?></td>
				</tr>
				<tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Data Final :</td>
					<? if($obj->auc_status=="3") { $enddate = $obj->auc_final_end_date; }
					 else { $enddate = $obj->auc_end_date; }?>
					<td>&nbsp;&nbsp;<?=ChangeDateFormatSlash($enddate);?></td>
				</tr>
				<tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Pre&ccedil;o Inicial :</td>
					<td>&nbsp;&nbsp;<?=$Currency.$obj->auc_start_price;?></td>
				</tr> -->
				<!-- <tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Pre&ccedil;o Fixo :</td>
					<td>&nbsp;&nbsp;<?=$Currency.$obj->auc_fixed_price;?></td>
				</tr> -->
				<? if($obj->auc_status=="3"){ ?>
				<!-- <tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Pre&ccedil;o Final :</td>
					<td>&nbsp;&nbsp;<?=$Currency.$obj->auc_final_price;?></td>
				</tr> -->
				<!-- <tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Ganhador :</td>
					<td>&nbsp;&nbsp;<?=$obj->username;?></td>
				</tr> -->
				<!-- <tr>
					<td class="f-c"  align="right">&nbsp;&nbsp;&nbsp;Profit/Loss :</td>
					<td>&nbsp;&nbsp;<?=$prloss1;?></td>
				</tr> -->
				<? } ?>
			</table>
		<?
			$qrb = "SELECT * from history h, auction a, registration r where h.auctionID=$aid and h.auctionID=a.auctionID and h.userID=r.id order by h.currentDate desc";
			$rb = mysql_query($qrb) or die(mysql_error());
			$total = mysql_num_rows($rb);
			if($obj->auc_status=="3" || $obj->auc_status=="2" || $obj->auc_status=="1")
			{
				if($total==0)
				{
				?>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="1" cellpadding="1" align="center" bgcolor="#000000">
							<tr> 
							  <td> 
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tr> 
									<td class=th-a > 
									  <div align="center">Nenhum dado para mostrar.</div>
									</td>
								  </tr>
								</table>
							  </td>
							</tr>
						</table>
					</td>
				</tr>			
				<?
				}
				else
				{
				?>
				<tr>
					<td height="5"></td>
				</tr>
				<TR> 
				  <TD width="100%" class="H1">Hist&oacute;rico de Lances</TD>
				</TR>
				<TR>
				<TD background="images/vdots.gif"><IMG height=1 
				  src="images/spacer.gif" width=1 border=0></TD>
				</TR>
				<tr>
					<td height="5"></td>
				</tr>
				<tr>
					<td>
		              <!-- <table border="0" width="100%">
		                <tr>
		                    <tr class="a">Note: Nomes marcados com a cor vermelha means that bids placed by admin users.</tr>
		                </tr>
		              </table> -->
			          <TABLE width="100%" border=1 cellSpacing=0 class=t-a>
			              <TR class=th-a> 
							<TD width="15%" align="left" nowrap="nowrap">Nome do Usu&atilde;rio</TD>
							<TD width="15%" align="left" nowrap="nowrap">Primeiro Nome</TD>
							<TD width="20%" align="left" nowrap="nowrap">Email</TD>
							<TD width="15%" align="left" nowrap="nowrap">Novo valor de Parcela</TD>
							<TD width="15%" align="left" nowrap="nowrap">Data</TD>
							<!-- <TD width="15%" align="left" nowrap="nowrap">BidButlers</TD> -->
							<!-- <TD width="10%" nowrap="nowrap" align="center">Placed Bids</TD> -->
						  </TR>						
				<?

			while($objres = mysql_fetch_array($rb))
			{
				if($ob->admin_user_flag=='1')
				{
					$clsname = " class='a' ";
				}
				else
				{
					$clsname = "";
				}
				
				if ($colorflg==1){
					$colorflg=0;
					echo "<TR bgcolor=\"#f4f4f4\" ".$clsname."> ";
				}else{
					$colorflg=1;
				  	echo "<TR ".$clsname.">";
				}

		?>
					<TD align="left"><?=$objres["username"];?></TD>
					<TD align="left"><?=$objres["firstname"]; ?></TD>
					<TD align="left"><?=$objres["email"];?></TD>
					<TD align="center"><?=$objres["moeda"];?>&nbsp;<?=$objres["currentValue"];?>,00</TD>
					<TD align="center"><?=$objres["currentDate"];?></TD>
					<!-- <TD align="center"><?=$butler->butlerbid!=""?$butler->butlerbid:"0";?></TD> -->
					<!-- <TD align="center"><?=$ob->bidcount;?></TD> -->
				</TR>
		<?
				$bids = $totalbids;
				$butbids = $butlerbids;
				$singbids = $singlebids;
			}
		?>
	             <!--  <TR class=th-a> 
					<td colspan="3" align="right">Total Bids:</td>
					<td align="center"><?=$singlebids;?></td>
					<td align="center"><?=$butlerbids;?></td>
					<td align="center" nowrap="nowrap"><?=$totalbids;?></td> 
				  </TR>-->
			</TABLE>
		<?
				}
			}
		?>
	</td>
	</tr>
	</TABLE>
</body>
</html>
