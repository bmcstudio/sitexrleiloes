<?
	include("config/connect.php");
	include("functions.php");
	$staticvar = "depoimentos";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript" src="function.js"></script>
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>


<body>
    <?
    	include("header.php");
    ?>
    <div id="main_div">
        <script type="text/javascript">
            startClock();
        </script>
            <?php require 'sideleft.php'; ?>
		<div id="middle_div">
		    <div class="openAuction_bar_mainDIV">
			    <div class="openAction_bar-left"></div>
			    <div class="openAction_bar-middle">
			      <div class="page_title_font">Depoimentos dos Participantes</div></div>
			    <div class="openAction_bar-right"></div>
		     </div>
		     <div id="cleaner"></div>
		     <div class="openAuction_bar_mainDIV2_static">
			    <div class="staticbody">
			    <div style="height: 20px;">&nbsp;</div>
				    <div class="staticright">
				    <div class="darkblue-static-title">Depoimentos</div>								               
                    
				    </div>
			    </div>
		     </div>
		     <div id="cleaner"></div>
		     <div class="openAuction_bar_bottom">
		     	<div class="openAuction_leftcorner"></div>
			    <div class="openAuction_bar_middle"></div>
		     	<div class="openAuction_rightcorner"></div>
		     </div>
		</div>
			<?
				include("leftstatic.php");
			?>	
    </div>
    <?
    	include("footer.php");
    ?>
</body>
</html>
