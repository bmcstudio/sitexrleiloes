<?
	include("config/connect.php");
	include("language/portugues.php");
	include("functions.php");

	if($_POST["submit"]!="")
	{
		$email = $_POST["email"];
		$qrysel = "select * from registration where email='$email' and user_delete_flag!='d'";
		$ressel = mysql_query($qrysel);
		$total = mysql_num_rows($ressel);
		$obj = mysql_fetch_object($ressel);

		if($total>0 && $obj->account_status!='0')
		{
			$fname = $obj->firstname;
			$username = $obj->username;
			$pass = $obj->password;
			$auctionwith_id = $obj->id;
			$encode_username = base64_encode($obj->username);
			$encode_password = base64_encode($obj->password);
			$session_id = session_id();

			$emailcont1 = sprintf($lng_emailcontent_forgotpassword,$fname,$username,$auctionwith_id,$encode_username,$encode_password,$session_id);
		}
		elseif($obj->account_status=='0')
		{
			$msg = 2;
		}
		else
		{
			$msg = 1;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript" src="function.js"></script>
<script language="javascript">
	function check()
	{
		if(document.forgot.email.value=="")
		{
			alert("<?=$lng_plsenteremailadd;?>");
			document.forgot.email.focus();
			document.forgot.email.select();
			return false;
		}
		else
		{
			if(!validate_email(document.forgot.email.value,"<?=$lng_entervalidemail;?>"))
				{
					document.forgot.email.select();
					return false;
				}
		}
	}
	function validate_email(field,alerttxt){
		with (field){
			var value;
			value = document.forgot.email.value;
			apos=value.indexOf("@");
			dotpos=value.lastIndexOf(".");
			if (apos<1||dotpos-apos<2){
				alert(alerttxt);return false;
			}else{
				return true;
			}
		}
	}
</script>
</head>


<body>
    <?
    	include("header.php");
    ?>
    <div id="main_div">
    <?
    	include("sideleft.php");
    ?>
		<div id="middle_div">
		<div class="openAuction_bar_mainDIV">
			<div class="openAction_bar-left"></div>
			<div class="openAction_bar-middle"><div class="page_title_font"><?=$lng_titleforgot;?></div></div>
			<div class="openAction_bar-right"></div>
		 </div>
		 <div class="openAuction_bar_mainDIV2">
		 	<div style="height: 20px;">&nbsp;</div>
			<div class="staticbody">
				<div class="staticright">
				<? if($msg==1){?>
					<div style="margin-left: 25pt; margin-top: 20px;" align="left"><span class="red-text-12-b"><?=$lng_didnotfindemail;?></span></div>
					<div style="height: 10px;">&nbsp;</div>
				<? } elseif($msg==2){ ?>
					<div style="margin-left: 25pt; margin-top: 20px;" align="left"><span class="red-text-12-b"><?=$lng_accountverify;?></span></div>
					<div style="height: 10px;">&nbsp;</div>
					<div><a href="forgotpassword.html" class="blue_link">voltar</a></div>
				<? } elseif($msg==3){ ?>
					<div style="margin-left: 25pt; margin-top: 20px;" align="left"><span class="red-text-12-b">SMS Enviado com sucesso</span></div>
					<div style="height: 10px;">&nbsp;</div>
					<div><a href="forgotpassword.html" class="blue_link">voltar</a></div>
				<? } ?>
				<?
				if($_POST["email"]!="" && $total>0 && $obj->account_status!='0' && $msg!=1)
				{
				?>
				<div style="height: 10px;">&nbsp;</div>
				<div style="margin-left: 25pt; min-height: 300px;" align="left">
					<div><?=$lng_emailsentto;?> <?=$email;?></div>
					<div style="height: 10px;">&nbsp;</div>
					<div><a href="index.html" class="blue_link"><?=$lng_tabhome;?></a></div>
					<div style="height: 10px;">&nbsp;</div>
				</div>
			    <?
				}
				else
				{
				?>
				<form name="forgot" method="post" action="" onsubmit="return check();">
					<div style="height: 210px;" align="left">
					<h2>Recuperar senha</h2>
						<div>Esqueceu seus dados de Login?</div>
						<div style="height: 10px;">&nbsp;</div>
						<div>Não tem problema, basta inserir o seu email e enviaremos as informações para a sua conta de e-mail.</div>
						<div style="height: 15px;">&nbsp;</div>
						<div><b><?=$lng_enteryouremail;?></b></div>
						<div style="height: 15px;">&nbsp;</div>
						<div><input type="text" name="email" size="50" class="logintextboxclas" />
						<div style="height: 25px;">&nbsp;</div>
						<div><input type="image" src="<?=$lng_imagepath;?>send.jpg" value="sub"  onmouseover="this.src='<?=$lng_imagepath;?>send.jpg'" onmouseout="this.src='<?=$lng_imagepath;?>send.jpg'" /></div>
						<input type="hidden" value="submit" name="submit" />
						</div>
					</div>
				</form>
				<?
				}
				?>
				</div>
					</div>
  		 </div>
		 <div class="openAuction_bar_bottom">
		 	<div class="openAuction_leftcorner"></div>
			<div class="openAuction_bar_middle"></div>
		 	<div class="openAuction_rightcorner"></div>
		 </div>
		</div>
    </div>
    <?
    	include("footer.php");
    ?>
</body>
</html>
