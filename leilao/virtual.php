<?php
  include("config/connect.php");

  include("functions.php");
  include("language/portugues.php");

  if( isset($_GET["aid"])) $catid = $_GET["aid"]; else $catid = "";
  if( isset($_SESSION["userid"]) )$uid = $_SESSION["userid"];
  if( isset($_GET["pgid"]) )$pageId = $_GET["pgid"];
  $total_linhas = 0;
  if($catid=="" || $catid=='undefined')
  {$catid = 1;}
  $pageId =1;
  if($pageId=="" || $pageId== 'undefined')
  {$pageId =1;}

  $ipaddress = $_SERVER['REMOTE_ADDR'];

  if($pageId == 1)
    $qrysel = "SELECT DISTINCT * FROM auction a, products p WHERE a.digital=1 and a.productID=p.productID and a.categoryID=".$catid." ORDER BY a.lote";
  else if($pageId == 2)
    $qrysel = "SELECT DISTINCT * FROM auction a, products p WHERE a.presencial=1 and a.productID=p.productID and a.categoryID=".$catid." ORDER BY a.auc_start_date";
  else if($pageId == 3)
    $qrysel = "SELECT DISTINCT * FROM auction a, products p WHERE a.virtual=1 and a.productID=p.productID and a.categoryID=".$catid." ORDER BY a.auc_start_date";
  else if($pageId == 4)
    $qrysel = "SELECT * FROM auction a, products p WHERE a.productID=p.productID and a.garanhao=1 ORDER BY a.garanhao DESC";
  else if($pageId == 6)
    $qrysel = "SELECT * FROM auction a, products p WHERE a.auc_status=1 and a.productID=p.productID and a.categoryID=".$catid." ORDER BY a.auc_start_date";

  $ressel = mysql_query($qrysel);
  $total = mysql_num_rows($ressel);

  $num = $total;

  if($_SESSION["ipid"]=="" && $_SESSION["login_logout"]=="") {

    $qryipins = "Insert into login_logout (ipaddress,load_time) values('".$ipaddress."',NOW())";

    mysql_query($qryipins) or die(mysql_error());

    $ipid = mysql_insert_id();



    $qryipsel = "select * from login_logout where id='$ipid'";

    $rsip = mysql_query($qryipsel);

    $obj = mysql_fetch_object($rsip);

    mysql_free_result($rsip);

    $_SESSION["ipid"] = $obj->load_time;

    $_SESSION["ipaddress"] = $ipaddress;

  }


  $qryauc = "SELECT *, p.productID AS pid,p.".$lng_prefix."name AS name,".$lng_prefix."short_desc AS short_desc FROM auction a LEFT JOIN products p ON a.productID=p.productID LEFT JOIN auc_due_table adt ON a.auctionID=adt.auction_id WHERE adt.auc_due_time!=0 AND featured_flag='0' AND a.categoryID='$catid' AND a.digital=1 ORDER BY auc_status DESC limit 0,9";
  $resauc = mysql_query($qryauc) or die ( mysql_error() );
  $totalauc = mysql_num_rows($resauc);

  $qryban = "select * from banners";
  $resban = mysql_query($qryban) or die ( mysql_error() );
  $totalban = mysql_num_rows($resban);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:image" content="http://www.xrleiloes.com.br/leilao/images/logo_social.png" />
<title>XRLeilões</title>
<link rel="image_src" href="http://www.xrleiloes.com.br/leilao/images/logo_social.png"/>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="vendor/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>

<!-- Scripts -->
<script src="jquery-latest.js"></script>
<script src="jquery-migrate-1.0.0.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script language="javascript" src="Scripts/jquery.plugin.js"></script>
<script language="javascript" src="Scripts/jquery.countdown.js"></script>
<script language="javascript" src="Scripts/jquery.bxslider.js"></script>
<script language="javascript" src="vendor/jquery.mlens-1.4.js"></script>
<script language="javascript" src="effect.js"></script>
<?php if($catid == 33){
?>
<script src="http://jwpsrv.com/library/YOUR_JW_PLAYER_ACCOUNT_TOKEN.js" ></script>
<?}?>

  <!-- // <script language="javascript" src="default2.js"></script>/ -->

<script language="javascript" src="jqModal.js"></script>
<script type="text/javascript" src="http://www.youtube.com/player_api"></script>
<script language="javascript" src="vendor/fancybox/jquery.fancybox.pack.js"></script>
<script language="javascript" src="function.js"></script>
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53eb791f08ec5473"></script>
</head>

<body onload="OnloadPage();">
<?php include("header.php"); ?>

<div class="corpo">
  <script type="text/javascript">

    inBot = false;

    startClock();

    function ChangeActive(elem){

      var cont = $(".menu_miolo");
      for( el in cont )
      {
        $(el).find("a").attr("class", '');
      }

      $(elem).attr("class", 'active');
    }

     function ChangeActiveGalery(_this, _idthis, _idother){

      var cont = $(".menu_galeria");
      for( el in cont )
      {
        $(el + " a.active").attr("class", '');
      }

      $(_this).attr("class", 'active');
        $('#'+_idthis).css("display", "block");
        $('#'+_idother).css("display", "none");
    }

    function Lance(aid, pid, val, luser)
    {
      if($('#green_'+aid).is(':visible')){
        alert("Atenção, você não pode rebater seu próprio lance!");
        return;
      }
      else if ($('#sold_'+aid).is(':visible'))
      {
        alert("Este Lote Já está Vendido! Por favor atualize a página.");
        return;
      }
      else
      {
        alert("Atenção, seu lance pode demorar até 3 segundos para ser processado!");
      }

      globalControl = true;
        $.getJSON('getbid.php?prid='+pid+'&aid='+aid+'&uid=<?=$uid;?>&value='+val,
        function(data) {
          //executionTime = Math.floor(Math.random() * 3) + 1;
          //console.log("inbot", inBot);
          // if (!inBot) {
          //  inBot = true;
          //  setTimeout(function(){CallAutoBots(aid, pid, '10')}, 1000 * 20 );
          //  // console.log(executionTime);
          // };

          //if(data =="changedata")
          //{
            // console.log('dfasdffasdf');
            changeDate2();
            // window.location.reload();
         // }
        });
    }

    // function CallAutoBots(aid, pid, val){
    //   // console.log(aid, pid, val, luser);
    //   bee = '1';
    //   inBot = false;
    //   $.getJSON('getbid.php?prid='+pid+'&aid='+aid+'&uid='+bee+'&value='+val,
    //      function(data) {
    //       if(data =="changedata")
    //       {
    //         changeDate2();
    //         // window.location.reload();
    //       }
    //     });
    // }

</script>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="23%" valign="top">
        <?php require 'sideleft.php'; ?>
    </td>
      <td width="77%" valign="top">
      <nav class="menu2">
        <ul class="menu_miolo">
          <li><a <?php if($pageId==1 || empty($pageId)){ ?>class="active"<?php } ?> href="index.php?pageId=1" onclick="ChangeActive(this);">Leilão Online</a></li>
          <li><a <?php if($pageId==2){ ?>class="active"<?php } ?> href="index.php?pageId=2" onclick="ChangeActive(this);">Leilão Presencial</a></li>
          <li><a <?php if($pageId==3){ ?>class="active"<?php } ?> href="index.php?pageId=3" onclick="ChangeActive(this);">Leilão Virtual</a></li>
          <li><a <?php if($pageId==4){ ?>class="active"<?php } ?> href="garanhoes_4.html" onclick="ChangeActive(this);">Garanhões</a></li>
      </ul>
      </nav>
    <div class="miolo">

      <?php
        if($totalban >0)
        {
          $str = "<ul class='bxslider'>";
          while ( $objban = mysql_fetch_array($resban) )
          {
            $str .= "<li>";
            if($objban["linkbanner"]!="") $str.= "<a href=".$objban["linkbanner"]." target='_blank'>";
            $str .= "<img src='banners/".$objban["banner"]."' /></li>";
            if($objban["linkbanner"]!="") $str .= "</a>";
          }
          $str .= "</ul>";
        }
        else
        {
          $str = '<div class="tela"><img src="images/tela.jpg" width="840" height="209" /></div>';
        }

        echo $str;
      ?>
      <div class="destaques">
        <!-- LEILÕES FUTUROS -->

        <!-- <h2>Próximos Leilões</h2> -->
        <div class="intro" style="text-align: justify;">
        <h2>Leilão Online</h2>
        <hr />
        <font size="2em">

        <div class="toggle" style="text-align: center !important;">
          <font color="#D00" style="font-size: 20px;">Clique aqui para ver como funciona</font>
        </div>
        <div id="regul">
          <br><br>
          <?
            $static = mysql_query("SELECT *from static_pages where id=7") or die(mysql_error());
            $obstatic = mysql_fetch_array($static);

            echo $obstatic['content'];
          ?>
        </div>
          <script type="text/javascript">
              $("#regul").css("display","none");
              $(".toggle").css("cursor","pointer");

              $(".toggle").click(function () {
                  content = $("#regul");
                  //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
                  content.slideToggle(500, function () {
                      //execute this after slideToggle is done
                      //change text of header based on visibility of content div
                      $(".toggle font").text(function () {
                          //change text based on condition
                          return content.is(":visible") ? 'Clique aqui para esconder' : 'Clique aqui para ver como funciona';
                      });
                  });
              });
          </script>
          <hr />

          <style type="text/css" media="screen">
            .block-content{
              width: 460px;
              margin-left: 180px;
            }
            .current-status{
              margin-bottom: 20px;
            }
            .txt-right{
              text-align: right;
            }
            .valor-lance{
              color: white;
              padding: 8px 12px;
              background-color: #8b8b8b;
              border-radius: 4px;
            }
            .comments{
              margin-top: 10px;
            }
            .dar-lance{
              padding: 0 10px;
              background-color: #c0c0c0;
              border-radius: 5px;
            }
            .dar-lance > select{
              margin: 0;
            }
            .select-value{
              display: inline-block;
              margin-right: 5px;
            }
            .btn-custom{
              border: none;
              font-size: 12px;
              font-weight: 200 !important;
              color: white !important;
              padding: 7px 12px;
              border-radius: 5px;
              outline: none !important;
              text-transform: uppercase;
              text-decoration: none !important;
            }
            .btn-custom:hover{
              color: white !important;
            }
            .btn-lance{
              margin-left: 140px;
              background-color: #2a8800;
            }
            .btn-catalogo{
              background-color: #cfbe00;
            }
            .btn-regulamento{
              background-color: #8b8b8b;
            }
            .group-buttons{
              margin-top: 20px;
              text-align: center;
            }


          </style>

          <!-- DOIDERA COMEÇA AQUI -->
          <div>
            <div class="block-content">
              <div class="current-status txt-right">
                <p>Lance Atual:</p>
                <span class="valor-lance">R$ 2000,00</span>
              </div>
              <div>
                <iframe width="460" height="345" src="https://www.youtube.com/embed/AnNbRQyXZeg" frameborder="0" allowfullscreen></iframe>
              </div>
              <div class="comments">
                <p>Lorem ipsum dolor sit amet.</p>
                <p>Lorem ipsum dolor sit amet.</p>
                <p>Lorem ipsum dolor sit amet.</p>
              </div>
              <div class="dar-lance">
                <p class="select-value">Selecione o valor:</p>
                <select>
                  <option value="0">R$ 200,00</option>
                  <option value="1">R$ 300,00</option>
                  <option value="2">R$ 400,00</option>
                  <option value="3">R$ 500,00</option>
                </select>
                <button class="btn-custom btn-lance">Dar lance</button>
              </div>
              <div class="group-buttons">
                <a href="" class="btn-custom btn-catalogo">Ver Catálogo</a>
                <a href="" class="btn-custom btn-regulamento">Regulamento</a>
              </div>
            </div>
          </div>
          <!-- DOIDERA TERMINA AQUI -->
      </div>

    </td>
  </tr>
</table>

</div>

<?php include("footer.php") ?>
<script type="text/javascript" src="Scripts/popup.js"></script>
<script type="text/javascript">
    function ZoomIn(id){
      $("#contentBox").css("display","none");
      $("#divZoomIn").css("display","block");
      $(".imgZoomIn"+id).css("display","block");
      $(".imgZoomIn"+id).mlens(
      {
        imgSrc: $("#imgZoomIn").attr("data-big"), // path of the hi-res version of the image
        imgSrc2x: $("#imgZoomIn").attr("data-big2x"), // path of the hi-res @2x version of the image //for retina displays (optional)
        lensShape: "circle", // shape of the lens (circle/square)
        lensSize: 160, // size of the lens (in px)
        borderSize: 2, // size of the lens border (in px)
        borderColor: "#fff", // color of the lens border (#hex)
        borderRadius: 0, // border radius (optional, only if the shape is square)
        imgOverlay: $("#imgZoomIn").attr("data-overlay"), // path of the overlay image (optional)
        overlayAdapt: true, // true if the overlay image has to adapt to the lens size (true/false)
        zoomLevel: 2 // zoom level multiplicator (number)
        //clickFunction: function(){ ZoomOut(); }
      });
    }

    function ZoomOut(){
      $("#contentBox").css("display","block");
      $("#divZoomIn").css("display","none");
      $(".imgZoomIn1").css("display","none");
      $(".imgZoomIn2").css("display","none");
      $(".imgZoomIn3").css("display","none");
      $(".imgZoomIn4").css("display","none");
    }

  $('.bxslider').bxSlider({
    pause: 6000,
    auto: true,
    mode: 'fade',
    controls: true
  });

function changeDate(){

}

function changeDate2(){

}

// changeDate();
</script>
          <script type="text/javascript">
                  $(".destaques .item.item_box").css("height", "680px");
                  $(".garanhao").css("height","auto");
              </script>
        <?
        $qrycat = "SELECT status FROM categories WHERE categoryID=".$catid;
        $rescat = mysql_query($qrycat);
        $objcat = mysql_fetch_array($rescat);
        mysql_free_result($rescat);

        if($pageId == 1 && ($objcat["status"]==1 || $objcat["status"]==3 ) )
        {?>
        <script type="text/javascript">
          $(".destaques .item.item_box").css("height", "620px");
          $(".garanhao").css("height","auto");
        </script>
        <?}
        ?>

</body>
</html>
<?mysql_close();?>
