var flipflop=1;
var storedata;
var storedata2;
var globalControl = true;
var categoryActive = 0;

function OnloadPage()
{
    if($.browser.msie){
        //Configuring ajax
        $.ajaxSetup({
            cache: false
        });
    }

    var catId = $('.category-item').attr('title');
    // console.log('catID: ',catId);

    function UpdateInfo(){

        $.ajax({
            url: 'update_information2.php',
            dataType: 'json',
            type: 'GET',
            data: {cid: catId},
			global: false,
			// async: false,
            success: function(data)
            {
            	cat_time = data.leilao.tempo;
            	cat_status = data.leilao.status;

				for (var i = 0; i < data.leilao.lote.length; i++)
				{
					auction_id = data.leilao.lote[i].id;
					auction_price = data.leilao.lote[i].valor_parcela;
					status_usuario = data.leilao.lote[i].status_usuario;
					status_lote = data.leilao.lote[i].status_lote;
					tempo_lote = data.leilao.lote[i].tempo;
					email_enviado = data.leilao.lote[i].email_enviado;

					if( status_lote == 2 && email_enviado==0)
					{
						$.getJSON('finalizarLeilao.php?cid=' + auction_id,
				          function(data) {
				            console.log(data);
				        });
					}

				    if(document.getElementById('price_index_page_' + auction_id))
					if(document.getElementById('price_index_page_' + auction_id).innerHTML != auction_price)
                        document.getElementById('price_index_page_' + auction_id).innerHTML = data.leilao.lote[i].moeda + auction_price + ",00";

                    if(document.getElementById('price2_index_page_' + auction_id))
                        document.getElementById('price2_index_page_' + auction_id).innerHTML = data.leilao.lote[i].moeda + auction_price + ",00";

                    if(status_usuario == 0)
					{
						if( document.getElementById('green_' + auction_id) )
						{
							document.getElementById('green_' + auction_id).style.display = 'none';
						}
						if( document.getElementById('red_' + auction_id) )
						{
							document.getElementById('red_' + auction_id).style.display = 'none';
						}
						if( document.getElementById('sold_' + auction_id) )
						{
							document.getElementById('sold_' + auction_id).style.display = 'none';
						}
					}
					else if(status_usuario == 1)
					{
						if( document.getElementById('green_' + auction_id) )
						{
							document.getElementById('green_' + auction_id).style.display = 'block';
						}
						if( document.getElementById('red_' + auction_id) )
						{
							document.getElementById('red_' + auction_id).style.display = 'none';
						}
						if( document.getElementById('sold_' + auction_id) )
						{
							document.getElementById('sold_' + auction_id).style.display = 'none';
						}
					}
					else if(status_usuario == 2)
					{
						if( document.getElementById('green_' + auction_id) )
						{
							document.getElementById('green_' + auction_id).style.display = 'none';
						}
						if( document.getElementById('red_' + auction_id) )
						{
							document.getElementById('red_' + auction_id).style.display = 'block';
						}
						if( document.getElementById('sold_' + auction_id) )
						{
							document.getElementById('sold_' + auction_id).style.display = 'none';
						}
					}
					else if(status_usuario == 3)
					{
						if( document.getElementById('green_' + auction_id) )
						{
							document.getElementById('green_' + auction_id).style.display = 'none';
						}
						if( document.getElementById('red_' + auction_id) )
						{
							document.getElementById('red_' + auction_id).style.display = 'none';
						}
						if( document.getElementById('sold_' + auction_id) )
						{
							document.getElementById('sold_' + auction_id).style.display = 'block';
						}
					}
				}

				// if(cat_time<=0)
				// {
				// 	if( cat_status == 2 && cat_mail==0)
				// 	{
				// 		$.getJSON('finalizarLeilao.php?cid='+catId,
				//           function(data) {
				//             console.log(data);
				//         });
				// 	}
				// }
			},
			error: function(XMLHttpRequest,textStatus, errorThrown)
			{
					console.log(XMLHttpRequest);
					console.log(textStatus);
					console.log(errorThrown);
			}
		});

	}

	UpdateInfo();
	setInterval( UpdateInfo,3000);
}
