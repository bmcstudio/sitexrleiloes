<?
	include("config/connect.php");
	include("language/portugues.php");
	include("session.php");
	include("functions.php");
	$uid = $_SESSION["userid"];

	if(!$_GET['pgno'])
	{
		$PageNo = 1;
	}
	else
	{
		$PageNo = $_GET['pgno'];
	}
	$qrysel = "select *,".$lng_prefix."name as name,".$lng_prefix."short_desc as short_desc from won_auctions w left join auction a on w.auction_id=a.auctionID  left join products p on a.productID=p.productID where w.userid=$uid order by won_date desc";
	$ressel = mysql_query($qrysel);
	$total = mysql_num_rows($ressel);
	$totalpage=ceil($total/$PRODUCTSPERPAGE_MYACCOUNT);

	if($totalpage>=1)
	{
	$startrow=$PRODUCTSPERPAGE_MYACCOUNT*($PageNo-1);
	$qrysel.=" LIMIT $startrow,$PRODUCTSPERPAGE_MYACCOUNT";
	//echo $sql;
	$ressel=mysql_query($qrysel);
	$total=mysql_num_rows($ressel);
	}

	$qryvou = "select * from user_vouchers where user_id='".$uid."' and voucher_status='0'";
	$resvou = mysql_query($qryvou);
	$totalvou1 = mysql_num_rows($resvou);
	/*$totalvou1 = 0;
	
	while($objvou = mysql_fetch_object($resvou))
	{
		$expiry = strtotime($objvou->expirydate);
		$today = time();
		if($today>$expiry) {
		} else {
			$totalvou1++;
		}
	}	*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?=$lng_characset;?>" />
<title><?=$AllPageTitle;?></title>
<link href="css/style_youbid.css" rel="stylesheet" type="text/css" />
<link href="css/style_.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]>
<link href="css/menu_ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script language="javascript" type="text/javascript" src="function.js"></script>
<script type="text/javascript" language="javascript">
function ShowMakepaymentbutton(Aucid,Accden,paydate)
{
	if(Accden=='Accepted' && paydate=='')
	{
		document.getElementById("wonacceptdenied_"+Aucid).innerHTML = "<font color='green'><b><?=$lng_wonaccepted;?></b></font>";
		document.getElementById("makepayment_"+Aucid).style.visibility = 'visible';
	}
	else if(Accden=='Denied')
	{
		document.getElementById("wonacceptdenied_"+Aucid).innerHTML = "<font color='#C82C2F'><b><?=$lng_wondenied;?></b></font>";
	}
	else if(paydate!="")
	{
		document.getElementById("wonacceptdenied_"+Aucid).innerHTML = "<font color='green'><b><?=$lng_wonaccepted;?></b></font>";
		document.getElementById('paymentdate_' + Aucid).style.visibility= 'visible';
		document.getElementById('paymentdate_' + Aucid).innerHTML = "<b>" + paydate + "</b>";
	}
}
function OpenAcceptdeniedPopup(url)
{
		window.open(url,'popupWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=300,height=130,screenX=150,screenY=200,top=200,left=200')
}
</script>
</head>


<body class="admin_page">
    <? include("header.php"); ?>
    <div id="main_div">
		<div id="middle_div">
            <div id="cleaner"></div>
			<? include("leftside.php"); ?>
			<div class="inner-container content-center">
				<div class="titlebar">
					<div class="leftbar"></div>
					<div class="middlebar"><div class="page_title_font"><?=$lng_myauctionsavenue;?> - <?=$lng_wonauctions;?></div></div>
					<div class="rightbar"></div>
				</div>
				<div class="bodypart">	
				<?
				if($total>0)
				{
				?>
					<div class="strip">
						<div class="wonimage" style="padding:0px;"><?=$lng_image;?></div>
						<div class="wondescription" style="padding:0px; width: 220px;"><?=$lng_description;?></div>
						<div class="wonprice" style="padding:0px; width: 110px;"><?=$lng_winprice;?></div>
						<div class="wonbidder" align="center" style="padding:0px; width:132px;"><?=$lng_acceptdenied;?></div>
						<div class="woncountdown" align="right" style="padding:0px; width: 60px;"><?=$lng_payment;?></div>
					</div>
					<div style="height:15px;">&nbsp;</div>
			  <?
				$counter = 1;
				while($obj = mysql_fetch_array($ressel))
				{
					$qryshipping = "select * from shipping where id='".$obj["shipping_id"]."'";
					$resshipping = mysql_query($qryshipping);
					$objshipping = mysql_fetch_array($resshipping);

					if($obj["fixedpriceauction"]=="1")
					{
						$finalprice = $obj["auc_fixed_price"];
					}
					elseif($obj["offauction"]=="1")
					{
						$finalprice = "0.00";
					}
					else
					{
						$finalprice = $obj["auc_final_price"];
					}
	
						$expiry = AcceptDateFunctionStatus($obj["won_date"]);
						
						$todaytime = time();
						$expirytime = mktime($expiry["Hour"],$expiry["Min"],$expiry["Sec"],$expiry["Month"],$expiry["Day"],$expiry["Year"]);
						$dateDiff = $todaytime - $expirytime;   
						$fullDays = floor($dateDiff/(60*60*24));
						if($todaytime>$expirytime)
						{
							$new_status = "Expire"; 
						}
						else
						{
							$new_status = "Running";
						}
	/*					if ($fullDays>0) { 
						$new_status = "Expire"; 
						} else { 
						$new_status = "Running"; 
						} */
						
						$expirywon = AcceptDateFunctionStatus($obj["accept_date"]);
						
						$todaywontime = time();
						$expirywontime = mktime($expirywon["Hour"],$expirywon["Min"],$expirywon["Sec"],$expirywon["Month"],$expirywon["Day"],$expirywon["Year"]);
						$wondateDiff = $todaywontime - $expirywontime;   
						$wonfullDays = floor($dateDiff/(60*60*24));
	
						if($todaywontime>$expirywontime)
						{
							$new_status_won = "Expire";
						}
						else
						{
							$new_status_won = "Running";
						}	
						
	/*					if ($wonfullDays>0) { 
						$new_status_won = "Expire"; 
						} else { 
						$new_status_won = "Running"; 
						} */
				?>	
					<div class="even-row">
							<div class="wonimage">
							  <div class="auctionimage1" align="center"><a href="auction_<?=str_replace(" ","_",strtolower($obj["name"]));?>_<?=$obj["auctionID"];?>.html"><img src="uploads/products/thumbs_big/thumbbig_<?=$obj["picture1"];?>" border="0"></a></div>
								  
							</div>
							<div class="wondescription">
								<span><a href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html" class="black_link"><?=stripslashes($obj["name"]);?></a></span><br /><br />
								<?=stripslashes(choose_short_desc($obj["short_desc"],110));?>
									<a class="black_link" href="auction_<?=str_replace(" ","_",strtolower(stripslashes($obj["name"])));?>_<?=$obj["auctionID"];?>.html"><?=$lng_linkmore;?></a>
							</div>					
							<div class="wonprice"><b><?=$Currency;?><?=number_format($finalprice,2);?>&nbsp;<br /></b><span style="font-size:10px;"><!--<?=$lng_inclvatexcl2;?>--> + <?=$Currency.$objshipping["shippingcharge"];?>&nbsp;<?=$lng_deliverycharge;?></span><br />
							</div>
							<div class="wonbidder" align="center" id="wonacceptdenied_<?=$obj["auctionID"];?>">
									<? if($obj['accept_denied']=="Accepted"){?><font color="green" style="font-weight: bold;"><?=$lng_wonaccepted;?></font><? }elseif($obj['accept_denied']=="Denied"){ ?><font class="red-text-12-b"><?=$lng_wondenied;?></font><? }else{ if($new_status=="Running"){?><a href="javascript:void(0)" onclick="javascript:OpenAcceptdeniedPopup('acceptordenied.php?auctionid=<?=$obj["auction_id"];?>','','width=150,height=100')" class="alink"><?=$lng_clickhere;?></a><br /><?=$lng_lastdateaccept;?>(<?=AcceptDateFunction(substr($obj["won_date"],0,10));?>)<? }else { ?><span class="red-text-12-b"><?=$lng_acceptperiodover;?></span><? } } ?>
							</div>
									<div class="woncountdown" align="center">
									<? if($obj['accept_denied']=="Accepted" && $obj['payment_date']=='0000-00-00 00:00:00'){?>
									<span id="makepayment_<?=$obj["auction_id"]?>">
									<? if($new_status_won=="Running"){ ?>
										<? if($totalvou1>0){ 
										?>	
                                        						
									<input type="image" src="<?=$lng_imagepath;?>make a payment_btn.png" onclick="window.location.href='choosevoucher.html?winid=<?=base64_encode($finalprice."&".$obj["auctionID"]);?>'" name="makepayment" onmouseover="this.src='<?=$lng_imagepath;?>make a payment_hover_btn.png'" onmouseout="this.src='<?=$lng_imagepath;?>make a payment_btn.png'" />
									<? } else { ?>							
									<input type="image" src="<?=$lng_imagepath;?>make a payment_btn.png" onclick="window.location.href='payment.php?winid=<?=base64_encode($finalprice."&".$obj["auctionID"]);?>'" name="makepayment" onmouseover="this.src='<?=$lng_imagepath;?>make a payment_hover_btn.png'" onmouseout="this.src='<?=$lng_imagepath;?>make a payment_btn.png'" />
									<? } ?>
									<? } else { ?><div class="red-text-12-b" align="center"><?=$lng_paymentperiodover;?></div><? } ?></span>
							<? }
							elseif($obj['payment_date']!='0000-00-00 00:00:00'){?>
							<div align="center"><span id="wonpayment"><b><? $paydate = arrangedate(substr($obj['payment_date'],0,10)); ?><?=$paydate?><br /><?=substr($obj['payment_date'],11)?></b></span></div>
							<? }else{ ?>
							<span id="paymentdate_<?=$obj["auction_id"];?>" style="visibility: hidden;"></span>
							<span style="visibility:hidden;" id="makepayment_<?=$obj["auction_id"]?>">
							<? if($totalvou1>0){ ?>					
							<input type="image" src="<?=$lng_imagepath;?>make a payment_btn.png" value="Make Payment" onclick="window.location.href='choosevoucher.html?winid=winid=<?=base64_encode($finalprice."&".$obj["auctionID"]);?>'" name="makepayment"  onmouseover="this.src='<?=$lng_imagepath;?>make a payment_hover_btn.png'" onmouseout="this.src='<?=$lng_imagepath;?>make a payment_btn.png'" />
							<? } else { ?>										
							<input type="image" src="<?=$lng_imagepath;?>make a payment_btn.png" value="Make Payment" onclick="window.location.href='payment.php?winid=<?=base64_encode($finalprice."&".$obj["auctionID"]);?>'" name="makepayment"  onmouseover="this.src='<?=$lng_imagepath;?>make a payment_hover_btn.png'" onmouseout="this.src='<?=$lng_imagepath;?>make a payment_btn.png'" />
							<? } ?>
							</span>
							<? } ?>
								</div>
					</div>
				<?
					}
				?>
					<div class="strip">
						<div style="padding-top: 3px;">
						<?
						if($PageNo>1)
						{
						  $PrevPageNo = $PageNo-1;
						?>
						  <a class="alink" href="wonauctions_<?=$PrevPageNo; ?>.html">&lt; <?=$lng_previouspage;?></a>
						<?
							if($totalpage>2 && $totalpage!=$PageNo)
							{
						?>
								<span class="paging">&nbsp;|</span>
						<?
							}
						 }
						?>&nbsp;
						<?
						if($PageNo<$totalpage)
						{
						 	$NextPageNo = 	$PageNo + 1;
						?>
							 <a class="alink" id="next" href="wonauctions_<?=$NextPageNo;?>.html"><?=$lng_nextpage;?> &gt;</a>
						<?
						}
						?>
						</div>
					</div>
				<?
				}
				else
				{
				?>
					<div style="height: 15px;"></div>
					<div class="noauction_message" align="center"><?=$lng_nowonauctions;?></div>
					<div style="height: 15px;"></div>
				<?
				}
				?>
				</div>
			</div>
            <div id="cleaner"></div>
		</div>
    </div>
    <? include("footer.php"); ?>
</body>
</html>
